# Variables en Python


botella = "hola"	# texto		string	cadena	char	varchar	str

botella = 100		# número	integer	entero			int

botella = 5.8		# número	float	decimal	flotante	float

botella = True|False	# boleanos	bool	Falso|Verdadero	1/0	bool

botella = None		# Nulos		null	Valor no cocido...	None

nombre = "Pedro"
edad = 30

# concatenación		unir

print("Hola", nombre, "como estás?, tu edad es:", edad)







