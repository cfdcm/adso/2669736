var dialogModule = require('@nativescript/core/ui/dialogs');
var observableModule = require('@nativescript/core/data/observable');
var ObservableArray = require('@nativescript/core/data/observable-array').ObservableArray;
var SolucionListViewModel = require('../shared/view-models/solucion-list-view-model');
var frameModule = require('@nativescript/core/ui/frame');
var config = require("../shared/config");

var page;


var solucionList = new SolucionListViewModel([]);

var pageData = new observableModule.fromObject({
    solucionList: solucionList,
    problema: '',
    usuario_creador: '',
    descripcion: '',
    lenguaje_prog: '',
    moderado: '',
    fecha: '',
    puedeVer: "",
    mostrar: false
});

exports.regresar = function (args) {
  console.log("Regresar al listado de problemas del tema..."+config.id_problema)
  frameModule.Frame.topmost().navigate({ moduleName: "problemas/problemas" });
};

exports.loaded = function (args) {
    page = args.object;

    var listView = page.getViewById('solucionList');
    page.bindingContext = pageData;

    solucionList.empty();
    pageData.set('isLoading', true);
    solucionList.load().then(function () {
        pageData.set('isLoading', false);
        listView.animate({
            opacity: 1,
            duration: 1000
        });
    });

    pageData.set('user', "Bienvenido ("+config.rol+"), "+config.nombre);
    pageData.set('tema_seleccionado', "TEMA: "+config.id_tema_nombre);
    pageData.set('problema_seleccionado', "Soluciones del PROBLEMA: ");

    pageData.set('titul', config.id_problema_titulo);
    pageData.set('problema', config.id_problema_descripcion);

    if (config.rol == "ADMIN"){
      pageData.set('puedeVer', 'collapsed');
    }
    else{
      pageData.set('puedeVer', 'visible');
    }

};

exports.delete = function (args) {
  var item = args.view.bindingContext;
  var index = solucionList.indexOf(item);

  dialogModule.confirm({
    title: "Confirmación ??",
    message: "Está seguro de eliminar esta solucion al problema? " + solucionList.getItem(index).id,
    okButtonText: "Aceptar",
    cancelButtonText: "Cancelar",
  }).then(function (result) {
    if(result){
      //eliminar
      solucionList.delete(index);
    }
  });
}


exports.califica = function (args) {
    console.log("califica...."+JSON.stringify(args.object.calificacion_current_user))
    const promptOptions = {
      title: "Califica esta solución",
      defaultText: String(args.object.calificacion_current_user),
      message: "La calificación es entre 0 - 5 estrellas",
      okButtonText: "OK",
      cancelButtonText: "Cancel",
      cancelable: true,
      inputType: "number", // email, number, text, password, or email
      capitalizationType: "none" // all, none, sentences or words
    };
    prompt(promptOptions).then((result) => {
      if (result.result){
        if (parseInt(result.text) >=0 && parseInt(result.text) <=5){

          //console.log("Calificacion:"+result.text);
          //console.log("User:"+config.user_id)
          //console.log("Solucion ID:"+args.object.args);
          //console.log("Comentario ID:"+args.object.id_comentario)

          // Este dato viene de consulta en Comentarios para saber si genero o modifico un registro.
          let tipo_peticion = ""
          if (args.object.post_or_put){
            console.log("Realizar petición PUT")
            tipo_peticion = "PUT"
          }
          else {
            console.log("Realizar petición POST, nuevo")
            tipo_peticion = "POST"
          }
          solucionList.add_estrellas(tipo_peticion, args.object.args, config.user_id, parseInt(result.text), args.object.id_comentario)
            .catch(function () {
              dialogModule.alert({ message: 'No se pudo guardar la calificación...', okButtonText: 'OK'});
            });
        }
        else{
          dialogModule.alert({message: 'Digite un valor entero entre 0 y 5', okButtonText: 'OK' });
        }
      }
    });
}

exports.comenta = function (args) {
  const promptOptions = {
    title: "Comenta esta solución",
    defaultText: String(args.object.comentario_current_user),
    message: "Por favor se muy puntual",
    okButtonText: "OK",
    cancelButtonText: "Cancel",
    cancelable: true,
    inputType: "text", // email, number, text, password, or email
    capitalizationType: "sentences" // all, none, sentences or words
  };
  prompt(promptOptions).then((result) => {
    if (result.result){
      console.log("Len:"+result.text.length)
      if (result.text.length > 0){

        //console.log("Calificacion:"+result.text);
        //console.log("User:"+config.user_id)
        //console.log("Solucion ID:"+args.object.args);
        //console.log("Comentario ID:"+args.object.id_comentario)

        // Este dato viene de consulta en Comentarios para saber si genero o modifico un registro.
        let tipo_peticion = ""
        if (args.object.post_or_put){
          console.log("Realizar petición PUT")
          tipo_peticion = "PUT"
        }
        else {
          console.log("Realizar petición POST, nuevo")
          tipo_peticion = "POST"
        }
        solucionList.add_comentarios(tipo_peticion, args.object.args, config.user_id, result.text, args.object.id_comentario)
          .catch(function () {
            dialogModule.alert({ message: 'No se pudo guardar el comentario...', okButtonText: 'OK'});
          });
      }
      else{
        dialogModule.alert({message: 'Digite un comentario por favor...', okButtonText: 'OK' });
      }
    }
  });
}


exports.verComentarios = function(args) {
  var item = args.view.bindingContext;
  var index = solucionList.indexOf(item);
  console.log(solucionList.getItem(index).id);

  config.id_solucion = solucionList.getItem(index).id;
  config.id_solucion_problema = solucionList.getItem(index).webViewSrc;

  console.log("ID Sol para comment: "+config.id_solucion)
  console.log("ID Problema para comment: "+config.id_solucion_problema)

  const navigationEntry = {

    moduleName: "comentarios/comentarios",
    context: {
      solucion_code: solucionList.getItem(index).webViewSrc
    }
  };

  frameModule.Frame.topmost().navigate(navigationEntry);
}

exports.add_solucion_page = function() {
  frameModule.Frame.topmost().navigate({moduleName: "soluciones/add-soluciones"});
}


exports.toggleProblema = function() {
  console.log("Estado: "+pageData.mostrar);
  if (pageData.mostrar){
    pageData.set('mostrar', false);
  }
  else{
    pageData.set('mostrar', true);
  }
}


// disabling the WebView's zoom control
exports.onWebViewLoaded = function(webargs) {
  const webview = webargs.object;
  webview.android.getSettings().setDisplayZoomControls(false);
}
