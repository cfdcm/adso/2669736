var dialogModule = require('@nativescript/core/ui/dialogs');
var observableModule = require('@nativescript/core/data/observable');
var ObservableArray = require('@nativescript/core/data/observable-array').ObservableArray;
var ProblemaListViewModel = require('../shared/view-models/problema-list-view-model');
var frameModule = require('@nativescript/core/ui/frame');
var config = require("../shared/config");

var page;


var problemaList = new ProblemaListViewModel([]);

var pageData = new observableModule.fromObject({
    problemaList: problemaList,
    tema: '',
    usuario_creador: '',
    titulo: '',
    description: '',
    fecha: '',
    moderado: '',
    nivel: '',
    puedeVer: ""
});

exports.regresar = function (args) {
  console.log("Regresar al listado de temas...")
  frameModule.Frame.topmost().navigate({ moduleName: "temas/temas" });
};

exports.loaded = function (args) {
    page = args.object;

    var listView = page.getViewById('problemaList');
    page.bindingContext = pageData;
    //console.log("Datos" + JSON.stringify(page.bindingContext))
    problemaList.empty();
    pageData.set('isLoading', true);
    problemaList.load().then(function () {
        pageData.set('isLoading', false);
        listView.animate({
            opacity: 1,
            duration: 1000
        });
    });

    pageData.set('user', "Bienvenido ("+config.rol+"), "+config.nombre);
    pageData.set('movimiento', "TEMA: "+config.id_tema_nombre);
    pageData.set('tema', config.id_tema);
    pageData.set('usuario_creador', config.user_id);

    if (config.rol == "ADMIN"){
      pageData.set('puedeVer', 'collapsed');
    }
    else{
      pageData.set('puedeVer', 'visible');
    }

};

exports.nivelDialog = function(args) {
  console.log("Parámetro para cambiar nivel: ID: "+args.object.args_id)
  const actionOptions = {
    title: "Nivel del Problema",
    message: "Seleccione el grado de dificultad del problema que más se ajuste...",
    cancelButtonText: "Cancelar",
    actions: ["Sin categorizar", "Fácil", "Medio", "Difícil"],
    cancelable: true // Android only
  };

  action(actionOptions).then((result) => {

    if (result) {
      console.log(result);
      console.log("ID: " + args.object.args_id)
      console.log("Tema: " + args.object.args_tema)
      console.log("Usuario creador: " + args.object.args_usuario_creador)
      console.log("Desc: " + args.object.args_descripcion)
      console.log("Nivel: " + args.object.args_nivel)

      let nuevo_nivel = ""
      if (result == "Sin categorizar") {
        nuevo_nivel = "-";
      }
      else if (result == "Fácil") {
        nuevo_nivel = "F";
      }
      else if (result == "Medio") {
        nuevo_nivel = "M";
      }
      else if (result == "Difícil") {
        nuevo_nivel = "D";
      }

      problemaList.cambio_nivel(args.object.args_id, args.object.args_tema, args.object.args_usuario_creador, args.object.args_descripcion, nuevo_nivel)
        .catch(function () {
          dialogModule.alert({ message: 'No se pudo cambiar el nivel...', okButtonText: 'OK'});
        });
    }

  });
};


exports.moderarDialog = function(args) {
  console.log("Parámetro para cambiar Moderador: ID: "+args.object.args_id)
  const actionOptions = {
    title: "Moderación del Problema",
    message: "Seleccione el nuevo estado de elemento...",
    cancelButtonText: "Cancelar",
    actions: ["Aplausos!!", "Publicado", "Silenciado", "Plagio", "Baneado"],
    cancelable: true // Android only
  };

  action(actionOptions).then((result) => {

    if (result) {
      console.log(result);
      console.log("ID: " + args.object.args_id)
      console.log("Tema: " + args.object.args_tema)
      console.log("Usuario creador: " + args.object.args_usuario_creador)
      console.log("Desc: " + args.object.args_descripcion)
      console.log("Moderado: " + args.object.args_moderado)

      let nuevo_estado = ""
      if (result == "Aplausos!!") {
        nuevo_estado = "F";
      }
      else if (result == "Publicado") {
        nuevo_estado = "C";
      }
      else if (result == "Silenciado") {
        nuevo_estado = "A";
      }
      else if (result == "Plagio") {
        nuevo_estado = "P";
      }
      else if (result == "Baneado") {
        nuevo_estado = "B";
      }

      problemaList.cambio_estado(args.object.args_id, args.object.args_tema, args.object.args_usuario_creador, args.object.args_descripcion, nuevo_estado)
        .catch(function () {
          dialogModule.alert({ message: 'No se pudo cambiar el estado...', okButtonText: 'OK'});
        });
    }

  });
};



exports.delete = function (args) {
  var item = args.view.bindingContext;
  var index = problemaList.indexOf(item);

  dialogModule.confirm({
    title: "Confirmación ??",
    message: "Está seguro de eliminar este problema? " + problemaList.getItem(index).titulo,
    okButtonText: "Aceptar",
    cancelButtonText: "Cancelar",
  }).then(function (result) {
    if(result){
      //eliminar
      problemaList.delete(index);
    }
  });
}

exports.onItemTap = function(args) {
  var item = args.view.bindingContext;
  var index = problemaList.indexOf(item);
  console.log(problemaList.getItem(index).id);

  config.id_problema = problemaList.getItem(index).id;
  config.id_problema_titulo = problemaList.getItem(index).titulo;
  config.id_problema_descripcion = problemaList.getItem(index).descripcion;

  console.log(problemaList.getItem(index))

  const navigationEntry = {
    moduleName: "soluciones/soluciones",
    context: {
      problema_id: problemaList.getItem(index).id
    }
  };

  frameModule.Frame.topmost().navigate(navigationEntry);
}

exports.add_problemas_page = function() {
  frameModule.Frame.topmost().navigate({moduleName: "problemas/add-problemas"});
}
