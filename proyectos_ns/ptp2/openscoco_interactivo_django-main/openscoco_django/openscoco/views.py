from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse, reverse_lazy

def index(request):
    # Política de privacidad
    return render(request, 'openscoco/index.html')
def entrenar(request):
    #preparar la sesion
    sesion = request.session.get('entrenar', False)
    if not sesion:
        request.session["entrenar"] = [True, 0, 0, 0, 0, 0]

    return render(request, 'openscoco/entrenar.html')

def reto1(request):
    #detectar nivel y redireccionar
    sesion = request.session.get('entrenar', False)
    
    if sesion[1] == 0:
        return render(request, 'openscoco/reto1.html')
    elif sesion[1] == 25:
        return HttpResponseRedirect(reverse('openscoco:reto2'))
    elif sesion[1] == 50:
        return HttpResponseRedirect(reverse('openscoco:reto3'))
    elif sesion[1] == 75:
        return HttpResponseRedirect(reverse('openscoco:reto4'))
    elif sesion[1] == 100:
        return HttpResponseRedirect(reverse('openscoco:reto5'))
    else:
        return HttpResponseRedirect(reverse('openscoco:index'))
    

def reto2(request):
    sesion = request.session.get('entrenar', False)
    #actualizar datos en variable de sesión y prevenir botón atrás
    if sesion and sesion[1] <= 25:
        sesion[1] = 25
        request.session["entrenar"] = sesion

    return render(request, 'openscoco/reto2.html')

def reto3(request):
    sesion = request.session.get('entrenar', False)
    if sesion and sesion[1] <= 50:
        sesion[1] = 50
        request.session["entrenar"] = sesion

    return render(request, 'openscoco/reto3.html')

def reto4(request):
    sesion = request.session.get('entrenar', False)
    if sesion and sesion[1] <= 75:
        sesion[1] = 75
        request.session["entrenar"] = sesion

    return render(request, 'openscoco/reto4.html')

def reto5(request):
    sesion = request.session.get('entrenar', False)
    if sesion and sesion[1] <= 100:
        sesion[1] = 100
        request.session["entrenar"] = sesion

    return render(request, 'openscoco/reto5.html')

def reset(request):
    sesion = request.session.get('entrenar', False)
    if sesion:
        sesion[1] = 0
        request.session["entrenar"] = sesion

    return HttpResponseRedirect(reverse('openscoco:index'))


    
def condicionales_1(request):
    #detectar nivel y redireccionar
    sesion = request.session.get('entrenar', False)
    
    if sesion[2] == 0:
        return render(request, 'openscoco/condicionales_1.html')
    elif sesion[2] == 25:
        return HttpResponseRedirect(reverse('openscoco:condicionales2'))
    elif sesion[2] == 50:
        return HttpResponseRedirect(reverse('openscoco:condicionales3'))
    elif sesion[2] == 75:
        return HttpResponseRedirect(reverse('openscoco:condicionales4'))
    elif sesion[2] == 100:
        return HttpResponseRedirect(reverse('openscoco:condicionales5'))
    else:
        return HttpResponseRedirect(reverse('openscoco:index'))

def condicionales_2(request):
    sesion = request.session.get('entrenar', False)
    #actualizar datos en variable de sesión y prevenir botón atrás
    if sesion and sesion[2] <= 25:
        sesion[2] = 25
        request.session["entrenar"] = sesion

    return render(request, 'openscoco/condicionales_2.html')

def condicionales_3(request):
    sesion = request.session.get('entrenar', False)
    #actualizar datos en variable de sesión y prevenir botón atrás
    if sesion and sesion[2] <= 50:
        sesion[2] = 50
        request.session["entrenar"] = sesion

    return render(request, 'openscoco/condicionales_3.html')

def condicionales_4(request):
    sesion = request.session.get('entrenar', False)
    #actualizar datos en variable de sesión y prevenir botón atrás
    if sesion and sesion[2] <= 75:
        sesion[2] = 75
        request.session["entrenar"] = sesion

    return render(request, 'openscoco/condicionales_4.html')

def condicionales_5(request):
    sesion = request.session.get('entrenar', False)
    #actualizar datos en variable de sesión y prevenir botón atrás
    if sesion and sesion[2] <= 100:
        sesion[2] = 100
        request.session["entrenar"] = sesion

    return render(request, 'openscoco/condicionales_5.html')


def reset2(request):
    sesion = request.session.get('entrenar', False)
    if sesion:
        sesion[2] = 0
        request.session["entrenar"] = sesion

    return HttpResponseRedirect(reverse('openscoco:index'))


    
def ciclos_1(request):
    #detectar nivel y redireccionar
    sesion = request.session.get('entrenar', False)
    
    if sesion[3] == 0:
        return render(request, 'openscoco/ciclos_1.html')
    elif sesion[3] == 25:
        return HttpResponseRedirect(reverse('openscoco:ciclos2'))
    elif sesion[3] == 50:
        return HttpResponseRedirect(reverse('openscoco:ciclos3'))
    elif sesion[3] == 75:
        return HttpResponseRedirect(reverse('openscoco:ciclos4'))
    elif sesion[3] == 100:
        return HttpResponseRedirect(reverse('openscoco:ciclos5'))
    else:
        return HttpResponseRedirect(reverse('openscoco:index'))

def ciclos_2(request):
    sesion = request.session.get('entrenar', False)
    #actualizar datos en variable de sesión y prevenir botón atrás
    if sesion and sesion[3] <= 25:
        sesion[3] = 25
        request.session["entrenar"] = sesion

    return render(request, 'openscoco/ciclos_2.html')

def ciclos_3(request):
    sesion = request.session.get('entrenar', False)
    #actualizar datos en variable de sesión y prevenir botón atrás
    if sesion and sesion[3] <= 50:
        sesion[3] = 50
        request.session["entrenar"] = sesion

    return render(request, 'openscoco/ciclos_3.html')

def ciclos_4(request):
    sesion = request.session.get('entrenar', False)
    #actualizar datos en variable de sesión y prevenir botón atrás
    if sesion and sesion[3] <= 75:
        sesion[3] = 75
        request.session["entrenar"] = sesion

    return render(request, 'openscoco/ciclos_4.html')

def ciclos_5(request):
    sesion = request.session.get('entrenar', False)
    #actualizar datos en variable de sesión y prevenir botón atrás
    if sesion and sesion[3] <= 100:
        sesion[3] = 100
        request.session["entrenar"] = sesion

    return render(request, 'openscoco/ciclos_5.html')


def reset3(request):
    sesion = request.session.get('entrenar', False)
    if sesion:
        sesion[3] = 0
        request.session["entrenar"] = sesion

    return HttpResponseRedirect(reverse('openscoco:index'))


    
def funciones_1(request):
    #detectar nivel y redireccionar
    sesion = request.session.get('entrenar', False)
    
    if sesion[4] == 0:
        return render(request, 'openscoco/funciones_1.html')
    elif sesion[4] == 25:
        return HttpResponseRedirect(reverse('openscoco:funciones2'))
    elif sesion[4] == 50:
        return HttpResponseRedirect(reverse('openscoco:funciones3'))
    elif sesion[4] == 75:
        return HttpResponseRedirect(reverse('openscoco:funciones4'))
    elif sesion[4] == 100:
        return HttpResponseRedirect(reverse('openscoco:funciones5'))
    else:
        return HttpResponseRedirect(reverse('openscoco:index'))

def funciones_2(request):
    sesion = request.session.get('entrenar', False)
    #actualizar datos en variable de sesión y prevenir botón atrás
    if sesion and sesion[4] <= 25:
        sesion[4] = 25
        request.session["entrenar"] = sesion

    return render(request, 'openscoco/funciones_2.html')

def funciones_3(request):
    sesion = request.session.get('entrenar', False)
    #actualizar datos en variable de sesión y prevenir botón atrás
    if sesion and sesion[4] <= 50:
        sesion[4] = 50
        request.session["entrenar"] = sesion

    return render(request, 'openscoco/funciones_3.html')

def funciones_4(request):
    sesion = request.session.get('entrenar', False)
    #actualizar datos en variable de sesión y prevenir botón atrás
    if sesion and sesion[4] <= 75:
        sesion[4] = 75
        request.session["entrenar"] = sesion

    return render(request, 'openscoco/funciones_4.html')

def funciones_5(request):
    sesion = request.session.get('entrenar', False)
    #actualizar datos en variable de sesión y prevenir botón atrás
    if sesion and sesion[4] <= 100:
        sesion[4] = 100
        request.session["entrenar"] = sesion

    return render(request, 'openscoco/funciones_5.html')


def reset4(request):
    sesion = request.session.get('entrenar', False)
    if sesion:
        sesion[4] = 0
        request.session["entrenar"] = sesion

    return HttpResponseRedirect(reverse('openscoco:index'))


    
def vectores_1(request):
    #detectar nivel y redireccionar
    sesion = request.session.get('entrenar', False)
    
    if sesion[5] == 0:
        return render(request, 'openscoco/vectores_1.html')
    elif sesion[5] == 25:
        return HttpResponseRedirect(reverse('openscoco:vectores2'))
    elif sesion[5] == 50:
        return HttpResponseRedirect(reverse('openscoco:vectores3'))
    elif sesion[5] == 75:
        return HttpResponseRedirect(reverse('openscoco:vectores4'))
    elif sesion[5] == 100:
        return HttpResponseRedirect(reverse('openscoco:vectores5'))
    else:
        return HttpResponseRedirect(reverse('openscoco:index'))

def vectores_2(request):
    sesion = request.session.get('entrenar', False)
    #actualizar datos en variable de sesión y prevenir botón atrás
    if sesion and sesion[5] <= 25:
        sesion[5] = 25
        request.session["entrenar"] = sesion

    return render(request, 'openscoco/vectores_2.html')

def vectores_3(request):
    sesion = request.session.get('entrenar', False)
    #actualizar datos en variable de sesión y prevenir botón atrás
    if sesion and sesion[5] <= 50:
        sesion[5] = 50
        request.session["entrenar"] = sesion

    return render(request, 'openscoco/vectores_3.html')

def vectores_4(request):
    sesion = request.session.get('entrenar', False)
    #actualizar datos en variable de sesión y prevenir botón atrás
    if sesion and sesion[5] <= 75:
        sesion[5] = 75
        request.session["entrenar"] = sesion

    return render(request, 'openscoco/vectores_4.html')

def vectores_5(request):
    sesion = request.session.get('entrenar', False)
    #actualizar datos en variable de sesión y prevenir botón atrás
    if sesion and sesion[5] <= 100:
        sesion[5] = 100
        request.session["entrenar"] = sesion

    return render(request, 'openscoco/vectores_5.html')


def reset5(request):
    sesion = request.session.get('entrenar', False)
    if sesion:
        sesion[5] = 0
        request.session["entrenar"] = sesion

    return HttpResponseRedirect(reverse('openscoco:index'))
