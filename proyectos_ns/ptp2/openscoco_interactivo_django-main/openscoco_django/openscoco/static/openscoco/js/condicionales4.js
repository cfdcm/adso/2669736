function shuffle(o){ //v1.0
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
}

Ext.onReady(function(){
    $('#spinner').hide();
    var datos = [
        {
            id: 'l0',
            text: 'hora1 = int(input("Digite la hora1: "));',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l1',
            text: 'minuto1 = int(input("Digite los minutos1: "));',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l2',
            text: 'M1 = input("Digite AM/PM: ");',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l3',
            text: 'hora2 = int(input("Digite la hora1: "));',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l4',
            text: 'minuto2 = int(input("Digite los minutos1: "));',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l5',
            text: 'M2 = input("Digite AM/PM: ");',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l6',
            text: 'if M1 == M2:',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l7',
            text: 'if hora1 == hora2:',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l8',
            text: 'if minuto1 == minuto2:',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l9',
            text: 'print("Las horas son iguales")',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l10',
            text: 'elif minuto1 > minuto2:',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l11',
            text: 'print("La primera es mayor por sus minutos")',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l12',
            text: 'else:	#nivel 3 interno',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l13',
            text: 'print("La segunda es mayor por sus minutos")',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l14',
            text: 'elif hora1 > hora2:',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l15',
            text: 'print("La primera es mayor por sus horas")',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l16',
            text: 'else:	#nivel 2 interno',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l17',
            text: 'print("La segunda es mayor por sus horas")',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l18',
            text: 'elif M1=="PM":',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l19',
            text: 'print(f"{hora1}:{minuto1} {M1} es mayor")',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l20',
            text: 'else:	#nivel 1 externo',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l21',
            text: 'print(f"{hora2}:{minuto2} {M2} es mayor")',
            height: 25,
            width: '90%',
            margin: 2
        }
    ]
    //console.log(datos);
    var randomDatos = shuffle(datos);
    //console.log(randomDatos);
    Ext.application({
        name : 'Fiddle',
        requires: ['Ext.ux.BoxReorderer'],
        launch : function () {
            Ext.widget('toolbar', {
                id: 'toolbar',
                layout: 'vbox',
                style: 'background-image: url("../static/openscoco/img/textura1_small.jpg")',
                renderTo: 'ejercicio_opciones',//Ext.getBody(),
                plugins : Ext.create('Ext.ux.BoxReorderer', {}),
                items   : randomDatos
            });
        }
    });

});