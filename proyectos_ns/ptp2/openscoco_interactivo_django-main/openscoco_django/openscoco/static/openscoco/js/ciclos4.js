Ext.onReady(function(){
    $('#spinner').hide();
    Ext.application({
        name : 'Fiddle',
        requires: ['Ext.ux.BoxReorderer'],
        launch : function () {
            Ext.widget('toolbar', {
                id: 'toolbar',
                layout: 'vbox',
                style: 'background-image: url("../static/openscoco/img/textura1_small.jpg")',
                renderTo: 'ejercicio_opciones',//Ext.getBody(),
                plugins : Ext.create('Ext.ux.BoxReorderer', {}),
                items   : [
                    {
                        id: 'l9',
                        text: 'promedio = suma / len(edades)',
                        height: 30,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l10',
                        text: 'print(f"Promedio: {promedio}. Edad menor: {menor} y mayor: {mayor} ")',
                        height: 30,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l1',
                        text: 'suma = 0',
                        height: 30,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l3',
                        text: 'menor = 999999',
                        height: 30,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l0',
                        text: 'edades = [20, 35, 18, 83, 50, 65, 20, 18]',
                        height: 30,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l2',
                        text: 'promedio = 0',
                        height: 30,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l4',
                        text: 'mayor = 0',
                        height: 30,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l5',
                        text: 'for i in edades:',
                        height: 30,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l7',
                        text: 'if i > mayor: mayor = i',
                        height: 30,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l8',
                        text: 'if i < menor: menor = i',
                        height: 30,
                        width: '90%',
                        margin: 2
                    },
                    {
                        id: 'l6',
                        text: 'suma += i',
                        height: 30,
                        width: '90%',
                        margin: 2
                    }
                ]
            });
        }
    });
});





