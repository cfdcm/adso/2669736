function shuffle(o){ //v1.0
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
}

Ext.onReady(function(){
    $('#spinner').hide();
    var datos = [
        {
            id: 'l0',
            text: 'N1 = int(input("Digite el N1"));',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l1',
            text: 'N2 = int(input("Digite el N2"));',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l2',
            text: 'if N1 % N2 == 0:',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l3',
            text: 'print("Son factores entre ellos")',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l4',
            text: 'else:	#condicional externo',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l5',
            text: 'print("No son factores")',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l6',
            text: 'N3 = int(input("Digite el N3"));',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l7',
            text: 'if N3 % N1 == 0 or N3 % N2 == 0 :',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l8',
            text: 'print("El N3 es factor de N1 o N2")',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l9',
            text: 'else:	#condicional interno',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l10',
            text: 'print("El N3 no es factor de N1 y N2")',
            height: 25,
            width: '90%',
            margin: 2
        }
    ]
    //console.log(datos);
    var randomDatos = shuffle(datos);
    //console.log(randomDatos);
    Ext.application({
        name : 'Fiddle',
        requires: ['Ext.ux.BoxReorderer'],
        launch : function () {
            Ext.widget('toolbar', {
                id: 'toolbar',
                layout: 'vbox',
                style: 'background-image: url("../static/openscoco/img/textura1_small.jpg")',
                renderTo: 'ejercicio_opciones',//Ext.getBody(),
                plugins : Ext.create('Ext.ux.BoxReorderer', {}),
                items   : randomDatos
            });
        }
    });

});





