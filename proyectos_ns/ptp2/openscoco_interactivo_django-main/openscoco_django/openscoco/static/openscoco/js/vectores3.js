function shuffle(o){ //v1.0
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
}

Ext.onReady(function(){
    $('#spinner').hide();
    var datos = [
        {
            id: 'l0',
            text: 'tarea=[]',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l1',
            text: 'opt = ""',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l2',
            text: 'while opt.lower() != "no":',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l3',
            text: 'opt = input("Quiere agregar una tares: SI/NO: ")',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l4',
            text: 'if opt.lower() == "si":',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l5',
            text: 'tarea.append(input("Agregar tarea: "))',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l6',
            text: 'if len(tarea) > 0:',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l7',
            text: 'num = int(input(f"Cuál tarea quieres eliminar?(0 - {len(tarea)-1}): "))',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l8',
            text: 'tarea.pop(num)',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l9',
            text: 'else:',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l10',
            text: 'print("Usted no tiene tareas pendientes")',
            height: 25,
            width: '90%',
            margin: 2
        }
    ]
    //console.log(datos);
    var randomDatos = shuffle(datos);
    //console.log(randomDatos);
    Ext.application({
        name : 'Fiddle',
        requires: ['Ext.ux.BoxReorderer'],
        launch : function () {
            Ext.widget('toolbar', {
                id: 'toolbar',
                layout: 'vbox',
                style: 'background-image: url("../static/openscoco/img/textura1_small.jpg")',
                renderTo: 'ejercicio_opciones',//Ext.getBody(),
                plugins : Ext.create('Ext.ux.BoxReorderer', {}),
                items   : randomDatos
            });
        }
    });

});





