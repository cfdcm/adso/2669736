Ext.onReady(function(){
    $('#spinner').hide();
    Ext.application({
        name : 'Fiddle',
        requires: ['Ext.ux.BoxReorderer'],
        launch : function () {
            Ext.widget('toolbar', {
                id: 'toolbar',
                //layout: 'vbox',   //if B > A and (C == E or D):
                style: 'background-image: url("../static/openscoco/img/textura1_small.jpg")',
                renderTo: 'ejercicio_opciones',//Ext.getBody(),
                plugins : Ext.create('Ext.ux.BoxReorderer', {}),
                items   : [
                    {
                        id: 'l5',
                        text: 'C',
                        height: 50,
                        width: '10%',
                        margin: 2
                    },
                    {
                        id: 'l2',
                        text: 'A',
                        height: 50,
                        width: '10%',
                        margin: 2
                    },
                    {
                        id: 'l0',
                        text: 'B',
                        height: 50,
                        width: '10%',
                        margin: 2
                    },
                    {
                        id: 'l4',
                        text: '(',
                        height: 50,
                        width: '10%',
                        margin: 2
                    },
                    {
                        id: 'l1',
                        text: '<',
                        height: 50,
                        width: '10%',
                        margin: 2
                    },
                    {
                        id: 'l3',
                        text: 'or',
                        height: 50,
                        width: '13%',
                        margin: 2
                    },
                    {
                        id: 'l6',
                        text: 'and',
                        height: 50,
                        width: '16%',
                        margin: 2
                    },
                    {
                        id: 'l8',
                        text: ')',
                        height: 50,
                        width: '10%',
                        margin: 2
                    },
                    {
                        id: 'l7',
                        text: 'D',
                        height: 50,
                        width: '10%',
                        margin: 2
                    }
                ]
            });
        }
    });
});





