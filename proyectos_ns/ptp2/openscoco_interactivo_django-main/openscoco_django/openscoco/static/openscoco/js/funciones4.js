function shuffle(o){ //v1.0
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
}

Ext.onReady(function(){
    $('#spinner').hide();
    var datos = [
        {
            id: 'l0',
            text: 'def buscar(vector, dato, opciones):',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l1',
            text: 'if opciones == 1:',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l2',
            text: 'print(vector[dato])',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l3',
            text: 'elif opciones == 2:',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l4',
            text: 'for x in range(0, len(vector)):',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l5',
            text: 'if vector[x].lower() == dato.lower():',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l6',
            text: 'return "Persona encontrada"',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l7',
            text: 'break',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l8',
            text: 'return "Persona no encontrada"',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l9',
            text: 'if __name__ == "__main__":',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l10',
            text: 'personas = ["Jose", "María", "Pedro", "Jorge", "Lina"]',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l11',
            text: 'opt = int(input("Digite 1 para índice, 2 para valor: "))',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l12',
            text: 'if opt == 1:',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l13',
            text: 'dato = int(input(f"Digite un índice entre 0 y {len(personas)-1}: "))',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l14',
            text: 'buscar(personas, dato, opt)',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l15',
            text: 'elif opt == 2:',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l16',
            text: 'dato = input("Digite el nombre a buscar: ")',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l17',
            text: 'print(buscar(personas, dato, opt))',
            height: 25,
            width: '90%',
            margin: 2
        }
    ]
    //console.log(datos);
    var randomDatos = shuffle(datos);
    //console.log(randomDatos);
    Ext.application({
        name : 'Fiddle',
        requires: ['Ext.ux.BoxReorderer'],
        launch : function () {
            Ext.widget('toolbar', {
                id: 'toolbar',
                layout: 'vbox',
                style: 'background-image: url("../static/openscoco/img/textura1_small.jpg")',
                renderTo: 'ejercicio_opciones',//Ext.getBody(),
                plugins : Ext.create('Ext.ux.BoxReorderer', {}),
                items   : randomDatos
            });
        }
    });

});