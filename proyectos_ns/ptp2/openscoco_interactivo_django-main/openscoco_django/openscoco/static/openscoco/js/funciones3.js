function shuffle(o){ //v1.0
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
}

Ext.onReady(function(){
    $('#spinner').hide();
    var datos = [
        {
            id: 'l0',
            text: 'def llenar(vector, tamano):',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l1',
            text: 'for x in range(0,tamano):	#llenar',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l2',
            text: 'nombre = input("Digite su nombre: ")',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l3',
            text: 'vector.append(nombre)',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l4',
            text: 'return vector',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l5',
            text: 'def mostrar(vector, tamano):',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l6',
            text: 'for x in range(0,tamano):	#mostrar',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l7',
            text: 'print(vector[x])',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l8',
            text: 'if __name__ == "__main__":',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l9',
            text: 'cant = int(input("Digite la cantidad de personas: "))',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l10',
            text: 'personas = []',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l11',
            text: 'vector_lleno = llenar(personas, cant)',
            height: 25,
            width: '90%',
            margin: 2
        },
        {
            id: 'l12',
            text: 'mostrar(vector_lleno, cant)',
            height: 25,
            width: '90%',
            margin: 2
        }
    ]
    //console.log(datos);
    var randomDatos = shuffle(datos);
    //console.log(randomDatos);
    Ext.application({
        name : 'Fiddle',
        requires: ['Ext.ux.BoxReorderer'],
        launch : function () {
            Ext.widget('toolbar', {
                id: 'toolbar',
                layout: 'vbox',
                style: 'background-image: url("../static/openscoco/img/textura1_small.jpg")',
                renderTo: 'ejercicio_opciones',//Ext.getBody(),
                plugins : Ext.create('Ext.ux.BoxReorderer', {}),
                items   : randomDatos
            });
        }
    });

});





