function shuffle(o){ //v1.0
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
}

Ext.onReady(function(){
    $('#spinner').hide();
    var datos = [
        {
            id: 'l0',
            text: 'def cuadrado():',
            height: 35,
            width: '90%',
            margin: 2
        },
        {
            id: 'l1',
            text: 'for x in range(0,4):     #externo',
            height: 35,
            width: '90%',
            margin: 2
        },
        {
            id: 'l2',
            text: 'for y in range(0,4):     #interno',
            height: 35,
            width: '90%',
            margin: 2
        },
        {
            id: 'l3',
            text: 'print("*", end="")',
            height: 35,
            width: '90%',
            margin: 2
        },
        {
            id: 'l4',
            text: 'print("*", end="\\n")',
            height: 35,
            width: '90%',
            margin: 2
        },
        {
            id: 'l5',
            text: 'if __name__ == "__main__":',
            height: 35,
            width: '90%',
            margin: 2
        },
        {
            id: 'l6',
            text: 'cuadrado()',
            height: 35,
            width: '90%',
            margin: 2
        }
    ]
    //console.log(datos);
    var randomDatos = shuffle(datos);
    //console.log(randomDatos);
    Ext.application({
        name : 'Fiddle',
        requires: ['Ext.ux.BoxReorderer'],
        launch : function () {
            Ext.widget('toolbar', {
                id: 'toolbar',
                layout: 'vbox',
                style: 'background-image: url("../static/openscoco/img/textura1_small.jpg")',
                renderTo: 'ejercicio_opciones',//Ext.getBody(),
                plugins : Ext.create('Ext.ux.BoxReorderer', {}),
                items   : randomDatos
            });
        }
    });

});





