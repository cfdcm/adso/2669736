from django.db import models

# Create your models here.
from django.contrib.auth.models import User


class Tema(models.Model):
    nombre = models.CharField(max_length=254)
    descripcion = models.TextField(null=True, blank=True)
    usuario_creador = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    # Foto

    def __str__(self):
        return self.nombre


class Problema(models.Model):
    tema = models.ForeignKey(Tema, on_delete=models.DO_NOTHING)
    usuario_creador = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    titulo = models.CharField(max_length=254, null=True, blank=True)
    descripcion = models.TextField()
    fecha = models.DateField(auto_now_add=True)
    MODERADO = (
        ("F", "Aplausos!!"),
        ("C", "Publicado"),
        ("A", "Silenciado"),
        ("P", "Plagio"),
        ("B", "Baneado"),
    )
    moderado = models.CharField(choices= MODERADO, max_length=1, default="C")
    NIVEL = (
        ("-", "Sin categorizar"),
        ("F", "Fácil"),
        ("M", "Medio"),
        ("D", "Difícil"),
    )
    nivel = models.CharField(choices=NIVEL, max_length=1, default="-")

    def __str__(self):
        return f"{self.id} - {self.titulo or ''}"


class Solucion(models.Model):
    problema = models.ForeignKey(Problema, on_delete=models.DO_NOTHING)
    usuario_creador = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    descripcion = models.TextField(help_text="Por favor, sólo soluciones de creación propia, de lo contrario dar los créditos respectivos. ")
    lenguaje_prog = models.CharField(max_length=100)
    MODERADO = (
        ("F", "Aplausos!!"),
        ("C", "Publicado"),
        ("A", "Silenciado"),
        ("P", "Plagio"),
        ("B", "Baneado"),
    )
    moderado = models.CharField(choices=MODERADO, max_length=1, default="C")
    fecha = models.DateField(auto_now_add=True)

    def __str__(self):
        return f"{self.id} - {self.usuario_creador} - {self.lenguaje_prog}"


class Comentario(models.Model):
    solucion = models.ForeignKey(Solucion, on_delete=models.DO_NOTHING)
    usuario_creador = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    mensaje = models.TextField(null=True, blank=True)
    calificacion = models.PositiveSmallIntegerField(null=True, blank=True)  # Estrellas 1 - 5
    MODERADO = (
        ("F", "Aplausos!!"),
        ("C", "Publicado"),
        ("A", "Silenciado"),
        ("P", "Plagio"),
        ("B", "Baneado"),
    )
    moderado = models.CharField(choices=MODERADO, max_length=1, default="C")
    fecha = models.DateField(auto_now_add=True)

    def __str__(self):
        return f"{self.id} - {self.usuario_creador} - {self.calificacion}"
