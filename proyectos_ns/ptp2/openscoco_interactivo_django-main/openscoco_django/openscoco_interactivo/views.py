from django.shortcuts import render

from rest_framework import viewsets
from rest_framework import permissions
from rest_framework import generics

from .models import *
from .serializers import *


def index(request):
    # Política de privacidad
    return render(request, 'openscoco/index.html')


# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAdminUser]  #

    queryset = User.objects.all()
    serializer_class = UserSerializer


class TemaViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.DjangoModelPermissions]

    serializer_class = TemaSerializer
    queryset = Tema.objects.all()


class ProblemaViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.DjangoModelPermissions]

    serializer_class = ProblemaSerializer
    queryset = Problema.objects.all()  # filter(moderado="P")


class ProblemaList(generics.ListAPIView):
    serializer_class = ProblemaSerializer

    def get_queryset(self):
        """
        Vista para mostrar todos los problemas de un tema específico.
        """
        tema = self.kwargs['tema']
        # tema_obj = Tema.objects.get(pk=tema)
        return Problema.objects.filter(tema__id=tema).order_by('-fecha')


class SolucionViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.DjangoModelPermissions]

    serializer_class = SolucionSerializer
    queryset = Solucion.objects.all()  # filter(moderado="P")


class SolucionList(generics.ListAPIView):
    serializer_class = SolucionSerializer

    def get_queryset(self):
        """
        Vista para mostrar todas las soluciones de un problema específico.
        """
        problema = self.kwargs['problema']
        return Solucion.objects.filter(problema__id=problema).order_by('-fecha')


class ComentarioViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.DjangoModelPermissions]

    serializer_class = ComentarioSerializer
    queryset = Comentario.objects.all()  # filter(moderado="P")


class ComentarioList(generics.ListAPIView):
    serializer_class = ComentarioSerializer

    def get_queryset(self):
        """
        Vista para mostrar todos los comentarios de una solución específica.
        """
        solucion = self.kwargs['solucion']
        return Comentario.objects.filter(solucion__id=solucion).order_by('-fecha')

