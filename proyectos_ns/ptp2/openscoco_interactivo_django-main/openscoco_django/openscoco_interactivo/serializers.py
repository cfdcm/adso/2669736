from rest_framework import serializers
from .models import *

# Serializers define the API representation.
from django.contrib.auth.models import User
from django.contrib.auth.models import Group

from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response

# Aplicar Pygments al código
from pygments import highlight
from pygments.lexers import get_lexer_by_name
from pygments.lexers import guess_lexer
from pygments.formatters import HtmlFormatter


class CustomAuthToken(ObtainAuthToken):
    """Serializador POST para conocer la información del usuario a partir de su token + usuario + password """
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})

        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        # Consulto los grupos a los que pertenece el usuario y los devuelvo como dict
        groups = Group.objects.filter(user=user).values()

        token, created = Token.objects.get_or_create(user=user)
        return Response({
            # 'token': token.key,
            'user_id': user.pk,
            'email': user.email,
            'first_name': user.first_name,
            'last_name': user.last_name,
            'is_active': user.is_active,
            'is_staff': user.is_staff,
            'groups': groups
        })


class GroupListingField(serializers.RelatedField):
    """Representación específica de campo para Groups"""
    def to_representation(self, obj):
        return obj.id

    def to_internal_value(self, data):
        try:
            try:
                obj_id = int(data)
                return Group.objects.get(id=obj_id)
            except KeyError:
                raise serializers.ValidationError(
                    'id is a required field.'
                )
            except ValueError:
                raise serializers.ValidationError(
                    'id must be an integer.'
                )
        except Group.DoesNotExist:
            raise serializers.ValidationError(
                'Obj does not exist.'
            )


class UserSerializer(serializers.ModelSerializer):
    """Serializador de usuario en el que aplico modificaciones de password y grupos"""
    groups = GroupListingField(many=True, queryset=Group.objects.all())

    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'is_superuser', 'first_name', 'last_name', 'is_active',
                  'password', 'groups']

    def create(self, validated_data):
        # Agregar los campos faltantes aquí... <<<<<<<<<<<<<<<<<<
        user = User(username=validated_data['username'], email=validated_data['email'], is_active=validated_data['is_active'], first_name=validated_data['first_name'], last_name=validated_data['last_name'])

        # Guardar usuario con algunos campos, pero sin grupos dado que es ManyToMany y da problema al no existir aún el usuario y no tener ID.
        user.save()

        # Realizo un last insert ID
        obj = User.objects.latest('id')

        # Encriptar contraseña
        obj.set_password(validated_data['password'])

        # Asigno los grupos al usuario
        obj.groups.set(validated_data['groups'])
        # Guardo los cambios al usuario específico
        obj.save()

        return obj

    def update(self, instance, validated_data):

        cambio = False
        nueva = ""
        if instance.password != validated_data['password']:
            cambio = True
            nueva = validated_data['password']

        updated_user = super().update(instance, validated_data)

        if cambio:
            # Encriptar clave
            updated_user.set_password(nueva)

        updated_user.save()

        return updated_user


class TemaSerializer(serializers.ModelSerializer):
    # Campo especial conteo de problemas para este tema.
    conteo = serializers.SerializerMethodField()

    class Meta:
        model = Tema
        fields = ['id', 'nombre', 'descripcion', 'usuario_creador', 'conteo']

    def get_conteo(self, obj):
        query = Problema.objects.filter(tema=obj.id).count()
        return query


class ProblemaSerializer(serializers.ModelSerializer):
    # Campos con choices
    nivel_name = serializers.SerializerMethodField()
    moderado_name = serializers.SerializerMethodField()
    # Campos con relaciones, para traer dato específico
    usuario_creador_name = serializers.StringRelatedField(many=False, source='usuario_creador.first_name')
    usuario_creador_apell = serializers.StringRelatedField(many=False, source='usuario_creador.last_name')

    # Campo especial conteo de soluciones para este problema.
    conteo = serializers.SerializerMethodField()

    class Meta:
        model = Problema
        fields = ['id', 'tema', 'usuario_creador', 'usuario_creador_name', 'usuario_creador_apell', 'titulo', 'descripcion', 'fecha', 'moderado', 'moderado_name', 'nivel', 'nivel_name', 'conteo']

    def get_nivel_name(self, obj):
        return obj.get_nivel_display()

    def get_moderado_name(self, obj):
        return obj.get_moderado_display()

    def get_conteo(self, obj):
        query = Solucion.objects.filter(problema=obj.id).count()
        return query

class SolucionSerializer(serializers.ModelSerializer):

    # Campos con choices
    moderado_name = serializers.SerializerMethodField()

    # Aplicar Pygments al código
    prettyCode = serializers.SerializerMethodField()

    # Campos con relaciones, para traer dado específico
    usuario_creador_name = serializers.StringRelatedField(many=False, source='usuario_creador.first_name')
    usuario_creador_apell = serializers.StringRelatedField(many=False, source='usuario_creador.last_name')

    # Campo especial promedio de comentarios (estrellas).
    estrellas_promedio = serializers.SerializerMethodField()

    current_user_has_comment_or_star = serializers.SerializerMethodField()

    class Meta:
        model = Solucion
        fields = ['id', 'problema', 'usuario_creador', 'usuario_creador_name', 'usuario_creador_apell', 'descripcion', 'prettyCode', 'lenguaje_prog', 'moderado', 'moderado_name', 'fecha', 'estrellas_promedio', 'current_user_has_comment_or_star']

    def get_moderado_name(self, obj):
        return obj.get_moderado_display()

    def get_prettyCode(self, obj):
        try:
            lexer = get_lexer_by_name(obj.lenguaje_prog, stripall=True)
        except Exception as e:
            lexer = guess_lexer(obj.descripcion, stripall=True)
        formatter = HtmlFormatter(linenos=True, noclasses="True", style='default')  # paraiso-dark
        result = highlight(obj.descripcion, lexer, formatter)
        return result

    def get_estrellas_promedio(self, obj):
        from django.db.models import Avg

        Comentario.objects.aggregate(Avg('calificacion'))
        estrellas_prom = Comentario.objects.filter(solucion=obj.id).aggregate(Avg('calificacion'))['calificacion__avg']
        return estrellas_prom

    def get_current_user_has_comment_or_star(self, obj):
        if self.context['request'].user.is_authenticated:
            current_user = self.context['request'].user.id
            query = Comentario.objects.filter(usuario_creador_id=current_user, solucion=obj.id).values()
            # print("Conteo: ", query.count())
            if query.count() > 0:
                # print(obj.id, "Query ID: ", query[0]["id"])
                # print(obj.id, "Comentario: ", query[0]["mensaje"])
                # print(obj.id, "Calificacion: ", query[0]["calificacion"])
                return True, query[0]["id"], query[0]["calificacion"], query[0]["mensaje"]

            return False, None


class ComentarioSerializer(serializers.ModelSerializer):
    # Campos con choices
    moderado_name = serializers.SerializerMethodField()
    # Campos con relaciones, para traer dado específico
    usuario_creador_name = serializers.StringRelatedField(many=False, source='usuario_creador.first_name')
    usuario_creador_apell = serializers.StringRelatedField(many=False, source='usuario_creador.last_name')
    # Aplicar Pygments al código
    prettyCode = serializers.SerializerMethodField()

    class Meta:
        model = Comentario
        fields = ['id', 'solucion', 'prettyCode', 'usuario_creador', 'usuario_creador_name', 'usuario_creador_apell', 'mensaje', 'calificacion', 'moderado', 'moderado_name', 'fecha']

    def get_moderado_name(self, obj):
        return obj.get_moderado_display()

    def get_prettyCode(self, obj):
        try:
            lexer = get_lexer_by_name(obj.solucion.lenguaje_prog, stripall=True)
        except Exception as e:
            lexer = guess_lexer(obj.solucion.descripcion, stripall=True)
        formatter = HtmlFormatter(linenos=True, noclasses="True", style='default')  # paraiso-dark
        result = highlight(obj.solucion.descripcion, lexer, formatter)
        return result