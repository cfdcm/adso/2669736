import { LoginComponent } from "./components/login/login";
import { HomeComponent } from "./components/home/home";
import { TiendaComponent } from "./components/tienda/tienda";
import { TiendaEditarComponent } from "./components/tienda/tienda-editar";
import { WidgetsComponent } from "./components/widgets/widgets";
import { Ejemplo1Component } from "./components/widgets/ejemplo1";
import { Ejemplo2Component } from "./components/widgets/ejemplo2";

export const appRoutes: any = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: "login", component: LoginComponent },
  { path: "home", component: HomeComponent },
  { path: "tienda", component: TiendaComponent },
  { path: "tienda-editar", component: TiendaEditarComponent },
  { path: "widgets", component: WidgetsComponent },
  { path: "ejemplo1", component: Ejemplo1Component },
  { path: "ejemplo2", component: Ejemplo2Component },
];

export const appComponents: any = [
  LoginComponent,
  HomeComponent,
  TiendaComponent,
  TiendaEditarComponent,
  WidgetsComponent,
  Ejemplo1Component,
  Ejemplo2Component,
];
