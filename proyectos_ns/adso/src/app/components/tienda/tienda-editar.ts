import { Component, OnInit } from '@angular/core'
import { Router } from "@angular/router";
import { ApiService } from './api.service';

import { Dialogs } from '@nativescript/core'
import { ActivatedRoute } from '@angular/router';
import { map, filter, scan } from 'rxjs/operators';
import { TextField } from "@nativescript/core/ui/text-field";


@Component({
    selector: 'tienda-editar',
    templateUrl: './tienda-editar.html',
})
export class TiendaEditarComponent {

    id: number;
    nombre: string;
    descripcion: string;
    public constructor(private router: Router, private apiService: ApiService, private activatedRoute: ActivatedRoute ) {
        this.activatedRoute.queryParams
          .subscribe((params) => {
            this.id = params.id;
            this.apiService.getRegisterById(params.id).subscribe((res) => {
                console.info(res)
                this.nombre = res.nombre;
                this.descripcion = res.descripcion;
            },error => {
                console.log(error.status)
                if (error.status == 400){
                    Dialogs.alert({
                        title: 'Respuesta:',
                        message: error.error.message,
                        okButtonText: 'OK',
                        cancelable: true,
                    });
                }
                else{
                    Dialogs.alert({
                        title: 'Respuesta:',
                        message: error.message,
                        okButtonText: 'OK',
                        cancelable: true,
                    });
                }

            });
          }
        );
    }

    public actualizarRegistro(){
        let data = {
            nombre: this.nombre,
            descripcion: this.descripcion
        };
        console.log(data)
        this.apiService.updateRegister(this.id, data).subscribe((res) => {
            console.info("ok")
            Dialogs.alert({
                title: 'Detalles!',
                message: 'Categoría actualizada correctamente!!',
                okButtonText: 'OK',
                cancelable: true,
            });
            this.router.navigate(['tienda']);
        });
    }

    inputChange(args, campo) {
        // blur event will be triggered when the user leaves the TextField
        let textField = <TextField>args.object;
        if (campo == "nombre"){
            this.nombre = textField.text;
        }
        else if(campo == "descripcion"){
            this.descripcion = textField.text;
        }
    }
}

