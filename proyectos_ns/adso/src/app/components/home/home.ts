import { Component, OnInit } from '@angular/core'
import { Router } from "@angular/router";
import { exit } from "nativescript-exit";

@Component({
    selector: 'home',
    templateUrl: './home.html',
})
export class HomeComponent {
    rol: string;
    nombre: string;
    foto: string;
    perfil;
    public constructor(private router: Router) {
        // Use the component constructor to inject providers.
        console.log("home")
        console.info("Averiguando si hay datos...");
        if (localStorage.getItem('sena.token')){
            this.perfil = JSON.parse( localStorage.getItem('sena.user'))
            console.log("Bienvenido "+this.perfil.nombre+"!!");
            this.rol = this.perfil.rol
            this.nombre = this.perfil.nombre
            this.foto = global.url+this.perfil.foto
        }
        else{
            this.rol = ""
            this.nombre = ""
            this.foto = ""
            this.router.navigate(['login']);
        }
    }

    public cerrarSesion(){
        console.log("Eliminar sesión...")
        localStorage.clear();
        this.router.navigate(['login']);
    }

    public verificarPermisos(rol){
        console.log("necesita: "+rol)
        console.log("tengo: "+this.rol)
        return rol == this.rol;
    }

    public onExit(): void {
        exit(); // will close application
    }
}
