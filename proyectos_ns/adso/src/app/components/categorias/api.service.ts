import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  headers = {"Authorization": "Token "+ localStorage.getItem('sena.token')}

  constructor(private http: HttpClient) { }

  getRegisters(): Observable<any[]> {
    return this.http.get<any[]>(`${global.apiUrl}/categoria/`, { headers: this.headers});
  }

  getRegisterById(id: number): Observable<any> {
    return this.http.get<any>(`${global.apiUrl}/categoria/${id}/`, { headers: this.headers});
  }

  addRegister(post: any): Observable<any> {
    return this.http.post<any>(`${global.apiUrl}/categoria/`, post, { headers: this.headers});
  }

  updateRegister(id: number, post: any): Observable<any> {
    return this.http.put<any>(`${global.apiUrl}/categoria/${id}/`, post, { headers: this.headers});
  }

  deleteRegister(id: number): Observable<any> {
    return this.http.delete<any>(`${global.apiUrl}/categoria/${id}/`, { headers: this.headers});
  }

}
