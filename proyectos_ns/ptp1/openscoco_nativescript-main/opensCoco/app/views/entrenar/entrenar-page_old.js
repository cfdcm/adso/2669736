const observableModule = require("@nativescript/core/data/observable")
const WebView = require("@nativescript/core/ui/web-view")
const LoadEventData = require("@nativescript/core/ui/web-view")

var timer = require("@nativescript/core/timer")

//aqui puedo poner muchas variables que estarían disponibles en la UI
var datos = observableModule.fromObject({
    estado : true
});

var web;
/*var cont=0;

WebView.loadFinishedEvent = function (args) {
    cont++;
    if (cont==3){
        //de esta manera cambio un valor de variable para UI
        datos.set("estado", false);
        cont = 0;
    }
    console.log("Carga completa: "+cont);
};
*/

var timeoutId;

var callback = function(){
    datos.set("estado", false);
    console.log("Quitar spinner");
    timer.clearTimeout(timeoutId);
}



exports.pageLoaded = function(args){
    page = args.object;
    page.bindingContext = datos;
    datos.set("estado", true);

    web = page.getViewById("webView");
    web.src = "http://openscoco.tikole.net/?no_cache="+String(Math.random());
    timeoutId = timer.setTimeout(callback, 5000);
}

exports.regresar = function(args){
    const button = args.object;
    const page = button.page;
    page.frame.navigate("views/home/home-page");
}


exports.inicio_entrenar = function(args){
    const button = args.object;
    const page = button.page;
    page.frame.navigate("views/entrenar/entrenar-page");
}
