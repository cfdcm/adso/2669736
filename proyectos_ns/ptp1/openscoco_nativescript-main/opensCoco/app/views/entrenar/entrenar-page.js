const observableModule = require("@nativescript/core/data/observable")
const WebView = require("@nativescript/core/ui/web-view")
const LoadEventData = require("@nativescript/core/ui/web-view")

var timer = require("@nativescript/core/timer")

//aqui puedo poner muchas variables que estarían disponibles en la UI
var datos = observableModule.fromObject({
    estado : true
});

var web;
var timeoutId;

var callback = function(){
    datos.set("estado", false);
    console.log("Quitar spinner");
    timer.clearTimeout(timeoutId);
}

exports.pageLoaded = function(args){
    page = args.object;
    page.bindingContext = datos;
    datos.set("estado", true);

    web = page.getViewById("webView");
    web.src = "http://openscoco.tikole.net/openscoco/entrenar/?no_cache="+String(Math.random());
    timeoutId = timer.setTimeout(callback, 5000);
}

exports.regresar = function(args){
    const button = args.object;
    const page = button.page;
    page.frame.navigate("views/home/home-page");
}


exports.inicio_entrenar = function(args){
    const button = args.object;
    const page = button.page;
    page.frame.navigate("views/entrenar/entrenar-page");
}
