
const { fromObject } = require("@nativescript/core/data/observable")


function ready(args) {

    const page = args.object;
    const vm = fromObject({
        // Setting the listview binding source
        myTitles: [
            {
                "title": "Ejercicio 133",
                "desc": "Construya el juego del ahorcado. No gráfico",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 134",
                "desc": "Construya el juego del triqui. No gráfico",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 135",
                "desc": "Simule una pelea del juego de Pokemon. No gráfico",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            }
        ]
    });
    page.bindingContext = vm;
}
exports.ready = ready;



function onListViewLoaded(args) {
    const listView = args.object;
    
}
exports.onListViewLoaded = onListViewLoaded;

function onItemTap(args) {
    const index = args.index;
    console.log(`Second ListView item tap ${index}`);

    var info = args.view.bindingContext;
    console.log(info); // [Object object]
    console.log(info["title"]); // e.g. info["name"] === "Reg4"
    // info is Object of type { name: "Reg4" }

    var navigationEntry = {
        moduleName: "views/aprender/detalle-juegos",
        context: info
    }
    console.log("detalle algoritmo");
    
    const button = args.object;
    const page = button.page;
    page.frame.navigate(navigationEntry);
}
exports.onItemTap = onItemTap;

exports.regresar = function(args){
    const button = args.object;
    const page = button.page;
    page.frame.navigate("views/aprender/aprender-page");
}