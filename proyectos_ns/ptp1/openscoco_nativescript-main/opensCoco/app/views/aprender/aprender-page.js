exports.regresar = function(args){
    const button = args.object;
    const page = button.page;
    page.frame.navigate("views/home/home-page");
}

exports.operadores = function(args){
    console.log("operadores");
    const button = args.object;
    const page = button.page;
    page.frame.navigate("views/aprender/ejercicios-operadores-page");
}

exports.condicionales = function(args){
    console.log("condicionales");
    const button = args.object;
    const page = button.page;
    page.frame.navigate("views/aprender/ejercicios-condicionales-page");
}

exports.ciclos = function(args){
    console.log("ciclos");
    const button = args.object;
    const page = button.page;
    page.frame.navigate("views/aprender/ejercicios-ciclos-page");
}

exports.funciones = function(args){
    console.log("funciones");
    const button = args.object;
    const page = button.page;
    page.frame.navigate("views/aprender/ejercicios-funciones-page");
}

exports.vectores = function(args){
    console.log("vectores");
    const button = args.object;
    const page = button.page;
    page.frame.navigate("views/aprender/ejercicios-vectores-page");
}

exports.ord_bus = function(args){
    console.log("Ordenamiento y Búsqueda");
    const button = args.object;
    const page = button.page;
    page.frame.navigate("views/aprender/ejercicios-ordenamiento-busqueda-page");
}

exports.juegos = function(args){
    console.log("juegos");
    const button = args.object;
    const page = button.page;
    page.frame.navigate("views/aprender/ejercicios-juegos-page");
}
