var fs = require("@nativescript/core/file-system")
var documents = fs.knownFolders.currentApp();

module.exports = {
    readJSON: function (path) {
        var jsonFile = documents.getFile(path);
        return new Promise(function (resolve, reject) {
            try {
                jsonFile.readText().then(function (content) {
                    var data = JSON.parse(content);
                    resolve(data);
                });
            }
            catch (err) {
                reject(err);
            }
        });
    }
};
