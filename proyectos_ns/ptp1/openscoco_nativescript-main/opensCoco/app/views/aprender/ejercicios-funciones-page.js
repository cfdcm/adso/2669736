
const { fromObject } = require("@nativescript/core/data/observable")


function ready(args) {

    const page = args.object;
    const vm = fromObject({
        // Setting the listview binding source
        myTitles: [
            {
                "title": "Ejercicio 108",
                "desc": "Construya un algoritmo usando funciones que calcule la mitad del valor que se le pasó como parámetro.",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=1"
            },
            {
                "title": "Ejercicio 109",
                "desc": "Implementar una función que realice la serie de Fibonacci, que es: \nFibonacci (0) = Fibonacci (1) = 1 \nFibonnaci(n) = Fn-1 + Fn-2 …",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=2"
            },
            {
                "title": "Ejercicio 110",
                "desc": "Implementar una función recursiva que realice la serie de Fibonacci, que es: \nFibonacci (0) = Fibonacci (1) = 1 \nN > 1, Fibonnaci(n) = Fn-1 + Fn-2 …",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=3"
            },
            {
                "title": "Ejercicio 111",
                "desc": "Implementar un programa con funciones al que pasándole como parámetros 2 valores enteros M y N, me calcule la suma.",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=4"
            },
            {
                "title": "Ejercicio 112",
                "desc": "Implementar un programa con funciones que me halle cuál es la primera potencia en base 2 mayor que un número que pasamos como parámetro, devolviendo el valor de dicha potencia y el exponente al que está elevado.",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=5"
            },
            {
                "title": "Ejercicio 113",
                "desc": "Implementar un programa con funciones que convirta de dólares a pesos o viceversa. Pedir al usuario la TRM (Tasa Representativa del Mercado).",
                "level": "~/images/icons/info/icons3-circled-3-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=6"
            },
            {
                "title": "Ejercicio 114",
                "desc": "Implementar un programa con un menú de funciones que permita simular una calculadora sencilla entre dos números con las operaciones: suma, resta, multiplicación, división, residuo.",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=7"
            },
            {
                "title": "Ejercicio 115",
                "desc": "Implementar un programa que permita a un usuario comprar varios productos en una tienda y que  de acuerdo al día de la semana “martes” y si el precio del producto es > $.5000, entonces aplicar el 20% de descuento. Mostrar el pago total para el cliente.",
                "level": "~/images/icons/info/icons2-circled-2-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=8"
            },
            {
                "title": "Ejercicio 116",
                "desc": "Simular el login y cerrado de sesión de un software. Validar estados.",
                "level": "~/images/icons/info/icons1-circled-1-100.png",
                "sol": "https://www.dropbox.com/s/d4h6m8js57wgdzo/prueba.png?dl=9"
            }
        ]
    });
    page.bindingContext = vm;
}
exports.ready = ready;



function onListViewLoaded(args) {
    const listView = args.object;

}
exports.onListViewLoaded = onListViewLoaded;

function onItemTap(args) {
    const index = args.index;
    console.log(`Second ListView item tap ${index}`);

    var info = args.view.bindingContext;
    console.log(info); // [Object object]
    console.log(info["title"]); // e.g. info["name"] === "Reg4"
    // info is Object of type { name: "Reg4" }

    var navigationEntry = {
        moduleName: "views/aprender/detalle-funciones",
        context: info
    }
    console.log("detalle algoritmo");

    const button = args.object;
    const page = button.page;
    page.frame.navigate(navigationEntry);
}
exports.onItemTap = onItemTap;

exports.regresar = function(args){
    const button = args.object;
    const page = button.page;
    page.frame.navigate("views/aprender/aprender-page");
}
