const { exit } = require("nativescript-exit");

exports.ready = function(){
    console.log("Inicio!");
}


exports.info = function(args){
    console.log("Información");
    const button = args.object;
    const page = button.page;
    page.frame.navigate("views/informacion/informacion-page");
}

exports.aprender = function(args){
    console.log("Aprender");
    const button = args.object;
    const page = button.page;
    page.frame.navigate("views/aprender/aprender-page");
}

exports.entrenar = function(args){
    console.log("Entrenar");
    const button = args.object;
    const page = button.page;
    page.frame.navigate("views/entrenar/entrenar-page");
}

exports.leerQRGuia = function(args){
    /*console.log("Leer código QR de la guía de Aprendizaje");
    const button = args.object;
    const page = button.page;
    page.frame.navigate("views/qr_guia/qr_guia-page");*/
    console.log("Iniciando lector de códigos QR!");
    var BarcodeScanner = require("nativescript-barcodescanner").BarcodeScanner;
    var barcodescanner = new BarcodeScanner();

    barcodescanner.scan({
    formats: "QR_CODE,PDF_417",   // Pass in of you want to restrict scanning to certain types
    cancelLabel: "EXIT. Also, try the volume buttons!", // iOS only, default 'Close'
    cancelLabelBackgroundColor: "#333333", // iOS only, default '#000000' (black)
    message: "Por favor mantenga el celular leyendo el código QR hasta que avance a la página correspondiente.", // Android only, default is 'Place a barcode inside the viewfinder rectangle to scan it.'
    showFlipCameraButton: true,   // default false
    preferFrontCamera: false,     // default false
    showTorchButton: false,        // default false
    beepOnScan: true,             // Play or Suppress beep on scan (default true)
    fullScreen: false,             // Currently only used on iOS; with iOS 13 modals are no longer shown fullScreen by default, which may be actually preferred. But to use the old fullScreen appearance, set this to 'true'. Default 'false'.
    torchOn: false,               // launch with the flashlight on (default false)
    closeCallback: function () {
        console.log("Scanner closed");

    }, // invoked when the scanner was closed (success or abort)
    resultDisplayDuration: 500,   // Android only, default 1500 (ms), set to 0 to disable echoing the scanned text
    orientation: "portrait",     // Android only, optionally lock the orientation to either "portrait" or "landscape"
    openSettingsIfPermissionWasPreviouslyDenied: true // On iOS you can send the user to the settings app if access was previously denied
    }).then(
        function(result) {
            console.log("Scan format: " + result.format);
            console.log("Scan text:   " + result.text);
            switch(result.text){
                case '{ "block":"Entrenar" }':
                    pagina = "views/entrenar/entrenar-page"
                break;
                case '{ "block":"Operadores, Entradas y Salidas" }':
                    pagina = "views/aprender/ejercicios-operadores-page"
                break;
                case '{ "block":"Condicionales" }':
                    pagina = "views/aprender/ejercicios-condicionales-page"
                break;
                case '{ "block":"Ciclos" }':
                    pagina = "views/aprender/ejercicios-ciclos-page"
                break;
                case '{ "block":"Funciones" }':
                    pagina = "views/aprender/ejercicios-funciones-page"
                break;
                case '{ "block":"Vectores o Listas" }':
                    pagina = "views/aprender/ejercicios-vectores-page"
                break;
                case '{ "block":"Ordenamiento y Búsqueda" }':
                    pagina = "views/aprender/ejercicios-ordenamiento-busqueda-page"
                break;
                case '{ "block":"Juegos" }':
                    pagina = "views/aprender/ejercicios-juegos-page"
                break;
                default:
                    pagina = "views/home/home-page"
            }

            console.log("Redirigiendo a: " + pagina);
            const button = args.object;
            const page = button.page;
            page.frame.navigate(pagina);
        },
        function(error) {
        console.log("No scan: " + error);

        }
    );
}

exports.salir = function(){
    exit();
}
