/*const observableModule = require("tns-core-modules/data/observable");
const dialogsModule = require("tns-core-modules/ui/dialogs");
const topmost = require("tns-core-modules/ui/frame").topmost;

const userService = require("~/services/user-service");

function LoginViewModel() {
    const viewModel = observableModule.fromObject({
        email: "jor@misena.edu.co",
        password: "12345",
        confirmPassword: "",
        isLoggingIn: true,
        processing: false,

        toggleForm() {
            this.isLoggingIn = !this.isLoggingIn;
        },
        submit() {
            if (this.email.trim() === "" || this.password.trim() === "") {
                alert("Digite su nombre de usuario y contraseña.");
                return;
            }

            this.set("processing", true);
            if (this.isLoggingIn) {
                this.login();
            } else {
                this.register();
            }
        },
        login() {
            userService.login({
                email: this.email,
                password: this.password
            }).then(() => {
                this.set("processing", false);
                topmost().navigate({
                    moduleName: "/views/home/home-page",
                    clearHistory: true
                });
            }).catch((e) => {
                this.set("processing", false);
                console.log(e);
                alert("No se encontró su cuenta de usuario");
            });
        },
        register() {
            if (this.password != this.confirmPassword) {
                alert("Sus contraseñas no coinciden.");
                return;
            }
            userService.register({
                email: this.email,
                password: this.password
            }).then(() => {
                this.set("processing", false);
                alert("Su cuenta fue creada correctamente. Ahora puede Acceder.");
                this.isLoggingIn = true;
            })
                .catch(() => {
                    this.set("processing", false);
                    alert("Infortunadamente no pudimos crear su cuenta.");
                });
        },
        forgotPassword() {
            dialogsModule.prompt({
                title: "Olvidé mi contraseña",
                message: "Introduzca la dirección de correo electrónico que utilizó para registrarse en OPENS COCO para restablecer su contraseña..",
                inputType: "email",
                defaultText: "",
                okButtonText: "Ok",
                cancelButtonText: "Cancel"
            }).then((data) => {
                if (data.result) {
                    userService.resetPassword(data.text.trim())
                        .then(() => {
                            alert("Su contraseña ha sido restablecida con éxito. Por favor, compruebe su correo electrónico para obtener instrucciones sobre la elección de una nueva contraseña.");
                        }).catch(() => {
                            alert("Lamentablemente, se ha producido un error al restablecer su contraseña.");
                        });
                }
            });
        }
    });

    return viewModel;
}

module.exports = LoginViewModel;
*/