CREATE TABLE IF NOT EXISTS "scp_config" (
	"id"	INTEGER NOT NULL,
	"param"	VARCHAR(150) NOT NULL UNIQUE,
	"value1"	VARCHAR(254) NOT NULL,
	"value2"	VARCHAR(254),
	"value3"	VARCHAR(254),
	PRIMARY KEY("id" AUTOINCREMENT)
);
INSERT INTO scp_config VALUES(1,'tema','System',NULL,NULL);
INSERT INTO scp_config VALUES(2,'activo','True','',NULL);
INSERT INTO scp_config VALUES(3,'salario_min','1160000',NULL,NULL);
INSERT INTO scp_config VALUES(4,'aux_transporte','140606.00','',NULL);
INSERT INTO scp_config VALUES(5,'prima_servicios','108383.83','9.343433620689655',NULL);
INSERT INTO scp_config VALUES(6,'aux_cesantias','108383.83','9.343433620689655',NULL);
INSERT INTO scp_config VALUES(7,'intereses_cesantias','13006.06','1.121212068965517',NULL);
INSERT INTO scp_config VALUES(8,'vacaciones','48372.00','4.17',NULL);
INSERT INTO scp_config VALUES(9,'caja_compensacion','46400.00','4',NULL);
INSERT INTO scp_config VALUES(10,'pension','139200.00','12',NULL);
INSERT INTO scp_config VALUES(11,'arl','6055.20','0.522',NULL);
INSERT INTO scp_config VALUES(12,'salud','98600.00','8.5',NULL);
INSERT INTO scp_config VALUES(13,'total_costo_mensual_trabajador','1770406.92','49.00007931034483',NULL);

CREATE TABLE IF NOT EXISTS "scp_producto" (
	"id"	INTEGER NOT NULL,
	"nombre"	VARCHAR(254) NOT NULL,
	"referencia"	VARCHAR(254) NOT NULL UNIQUE,
	"tiempo_estandar"	REAL,
	PRIMARY KEY("id" AUTOINCREMENT)
);
INSERT INTO scp_producto VALUES(1,'CAM T-SHIRT','001CTSHIRT',4.0);
INSERT INTO scp_producto VALUES(2,'BUZO','002BUZO',10.0);
INSERT INTO scp_producto VALUES(3,'CAMISA CLASICA','003CAMCLASSIC',15.0);


CREATE TABLE scp_hoja_costo (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	fecha_creacion DATE,
	eficiencia INTEGER,
	jornada INTEGER,
	operarios INTEGER,
	minutos_mes INTEGER,
	utilidad INTEGER,
	producto INTEGER,
	CONSTRAINT scp_hoja_costo_FK FOREIGN KEY (producto) REFERENCES scp_producto(id) ON UPDATE CASCADE
);
INSERT INTO scp_hoja_costo VALUES(1,'2023/12/1',80,480,7,11520,70,1);
INSERT INTO scp_hoja_costo VALUES(2,'2023/11/25',80,480,4,11520,70,2);

CREATE TABLE IF NOT EXISTS "scp_material" (
	"id"	INTEGER NOT NULL,
	"nombre_tipo"	VARCHAR(254) NOT NULL,
	"proveedor"	VARCHAR(254),
	"unidad_med"	VARCHAR(50) NOT NULL,
	"costo_unitario"	REAL NOT NULL, tipo VARCHAR,
	PRIMARY KEY("id" AUTOINCREMENT)
);
INSERT INTO scp_material VALUES(1,'Algodón perchado',NULL,'mts',5540.0,'Directo');
INSERT INTO scp_material VALUES(2,'Tela franela','Fabricato','mts',9000.0,'Directo');
INSERT INTO scp_material VALUES(3,'marquilla composición','Bombay','und',150.0,'Directo');
INSERT INTO scp_material VALUES(4,'marquilla estampada','Bombay','und',200.0,'Directo');
INSERT INTO scp_material VALUES(5,'plastiflecha','Bombay','und',49.900000000000002131,'Directo');
INSERT INTO scp_material VALUES(6,'Etiquetas','Bombay','und',100.0,'Directo');
INSERT INTO scp_material VALUES(7,'Simbra ','Bombay','mts',14000.0,'Directo');
INSERT INTO scp_material VALUES(8,'Bolsas','Bombay','und',226.0,'Directo');
INSERT INTO scp_material VALUES(9,'Hilos','Coats cadena','mts',2.2000000000000001776,'Directo');
INSERT INTO scp_material VALUES(10,'hilo',NULL,'mts',2.2000000000000001776,'Directo');
INSERT INTO scp_material VALUES(11,'marquilla estampada',NULL,'mts',150.0,'Directo');
INSERT INTO scp_material VALUES(12,'Rib',NULL,'und',3000.0,'Directo');
INSERT INTO scp_material VALUES(13,'Hilaza',NULL,'mts',1.5,'Directo');
INSERT INTO scp_material VALUES(14,'plastiflecha',NULL,'mts',5.0,'Directo');
INSERT INTO scp_material VALUES(15,'marquilla composición',NULL,'und',140.0,'Directo');
INSERT INTO scp_material VALUES(16,'Bolsa compostable',NULL,'und',226.0,'Directo');
INSERT INTO scp_material VALUES(17,'Agujas para maquina de coser	',NULL,'und',1250.0,'Indirecto');
INSERT INTO scp_material VALUES(18,'Aceite Lubricante para Maquinas	',NULL,'und',2972.0,'Indirecto');
INSERT INTO scp_material VALUES(19,'Repuestos para maquinas de coser	',NULL,'und',250000.0,'Indirecto');
INSERT INTO scp_material VALUES(20,'Cuchillas para maquina cortadora	',NULL,'und',5500.0,'Indirecto');


CREATE TABLE scp_hoja_material (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	material_id INTEGER,
	consumo_unit INTEGER,
    costo_unit_historico INTEGER,
	hoja_costo_id INTEGER,
	CONSTRAINT FK_scp_hoja_material_scp_material FOREIGN KEY (material_id) REFERENCES scp_material(id) ON UPDATE CASCADE,
	CONSTRAINT scp_hoja_material_FK FOREIGN KEY (hoja_costo_id) REFERENCES scp_hoja_costo(id) ON UPDATE CASCADE
);

INSERT INTO scp_hoja_material VALUES(1,2,0.8,1000,1);
INSERT INTO scp_hoja_material VALUES(2,3,1,1000,1);
INSERT INTO scp_hoja_material VALUES(3,4,1,1000,1);
INSERT INTO scp_hoja_material VALUES(4,5,2,1000,1);
INSERT INTO scp_hoja_material VALUES(5,6,1,1000,1);
INSERT INTO scp_hoja_material VALUES(6,7,0.2,1000,1);
INSERT INTO scp_hoja_material VALUES(7,8,1,1000,1);
INSERT INTO scp_hoja_material VALUES(8,9,90,1000,1);
INSERT INTO scp_hoja_material VALUES(9,1,1.5,1000,2);
INSERT INTO scp_hoja_material VALUES(10,10,210,1000,2);
INSERT INTO scp_hoja_material VALUES(11,11,1,1000,2);
INSERT INTO scp_hoja_material VALUES(12,12,0.3,1000,2);
INSERT INTO scp_hoja_material VALUES(13,13,420,1000,2);
INSERT INTO scp_hoja_material VALUES(14,14,1,1000,2);
INSERT INTO scp_hoja_material VALUES(15,15,1,1000,2);
INSERT INTO scp_hoja_material VALUES(16,16,1,1000,2);

CREATE TABLE scp_hoja_material_indirecto (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	material_id INTEGER,
	consumo_mes INTEGER,
    costo_historico INTEGER,
	hoja_costo_id INTEGER,
	FOREIGN KEY (material_id) REFERENCES scp_material(id) ON UPDATE CASCADE,
	FOREIGN KEY (hoja_costo_id) REFERENCES scp_hoja_costo(id) ON UPDATE CASCADE
);
INSERT INTO scp_hoja_material_indirecto VALUES(1,17,20,1000,1);
INSERT INTO scp_hoja_material_indirecto VALUES(2,18,7,1000,1);
INSERT INTO scp_hoja_material_indirecto VALUES(3,19,1,1000,1);
INSERT INTO scp_hoja_material_indirecto VALUES(4,20,24,1000,1);
INSERT INTO scp_hoja_material_indirecto VALUES(5,17,20,1000,2);
INSERT INTO scp_hoja_material_indirecto VALUES(6,18,4,1000,2);
INSERT INTO scp_hoja_material_indirecto VALUES(7,19,1,1000,2);
INSERT INTO scp_hoja_material_indirecto VALUES(8,20,24,1000,2);

CREATE TABLE scp_cargos_mano_obra (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	cargo VARCHAR(254),
	salario INTEGER
);

CREATE TABLE scp_costos_admon (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	tipo_costo VARCHAR(254),
	costo_mes INTEGER
);

CREATE TABLE scp_maquinaria (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	tipo_bien VARCHAR(254),
	valor_comercial INTEGER,
	vida_util INTEGER
);

CREATE TABLE scp_hoja_mano_obra_indirecta (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	hoja_costo_id INTEGER,
	cargos_id INTEGER,
	salario_mes_historico INTEGER,
	total_salario_historico INTEGER,
	FOREIGN KEY (hoja_costo_id) REFERENCES scp_hoja_costo(id) ON UPDATE CASCADE,
	FOREIGN KEY (cargos_id) REFERENCES scp_cargos_mano_obra(id) ON UPDATE CASCADE
);
CREATE TABLE scp_hoja_costos_administrativos (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	hoja_costo_id INTEGER,
	costos_admin_id INTEGER,
	
	costo_mes_historico INTEGER,
	
	FOREIGN KEY (hoja_costo_id) REFERENCES scp_hoja_costo(id) ON UPDATE CASCADE,
	FOREIGN KEY (costos_admin_id) REFERENCES scp_costos_admon(id) ON UPDATE CASCADE
);
CREATE TABLE scp_hoja_depreciacion_maquinaria (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	hoja_costo_id INTEGER,
	bien_id INTEGER,
	
	cantidad INTEGER,
	valor_comercial_historico INTEGER,
	
	FOREIGN KEY (hoja_costo_id) REFERENCES scp_hoja_costo(id) ON UPDATE CASCADE,
	FOREIGN KEY (bien_id) REFERENCES scp_maquinaria(id) ON UPDATE CASCADE
);
