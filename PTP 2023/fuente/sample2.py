from tkinter import *
from customtkinter import *

# light | dark | system
set_appearance_mode("system")
#set_default_color_theme("green")
set_default_color_theme("./purple.json")
root = CTk()

button1 = CTkButton(master=root, text="Hola mundo!")
button1.place(relx=0.5, rely=0.5, anchor=CENTER)

root.mainloop()