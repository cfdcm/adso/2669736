from customtkinter import *
from tkinter import messagebox, PhotoImage
import tkinter as tk
from tkinter import ttk
import tk_tools
import sqlite3
from PIL import Image, ImageTk
from tkinter import filedialog
from tkinter.filedialog import askopenfile
from io import BytesIO
from functools import partial
BASE_DIR = os.path.abspath('./')


class App(CTk):
    def __init__(self):
        super().__init__()
        self.title("Simulador de Costos de Producción")
        self.resizable(True, True)

        w, h = self.winfo_screenwidth(), (670-150)    # self.winfo_screenheight()
        # w, h = self.winfo_screenwidth(), 670    # self.winfo_screenheight()
        self.geometry("%dx%d+-8+0" % (w, h))

        self.conexion = False
        #icon
        self.iconpath = PhotoImage(file = f"{BASE_DIR}/favicon_io/logo32.png")
        self.wm_iconbitmap()
        self.iconphoto(False, self.iconpath)

        # set grid layout 1x2
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(1, weight=1)

        # load images with light and dark mode image
        self.logo_image = CTkImage(Image.open(f"{BASE_DIR}/favicon_io/logo256.png"), size=(55, 55))
        self.logo_sena_image = CTkImage(Image.open(f"{BASE_DIR}/img/logoSena.png"), size=(180, 176))
        self.logo_sena_image_small = CTkImage(Image.open(f"{BASE_DIR}/img/logoSena.png"), size=(41, 40))

        self.inicio_image = CTkImage(light_image=Image.open(f"{BASE_DIR}/img/icons/house2.png"),
                                     dark_image=Image.open(f"{BASE_DIR}/img/icons/house2.png"), size=(40, 40))

        self.products_image = CTkImage(light_image=Image.open(f"{BASE_DIR}/img/icons/products.png"),
                                       dark_image=Image.open(f"{BASE_DIR}/img/icons/products.png"), size=(40, 40))

        self.materials_image = CTkImage(light_image=Image.open(f"{BASE_DIR}/img/icons/materials.png"),
                                        dark_image=Image.open(f"{BASE_DIR}/img/icons/materials.png"), size=(40, 40))

        self.cost_sheet_image = CTkImage(light_image=Image.open(f"{BASE_DIR}/img/icons/cost_sheet3.png"),
                                         dark_image=Image.open(f"{BASE_DIR}/img/icons/cost_sheet3.png"), size=(40, 40))

        self.benefits_image = CTkImage(light_image=Image.open(f"{BASE_DIR}/img/icons/benefits1.png"),
                                       dark_image=Image.open(f"{BASE_DIR}/img/icons/benefits1.png"), size=(40, 40))

        self.time_cost_image = CTkImage(light_image=Image.open(f"{BASE_DIR}/img/icons/time_cost.png"),
                                        dark_image=Image.open(f"{BASE_DIR}/img/icons/time_cost.png"), size=(40, 40))

        self.costo_corte_piezas_image = CTkImage(light_image=Image.open(f"{BASE_DIR}/img/icons/cutting1.png"),
                                                 dark_image=Image.open(f"{BASE_DIR}/img/icons/cutting1.png"),
                                                 size=(40, 40))

        self.consumo_hilo_image = CTkImage(light_image=Image.open(f"{BASE_DIR}/img/icons/thread3.png"),
                                           dark_image=Image.open(f"{BASE_DIR}/img/icons/thread3.png"), size=(40, 40))

        self.vm_formula1_image = CTkImage(light_image=Image.open(f"{BASE_DIR}/img/formula.png"),
                                          dark_image=Image.open(f"{BASE_DIR}/img/formula.png"), size=(308, 48))

        self.vm_formula2_image = CTkImage(light_image=Image.open(f"{BASE_DIR}/img/formula1.png"),
                                          dark_image=Image.open(f"{BASE_DIR}/img/formula1.png"), size=(222, 4))

        self.img_xlsx = CTkImage(light_image=Image.open(f"{BASE_DIR}/img/icons/xls-file64.png"),
                                 dark_image=Image.open(f"{BASE_DIR}/img/icons/xls-file64.png"), size=(32, 32))

        self.save_big = CTkImage(light_image=Image.open(f"{BASE_DIR}/img/icons/save_big.png"),
                                 dark_image=Image.open(f"{BASE_DIR}/img/icons/save_big.png"), size=(32, 32))

        self.exchange = CTkImage(light_image=Image.open(f"{BASE_DIR}/img/icons/exchange.png"),
                                 dark_image=Image.open(f"{BASE_DIR}/img/icons/exchange.png"), size=(32, 32))

        img_size = 20
        self.img_save = CTkImage(Image.open(f"{BASE_DIR}/img/icons/save.png").resize((img_size, img_size)))
        self.img_edit = CTkImage(Image.open(f"{BASE_DIR}/img/icons/edit.png").resize((img_size, img_size)))
        self.img_delete = CTkImage(Image.open(f"{BASE_DIR}/img/icons/delete.png").resize((img_size, img_size)))
        self.img_right_arrow = CTkImage(
            Image.open(f"{BASE_DIR}/img/icons/right-arrow.png").resize((img_size, img_size)))

        # create navigation frame
        self.navigation_frame = CTkFrame(self, corner_radius=10, width=200)
        self.navigation_frame.grid(row=0, column=0, sticky="nsew")
        self.navigation_frame.grid_rowconfigure(10, weight=1)

        self.navigation_frame_label = CTkLabel(self.navigation_frame, text="  Simulador de Costos\nde Producción",
                                               image=self.logo_image,
                                               compound="left", font=CTkFont(size=16, weight="bold"))
        self.navigation_frame_label.grid(row=0, column=0, padx=20, pady=20)

        self.home_button = CTkButton(self.navigation_frame, corner_radius=0, height=40, border_spacing=10,
                                     text="Inicio",
                                     fg_color="transparent", text_color=("gray10", "gray90"),
                                     hover_color=("gray70", "gray30"),
                                     image=self.inicio_image, anchor="w", command=self.home_button_event)
        self.home_button.grid(row=1, column=0, sticky="ew")

        self.productos_menu = CTkButton(self.navigation_frame, corner_radius=0, height=40, border_spacing=10,
                                        text="Productos",
                                        fg_color="transparent", text_color=("gray10", "gray90"),
                                        hover_color=("gray70", "gray30"),
                                        image=self.products_image, anchor="w", command=self.productos_menu_event)
        self.productos_menu.grid(row=2, column=0, sticky="ew")

        # create home frame
        self.home_frame = CTkFrame(self, corner_radius=0, fg_color="transparent")
        self.home_frame.grid_columnconfigure(0, weight=1)

        # Using treeview widget style
        style = ttk.Style()
        style.configure('Treeview', rowheight=20)  # increase height

        trv = ttk.Treeview(self.home_frame, selectmode='browse', height=3)
        trv.grid(row=1, column=1, rowspan=5, padx=30, pady=20)
        # number of columns
        trv["columns"] = ("1")
        # Defining heading
        trv['show'] = 'tree headings'
        # trv['show'] = 'tree'

        trv.column("#0", width=150, anchor='center')  # Width of column
        trv.column("1", width=130, anchor='center')
        # Headings
        # respective columns
        trv.heading("#0", text="Foto")  # Text of the column
        trv.heading("1", text="Name")

        img_size = 60

        img_old = Image.open(f"{BASE_DIR}/tmp/BUZO.JPEG")
        width, height = img_old.size
        width_new = img_size
        height_new = int((1 - ((width - img_size) / width)) * height)
        img_resized = img_old.resize((width_new, height_new))
        img1 = ImageTk.PhotoImage(img_resized)

        # img1 = ImageTk.PhotoImage(Image.open(f"{BASE_DIR}/tmp/opera.jpg").thumbnail(img_size) )
        img2 = ImageTk.PhotoImage(Image.open(f"{BASE_DIR}/tmp/chrome.PNG").resize((img_size, img_size)))
        img3 = ImageTk.PhotoImage(Image.open(f"{BASE_DIR}/tmp/firefox.png").resize((img_size, img_size)))

        for i in range(4):
            trv.insert("", END, image=img1, values=('na-Alex'))
        #trv.insert("", END, image=img1, values=('n1-Alex'))
        #trv.insert("", END, image=img2, values=('Child-Alex'))

        # Productos frame
        self.productos_frame = CTkFrame(self, corner_radius=0, fg_color="transparent")
        self.productos_frame.grid_columnconfigure(0, weight=1)

        self.productos_frame_logo_sena = CTkLabel(self.productos_frame,
                                                  text=" Servicio Nacional de Aprendizaje - CFDCM",
                                                  image=self.logo_sena_image_small, compound="left",
                                                  font=CTkFont(size=12, weight="normal"))
        self.productos_frame_logo_sena.grid(row=0, column=0, padx=0, pady=0, ipady=10, columnspan=3, sticky="nsew")

        # Contenedor módulo Frame
        self.p_frame = CTkScrollableFrame(self.productos_frame, corner_radius=10, height=390)
        self.p_frame.grid(row=1, column=0, padx=(10, 0), pady=0, ipady=10, columnspan=3, sticky="nsew")

        self.productos_frame_label = CTkLabel(self.p_frame, text=" Productos",
                                              image=self.products_image, compound="left", height=40,
                                              font=CTkFont(size=35, weight="bold"))
        self.productos_frame_label.grid(row=0, column=0, columnspan=3, padx=20, pady=10)

        self.p_frame.columnconfigure(0, weight=1)
        self.p_frame.columnconfigure(1, weight=1)
        self.p_frame.columnconfigure(2, weight=1)

        self.style = ttk.Style()
        self.style.configure("mystyle.Treeview.Heading", font=('Calibri', 13, 'bold'))
        self.style.configure('Treeview', rowheight=80)
        # Add a Treeview widget
        self.tree_productos = ttk.Treeview(self.p_frame, style="mystyle.Treeview",
                                           selectmode="browse", height=3)  # show='headings'
        self.tree_productos.grid(row=1, column=0, padx=(10, 0), columnspan=3, pady=5, sticky="nsew")

        self.tree_productos['columns'] = ('nombre', 'referencia', 'tiempo_estandar')

        self.tree_productos.column("#0", stretch=YES, minwidth=130, anchor=CENTER)
        self.tree_productos.column('nombre', stretch=YES, minwidth=150, anchor=CENTER)
        self.tree_productos.column('referencia', stretch=YES, minwidth=150, anchor=CENTER)
        self.tree_productos.column('tiempo_estandar', stretch=YES, minwidth=150, anchor=CENTER)

        self.tree_productos.heading("#0", text="Foto Producto")
        self.tree_productos.heading('nombre', text='Nombre')
        self.tree_productos.heading('referencia', text='Referencia')
        self.tree_productos.heading('tiempo_estandar', text='Tiempo Estándar')

        self.scroll = ttk.Scrollbar(self.p_frame, orient="vertical", command=self.tree_productos.yview)
        self.scroll.grid(row=1, column=6, padx=(0, 10), pady=5, sticky=NS)
        self.tree_productos.configure(yscrollcommand=self.scroll.set)

        self.p_frame_button1 = CTkButton(self.p_frame, text="Agregar Producto", image=self.img_save,
                                         fg_color="#0AA316",
                                         hover_color="#0A7E15",
                                         command=self.productos_guardar).grid(row=2, column=0, pady=(30, 10))
        self.p_frame_button2 = CTkButton(self.p_frame, text="Eliminar Producto", image=self.img_delete,
                                         fg_color="#B02900",
                                         hover_color="#992900",
                                         command=self.productos_eliminar).grid(row=2, column=1, pady=(30, 10))
        self.p_frame_button3 = CTkButton(self.p_frame, text="Editar Producto", image=self.img_edit,
                                         fg_color="#00719C",
                                         hover_color="#005C7D",
                                         command=self.productos_editar).grid(row=2, column=2, pady=(30, 10))

        self.home_button_event()
        self.select_frame_by_name("home")  # home
        self.mainloop()
    def select_frame_by_name(self, name):
        # set button color for selected button
        self.home_button.configure(fg_color=("gray75", "gray25") if name == "home" else "transparent")
        self.productos_menu.configure(fg_color=("gray75", "gray25") if name == "productos" else "transparent")

        # show selected frame
        if name == "home":
            self.home_frame.grid(row=0, column=1, sticky="nsew")
        else:
            self.home_frame.grid_forget()

        if name == "productos":
            self.productos_frame.grid(row=0, column=1, sticky="nsew")
        else:
            self.productos_frame.grid_forget()

    def home_button_event(self):
        self.select_frame_by_name("home")

    def productos_menu_event(self):
        self.select_frame_by_name("productos")
        import traceback
        try:
            self.abrir_conexion()
            self.conexion.row_factory = sqlite3.Row
            cursor = self.conexion.cursor()
            sql = "select * from scp_producto"
            cursor.execute(sql)
            registro = cursor.fetchall()

            # Clear the data in Treeview widget
            self.tree_productos.delete(*self.tree_productos.get_children())


            todos = []

            for r in registro:
                if r["foto"] is None:
                    todos.append("")
                else:
                    img_size = 60
                    foto = self.generar_foto_tmp(r['foto'], f"{BASE_DIR}/tmp/productos/{r['nombre']}.img")
                    img_old = Image.open(foto)
                    width, height = img_old.size
                    width_new = img_size
                    height_new = int((1 - ((width - img_size) / width)) * height)
                    img_resized = img_old.resize((width_new, height_new))
                    imagen_miniatura_tk = ImageTk.PhotoImage(img_resized)
                    todos.append(imagen_miniatura_tk)


            # Insert the data in Treeview widget
            cont = 0
            for r in registro:
                self.tree_productos.insert("", END, image=todos[cont], values=(r["nombre"], r["referencia"], r["tiempo_estandar"]))
                cont += 1

        except Exception as e:
            print(traceback.format_exc())
            messagebox.showerror(title="SCP", message=f"Error conectando a la base de datos.\n{e}\n{traceback.format_exc()}")

        finally:
            self.conexion.close()
            self.mainloop()

    def productos_guardar(self):
        # Crear una ventana secundaria.
        self.ventana_operaciones = CTkToplevel()
        self.ventana_operaciones.resizable(False, False)
        # quitar minimizar....
        self.ventana_operaciones.title("Agregar Producto")

        ancho_ventana = 400
        alto_ventana = 350
        x_ventana = self.winfo_screenwidth() // 2 - ancho_ventana // 2
        y_ventana = self.winfo_screenheight() // 2 - alto_ventana // 2

        posicion = str(ancho_ventana) + "x" + str(alto_ventana) + "+" + str(x_ventana) + "+" + str(y_ventana)
        self.ventana_operaciones.geometry(posicion)
        self.ventana_operaciones.focus()

        self.ventana_operaciones.columnconfigure(0, weight=1)
        self.ventana_operaciones.columnconfigure(1, weight=2)

        # GUI
        global filename, aux
        filename = ""
        aux = ""

        # Nombre
        CTkLabel(self.ventana_operaciones, text='Nombre: ').grid(row=1, column=0)
        self.operaciones_nombre = CTkEntry(self.ventana_operaciones)
        self.operaciones_nombre.focus()
        self.operaciones_nombre.grid(row=1, column=1, padx=10, pady=5, sticky="ew")

        # Referencia
        CTkLabel(self.ventana_operaciones, text='Referencia: ').grid(row=2, column=0)
        self.operaciones_referencia = CTkEntry(self.ventana_operaciones)
        self.operaciones_referencia.grid(row=2, column=1, padx=10, pady=5, sticky="ew")

        # Tiempo Estándar
        CTkLabel(self.ventana_operaciones, text='Tiempo Estándar: ').grid(row=3, column=0)
        self.operaciones_tiempo_e = CTkEntry(self.ventana_operaciones)
        self.operaciones_tiempo_e.grid(row=3, column=1, padx=10, pady=5, sticky="ew")

        # Foto
        CTkLabel(self.ventana_operaciones, text='Foto: ').grid(row=4, column=0)
        self.operaciones_foto = CTkButton(self.ventana_operaciones, text='Subir archivo', fg_color="#878787",
                                          hover_color="#6D6D6D", command=self.upload_file)
        self.operaciones_foto.grid(row=4, column=1, padx=10, pady=5, sticky="ew")

        self.operaciones_label_foto = CTkLabel(self.ventana_operaciones, wraplength=380, text="...", image=None,
                                               compound="top")
        self.operaciones_label_foto.grid(row=5, column=0, columnspan=2, pady=(5, 0))

        boton_guardar = CTkButton(self.ventana_operaciones, text="Guardar", command=self.add_product)
        boton_guardar.grid(row=6, column=0, columnspan=2, padx=10, pady=10, sticky="ew")
        # fin - GUI

        # Modal...
        self.ventana_operaciones.transient(self)  # dialog window is related to main
        self.ventana_operaciones.wait_visibility()
        self.ventana_operaciones.grab_set()
        self.ventana_operaciones.wait_window()

    def upload_file(self):
        global filename, aux
        # Image upload and display
        f_types = [('Png files', '*.png'), ('Jpg Files', '*.jpg'), ('Jpeg Files', '*.jpeg')]
        try:
            filename = filedialog.askopenfilename(filetypes=f_types)
            img = CTkImage(Image.open(filename), size=(110, 110))  # using Button
            self.operaciones_label_foto.configure(text=f"{filename.split('/')[-1]}", image=img)
            aux = filename
        except Exception as e:
            print("No seleccionó foto")

        # Mantener la última foto seleccionada o limpiar si se cierra la ventana de operaciones
        filename = aux if aux else ""

    def productos_eliminar(self):
        try:
            item = self.tree_productos.item(self.tree_productos.selection())['values']
            respuesta = messagebox.askokcancel(title=f"Confirmación",
                                               message=f"""Está seguro de eliminar el producto <<{item[1]}>>""")
            if respuesta:
                respuesta2 = messagebox.askretrycancel(title=f"Advertencia!!",
                                                       message=f"""Advertencia!!\n\nSi elimina este producto perderá los datos asociado como: Hoja de Costos y Consumo de Hilo.\n\nAún así desea eliminar el producto  <<{item[1]}>> y todos sus datos?""")
                if respuesta2:
                    import traceback
                    try:
                        # buscar hoja de costo del producto
                        query1 = 'SELECT * FROM scp_hoja_costo WHERE producto = ?'
                        registro1 = self.run_query(query1, (item[0],))
                        datos1 = registro1.fetchone()

                        # buscar id de material consumo hilo
                        query2 = f'SELECT * FROM scp_material WHERE nombre_tipo = "Hilo-{item[0]}.{item[1]}"'
                        print(query2)
                        registro2 = self.run_query(query2)
                        datos2 = registro2.fetchone()

                        # eliminar hoja_material por id de material e id de hoja_costo
                        query3 = 'DELETE FROM scp_hoja_material WHERE material_id = ? and hoja_costo_id = ?'
                        self.run_query(query3, (datos2[0], datos1[0]))

                        # eliminar material
                        query4 = 'DELETE FROM scp_material WHERE id = ?'
                        self.run_query(query4, (datos2[0],))

                        # eliminar hoja de costo
                        query5 = 'DELETE FROM scp_hoja_costo WHERE id = ?'
                        self.run_query(query5, (datos1[0],))

                        # eliminar registro de consumo_hilo por material_id (este dato es único)
                        query6 = 'DELETE FROM scp_consumo_hilo WHERE id_material = ?'
                        self.run_query(query6, (datos2[0],))

                        # finalmente eliminar el producto
                        query = 'DELETE FROM scp_producto WHERE id = ?'

                        self.run_query(query, (item[0],))
                        messagebox.showinfo(title="SCP", message=f"Producto '{item[1]}' eliminado correctamente!!")
                        # actualizamos listado en productos
                        self.productos_menu_event()
                    except Exception as e:
                        messagebox.showerror(title="SCP", message=f"Error:\n{e}\n{traceback.format_exc()}")
                    finally:
                        self.conexion.close()
        except IndexError as e:
            messagebox.showwarning(title=f"Error: ", message=f"""Seleccione un Producto...""")

    def productos_editar(self):
        try:
            item = self.tree_productos.item(self.tree_productos.selection())['values']

            query = 'SELECT * FROM scp_producto WHERE id = ?'
            parameters = (item[0],)
            registro = self.run_query(query, parameters)
            datos = registro.fetchone()

            # =================================================
            # Crear una ventana secundaria.
            self.ventana_operaciones = CTkToplevel()
            self.ventana_operaciones.resizable(False, False)
            # quitar minimizar....
            self.ventana_operaciones.title(f"Editar Producto: <<{datos[1]}>>")

            ancho_ventana = 400
            alto_ventana = 350
            x_ventana = self.winfo_screenwidth() // 2 - ancho_ventana // 2
            y_ventana = self.winfo_screenheight() // 2 - alto_ventana // 2

            posicion = str(ancho_ventana) + "x" + str(alto_ventana) + "+" + str(x_ventana) + "+" + str(y_ventana)
            self.ventana_operaciones.geometry(posicion)
            self.ventana_operaciones.focus()

            self.ventana_operaciones.columnconfigure(0, weight=1)
            self.ventana_operaciones.columnconfigure(1, weight=2)

            # GUI
            global filename, aux
            filename = ""
            aux = ""

            # ID
            self.operacionesid_label = CTkLabel(self.ventana_operaciones, text='Id: ')
            self.operacionesid_label.grid(row=0, column=0)
            self.operacionesid_label.grid_remove()

            self.operaciones_id = CTkEntry(self.ventana_operaciones)
            self.operaciones_id.grid(row=0, column=1, padx=10, pady=5, sticky="ew")
            self.operaciones_id.delete(0, END)
            self.operaciones_id.insert(0, datos[0])
            self.operaciones_id.configure(state='readonly', fg_color=('silver', '#c0c0c0'),
                                          text_color=('black', 'black'))
            self.operaciones_id.grid_remove()

            # Nombre
            CTkLabel(self.ventana_operaciones, text='Nombre: ').grid(row=1, column=0)
            self.operaciones_nombre = CTkEntry(self.ventana_operaciones)
            self.operaciones_nombre.focus()
            self.operaciones_nombre.grid(row=1, column=1, padx=10, pady=5, sticky="ew")
            self.operaciones_nombre.delete(0, END)
            self.operaciones_nombre.insert(0, datos[1])

            # Referencia
            CTkLabel(self.ventana_operaciones, text='Referencia: ').grid(row=2, column=0)
            self.operaciones_referencia = CTkEntry(self.ventana_operaciones)
            self.operaciones_referencia.grid(row=2, column=1, padx=10, pady=5, sticky="ew")
            self.operaciones_referencia.delete(0, END)
            self.operaciones_referencia.insert(0, datos[2])

            # Tiempo Estándar
            CTkLabel(self.ventana_operaciones, text='Tiempo Estándar: ').grid(row=3, column=0)
            self.operaciones_tiempo_e = CTkEntry(self.ventana_operaciones)
            self.operaciones_tiempo_e.grid(row=3, column=1, padx=10, pady=5, sticky="ew")
            self.operaciones_tiempo_e.delete(0, END)
            self.operaciones_tiempo_e.insert(0, datos[3])

            # Foto
            CTkLabel(self.ventana_operaciones, text='Foto: ').grid(row=4, column=0)
            self.operaciones_foto = CTkButton(self.ventana_operaciones, text='Subir archivo', fg_color="#878787",
                                              hover_color="#6D6D6D", command=self.upload_file)
            self.operaciones_foto.grid(row=4, column=1, padx=10, pady=5, sticky="ew")

            self.operaciones_label_foto = CTkLabel(self.ventana_operaciones, wraplength=380, text="...", image=None,
                                                   compound="top")
            self.operaciones_label_foto.grid(row=5, column=0, columnspan=2, pady=(5, 0))
            # cargar foto guardada
            if datos[4] is None:
                self.operaciones_label_foto.configure(text="Ninguna foto")
            else:
                img_byte = BytesIO(datos[4])
                img = ImageTk.PhotoImage(Image.open(img_byte).resize((110, 110)))
                self.operaciones_label_foto.configure(image=img, text="")

                # img = ImageTk.PhotoImage(data=datos[4])
                # self.operaciones_label_foto.configure(image=img).resize(110, 110)

            boton_guardar = CTkButton(self.ventana_operaciones, text="Actualizar", command=self.update_product)
            boton_guardar.grid(row=6, column=0, columnspan=2, padx=10, pady=20, sticky="ew")
            # fin - GUI

            # Modal...
            self.ventana_operaciones.transient(self)  # dialog window is related to main
            self.ventana_operaciones.wait_visibility()
            self.ventana_operaciones.grab_set()
            self.ventana_operaciones.wait_window()
            # =================================================

        except IndexError as e:
            messagebox.showwarning(title=f"Error: ", message=f"""Seleccione un Producto...""")

        # Function to Execute Database Querys

    def run_query(self, query, parameters=()):
        self.abrir_conexion()
        cursor = self.conexion.cursor()
        result = cursor.execute(query, parameters)
        self.conexion.commit()

        return result

    def add_product(self):
        global filename
        import traceback
        try:
            nombre = self.operaciones_nombre.get()
            referencia = self.operaciones_referencia.get()
            tiempo_e = self.operaciones_tiempo_e.get()
            if filename:
                fob = open(filename, 'rb')  # filename from upload_file()
                fob = fob.read()
            else:
                fob = None

            if len(nombre) != 0 and len(referencia) != 0 and len(tiempo_e) != 0:
                query = 'INSERT INTO scp_producto VALUES(null, ?, ?, ?, ?)'
                parameters = (nombre, referencia, tiempo_e, fob)
                res = self.run_query(query, parameters)
                # Capturar last insert ID
                id_producto = res.lastrowid

                # Crear material especial de Consumo de Hilo
                query2 = 'INSERT INTO scp_material VALUES(null, ?, ?, ?, ?, ?, ?)'
                parameters2 = (f"Hilo-{id_producto}.{nombre}", "Especial Consumo HILO", "mts", "0", "Hilo", None)
                res2 = self.run_query(query2, parameters2)
                id_material = res2.lastrowid

                # Crear hoja de costos para el producto
                query3 = 'INSERT INTO scp_hoja_costo VALUES(null, ?, ?, ?, ?, ?, ?, ?)'
                parameters3 = ("2014-01-01", "0", "0", "0", "0", "0", id_producto)
                res3 = self.run_query(query3, parameters3)
                id_hoja_costo = res3.lastrowid

                # Asocial material a Hoja de Costo del Producto
                query4 = 'INSERT INTO scp_hoja_material VALUES(NULL, ?, ?, ?, ?)'
                parameters4 = (id_material, "0", "0", id_hoja_costo)
                res4 = self.run_query(query4, parameters4)
                id_hoja_costo_material = res4.lastrowid

                messagebox.showinfo(title="SCP", message=f"Producto agregado correctamente!!")

                # Cerramos la ventana
                self.ventana_operaciones.destroy()
                # actualizamos listado en productos
                self.productos_menu_event()
            else:
                messagebox.showwarning(title="SCP", message=f"Por favor llene todos los datos..")
        except Exception as e:
            messagebox.showerror(title="SCP",
                                 message=f"Error guardando en base de datos.\n{e}\n{traceback.format_exc()}")
        finally:
            self.conexion.close()

    def update_product(self):
        global filename
        import traceback
        try:
            id = self.operaciones_id.get()
            nombre = self.operaciones_nombre.get()
            referencia = self.operaciones_referencia.get()
            tiempo_e = self.operaciones_tiempo_e.get()
            if filename:
                fob = open(filename, 'rb')  # filename from upload_file()
                fob = fob.read()
            else:
                fob = None

            if len(nombre) != 0 and len(referencia) != 0 and len(tiempo_e) != 0:
                query = 'UPDATE scp_producto SET nombre = ?, referencia = ?, tiempo_estandar = ?, foto = ? WHERE id = ? '
                parameters = (nombre, referencia, tiempo_e, fob, id)
                self.run_query(query, parameters)

                messagebox.showinfo(title="SCP", message=f"Producto '{id}' actualizado correctamente!!")

                # Cerramos la ventana
                self.ventana_operaciones.destroy()
                # actualizamos listado en productos
                self.productos_menu_event()
            else:
                messagebox.showwarning(title="SCP", message=f"Por favor llene todos los datos..")
        except Exception as e:
            messagebox.showerror(title="SCP",
                                 message=f"Error guardando en base de datos.\n{e}\n{traceback.format_exc()}")
        finally:
            self.conexion.close()

    def abrir_conexion(self):
        import traceback
        try:
            fullpath = os.path.join(BASE_DIR, "scp1.db")
            self.conexion = sqlite3.connect(fullpath)
        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error:\n{e}\n{traceback.format_exc()}\n{fullpath}")
        return self.conexion


    def generar_foto_tmp(self, data, file_name):
        with open(file_name, 'ab') as file:
            # Sobreescribir contenido binario archivo...
            file.truncate(0)
            file.write(data)
        img = Image.open(file_name)
        extension = img.format
        img.close()
        old_name = f"{file_name}"
        print(old_name)
        new_name = f"{file_name[:-4]}.{extension}"
        print(new_name)
        if os.path.exists(new_name):
            print("Ya existe el archivo")
            os.remove(new_name)
        else:
            print("No existe")
        os.rename(old_name, new_name)

        return new_name


if __name__ == "__main__":
    app = App()
    app.mainloop()
