BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "scp_config" (
	"id"	INTEGER NOT NULL,
	"param"	VARCHAR(150) NOT NULL UNIQUE,
	"value1"	VARCHAR(254) NOT NULL,
	"value2"	VARCHAR(254),
	"value3"	VARCHAR(254),
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "scp_producto" (
	"id"	INTEGER NOT NULL,
	"nombre"	VARCHAR(254) NOT NULL,
	"referencia"	VARCHAR(254) NOT NULL UNIQUE,
	"tiempo_estandar"	REAL,
	"foto"	BLOB,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "scp_material" (
	"id"	INTEGER NOT NULL,
	"nombre_tipo"	VARCHAR(254) NOT NULL,
	"proveedor"	VARCHAR(254),
	"unidad_med"	VARCHAR(50) NOT NULL,
	"costo_unitario"	REAL NOT NULL,
	"tipo"	VARCHAR,
	"foto"	BLOB,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "scp_hoja_material" (
	"id"	INTEGER,
	"material_id"	INTEGER,
	"consumo_unit"	INTEGER,
	"costo_unit_historico"	INTEGER,
	"hoja_costo_id"	INTEGER,
	PRIMARY KEY("id" AUTOINCREMENT),
	CONSTRAINT "FK_scp_hoja_material_scp_material" FOREIGN KEY("material_id") REFERENCES "scp_material"("id") ON UPDATE CASCADE,
	CONSTRAINT "scp_hoja_material_FK" FOREIGN KEY("hoja_costo_id") REFERENCES "scp_hoja_costo"("id") ON UPDATE CASCADE
);
CREATE TABLE IF NOT EXISTS "scp_hoja_material_indirecto" (
	"id"	INTEGER,
	"material_id"	INTEGER,
	"consumo_mes"	INTEGER,
	"costo_historico"	INTEGER,
	"hoja_costo_id"	INTEGER,
	PRIMARY KEY("id" AUTOINCREMENT),
	FOREIGN KEY("material_id") REFERENCES "scp_material"("id") ON UPDATE CASCADE,
	FOREIGN KEY("hoja_costo_id") REFERENCES "scp_hoja_costo"("id") ON UPDATE CASCADE
);
CREATE TABLE IF NOT EXISTS "scp_cargos_mano_obra" (
	"id"	INTEGER,
	"cargo"	VARCHAR(254),
	"salario"	INTEGER,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "scp_costos_admon" (
	"id"	INTEGER,
	"tipo_costo"	VARCHAR(254),
	"costo_mes"	INTEGER,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "scp_maquinaria" (
	"id"	INTEGER,
	"tipo_bien"	VARCHAR(254),
	"valor_comercial"	INTEGER,
	"vida_util"	INTEGER,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "scp_hoja_mano_obra_indirecta" (
	"id"	INTEGER,
	"hoja_costo_id"	INTEGER,
	"cargos_id"	INTEGER,
	"salario_mes_historico"	INTEGER,
	"total_salario_historico"	INTEGER,
	PRIMARY KEY("id" AUTOINCREMENT),
	FOREIGN KEY("hoja_costo_id") REFERENCES "scp_hoja_costo"("id") ON UPDATE CASCADE,
	FOREIGN KEY("cargos_id") REFERENCES "scp_cargos_mano_obra"("id") ON UPDATE CASCADE
);
CREATE TABLE IF NOT EXISTS "scp_hoja_costos_administrativos" (
	"id"	INTEGER,
	"hoja_costo_id"	INTEGER,
	"costos_admin_id"	INTEGER,
	"costo_mes_historico"	INTEGER,
	PRIMARY KEY("id" AUTOINCREMENT),
	FOREIGN KEY("costos_admin_id") REFERENCES "scp_costos_admon"("id") ON UPDATE CASCADE,
	FOREIGN KEY("hoja_costo_id") REFERENCES "scp_hoja_costo"("id") ON UPDATE CASCADE
);
CREATE TABLE IF NOT EXISTS "scp_hoja_depreciacion_maquinaria" (
	"id"	INTEGER,
	"hoja_costo_id"	INTEGER,
	"bien_id"	INTEGER,
	"cantidad"	INTEGER,
	"valor_comercial_historico"	INTEGER,
	PRIMARY KEY("id" AUTOINCREMENT),
	FOREIGN KEY("bien_id") REFERENCES "scp_maquinaria"("id") ON UPDATE CASCADE,
	FOREIGN KEY("hoja_costo_id") REFERENCES "scp_hoja_costo"("id") ON UPDATE CASCADE
);
CREATE TABLE IF NOT EXISTS "scp_consumo_hilo" (
	"id"	INTEGER NOT NULL,
	"id_material"	INTEGER NOT NULL UNIQUE,
	"tipo_tejido"	TEXT NOT NULL,
	"desperdicio"	REAL NOT NULL DEFAULT 0,
	"puntos_x_pulgada"	TEXT,
	"metros_costura"	TEXT,
	"metros_hilo"	TEXT,
	"total_hilo"	TEXT,
	"tipo_hilo"	TEXT,
	"color"	TEXT,
	"total_metros"	TEXT,
	"total_conos"	TEXT,
	"total_conos_redondeado"	TEXT,
	"total_global_hilo"	REAL,
	"calidad_hilo"	REAL,
	"costo_metro"	REAL,
	"costo_total"	REAL,
	PRIMARY KEY("id" AUTOINCREMENT),
	FOREIGN KEY("id_material") REFERENCES "scp_material"("id")
);
CREATE TABLE IF NOT EXISTS "scp_hoja_costo" (
	"id"	INTEGER,
	"fecha_creacion"	DATE,
	"eficiencia"	INTEGER,
	"jornada"	INTEGER,
	"operarios"	INTEGER,
	"minutos_mes"	INTEGER,
	"utilidad"	INTEGER,
	"producto"	INTEGER,
	PRIMARY KEY("id" AUTOINCREMENT),
	FOREIGN KEY("producto") REFERENCES "scp_producto"("id") ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE UNIQUE INDEX IF NOT EXISTS "producto_unique" ON "scp_hoja_costo" (
	"producto"
);
COMMIT;
