import os
from customtkinter import *
from tkinter import messagebox, PhotoImage
import tkinter as tk
from tkinter import ttk
import tk_tools
import sqlite3
from PIL import Image, ImageTk
from tkinter import filedialog
from tkinter.filedialog import askopenfile
from io import BytesIO

BASE_DIR = os.path.abspath('.')

class App(CTk):
    def __init__(self):
        super().__init__()

        self.geometry("1530x700+0+0")

        self.conexion = False
        # menu de plataforma macOS
        plattform = tk.Menu(self)
        app_menu = tk.Menu(plattform, name='apple')
        plattform.add_cascade(menu=app_menu)

        app_menu.add_command(label='Acerca de SCP', command=self.help_menu_about_event)
        app_menu.add_separator()
        self.config(menu=plattform)
        self.createcommand('tk::mac::ShowPreferences', self.help_menu_event)
        # self.createcommand('tk::mac::standardAboutPanel', self.help_menu_event)

        self.selected_option = tk.StringVar()
        self.hoja_costos_producto = tk.StringVar()
        self.valor_minuto_producto = tk.StringVar()
        self.consumo_hilo_producto = tk.StringVar()
        self.consumo_hilo_tipo_tejido = StringVar()

        self.carga_opt1 = tk.StringVar()
        self.carga_opt2 = tk.StringVar()
        self.salud_opt3 = tk.StringVar()

        self.porcentajes_carga_prestacional = {}
        self.material_id_consumo_hilo = None
        # fin - menu

        # menu
        # menubar = tk.Menu(self)

        file_menu = tk.Menu(plattform, tearoff=0)
        file_menu.add_command(label="Exportar datos...")
        file_menu.add_command(label="Importar datos")
        file_menu.add_separator()
        file_menu.add_command(label="Hoja de Costos", command=self.hoja_costos_menu_event)
        file_menu.add_separator()
        file_menu.add_command(label="Salir", command=self.quit)

        edit_menu = tk.Menu(plattform, tearoff=0)
        edit_menu.add_command(label="Productos", command=self.productos_menu_event)
        edit_menu.add_command(label="Materiales", command=self.materiales_menu_event)
        edit_menu.add_command(label="Consumo de Hilo", command=self.consumo_hilo_menu_event)

        conf_menu = tk.Menu(plattform, tearoff=0)
        conf_menu.add_radiobutton(label="Tema Claro", variable=self.selected_option, value="Light",
                                  command=self.change_appearance_mode_event)
        conf_menu.add_radiobutton(label="Tema Oscuro", variable=self.selected_option, value="Dark",
                                  command=self.change_appearance_mode_event)
        conf_menu.add_radiobutton(label="Tema Sistema", variable=self.selected_option, value="System",
                                  command=self.change_appearance_mode_event)
        conf_menu.add_separator()
        conf_menu.add_command(label="Carga Prestacional", command=self.carga_prestacional_event)
        conf_menu.add_command(label="Porcentajes Carga Prestacional", command=self.carga_prestacional_event)

        help_menu = tk.Menu(plattform, tearoff=0)
        help_menu.add_command(label="Ayuda", accelerator="Command+A", command=self.help_menu_event)
        help_menu.add_separator()
        help_menu.add_command(label="Acerca de...", accelerator="Command-Shift-a", command=self.help_menu_about_event)
        # self.bind_all("<Command-x>", self.help_menu_about_event)

        plattform.add_cascade(label="Archivo", menu=file_menu)
        plattform.add_cascade(label="Editar", menu=edit_menu)
        plattform.add_cascade(label="Configuración", menu=conf_menu)
        plattform.add_cascade(label="Ayuda", menu=help_menu)

        # self.config(menu=menubar)

        # fin - menu

        # set grid layout 1x2
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(1, weight=1)

        # load images with light and dark mode image
        self.logo_image = CTkImage(Image.open(f"{BASE_DIR}/favicon_io/logo256.png"), size=(65, 65))
        self.logo_sena_image = CTkImage(Image.open(f"{BASE_DIR}/img/logoSena.png"), size=(180, 176))
        self.logo_sena_image_small = CTkImage(Image.open(f"{BASE_DIR}/img/logoSena.png"), size=(41, 40))

        self.inicio_image = CTkImage(light_image=Image.open(f"{BASE_DIR}/img/icons/house2.png"),
                                   dark_image=Image.open(f"{BASE_DIR}/img/icons/house2.png"), size=(40, 40))

        self.products_image = CTkImage(light_image=Image.open(f"{BASE_DIR}/img/icons/products.png"),
                                   dark_image=Image.open(f"{BASE_DIR}/img/icons/products.png"), size=(40, 40))

        self.materials_image = CTkImage(light_image=Image.open(f"{BASE_DIR}/img/icons/materials.png"),
                                   dark_image=Image.open(f"{BASE_DIR}/img/icons/materials.png"), size=(40, 40))

        self.cost_sheet_image = CTkImage(light_image=Image.open(f"{BASE_DIR}/img/icons/cost_sheet3.png"),
                                   dark_image=Image.open(f"{BASE_DIR}/img/icons/cost_sheet3.png"), size=(40, 40))

        self.benefits_image = CTkImage(light_image=Image.open(f"{BASE_DIR}/img/icons/benefits1.png"),
                                   dark_image=Image.open(f"{BASE_DIR}/img/icons/benefits1.png"), size=(40, 40))

        self.time_cost_image = CTkImage(light_image=Image.open(f"{BASE_DIR}/img/icons/time_cost.png"),
                                   dark_image=Image.open(f"{BASE_DIR}/img/icons/time_cost.png"), size=(40, 40))
        
        self.costo_corte_piezas_image = CTkImage(light_image=Image.open(f"{BASE_DIR}/img/icons/cutting1.png"),
                                   dark_image=Image.open(f"{BASE_DIR}/img/icons/cutting1.png"), size=(40, 40))

        self.consumo_hilo_image = CTkImage(light_image=Image.open(f"{BASE_DIR}/img/icons/thread3.png"),
                                   dark_image=Image.open(f"{BASE_DIR}/img/icons/thread3.png"), size=(40, 40))
        
        self.vm_formula1_image = CTkImage(light_image=Image.open(f"{BASE_DIR}/img/formula.png"),
                                   dark_image=Image.open(f"{BASE_DIR}/img/formula.png"), size=(308, 48))
        
        self.vm_formula2_image = CTkImage(light_image=Image.open(f"{BASE_DIR}/img/formula1.png"),
                                   dark_image=Image.open(f"{BASE_DIR}/img/formula1.png"), size=(222, 4))
        
        self.img_xlsx = CTkImage(light_image=Image.open(f"{BASE_DIR}/img/icons/xls-file64.png"),
                                   dark_image=Image.open(f"{BASE_DIR}/img/icons/xls-file64.png"), size=(32, 32))
        
        img_size = 20
        self.img_save = CTkImage(Image.open(f"{BASE_DIR}/img/icons/save.png").resize((img_size, img_size)))
        self.img_edit = CTkImage(Image.open(f"{BASE_DIR}/img/icons/edit.png").resize((img_size, img_size)))
        self.img_delete = CTkImage(Image.open(f"{BASE_DIR}/img/icons/delete.png").resize((img_size, img_size)))

        # create navigation frame
        self.navigation_frame = CTkFrame(self, corner_radius=10, width=200)
        self.navigation_frame.grid(row=0, column=0, sticky="nsew")
        self.navigation_frame.grid_rowconfigure(10, weight=1)

        self.navigation_frame_label = CTkLabel(self.navigation_frame, text="  Simulador de Costos\nde Producción", image=self.logo_image,
                                                             compound="left", font=CTkFont(size=18, weight="bold"))
        self.navigation_frame_label.grid(row=0, column=0, padx=20, pady=20)

        self.home_button = CTkButton(self.navigation_frame, corner_radius=0, height=40, border_spacing=10, text="Inicio",
                                                   fg_color="transparent", text_color=("gray10", "gray90"), hover_color=("gray70", "gray30"),
                                                   image=self.inicio_image, anchor="w", command=self.home_button_event)
        self.home_button.grid(row=1, column=0, sticky="ew")

        self.productos_menu = CTkButton(self.navigation_frame, corner_radius=0, height=40, border_spacing=10,
                                        text="Productos",
                                        fg_color="transparent", text_color=("gray10", "gray90"),
                                        hover_color=("gray70", "gray30"),
                                        image=self.products_image, anchor="w", command=self.productos_menu_event)
        self.productos_menu.grid(row=2, column=0, sticky="ew")

        self.materiales_menu = CTkButton(self.navigation_frame, corner_radius=0, height=40, border_spacing=10,
                                         text="Materiales",
                                         fg_color="transparent", text_color=("gray10", "gray90"),
                                         hover_color=("gray70", "gray30"),
                                         image=self.materials_image, anchor="w", command=self.materiales_menu_event)
        self.materiales_menu.grid(row=3, column=0, sticky="ew")

        self.consumo_hilo_menu = CTkButton(self.navigation_frame, corner_radius=0, height=40, border_spacing=10,
                                                 text="Consumo de Hilo",
                                                 fg_color="transparent", text_color=("gray10", "gray90"),
                                                 hover_color=("gray70", "gray30"),
                                                 image=self.consumo_hilo_image, anchor="w",
                                                 command=self.consumo_hilo_menu_event)
        self.consumo_hilo_menu.grid(row=4, column=0, sticky="ew")

        self.hoja_costos_menu = CTkButton(self.navigation_frame, corner_radius=0, height=40, border_spacing=10,
                                        text="Hoja de Costos",
                                        fg_color="transparent", text_color=("gray10", "gray90"),
                                        hover_color=("gray70", "gray30"),
                                        image=self.cost_sheet_image, anchor="w", command=self.hoja_costos_menu_event)
        self.hoja_costos_menu.grid(row=5, column=0, sticky="ew")

        self.tab1_COSTO_TOTAL = 0
        self.tab1_PRECIO_VENTA = 0
        self.tab1_operarios = 0
        self.tab1_eficiencia = 0

        self.valor_minuto_menu = CTkButton(self.navigation_frame, corner_radius=0, height=40, border_spacing=10,
                                        text="Valor minuto",
                                        fg_color="transparent", text_color=("gray10", "gray90"),
                                        hover_color=("gray70", "gray30"),
                                        image=self.time_cost_image, anchor="w", command=self.valor_minuto_menu_event)
        self.valor_minuto_menu.grid(row=6, column=0, sticky="ew")



        self.conf_label = CTkLabel(self.navigation_frame, height=40, text="Herramientas:", font=CTkFont(size=14, weight="bold"))
        self.conf_label.grid(row=8, column=0, sticky="w", padx=10, pady=(20, 0))

        self.costo_corte_piezas_menu = CTkButton(self.navigation_frame, corner_radius=0, height=40, border_spacing=10,
                                                 text="Costos para Corte de Piezas",
                                                 fg_color="transparent", text_color=("gray10", "gray90"),
                                                 hover_color=("gray70", "gray30"),
                                                 image=self.costo_corte_piezas_image, anchor="w",
                                                 command=self.costo_corte_piezas_menu_event)
        self.costo_corte_piezas_menu.grid(row=9, column=0, sticky="ew")

        self.salario_min = 0
        self.aux_transporte = 0
        self.prima_servicios = 0
        self.aux_cesantias = 0
        self.intereses_cesantias = 0
        self.vacaciones = 0
        self.caja_compensacion = 0
        self.pension = 0
        self.arl = 0
        self.salud = 0
        self.dotacion = 0
        self.parafiscales = 0
        self.salud_porcentaje = 0
        self.dotacion_porcentaje = 0
        self.parafiscales_porcentaje = 0
        self.total_costo_mensual_trabajador = 0

        self.version = CTkLabel(self.navigation_frame, text="Colombia Versión 1.0, 2024")
        self.version.grid(row=10, column=0, padx=20, pady=20, sticky="s")

        # create home frame
        self.home_frame = CTkFrame(self, corner_radius=0, fg_color="transparent")
        self.home_frame.grid_columnconfigure(0, weight=1)

        self.home_frame_large_image_label = CTkLabel(self.home_frame,
                                                     text="\nProducto Técnico-Pedagógico:\n\nSIMULADOR DE COSTOS DE "
                                                          "PRODUCCIÓN\nPARA LA CONFECCIÓN DE PRENDAS DE "
                                                          "VESTIR\n\n\nSENA - Centro de Formación en Diseño, "
                                                          "Confección y Moda\n\nColombia - 2023",
                                                     image=self.logo_sena_image, compound="top",
                                                     font=CTkFont(size=15, weight="bold"))
        self.home_frame_large_image_label.place(relx=0.5, rely=0.5, anchor=CENTER)

        # Productos frame
        self.productos_frame = CTkFrame(self, corner_radius=0, fg_color="transparent")
        self.productos_frame.grid_columnconfigure(0, weight=1)

        self.productos_frame_logo_sena = CTkLabel(self.productos_frame,
                                                     text=" Servicio Nacional de Aprendizaje - CFDCM",
                                                     image=self.logo_sena_image_small, compound="left",
                                                     font=CTkFont(size=12, weight="normal"))
        self.productos_frame_logo_sena.grid(row=0, column=0, padx=0, pady=0, ipady=10, columnspan=3, sticky="nsew")

        # Contenedor módulo Frame
        self.p_frame = CTkScrollableFrame(self.productos_frame, corner_radius=10, height=600)
        self.p_frame.grid(row=1, column=0, padx=(10, 0), pady=0, ipady=10, columnspan=3, sticky="nsew")

        self.productos_frame_label = CTkLabel(self.p_frame, text=" Productos",
                                              image=self.products_image, compound="left", height=40,
                                              font=CTkFont(size=35, weight="bold"))
        self.productos_frame_label.grid(row=0, column=0, columnspan=3, padx=20, pady=10)

        self.p_frame.columnconfigure(0, weight=1)
        self.p_frame.columnconfigure(1, weight=1)
        self.p_frame.columnconfigure(2, weight=1)

        self.style = ttk.Style()
        self.style.configure("mystyle.Treeview.Heading", font=('Calibri', 14, 'bold'))
        self.style.configure('Treeview', rowheight=50)
        # Add a Treeview widget
        self.tree_productos = ttk.Treeview(self.p_frame, style="mystyle.Treeview", selectmode="browse") # show='headings'

        self.tree_productos['columns'] = ('id', 'nombre', 'referencia', 'tiempo_estandar', 'foto')

        self.tree_productos.column('id', stretch=NO, minwidth=20, width=50, anchor=CENTER)
        self.tree_productos.column('nombre', stretch=YES, minwidth=150, anchor=CENTER)
        self.tree_productos.column('referencia', stretch=YES, minwidth=150, anchor=CENTER)
        self.tree_productos.column('tiempo_estandar', stretch=YES, minwidth=150, anchor="e")
        self.tree_productos.column('foto', stretch=YES, minwidth=150, anchor=CENTER)
        self.tree_productos.column("#0", width=130)

        self.tree_productos.heading("#0", text="Foto Producto")
        self.tree_productos.heading('id', text='ID')
        self.tree_productos.heading('nombre', text='Nombre')
        self.tree_productos.heading('referencia', text='Referencia')
        self.tree_productos.heading('tiempo_estandar', text='Tiempo Estándar')
        self.tree_productos.heading('foto', text='Foto')

        self.tree_productos.bind("<Double-1>", self.OnDoubleClick)
        self.tree_productos.grid(row=1, column=0, padx=(10,0), columnspan=3, pady=5, sticky="nsew")

        self.scroll = ttk.Scrollbar(self.p_frame, orient="vertical", command=self.tree_productos.yview)
        self.scroll.grid(row=1, column=6, padx=(0,10), pady=5, sticky=NS)
        self.tree_productos.configure(yscrollcommand=self.scroll.set)


        self.p_frame_button1 = CTkButton(self.p_frame, text="Agregar Producto", image=self.img_save,
                                         fg_color="#0AA316",
                                         hover_color="#0A7E15",
                                         command=self.productos_guardar).grid(row=2, column=0, pady=(30, 10))
        self.p_frame_button2 = CTkButton(self.p_frame, text="Eliminar Producto", image=self.img_delete,
                                         fg_color="#B02900",
                                         hover_color="#992900",
                                         command=self.productos_eliminar).grid(row=2, column=1, pady=(30, 10))
        self.p_frame_button3 = CTkButton(self.p_frame, text="Editar Producto", image=self.img_edit,
                                         fg_color="#00719C",
                                         hover_color="#005C7D",
                                         command=self.productos_editar).grid(row=2, column=2, pady=(30, 10))

        # Materiales Frame Nuevo
        self.materiales_frame = CTkFrame(self, corner_radius=0, fg_color="transparent")
        self.materiales_frame.grid_columnconfigure(0, weight=1)

        self.productos_frame_logo_sena = CTkLabel(self.materiales_frame,
                                                  text=" Servicio Nacional de Aprendizaje - CFDCM",
                                                  image=self.logo_sena_image_small, compound="left",
                                                  font=CTkFont(size=12, weight="normal"))
        self.productos_frame_logo_sena.grid(row=0, column=0, padx=0, pady=0, ipady=10, columnspan=3, sticky="nsew")

        # Contenedor módulo Frame
        self.m_frame = CTkScrollableFrame(self.materiales_frame, corner_radius=10, height=600)
        self.m_frame.grid(row=1, column=0, padx=(10, 0), pady=0, ipady=10, columnspan=3, sticky="nsew")

        self.materiales_frame_label = CTkLabel(self.m_frame, text=" Materiales",
                                              image=self.materials_image, compound="left", height=40,
                                              font=CTkFont(size=35, weight="bold"))
        self.materiales_frame_label.grid(row=0, column=0, columnspan=3, padx=20, pady=10)

        self.m_frame.columnconfigure(0, weight=1)
        self.m_frame.columnconfigure(1, weight=1)
        self.m_frame.columnconfigure(2, weight=1)

        self.style = ttk.Style()
        self.style.configure("mystyle.Treeview.Heading", font=('Calibri', 14, 'bold'))
        self.style.configure('Treeview', rowheight=40)
        # Add a Treeview widget
        self.tree_materiales = ttk.Treeview(self.m_frame, style="mystyle.Treeview", selectmode="browse", show='headings', height=13)

        self.tree_materiales['columns'] = ('id', 'nombre_tipo', 'proveedor', 'unidad_med', 'costo_unitario', 'tipo')

        self.tree_materiales.column('id', stretch=NO, minwidth=20, width=50, anchor=CENTER)
        self.tree_materiales.column('nombre_tipo', stretch=YES, minwidth=150, anchor=CENTER)
        self.tree_materiales.column('proveedor', stretch=YES, minwidth=150, anchor=CENTER)
        self.tree_materiales.column('unidad_med', stretch=YES, minwidth=150, anchor=CENTER)
        self.tree_materiales.column('costo_unitario', stretch=YES, minwidth=150, anchor="e")
        self.tree_materiales.column('tipo', stretch=YES, minwidth=150, anchor=CENTER)

        self.tree_materiales.heading('id', text='ID')
        self.tree_materiales.heading('nombre_tipo', text='Nombre Tipo')
        self.tree_materiales.heading('proveedor', text='Proveedor')
        self.tree_materiales.heading('unidad_med', text='Unidad de Medida')
        self.tree_materiales.heading('costo_unitario', text='Costo Unitario')
        self.tree_materiales.heading('tipo', text='Tipo de Material')

        self.tree_materiales.grid(row=1, column=0, padx=(10,0), columnspan=3, pady=5, sticky="nsew")

        self.scroll = ttk.Scrollbar(self.m_frame, orient="vertical", command=self.tree_materiales.yview)
        self.scroll.grid(row=1, column=6, padx=(0,10), pady=5, sticky=NS)
        self.tree_materiales.configure(yscrollcommand=self.scroll.set)


        self.m_frame_button1 = CTkButton(self.m_frame, text="Agregar Material", image=self.img_save,
                                         fg_color="#0AA316",
                                         hover_color="#0A7E15",
                                         command=self.materiales_guardar).grid(row=2, column=0, pady=(30, 10))

        self.m_frame_button2 = CTkButton(self.m_frame, text="Eliminar Material", image=self.img_delete,
                                         fg_color="#B02900",
                                         hover_color="#992900",
                                         command=self.materiales_eliminar).grid(row=2, column=1, pady=(30, 10))
        self.m_frame_button3 = CTkButton(self.m_frame, text="Editar Material", image=self.img_edit,
                                         fg_color="#00719C",
                                         hover_color="#005C7D",
                                         command=self.materiales_editar).grid(row=2, column=2, pady=(30, 10))

        # Fin - Materiales Frame Nuevo

        # Hoja de Costos
        self.third_frame = CTkFrame(self, corner_radius=0, fg_color="transparent")
        self.third_frame.grid_columnconfigure(0, weight=1)

        self.productos_frame_logo_sena = CTkLabel(self.third_frame,
                                                  text=" Servicio Nacional de Aprendizaje - CFDCM",
                                                  image=self.logo_sena_image_small, compound="left",
                                                  font=CTkFont(size=12, weight="normal"))
        self.productos_frame_logo_sena.grid(row=0, column=0, padx=0, pady=0, ipady=10, sticky="nsew")

        # Contenedor módulo Frame
        self.hc_frame = CTkScrollableFrame(self.third_frame, corner_radius=10, height=600)
        self.hc_frame.grid(row=1, column=0, padx=(10, 0), pady=0, ipady=10, sticky="nsew")

        self.hoja_costos_frame_label = CTkLabel(self.hc_frame, text=" Hoja de Costos",
                                              image=self.cost_sheet_image, compound="left", height=40,
                                              font=CTkFont(size=35, weight="bold"))
        self.hoja_costos_frame_label.grid(row=0, column=0, padx=20, pady=(0, 5))

        self.hc_frame.columnconfigure(0, weight=1)

        self.style = ttk.Style()
        self.style.configure("mystyle.Treeview.Heading", font=('Calibri', 14, 'bold'))

        # create tabview
        self.tabview = CTkTabview(self.hc_frame, width=1240, fg_color="#D7D7D7")
        self.tabview.grid(row=1, column=0, padx=(0, 0), pady=(0, 0), sticky="nsew")

        tab1 = "RESUMEN"
        tab2 = "MATERIALES DIRECTOS"
        tab3 = "MANO DE OBRA DIRECTA"
        tab4 = "INDIRECTOS DE FABRICACIÓN"
        tab5 = "MANO DE OBRA INDIRECTA"
        tab6 = "COSTOS ADMINISTRATIVOS"
        tab7 = "DEPRECIACIÓN MAQUINARIA"

        self.tabview.add(tab1)
        self.tabview.add(tab2)
        self.tabview.add(tab3)
        self.tabview.add(tab4)
        self.tabview.add(tab5)
        self.tabview.add(tab6)
        self.tabview.add(tab7)

        # configure grid of individual tabs

        # tab1
        self.tabview.tab(tab1).grid_columnconfigure(0, weight=1, uniform=True)
        self.tabview.tab(tab1).grid_columnconfigure(1, weight=1, uniform=True)
        self.tabview.tab(tab1).grid_columnconfigure(2, weight=1, uniform=True)
        self.tabview.tab(tab1).grid_columnconfigure(3, weight=1, uniform=True)
        # tab2
        self.tabview.tab(tab2).grid_columnconfigure(0, weight=1, uniform=True)
        # tab3
        self.tabview.tab(tab3).grid_columnconfigure(0, weight=1, uniform=True)
        self.tabview.tab(tab3).grid_columnconfigure(1, weight=1, uniform=True)
        self.tabview.tab(tab3).grid_columnconfigure(2, weight=1, uniform=True)
        self.tabview.tab(tab3).grid_columnconfigure(3, weight=1, uniform=True)
        self.tabview.tab(tab3).grid_columnconfigure(4, weight=1, uniform=True)
        self.tabview.tab(tab3).grid_columnconfigure(5, weight=1, uniform=True)
        # tab4
        self.tabview.tab(tab4).grid_columnconfigure(0, weight=1, uniform=True)
        self.tabview.tab(tab4).grid_columnconfigure(1, weight=1, uniform=True)
        # tab5
        self.tabview.tab(tab5).grid_columnconfigure(0, weight=1, uniform=True)
        self.tabview.tab(tab5).grid_columnconfigure(1, weight=1, uniform=True)
        self.tabview.tab(tab5).grid_columnconfigure(2, weight=1, uniform=True)
        # tab6
        self.tabview.tab(tab6).grid_columnconfigure(0, weight=1, uniform=True)
        self.tabview.tab(tab6).grid_columnconfigure(1, weight=1, uniform=True)
        # tab7
        self.tabview.tab(tab7).grid_columnconfigure(0, weight=1, uniform=True)


        # tab1 ===========================

        productos = ["Seleccione el Producto"]
        self.hoja_costos_producto = StringVar(value="Seleccione el Producto")
        try:
            self.abrir_conexion()
            self.conexion.row_factory = sqlite3.Row
            cursor = self.conexion.cursor()
            sql = "select * from scp_producto"
            cursor.execute(sql)
            registro = cursor.fetchall()
            # Construir listado para combobox
            for r in registro:
                productos.append(f"{r['id']}.{r['nombre']}")
        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error conectando a la base de datos.\n{e}")
        finally:
            self.conexion.close()

        self.tab1_producto = CTkOptionMenu(self.tabview.tab(tab1), dynamic_resizing=True, values=productos,
                                           command=self.optionmenu_callback,
                                           variable=self.hoja_costos_producto)
        self.tab1_producto.grid(row=0, column=1, columnspan="2", padx=10, pady=10)

        self.p_frame_button2_xlsx = CTkButton(self.tabview.tab(tab1), text=" Exportar", image=self.img_xlsx,
                                         fg_color="white", text_color=("gray10", "gray90"), hover_color="gray90", compound="left",
                                         command=self.hoja_costo_exportar).grid(row=0, column=2, columnspan="2", padx=10, pady=10)

        self.tab1_fecha = CTkLabel(self.tabview.tab(tab1), text="FECHA:")
        self.tab1_fecha.grid(row=1, column=0, padx=20, pady=20)
        self.tab1_fecha_valor = CTkLabel(self.tabview.tab(tab1), text="---", fg_color="#c0c0c0", width=200)
        self.tab1_fecha_valor.grid(row=1, column=1, padx=20, pady=20)

        self.tab1_ref = CTkLabel(self.tabview.tab(tab1), text="REF:")
        self.tab1_ref.grid(row=1, column=2, padx=20, pady=20)
        self.tab1_ref_valor = CTkLabel(self.tabview.tab(tab1), text="---", fg_color="#c0c0c0", width=200)
        self.tab1_ref_valor.grid(row=1, column=3, padx=20, pady=20)

        self.tab1_tiempoE = CTkLabel(self.tabview.tab(tab1), text="Tiempo Estándar:")
        self.tab1_tiempoE.grid(row=2, column=0, padx=20, pady=20)
        self.tab1_tiempoE_valor = CTkLabel(self.tabview.tab(tab1), text="---", fg_color="#c0c0c0", width=200)
        self.tab1_tiempoE_valor.grid(row=2, column=1, padx=20, pady=20)

        self.tab1_eficiencia = CTkLabel(self.tabview.tab(tab1), text="Eficiencia:")
        self.tab1_eficiencia.grid(row=2, column=2, padx=20, pady=20)
        self.tab1_eficiencia_valor = CTkLabel(self.tabview.tab(tab1), text="---", fg_color="#c0c0c0", width=200)
        self.tab1_eficiencia_valor.grid(row=2, column=3, padx=20, pady=20)

        self.tab1_jornadaL = CTkLabel(self.tabview.tab(tab1), text="Jornada Laboral:")
        self.tab1_jornadaL.grid(row=3, column=0, padx=20, pady=20)
        self.tab1_jornadaL_valor = CTkLabel(self.tabview.tab(tab1), text="---", fg_color="#c0c0c0", width=200)
        self.tab1_jornadaL_valor.grid(row=3, column=1, padx=20, pady=20)

        self.tab1_num_operarios = CTkLabel(self.tabview.tab(tab1), text="N° Operarios:")
        self.tab1_num_operarios.grid(row=3, column=2, padx=20, pady=20)
        self.tab1_num_operarios_valor = CTkLabel(self.tabview.tab(tab1), text="---", fg_color="#c0c0c0", width=200)
        self.tab1_num_operarios_valor.grid(row=3, column=3, padx=20, pady=20)

        self.tab1_min_mes = CTkLabel(self.tabview.tab(tab1), text="Minutos/Mes:")
        self.tab1_min_mes.grid(row=4, column=0, padx=20, pady=20)
        self.tab1_min_mes_valor = CTkLabel(self.tabview.tab(tab1), text="---", fg_color="#c0c0c0", width=200)
        self.tab1_min_mes_valor.grid(row=4, column=1, padx=20, pady=20)

        self.tab1_utilidad = CTkLabel(self.tabview.tab(tab1), text="UTILIDAD (%):")
        self.tab1_utilidad.grid(row=4, column=2, padx=20, pady=20)
        self.tab1_utilidad_valor = CTkLabel(self.tabview.tab(tab1), text="---", fg_color="#c0c0c0", width=200)
        self.tab1_utilidad_valor.grid(row=4, column=3, padx=20, pady=20)

        self.tab1_u_prod_mes = CTkLabel(self.tabview.tab(tab1), text="*** Unid a Prod/mes: ****")
        self.tab1_u_prod_mes.grid(row=5, column=0, padx=20, pady=20)
        self.tab1_u_prod_mes_valor = CTkLabel(self.tabview.tab(tab1), text="---", fg_color="#6acee2", width=200)
        self.tab1_u_prod_mes_valor.grid(row=5, column=1, padx=20, pady=20)

        self.tab1_precio_venta = CTkLabel(self.tabview.tab(tab1), text="*** PRECIO DE VENTA: ****")
        self.tab1_precio_venta.grid(row=5, column=2, padx=20, pady=20)
        self.tab1_precio_venta_valor = CTkLabel(self.tabview.tab(tab1), text="---", fg_color="#6acee2", width=200)
        self.tab1_precio_venta_valor.grid(row=5, column=3, padx=20, pady=20)
        # sección tab1
        self.tab1_resumen_costo = CTkLabel(self.tabview.tab(tab1), text="== RESUMEN DEL COSTO ==")
        self.tab1_resumen_costo.grid(row=6, column=0, columnspan="4", padx=20, pady=20)
        # fin sección

        self.tab1_mat_directo = CTkLabel(self.tabview.tab(tab1), text="*** MATERIALES.DIRECTOS: ****")
        self.tab1_mat_directo.grid(row=7, column=0, padx=20, pady=20)
        self.tab1_mat_directo_valor = CTkLabel(self.tabview.tab(tab1), text="---", fg_color="#6acee2", width=200)
        self.tab1_mat_directo_valor.grid(row=7, column=1, padx=20, pady=20)

        self.tab1_MOD = CTkLabel(self.tabview.tab(tab1), text="*** M.O.D: ****")
        self.tab1_MOD.grid(row=7, column=2, padx=20, pady=20)
        self.tab1_MOD_valor = CTkLabel(self.tabview.tab(tab1), text="---", fg_color="#6acee2", width=200)
        self.tab1_MOD_valor.grid(row=7, column=3, padx=20, pady=20)

        self.tab1_CIF = CTkLabel(self.tabview.tab(tab1), text="*** CIF: ****")
        self.tab1_CIF.grid(row=8, column=0, padx=20, pady=20)
        self.tab1_CIF_valor = CTkLabel(self.tabview.tab(tab1), text="---", fg_color="#6acee2", width=200)
        self.tab1_CIF_valor.grid(row=8, column=1, padx=20, pady=20)
        self.tab1_CIF_total_acumulado = 0

        self.tab1_COSTO_TOTAL = CTkLabel(self.tabview.tab(tab1), text="*** COSTO TOTAL: ****")
        self.tab1_COSTO_TOTAL.grid(row=8, column=2, padx=20, pady=20)
        self.tab1_COSTO_TOTAL_valor = CTkLabel(self.tabview.tab(tab1), text="---", fg_color="#6acee2", width=200)
        self.tab1_COSTO_TOTAL_valor.grid(row=8, column=3, padx=20, pady=20)

        # tab2 ===========================

        # self.tab2_titul1 = CTkLabel(self.tabview.tab(tab2), text="Producto:")
        # self.tab2_titul1.grid(row=0, column=0, padx=20, pady=20)

        # Add a Treeview widget
        self.tree_materiales_directos = ttk.Treeview(self.tabview.tab(tab2), style="mystyle.Treeview", selectmode="extended", show='headings', height=13)

        self.tree_materiales_directos['columns'] = ('id', 'nombre_tipo', 'proveedor', 'unidad_med', 'consumo_unit', 'costo_unitario', 'costo_total')

        self.tree_materiales_directos.column('id', stretch=NO, minwidth=20, width=30, anchor=CENTER)
        self.tree_materiales_directos.column('nombre_tipo', stretch=YES, minwidth=50, anchor=CENTER)
        self.tree_materiales_directos.column('proveedor', stretch=YES, minwidth=50, anchor=CENTER)
        self.tree_materiales_directos.column('unidad_med', stretch=YES, minwidth=50, width=120, anchor=CENTER)
        self.tree_materiales_directos.column('consumo_unit', stretch=YES, minwidth=50, anchor=CENTER)
        self.tree_materiales_directos.column('costo_unitario', stretch=YES, minwidth=50, anchor="e")
        self.tree_materiales_directos.column('costo_total', stretch=YES, minwidth=50, anchor="e")

        self.tree_materiales_directos.heading('id', text='ID')
        self.tree_materiales_directos.heading('nombre_tipo', text='TIPO DE MATERIAL')
        self.tree_materiales_directos.heading('proveedor', text='PROVEEDOR')
        self.tree_materiales_directos.heading('unidad_med', text='UNIDAD MEDIDA')
        self.tree_materiales_directos.heading('consumo_unit', text='CONSUMO UNIT')
        self.tree_materiales_directos.heading('costo_unitario', text='COSTO UNITARIO')
        self.tree_materiales_directos.heading('costo_total', text='COSTO TOTAL')

        self.tree_materiales_directos.grid(row=1, column=0, padx=(10, 0), columnspan=1, pady=5, sticky="nsew")
        # self.tree_materiales_directos.grid(row=1, columnspan=6, padx=20, pady=20)

        self.tab2_total = CTkLabel(self.tabview.tab(tab2), text="TOTALES:", fg_color="#6acee2", width=200)
        self.tab2_total.grid(row=2, column=0, padx=20, pady=20)

        # tab3 ===========================

        self.tab3_titul1 = CTkLabel(self.tabview.tab(tab3), text="COSTO MANO DE OBRA DIRECTA")
        self.tab3_titul1.grid(row=0, column=0, columnspan="6", padx=10, pady=10)

        self.tab3_sal_basico = CTkLabel(self.tabview.tab(tab3), text="SAL BASICO:")
        self.tab3_sal_basico.grid(row=1, column=0, padx=20, pady=20)
        self.tab3_sal_basico_valor = CTkLabel(self.tabview.tab(tab3), text="---", fg_color="#c0c0c0", width=150)
        self.tab3_sal_basico_valor.grid(row=1, column=1, padx=20, pady=20)

        self.tab3_carga_pres = CTkLabel(self.tabview.tab(tab3), text="CARGA PREST:")
        self.tab3_carga_pres.grid(row=1, column=2, padx=20, pady=20)
        self.tab3_carga_pres_valor = CTkLabel(self.tabview.tab(tab3), text="---", fg_color="#c0c0c0", width=150)
        self.tab3_carga_pres_valor.grid(row=1, column=3, padx=20, pady=20)

        self.tab3_total_sal = CTkLabel(self.tabview.tab(tab3), text="TOTAL SALARIO:")
        self.tab3_total_sal.grid(row=1, column=4, padx=20, pady=20)
        self.tab3_total_sal_valor = CTkLabel(self.tabview.tab(tab3), text="---", fg_color="#c0c0c0", width=150)
        self.tab3_total_sal_valor.grid(row=1, column=5, padx=20, pady=20)

        self.tab3_sub_transp = CTkLabel(self.tabview.tab(tab3), text="SUB TRANSP:")
        self.tab3_sub_transp.grid(row=2, column=0, padx=20, pady=20)
        self.tab3_sub_transp_valor = CTkLabel(self.tabview.tab(tab3), text="---", fg_color="#c0c0c0", width=150)
        self.tab3_sub_transp_valor.grid(row=2, column=1, padx=20, pady=20)

        self.tab3_otros = CTkLabel(self.tabview.tab(tab3), text="OTROS:")
        self.tab3_otros.grid(row=2, column=2, padx=20, pady=20)
        self.tab3_otros_valor = CTkLabel(self.tabview.tab(tab3), text="---", fg_color="#b7de78", width=150)
        self.tab3_otros_valor.grid(row=2, column=3, padx=20, pady=20)

        self.tab3_total_sal_otros = CTkLabel(self.tabview.tab(tab3), text="TOTAL SALARIO:")
        self.tab3_total_sal_otros.grid(row=2, column=4, padx=20, pady=20)
        self.tab3_total_sal_otros_valor = CTkLabel(self.tabview.tab(tab3), text="---", fg_color="#b7de78", width=150)
        self.tab3_total_sal_otros_valor.grid(row=2, column=5, padx=20, pady=20)

        self.tab3_costo_mod = CTkLabel(self.tabview.tab(tab3), text="COSTO TOTAL M.O.D:")
        self.tab3_costo_mod.grid(row=3, column=0, columnspan="2",  padx=20, pady=20)
        self.tab3_costo_mod_valor = CTkLabel(self.tabview.tab(tab3), text="---", fg_color="#c0c0c0", width=150)
        self.tab3_costo_mod_valor.grid(row=3, column=2, padx=20, pady=20)

        self.tab3_min_mes = CTkLabel(self.tabview.tab(tab3), text="MINUTOS EFECTIVOS/MES:")
        self.tab3_min_mes.grid(row=3, column=3, columnspan="2", padx=20, pady=20)
        self.tab3_min_mes_valor = CTkLabel(self.tabview.tab(tab3), text="---", fg_color="#c0c0c0", width=150)
        self.tab3_min_mes_valor.grid(row=3, column=5, padx=20, pady=20)

        self.tab3_fact_min = CTkLabel(self.tabview.tab(tab3), text="FACTOR MINUTO:")
        self.tab3_fact_min.grid(row=4, column=0, columnspan="2",  padx=20, pady=20)
        self.tab3_fact_min_valor = CTkLabel(self.tabview.tab(tab3), text="---", fg_color="#c0c0c0", width=150)
        self.tab3_fact_min_valor.grid(row=4, column=2, padx=20, pady=20)

        self.tab3_unitario_mod = CTkLabel(self.tabview.tab(tab3), text="COSTO TOTAL UNITARIO M.O.D:")
        self.tab3_unitario_mod.grid(row=4, column=3, columnspan="2", padx=20, pady=20)
        self.tab3_unitario_mod_valor = CTkLabel(self.tabview.tab(tab3), text="---", fg_color="#6acee2", width=150)
        self.tab3_unitario_mod_valor.grid(row=4, column=5, padx=20, pady=20)

        # tab4 ===========================

        self.tab4_titul1 = CTkLabel(self.tabview.tab(tab4), text="COSTOS INDIRECTOS DE FABRICACION\nCOSTO MATERIALES INDIRECTOS:")
        self.tab4_titul1.grid(row=0, column=0, columnspan="6", padx=20, pady=20)

        # Add a Treeview widget
        self.tree_materiales_indirectos = ttk.Treeview(self.tabview.tab(tab4), style="mystyle.Treeview", selectmode="extended")

        self.tree_materiales_indirectos['columns'] = ('id', 'nombre_tipo', 'unidad_med', 'consumo_mes', 'costo_unitario', 'costo_total', 'costo_asignado')
        self.tree_materiales_indirectos.column('#0', stretch=NO, width=0, anchor=CENTER)
        self.tree_materiales_indirectos.column('#1', stretch=NO, minwidth=100, width=50, anchor=CENTER)
        self.tree_materiales_indirectos.column('#2', stretch=NO, minwidth=100, width=200, anchor=CENTER)
        self.tree_materiales_indirectos.column('#3', stretch=NO, minwidth=100, width=100, anchor=CENTER)
        self.tree_materiales_indirectos.column('#4', stretch=NO, minwidth=100, width=150, anchor=CENTER)
        self.tree_materiales_indirectos.column('#5', stretch=NO, minwidth=100, width=200, anchor="e")
        self.tree_materiales_indirectos.column('#6', stretch=NO, minwidth=100, width=200, anchor="e")
        self.tree_materiales_indirectos.column('#7', stretch=NO, minwidth=100, width=200, anchor="e")

        self.tree_materiales_indirectos.heading('id', text='ID', anchor=CENTER)
        self.tree_materiales_indirectos.heading('nombre_tipo', text='TIPO DE MATERIAL', anchor=CENTER)
        self.tree_materiales_indirectos.heading('unidad_med', text='UNIDAD MEDIDA', anchor=CENTER)
        self.tree_materiales_indirectos.heading('consumo_mes', text='CONSUMO/MES', anchor=CENTER)
        self.tree_materiales_indirectos.heading('costo_unitario', text='COSTO', anchor=CENTER)
        self.tree_materiales_indirectos.heading('costo_total', text='COSTO TOTAL', anchor=CENTER)
        self.tree_materiales_indirectos.heading('costo_asignado', text='COSTO ASIGNADO', anchor=CENTER)
        self.tree_materiales_indirectos.grid(row=1, column=0,  columnspan=6, padx=20, pady=20)

        self.tab4_total1 = CTkLabel(self.tabview.tab(tab4), text="TOTALES:", fg_color="#6acee2", width=300)
        self.tab4_total1.grid(row=2, column=0, columnspan="2", padx=20, pady=20)
        self.tab4_total2 = CTkLabel(self.tabview.tab(tab4), text="TOTALES ASIGNADO:", fg_color="#6acee2", width=400)
        self.tab4_total2.grid(row=2, column=2, columnspan="3", padx=20, pady=20)

        # tab5 ===========================

        self.tab5_titul1 = CTkLabel(self.tabview.tab(tab5), text="COSTOS MANO DE OBRA INDIRECTA:")
        self.tab5_titul1.grid(row=0, column=0, columnspan="3", padx=20, pady=20)

        # Add a Treeview widget
        self.tree_mano_obra_indirecta = ttk.Treeview(self.tabview.tab(tab5), style="mystyle.Treeview", selectmode="extended")

        self.tree_mano_obra_indirecta['columns'] = ('id', 'cargo', 'salario_mes_historico', 'total_salario_historico', 'costo_asignado')
        self.tree_mano_obra_indirecta.column('#0', stretch=NO, width=0, anchor=CENTER)
        self.tree_mano_obra_indirecta.column('#1', stretch=NO, minwidth=100, width=50, anchor=CENTER)
        self.tree_mano_obra_indirecta.column('#2', stretch=NO, minwidth=100, width=200, anchor=CENTER)
        self.tree_mano_obra_indirecta.column('#3', stretch=NO, minwidth=100, width=200, anchor="e")
        self.tree_mano_obra_indirecta.column('#4', stretch=NO, minwidth=100, width=200, anchor="e")
        self.tree_mano_obra_indirecta.column('#5', stretch=NO, minwidth=100, width=200, anchor="e")

        self.tree_mano_obra_indirecta.heading('id', text='ID', anchor=CENTER)
        self.tree_mano_obra_indirecta.heading('cargo', text='CARGO', anchor=CENTER)
        self.tree_mano_obra_indirecta.heading('salario_mes_historico', text='SALARIO/MES', anchor=CENTER)
        self.tree_mano_obra_indirecta.heading('total_salario_historico', text='TOTAL SALARIO', anchor=CENTER)
        self.tree_mano_obra_indirecta.heading('costo_asignado', text='COSTO ASIGNADO', anchor=CENTER)
        self.tree_mano_obra_indirecta.grid(row=1, column=0,  columnspan=3, padx=20, pady=20)

        self.tab5_total1 = CTkLabel(self.tabview.tab(tab5), text="TOTALES SALARIO/MES:", fg_color="#6acee2", width=300)
        self.tab5_total1.grid(row=2, column=0, padx=20, pady=20)
        self.tab5_total2 = CTkLabel(self.tabview.tab(tab5), text="TOTALES SALARIO:", fg_color="#6acee2", width=300)
        self.tab5_total2.grid(row=2, column=1, padx=20, pady=20)
        self.tab5_total3 = CTkLabel(self.tabview.tab(tab5), text="TOTALES COSTO ASIGNADO:", fg_color="#6acee2", width=300)
        self.tab5_total3.grid(row=2, column=2, padx=20, pady=20)

        # tab6 ===========================

        self.tab6_titul1 = CTkLabel(self.tabview.tab(tab6), text="COSTOS ADMINISTRATIVOS:")
        self.tab6_titul1.grid(row=0, column=0, columnspan="2", padx=20, pady=20)

        # Add a Treeview widget
        self.tree_costos_administrativos = ttk.Treeview(self.tabview.tab(tab6), style="mystyle.Treeview", selectmode="extended")

        self.tree_costos_administrativos['columns'] = ('id', 'tipo_costo', 'costo_mes_historico', 'costo_asignado')
        self.tree_costos_administrativos.column('#0', stretch=NO, width=0, anchor=CENTER)
        self.tree_costos_administrativos.column('#1', stretch=NO, minwidth=100, width=50, anchor=CENTER)
        self.tree_costos_administrativos.column('#2', stretch=NO, minwidth=100, width=200, anchor=CENTER)
        self.tree_costos_administrativos.column('#3', stretch=NO, minwidth=100, width=300, anchor="e")
        self.tree_costos_administrativos.column('#4', stretch=NO, minwidth=100, width=300, anchor="e")

        self.tree_costos_administrativos.heading('id', text='ID', anchor=CENTER)
        self.tree_costos_administrativos.heading('tipo_costo', text='TIPO DE COSTO', anchor=CENTER)
        self.tree_costos_administrativos.heading('costo_mes_historico', text='COSTO TOTAL/MES', anchor=CENTER)
        self.tree_costos_administrativos.heading('costo_asignado', text='COSTO ASIGNADO', anchor=CENTER)
        self.tree_costos_administrativos.grid(row=1, column=0,  columnspan=2, padx=20, pady=20)

        self.tab6_total1 = CTkLabel(self.tabview.tab(tab6), text="TOTALES COSTO TOTAL/MES:", fg_color="#6acee2", width=300)
        self.tab6_total1.grid(row=2, column=0, padx=20, pady=20)
        self.tab6_total2 = CTkLabel(self.tabview.tab(tab6), text="TOTALES COSTO ASIGNADO:", fg_color="#6acee2", width=300)
        self.tab6_total2.grid(row=2, column=1, padx=20, pady=20)

        # tab7 ===========================

        self.tab7_titul1 = CTkLabel(self.tabview.tab(tab7), text="DEPRECIACIÓN MAQUINARIA:")
        self.tab7_titul1.grid(row=0, column=0, padx=20, pady=20)

        # Add a Treeview widget
        self.tree_deprecia_maquinaria = ttk.Treeview(self.tabview.tab(tab7), style="mystyle.Treeview", selectmode="extended")

        self.tree_deprecia_maquinaria['columns'] = ('id', 'tipo_bien', 'cantidad', 'valor_comercial_historico', 'valor_total', 'vida_util', 'deprecia_mes')
        self.tree_deprecia_maquinaria.column('#0', stretch=NO, width=0, anchor=CENTER)
        self.tree_deprecia_maquinaria.column('#1', stretch=NO, minwidth=100, width=50, anchor=CENTER)
        self.tree_deprecia_maquinaria.column('#2', stretch=NO, minwidth=100, width=200, anchor=CENTER)
        self.tree_deprecia_maquinaria.column('#3', stretch=NO, minwidth=100, width=150, anchor=CENTER)
        self.tree_deprecia_maquinaria.column('#4', stretch=NO, minwidth=100, width=200, anchor="e")
        self.tree_deprecia_maquinaria.column('#5', stretch=NO, minwidth=100, width=200, anchor="e")
        self.tree_deprecia_maquinaria.column('#6', stretch=NO, minwidth=100, width=150, anchor=CENTER)
        self.tree_deprecia_maquinaria.column('#7', stretch=NO, minwidth=100, width=200, anchor="e")

        self.tree_deprecia_maquinaria.heading('id', text='ID', anchor=CENTER)
        self.tree_deprecia_maquinaria.heading('tipo_bien', text='TIPO BIEN', anchor=CENTER)
        self.tree_deprecia_maquinaria.heading('cantidad', text='CANTIDAD', anchor=CENTER)
        self.tree_deprecia_maquinaria.heading('valor_comercial_historico', text='VALOR COMERCIAL', anchor=CENTER)
        self.tree_deprecia_maquinaria.heading('valor_total', text='VALOR TOTAL', anchor=CENTER)
        self.tree_deprecia_maquinaria.heading('vida_util', text='VIDA UTIL', anchor=CENTER)
        self.tree_deprecia_maquinaria.heading('deprecia_mes', text='DEPRECIACIÓN/MES', anchor=CENTER)
        self.tree_deprecia_maquinaria.grid(row=1, column=0, padx=20, pady=20)

        self.tab7_total1 = CTkLabel(self.tabview.tab(tab7), text="TOTAL DEPRECIACIÓN/MES:", fg_color="#6acee2", width=300)
        self.tab7_total1.grid(row=2, column=0, padx=20, pady=20)
        self.tab7_total2 = CTkLabel(self.tabview.tab(tab7), text="COSTO ASIGNADO:", fg_color="#6acee2", width=300)
        self.tab7_total2.grid(row=3, column=0, padx=20, pady=20)

        # Fin - Hoja de Costos

        # Carga Prestacional frame
        self.carga_prestacional_frame = CTkFrame(self, corner_radius=0, fg_color="transparent")
        self.carga_prestacional_frame.grid_columnconfigure(0, weight=1)

        self.productos_frame_logo_sena = CTkLabel(self.carga_prestacional_frame,
                                                  text=" Servicio Nacional de Aprendizaje - CFDCM",
                                                  image=self.logo_sena_image_small, compound="left",
                                                  font=CTkFont(size=12, weight="normal"))
        self.productos_frame_logo_sena.grid(row=0, column=0, padx=0, pady=0, ipady=10, columnspan=2, sticky="nsew")

        # Contenedor módulo Frame
        self.cp_frame = CTkScrollableFrame(self.carga_prestacional_frame, corner_radius=10, height=600)
        self.cp_frame.grid(row=1, column=0, padx=(10, 0), pady=0, ipady=10, columnspan=2, sticky="nsew")

        self.carga_prestacional_frame_label = CTkLabel(self.cp_frame, text=" Carga Prestacional",
                                              image=self.benefits_image, compound="left", height=40,
                                              font=CTkFont(size=35, weight="bold"))
        self.carga_prestacional_frame_label.grid(row=0, column=0, columnspan=2, padx=20, pady=10)

        self.cp_frame.columnconfigure(0, weight=1)
        self.cp_frame.columnconfigure(1, weight=1)

        self.frame_col1 = CTkFrame(self.cp_frame)
        self.frame_col1.grid(row=1, column=0, sticky="e", padx=10, pady=10)

        self.frame_col2 = CTkFrame(self.cp_frame)
        self.frame_col2.grid(row=1, column=1, sticky="w", padx=10, pady=10)

        self.frame_row1 = CTkFrame(self.cp_frame)
        self.frame_row1.grid(row=2, column=0, sticky="n", columnspan=2, padx=10, pady=10)

        # ============ frame_col1 ============

        self.frame_col1_label1 = CTkLabel(self.frame_col1, text="Salario mímino", fg_color="transparent")
        self.frame_col1_label1.grid(row=0, column=0, padx=10, pady=10, sticky="w")
        self.frame_col1_entry1 = CTkEntry(self.frame_col1, placeholder_text="salario_min", justify="right")
        self.frame_col1_entry1.grid(row=0, column=1, padx=10, pady=10)

        self.frame_col1_label3 = CTkLabel(self.frame_col1, text="Prima de Servicios", fg_color="transparent")
        self.frame_col1_label3.grid(row=1, column=0, padx=10, pady=10, sticky="w")
        self.frame_col1_entry3 = CTkEntry(self.frame_col1, placeholder_text="prima_servicios", justify="right")
        self.frame_col1_entry3.grid(row=1, column=1, padx=10, pady=10)

        self.frame_col1_label5 = CTkLabel(self.frame_col1, text="Intereses de Cesantías", fg_color="transparent")
        self.frame_col1_label5.grid(row=2, column=0, padx=10, pady=10, sticky="w")
        self.frame_col1_entry5 = CTkEntry(self.frame_col1, placeholder_text="intereses_cesantias", justify="right")
        self.frame_col1_entry5.grid(row=2, column=1, padx=10, pady=10)

        self.frame_col1_label7 = CTkLabel(self.frame_col1, text="Caja de Compensación", fg_color="transparent")
        self.frame_col1_label7.grid(row=3, column=0, padx=10, pady=10, sticky="w")
        self.frame_col1_entry7 = CTkEntry(self.frame_col1, placeholder_text="caja_compensacion", justify="right")
        self.frame_col1_entry7.grid(row=3, column=1, padx=10, pady=10)

        self.frame_col1_label9 = CTkLabel(self.frame_col1, text="Arl", fg_color="transparent")
        self.frame_col1_label9.grid(row=4, column=0, padx=10, pady=10, sticky="w")
        self.frame_col1_entry9 = CTkEntry(self.frame_col1, placeholder_text="arl", justify="right")
        self.frame_col1_entry9.grid(row=4, column=1, padx=10, pady=10)

        self.frame_col1_carga_opt1 = CTkCheckBox(master=self.frame_col1, text="SENA + ICBF",
                                                 command=self.carga_opt1_event,
                                                 variable=self.carga_opt1, onvalue="on", offvalue="off")
        self.frame_col1_carga_opt1.grid(row=5, column=0, padx=10, pady=10, sticky="w")
        self.frame_col1_entry91 = CTkEntry(self.frame_col1, placeholder_text="parafiscales", justify="right")
        self.frame_col1_entry91.grid(row=5, column=1, padx=10, pady=10)

        # ============ frame_col2 ============

        self.frame_col2_label2 = CTkLabel(self.frame_col2, text="Subsidio de Transporte", fg_color="transparent")
        self.frame_col2_label2.grid(row=0, column=0, padx=10, pady=10, sticky="w")
        self.frame_col2_entry2 = CTkEntry(self.frame_col2, placeholder_text="aux_transporte", justify="right")
        self.frame_col2_entry2.grid(row=0, column=1, padx=10, pady=10)

        self.frame_col2_label4 = CTkLabel(self.frame_col2, text="Cesantías", fg_color="transparent")
        self.frame_col2_label4.grid(row=1, column=0, padx=10, pady=10, sticky="w")
        self.frame_col2_entry4 = CTkEntry(self.frame_col2, placeholder_text="aux_cesantias", justify="right")
        self.frame_col2_entry4.grid(row=1, column=1, padx=10, pady=10)

        self.frame_col2_label6 = CTkLabel(self.frame_col2, text="Vacaciones", fg_color="transparent")
        self.frame_col2_label6.grid(row=2, column=0, padx=10, pady=10, sticky="w")
        self.frame_col2_entry6 = CTkEntry(self.frame_col2, placeholder_text="vacaciones", justify="right")
        self.frame_col2_entry6.grid(row=2, column=1, padx=10, pady=10)

        self.frame_col2_label8 = CTkLabel(self.frame_col2, text="Pensión", fg_color="transparent")
        self.frame_col2_label8.grid(row=3, column=0, padx=10, pady=10, sticky="w")
        self.frame_col2_entry8 = CTkEntry(self.frame_col2, placeholder_text="pension", justify="right")
        self.frame_col2_entry8.grid(row=3, column=1, padx=10, pady=10)

        self.frame_col2_label10 = CTkCheckBox(master=self.frame_col2, text="Salud", command=self.salud_opt3_event,
                                                 variable=self.salud_opt3, onvalue="on", offvalue="off")
        self.frame_col2_label10.grid(row=4, column=0, padx=10, pady=10, sticky="w")
        self.frame_col2_entry10 = CTkEntry(self.frame_col2, placeholder_text="salud", justify="right")
        self.frame_col2_entry10.grid(row=4, column=1, padx=10, pady=10)

        self.frame_col2_carga_opt2 = CTkCheckBox(master=self.frame_col2, text="Dotación", command=self.carga_opt2_event,
                                             variable=self.carga_opt2, onvalue="on", offvalue="off")
        self.frame_col2_carga_opt2.grid(row=5, column=0, padx=10, pady=10, sticky="w")
        self.frame_col2_entry101 = CTkEntry(self.frame_col2, placeholder_text="dotacion", justify="right")
        self.frame_col2_entry101.grid(row=5, column=1, padx=10, pady=10)

        # ============ Zona Botones ============

        self.frame_row1_label11 = CTkLabel(self.frame_row1, text="Total Costo Mensual Trabajador",
                                           fg_color="transparent")
        self.frame_row1_label11.grid(row=0, column=0, padx=10, pady=10, sticky="w")
        self.frame_row1_entry11 = CTkEntry(self.frame_row1, placeholder_text="total_costo_mensual_trabajador",
                                           justify="right")
        self.frame_row1_entry11.grid(row=0, column=1, padx=10, pady=10)

        self.b1utton = CTkButton(self.carga_prestacional_frame, text="Guardar",
                                 command=self.guardar_carga_prestacional_event)
        self.b1utton.grid(row=2, column=0, pady=10)

        # Valor minuto frame
        self.valor_minuto_frame = CTkFrame(self, corner_radius=0, fg_color="transparent")
        self.valor_minuto_frame.grid_columnconfigure(0, weight=1)

        self.productos_frame_logo_sena = CTkLabel(self.valor_minuto_frame,
                                                  text=" Servicio Nacional de Aprendizaje - CFDCM",
                                                  image=self.logo_sena_image_small, compound="left",
                                                  font=CTkFont(size=12, weight="normal"))
        self.productos_frame_logo_sena.grid(row=0, column=0, padx=0, pady=0, ipady=10, columnspan=2, sticky="nsew")

        # Contenedor módulo Frame
        self.vm_frame = CTkScrollableFrame(self.valor_minuto_frame, corner_radius=10, height=600)
        self.vm_frame.grid(row=1, column=0, padx=(10, 0), pady=0, ipady=10, columnspan=2, sticky="nsew")

        self.valor_minuto_frame_label = CTkLabel(self.vm_frame, text=" Valor minuto",
                                              image=self.time_cost_image, compound="left", height=40,
                                              font=CTkFont(size=35, weight="bold"))
        self.valor_minuto_frame_label.grid(row=0, column=0, columnspan=2, padx=20, pady=10)

        self.vm_frame.columnconfigure(0, weight=1)
        self.vm_frame.columnconfigure(1, weight=1)

        productos = ["Seleccione el Producto"]
        self.valor_minuto_producto = StringVar(value="Seleccione el Producto")
        try:
            self.abrir_conexion()
            self.conexion.row_factory = sqlite3.Row
            cursor = self.conexion.cursor()
            sql = "select * from scp_producto"
            cursor.execute(sql)
            registro = cursor.fetchall()
            # Construir listado para combobox
            for r in registro:
                productos.append(f"{r['id']}.{r['nombre']}")
        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error conectando a la base de datos.\n{e}")
        finally:
            self.conexion.close()

        self.vm_button_producto = CTkOptionMenu(self.vm_frame, dynamic_resizing=True, values=productos,
                                           command=self.valor_minuto_seleccionar_producto,
                                           variable=self.valor_minuto_producto)
        self.vm_button_producto.grid(row=1, column=0, columnspan=2, padx=10, pady=10)

        self.vm_frame_col1 = CTkFrame(self.vm_frame, fg_color="gray85")
        self.vm_frame_col1.grid(row=2, column=0, sticky="ens", padx=10, pady=10)

        self.vm_frame_col2 = CTkFrame(self.vm_frame, fg_color="gray85")
        self.vm_frame_col2.grid(row=2, column=1, sticky="wns", padx=10, pady=10)

        self.vm_frame_row1 = CTkFrame(self.vm_frame)
        self.vm_frame_row1.grid(row=3, column=0, sticky="n", columnspan=2, padx=10, pady=10)

        # ============ vm_frame_col1 ============

        self.vm_frame_col1_label1 = CTkLabel(self.vm_frame_col1, text="Precio de Venta", fg_color="transparent")
        self.vm_frame_col1_label1.grid(row=0, column=0, padx=10, pady=10, sticky="w")
        self.vm_frame_col1_entry1 = CTkEntry(self.vm_frame_col1, placeholder_text="precio_venta", justify="right")
        self.vm_frame_col1_entry1.grid(row=0, column=1, padx=10, pady=10)
        self.vm_frame_col1_entry1.bind("<KeyRelease>", self.vm_onkeyup_precio_venta)

        self.vm_frame_col1_label3 = CTkLabel(self.vm_frame_col1, text="Costo variable unitario", fg_color="transparent")
        self.vm_frame_col1_label3.grid(row=1, column=0, padx=10, pady=10, sticky="w")
        self.vm_frame_col1_entry3 = CTkEntry(self.vm_frame_col1, placeholder_text="costo_variable_unitario", justify="right")
        self.vm_frame_col1_entry3.grid(row=1, column=1, padx=10, pady=10)
        self.vm_frame_col1_entry3.bind("<KeyRelease>", self.vm_onkeyup_costo_variable)

        self.vm_frame_col1_label5 = CTkLabel(self.vm_frame_col1, text="Costos y gastos fijos mensuales", fg_color="transparent")
        self.vm_frame_col1_label5.grid(row=2, column=0, padx=10, pady=10, sticky="w")
        self.vm_frame_col1_entry5 = CTkEntry(self.vm_frame_col1, placeholder_text="costos_gasto_fijos_mensuales", justify="right")
        self.vm_frame_col1_entry5.grid(row=2, column=1, padx=10, pady=10)
        # self.vm_frame_col1_entry5.insert(0, "17560000")
        self.vm_frame_col1_entry5.bind("<KeyRelease>", self.vm_onkeyup_costos_gastos_fijos)

        self.vm_frame_col1_label7 = CTkLabel(self.vm_frame_col1, text="El número de trabajadores en producción", fg_color="transparent")
        self.vm_frame_col1_label7.grid(row=3, column=0, padx=10, pady=10, sticky="w")
        self.vm_frame_col1_entry7 = CTkEntry(self.vm_frame_col1, placeholder_text="operarios", justify="right")
        self.vm_frame_col1_entry7.grid(row=3, column=1, padx=10, pady=10)
        self.vm_frame_col1_entry7.bind("<KeyRelease>", self.vm_onkeyup_operarios)

        self.vm_frame_col1_label9 = CTkLabel(self.vm_frame_col1, text="La jornada laboral", fg_color="transparent")
        self.vm_frame_col1_label9.grid(row=4, column=0, padx=10, pady=10, sticky="w")
        self.vm_frame_col1_entry9 = CTkEntry(self.vm_frame_col1, placeholder_text="jornada_laboral", justify="right")
        self.vm_frame_col1_entry9.grid(row=4, column=1, padx=10, pady=10)
        self.vm_frame_col1_entry9.insert(0, "8")
        self.vm_frame_col1_entry9.bind("<KeyRelease>", self.vm_onkeyup_jornada)

        self.vm_frame_col1_label11 = CTkLabel(self.vm_frame_col1, text="Número de días", fg_color="transparent")
        self.vm_frame_col1_label11.grid(row=5, column=0, padx=10, pady=10, sticky="w")
        self.vm_frame_col1_entry11 = CTkEntry(self.vm_frame_col1, placeholder_text="dias_promedio_labora_empresa", justify="right")
        self.vm_frame_col1_entry11.grid(row=5, column=1, padx=10, pady=10)
        self.vm_frame_col1_entry11.insert(0, "25")
        self.vm_frame_col1_entry11.bind("<KeyRelease>", self.vm_onkeyup_dias)

        self.vm_frame_col1_label13 = CTkLabel(self.vm_frame_col1, text="Porcentaje de eficiencia", fg_color="transparent")
        self.vm_frame_col1_label13.grid(row=6, column=0, padx=10, pady=10, sticky="w")
        self.vm_frame_col1_entry13 = CTkEntry(self.vm_frame_col1, placeholder_text="porcentaje_eficiencia", justify="right")
        self.vm_frame_col1_entry13.grid(row=6, column=1, padx=10, pady=10)
        self.vm_frame_col1_entry13.bind("<KeyRelease>", self.vm_onkeyup_eficiencia)

        self.vm_frame_col1_label15 = CTkLabel(self.vm_frame_col1, text="Tiempo estandar de la prenda", fg_color="transparent")
        self.vm_frame_col1_label15.grid(row=7, column=0, padx=10, pady=10, sticky="w")
        self.vm_frame_col1_entry15 = CTkEntry(self.vm_frame_col1, placeholder_text="tiempo_estandar", justify="right")
        self.vm_frame_col1_entry15.grid(row=7, column=1, padx=10, pady=10)
        self.vm_frame_col1_entry15.bind("<KeyRelease>", self.vm_onkeyup_tiempo_estandar)

        # ============ vm_frame_col2 ============

        self.vm_frame_col2_label2 = CTkLabel(self.vm_frame_col2, text="Una empresa de confección quiere calcular el valor minuto de un ", fg_color="transparent", justify="left", wraplength=500)
        self.vm_frame_col2_label2.grid(row=0, column=0, sticky="ew")

        self.vm_frame_col2_label4 = CTkLabel(self.vm_frame_col2, text="<< PRODUCTO >>", fg_color="transparent", justify="left", font=CTkFont(size=14, weight="bold"))
        self.vm_frame_col2_label4.grid(row=0, column=1, sticky="e")

        self.vm_frame_col2_label6 = CTkLabel(self.vm_frame_col2, text=" cuyo precio de venta es de ", fg_color="transparent", justify="left")
        self.vm_frame_col2_label6.grid(row=1, column=0, sticky="w")

        self.vm_frame_col2_label8 = CTkLabel(self.vm_frame_col2, text="$ 0", fg_color="transparent", justify="left", font=CTkFont(size=14, weight="bold"))
        self.vm_frame_col2_label8.grid(row=1, column=1, sticky="e")

        self.vm_frame_col2_label10 = CTkLabel(self.vm_frame_col2, text=" y su costo variable unitario es de ", fg_color="transparent", justify="left")
        self.vm_frame_col2_label10.grid(row=2, column=0, sticky="w")

        self.vm_frame_col2_label12 = CTkLabel(self.vm_frame_col2, text="$ 0", fg_color="transparent", justify="left", font=CTkFont(size=14, weight="bold"))
        self.vm_frame_col2_label12.grid(row=2, column=1, sticky="e")

        self.vm_frame_col2_label14 = CTkLabel(self.vm_frame_col2, text="\nAdemás se cuenta con la siguiente información:\n", fg_color="transparent", justify="left", font=CTkFont(size=14, weight="bold"), text_color="green")
        self.vm_frame_col2_label14.grid(row=3, column=0, sticky="w")

        self.vm_frame_col2_label16 = CTkLabel(self.vm_frame_col2, text="(A) Costos y gastos  fijos mensuales:", fg_color="transparent", justify="left")
        self.vm_frame_col2_label16.grid(row=4, column=0, sticky="w")

        self.vm_frame_col2_label18 = CTkLabel(self.vm_frame_col2, text="0", fg_color="transparent", justify="left", font=CTkFont(size=14, weight="bold"), text_color="red")
        self.vm_frame_col2_label18.grid(row=4, column=1, sticky="e")

        self.vm_frame_col2_label20 = CTkLabel(self.vm_frame_col2, text="(B) El número de trabajadores en producción: ", fg_color="transparent", justify="left")
        self.vm_frame_col2_label20.grid(row=5, column=0, sticky="w")

        self.vm_frame_col2_label22 = CTkLabel(self.vm_frame_col2, text="0", fg_color="transparent", justify="left", font=CTkFont(size=14, weight="bold"), text_color="red")
        self.vm_frame_col2_label22.grid(row=5, column=1, sticky="e")

        self.vm_frame_col2_label24 = CTkLabel(self.vm_frame_col2, text="(C) La jornada laboral efectiva: ", fg_color="transparent", justify="left")
        self.vm_frame_col2_label24.grid(row=6, column=0, sticky="w")

        self.vm_frame_col2_label26 = CTkLabel(self.vm_frame_col2, text="0", fg_color="transparent", justify="left", font=CTkFont(size=14, weight="bold"), text_color="red")
        self.vm_frame_col2_label26.grid(row=6, column=1, sticky="e")

        self.vm_frame_col2_label28 = CTkLabel(self.vm_frame_col2, text="(D) El numero de días en promedio al mes que labora la empresa:", fg_color="transparent", justify="left")
        self.vm_frame_col2_label28.grid(row=7, column=0, sticky="w")

        self.vm_frame_col2_label30 = CTkLabel(self.vm_frame_col2, text="0", fg_color="transparent", justify="left", font=CTkFont(size=14, weight="bold"), text_color="red")
        self.vm_frame_col2_label30.grid(row=7, column=1, sticky="e")

        self.vm_frame_col2_label32 = CTkLabel(self.vm_frame_col2, text="(E) El porcentaje de eficiencia:", fg_color="transparent", justify="left")
        self.vm_frame_col2_label32.grid(row=8, column=0, sticky="w")

        self.vm_frame_col2_label34 = CTkLabel(self.vm_frame_col2, text="0%", fg_color="transparent", justify="left", font=CTkFont(size=14, weight="bold"), text_color="red")
        self.vm_frame_col2_label34.grid(row=8, column=1, sticky="e")

        self.vm_frame_col2_label36 = CTkLabel(self.vm_frame_col2, text="Numero de minutos que tiene una hora: ", fg_color="transparent", justify="left")
        self.vm_frame_col2_label36.grid(row=9, column=0, sticky="w")

        self.vm_frame_col2_label38 = CTkLabel(self.vm_frame_col2, text="60", fg_color="transparent", justify="left", font=CTkFont(size=14, weight="bold"), text_color="red")
        self.vm_frame_col2_label38.grid(row=9, column=1, sticky="e")

        self.vm_frame_col2_label40 = CTkLabel(self.vm_frame_col2, text="El tiempo estándar de la prenda es:", fg_color="transparent", justify="left")
        self.vm_frame_col2_label40.grid(row=10, column=0, sticky="w")

        self.vm_frame_col2_label42 = CTkLabel(self.vm_frame_col2, text="0", fg_color="transparent", justify="left", font=CTkFont(size=14, weight="bold"), text_color="red")
        self.vm_frame_col2_label42.grid(row=10, column=1, sticky="e")

        self.vm_frame_col2_label44 = CTkLabel(self.vm_frame_col2, text="FÓRMULA ",
                                                 image=self.vm_formula1_image, compound="right", height=40,
                                                 font=CTkFont(size=14, weight="bold"))
        self.vm_frame_col2_label44.grid(row=11, column=0, columnspan=2, padx=20, pady=10)

        self.vm_frame_col2_label46 = CTkLabel(self.vm_frame_col2, text="COSTO FIJO UNITARIO = VALOR MINUTO * Tiempo estándar", fg_color="transparent", justify="left", font=CTkFont(size=14, weight="bold"))
        self.vm_frame_col2_label46.grid(row=16, column=0, columnspan=2, sticky="ew")

        # ============ Zona Botones ============

        self.vm_frame_b1utton = CTkButton(self.vm_frame_row1, text="Calcular", command=self.calcular_vm_event)
        self.vm_frame_b1utton.grid(row=0, column=0, pady=10, columnspan=2)

        self.vm_frame_row1_label1 = CTkLabel(self.vm_frame_row1, text="VALOR MINUTO", fg_color="transparent")
        self.vm_frame_row1_label1.grid(row=1, column=0, padx=10, pady=10, sticky="w")
        self.vm_frame_row1_entry1 = CTkEntry(self.vm_frame_row1, placeholder_text="valor_minuto", justify="right")
        self.vm_frame_row1_entry1.grid(row=1, column=1, padx=10, pady=10)
        self.vm_frame_row1_entry1.insert(0, "0")

        self.vm_frame_row1_label2 = CTkLabel(self.vm_frame_row1, text="COSTO FIJO UNITARIO", fg_color="transparent")
        self.vm_frame_row1_label2.grid(row=2, column=0, padx=10, pady=10, sticky="w")
        self.vm_frame_row1_entry2 = CTkEntry(self.vm_frame_row1, placeholder_text="costo_fijo_unitario", justify="right")
        self.vm_frame_row1_entry2.grid(row=2, column=1, padx=10, pady=10)
        self.vm_frame_row1_entry2.insert(0, "0")


        # FIN - valor minuto frame

        # Costos corte piezas frame
        self.costos_corte_piezas_frame = CTkFrame(self, corner_radius=0, fg_color="transparent")
        self.costos_corte_piezas_frame.grid_columnconfigure(0, weight=1)

        self.productos_frame_logo_sena = CTkLabel(self.costos_corte_piezas_frame,
                                                  text=" Servicio Nacional de Aprendizaje - CFDCM",
                                                  image=self.logo_sena_image_small, compound="left",
                                                  font=CTkFont(size=12, weight="normal"))
        self.productos_frame_logo_sena.grid(row=0, column=0, padx=0, pady=0, ipady=10, columnspan=2, sticky="nsew")

        # Contenedor módulo Frame
        self.ccp_frame = CTkScrollableFrame(self.costos_corte_piezas_frame, corner_radius=10, height=600)
        self.ccp_frame.grid(row=1, column=0, padx=(10, 0), pady=0, ipady=10, columnspan=2, sticky="nsew")

        self.costos_corte_piezas_frame_label = CTkLabel(self.ccp_frame, text=" Costos para Corte de Piezas",
                                                 image=self.costo_corte_piezas_image, compound="left", height=40,
                                                 font=CTkFont(size=35, weight="bold"))
        self.costos_corte_piezas_frame_label.grid(row=0, column=0, columnspan=2, padx=20, pady=10)

        self.ccp_frame.columnconfigure(0, weight=1)

        # FIN - costos corte piezas frame

        # Consumo de hilo frame
        self.consumo_hilo_frame = CTkFrame(self, corner_radius=0, fg_color="transparent")
        self.consumo_hilo_frame.grid_columnconfigure(0, weight=1)

        self.productos_frame_logo_sena = CTkLabel(self.consumo_hilo_frame,
                                                  text=" Servicio Nacional de Aprendizaje - CFDCM",
                                                  image=self.logo_sena_image_small, compound="left",
                                                  font=CTkFont(size=12, weight="normal"))
        self.productos_frame_logo_sena.grid(row=0, column=0, padx=0, pady=0, ipady=10, sticky="nsew")

        # Contenedor módulo Frame
        self.ch_frame = CTkScrollableFrame(self.consumo_hilo_frame, corner_radius=10, height=680)
        self.ch_frame.grid(row=1, column=0, padx=(10, 0), pady=0, ipady=10, sticky="nsew")

        self.consumo_hilo_frame_label = CTkLabel(self.ch_frame, text=" Cálculo de Consumo de Hilo",
                                                 image=self.consumo_hilo_image, compound="left", height=40,
                                                 font=CTkFont(size=35, weight="bold"))
        self.consumo_hilo_frame_label.grid(row=0, column=0, padx=20, pady=10)

        self.ch_frame.columnconfigure(0, weight=1)


        productos = ["Seleccione el Producto"]
        self.consumo_hilo_producto = StringVar(value="Seleccione el Producto")
        try:
            self.abrir_conexion()
            self.conexion.row_factory = sqlite3.Row
            cursor = self.conexion.cursor()
            sql = "select * from scp_producto"
            cursor.execute(sql)
            registro = cursor.fetchall()
            # Construir listado para combobox
            for r in registro:
                productos.append(f"{r['id']}.{r['nombre']}")
        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error conectando a la base de datos.\n{e}")
        finally:
            self.conexion.close()

        self.ch_button_producto = CTkOptionMenu(self.ch_frame, dynamic_resizing=True, values=productos,
                                                command=self.consumo_hilo_seleccionar_producto,
                                                variable=self.consumo_hilo_producto)
        self.ch_button_producto.grid(row=1, column=0, padx=10, pady=10)

        self.ch_frame_row0 = CTkFrame(self.ch_frame)
        # self.ch_frame_row0.grid(row=2, column=0, sticky="n", padx=0, pady=0)
        self.ch_frame_row1 = CTkScrollableFrame(self.ch_frame, fg_color="gray85", height=300, orientation="vertical")
        # self.ch_frame_row1.grid(row=3, column=0, sticky="n", padx=0, pady=0)

        # Tipos de tejido y Desperdicio
        self.ch_label1 = CTkLabel(self.ch_frame_row0, text="Tipo de tejido:")
        self.ch_label1.grid(row=0, column=0, sticky="e", padx=(100, 10))

        tipo_tejido = ["Liviano", "Mediano", "Pesado"]
        self.consumo_hilo_tipo_tejido = StringVar(value="Liviano")

        self.ch_button_tipo_tejido = CTkOptionMenu(self.ch_frame_row0, dynamic_resizing=True, values=tipo_tejido,
                                                   command=self.consumo_hilo_tipo_tejido_callback,
                                                   variable=self.consumo_hilo_tipo_tejido)
        self.ch_button_tipo_tejido.grid(row=0, column=1, padx=(10, 50), pady=10, sticky="w")

        self.ch_label2 = CTkLabel(self.ch_frame_row0, text="Desperdicio (%):")
        self.ch_label2.grid(row=0, column=2, sticky="e", padx=(50, 10))

        self.ch_entry1 = CTkEntry(self.ch_frame_row0, justify="right")
        self.ch_entry1.grid(row=0, column=3, sticky="w", padx=(10, 100))
        self.ch_entry1.insert(0, "0")

        # Fin - Tipos de tejido

        # Frame Consumo Hilo, Liviano

        label_grid1 = tk_tools.LabelGrid(self.ch_frame_row1, 2, ['Máquina/Puntada\n', 'Elemento de\nPuntada'], relief="flat")
        label_grid1.grid(row=1, column=0, sticky='ewn')

        label_grid1.add_row(["Plana 1A 301", "Aguja"])
        label_grid1.add_row(["", "Bobina"])
        label_grid1.add_row(["Plana 2A 301/2", "Aguja"])
        label_grid1.add_row(["", "Bobinas"])
        label_grid1.add_row(["Zig-Zag Sencillo 14-16-18 PPP 304", "Aguja"])
        label_grid1.add_row(["", "Bobina"])
        label_grid1.add_row(["Zig-Zag doble 16-18-20 PPP 308", "Aguja"])
        label_grid1.add_row(["", "Bobina"])
        label_grid1.add_row(["Zig-Zag triple 20-22-24 PPP 321", "Aguja"])
        label_grid1.add_row(["", "Bobina"])
        label_grid1.add_row(["Fileteadora 3 Hilos a 1/4 - 504", "1 Aguja"])
        label_grid1.add_row(["", "2 Loopers"])
        label_grid1.add_row(["Fileteadora 3 Hilos a 5/32 - 504", "1 Aguja"])
        label_grid1.add_row(["", "2 Loopers"])
        label_grid1.add_row(["Fileteadora de refuerzo 4 hilos 512", "2 Agujas"])
        label_grid1.add_row(["", "2 Loopers"])
        label_grid1.add_row(["Puntada Seg. Cadeneta doble 1 Aguja 401", "Aguja"])
        label_grid1.add_row(["", "Looper"])
        label_grid1.add_row(["Cadeneta Sencilla 3 - 4 - 5 P.P.P (101)", "1 Aguja"])
        label_grid1.add_row(["Cadeneta doble 1 Aguja 401", "Aguja"])
        label_grid1.add_row(["", "Looper"])
        label_grid1.add_row(["Recubridora 3 hilos (406)", "2 Agujas"])
        label_grid1.add_row(["", "1 Looper"])
        label_grid1.add_row(["Recubridora 4 Hilos 602", "2 Agujas"])
        label_grid1.add_row(["", "1 Looper"])
        label_grid1.add_row(["", "1 Recubrid"])
        label_grid1.add_row(["Recubridora 5 Hilos 605", "3 Agujas"])
        label_grid1.add_row(["", "1 Looper"])
        label_grid1.add_row(["", "1 Recubrid"])
        label_grid1.add_row(["Flat Seamer 4 Agujas 607", "4 Agujas"])
        label_grid1.add_row(["", "1 Looper"])
        label_grid1.add_row(["", "1 Recubrid"])
        label_grid1.add_row(["Presilladora 36 Puntadas 3045", "Aguja/bob"])
        label_grid1.add_row(["Ojaladora Recta 3043", "Aguja/bob"])
        label_grid1.add_row(["Botonadora 1012", "Aguja"])

        self.button_grid1 = tk_tools.ButtonGrid(self.ch_frame_row1, 1, ['Punt. X \nPulg.'], relief="flat")
        self.button_grid1.grid(row=1, column=1, sticky='ewn')

        self.button_grid1.add_row([("Seleccione", lambda: self.ventana_combo_box("PPP - Seleccione las Puntadas por Pulgada", "(Aguja)", 1))])
        self.button_grid1.add_row([("Seleccione", lambda:  self.ventana_combo_box("PPP - Seleccione las Puntadas por Pulgada", "(Bobina)", 2))])
        self.button_grid1.add_row([("Seleccione", lambda: self.ventana_combo_box("PPP - Seleccione las Puntadas por Pulgada", "(Aguja)", 3))])
        self.button_grid1.add_row([("Seleccione", lambda:  self.ventana_combo_box("PPP - Seleccione las Puntadas por Pulgada", "(Bobina)", 4))])
        self.button_grid1.add_row([("Seleccione", lambda: self.ventana_combo_box("PPP - Seleccione las Puntadas por Pulgada", "(Aguja)", 5))])
        self.button_grid1.add_row([("Seleccione", lambda:  self.ventana_combo_box("PPP - Seleccione las Puntadas por Pulgada", "(Bobina)", 6))])

        self.button_grid1.add_row([("Seleccione", lambda:  self.ventana_combo_box("PPP - Seleccione las Puntadas por Pulgada", "(Aguja)", 7))])
        self.button_grid1.add_row([("Seleccione", lambda:  self.ventana_combo_box("PPP - Seleccione las Puntadas por Pulgada", "(Bobina)", 8))])
        self.button_grid1.add_row([("Seleccione", lambda:  self.ventana_combo_box("PPP - Seleccione las Puntadas por Pulgada", "(Aguja)", 9))])
        self.button_grid1.add_row([("Seleccione", lambda:  self.ventana_combo_box("PPP - Seleccione las Puntadas por Pulgada", "(Bobina)", 10))])
        self.button_grid1.add_row([("Seleccione", lambda:  self.ventana_combo_box("PPP - Seleccione las Puntadas por Pulgada", "(1 Aguja)", 11))])
        self.button_grid1.add_row([("Seleccione", lambda:  self.ventana_combo_box("PPP - Seleccione las Puntadas por Pulgada", "(2 Loopers)", 12))])
        self.button_grid1.add_row([("Seleccione", lambda:  self.ventana_combo_box("PPP - Seleccione las Puntadas por Pulgada", "(1 Aguja)", 13))])
        self.button_grid1.add_row([("Seleccione", lambda:  self.ventana_combo_box("PPP - Seleccione las Puntadas por Pulgada", "(2 Loopers)", 14))])
        self.button_grid1.add_row([("Seleccione", lambda:  self.ventana_combo_box("PPP - Seleccione las Puntadas por Pulgada", "(2 Agujas)", 15))])
        self.button_grid1.add_row([("Seleccione", lambda:  self.ventana_combo_box("PPP - Seleccione las Puntadas por Pulgada", "(2 Loopers)", 16))])
        self.button_grid1.add_row([("Seleccione", lambda:  self.ventana_combo_box("PPP - Seleccione las Puntadas por Pulgada", "(Aguja)", 17))])
        self.button_grid1.add_row([("Seleccione", lambda:  self.ventana_combo_box("PPP - Seleccione las Puntadas por Pulgada", "(Looper)", 18))])
        self.button_grid1.add_row([("Seleccione", lambda:  self.ventana_combo_box("PPP - Seleccione las Puntadas por Pulgada", "(1 Aguja)", 19))])
        self.button_grid1.add_row([("Seleccione", lambda:  self.ventana_combo_box("PPP - Seleccione las Puntadas por Pulgada", "(Aguja)", 20))])
        self.button_grid1.add_row([("Seleccione", lambda:  self.ventana_combo_box("PPP - Seleccione las Puntadas por Pulgada", "(Looper)", 21))])
        self.button_grid1.add_row([("Seleccione", lambda:  self.ventana_combo_box("PPP - Seleccione las Puntadas por Pulgada", "(2 Agujas)", 22))])
        self.button_grid1.add_row([("Seleccione", lambda:  self.ventana_combo_box("PPP - Seleccione las Puntadas por Pulgada", "(1 Looper)", 23))])
        self.button_grid1.add_row([("Seleccione", lambda:  self.ventana_combo_box("PPP - Seleccione las Puntadas por Pulgada", "(2 Agujas)", 24))])
        self.button_grid1.add_row([("Seleccione", lambda:  self.ventana_combo_box("PPP - Seleccione las Puntadas por Pulgada", "(1 Looper)", 25))])
        self.button_grid1.add_row([("Seleccione", lambda:  self.ventana_combo_box("PPP - Seleccione las Puntadas por Pulgada", "(1 Recubrid)", 26))])
        self.button_grid1.add_row([("Seleccione", lambda:  self.ventana_combo_box("PPP - Seleccione las Puntadas por Pulgada", "(3 Agujas)", 27))])
        self.button_grid1.add_row([("Seleccione", lambda:  self.ventana_combo_box("PPP - Seleccione las Puntadas por Pulgada", "(1 Looper)", 28))])
        self.button_grid1.add_row([("Seleccione", lambda:  self.ventana_combo_box("PPP - Seleccione las Puntadas por Pulgada", "(1 Recubrid)", 29))])
        self.button_grid1.add_row([("Seleccione", lambda:  self.ventana_combo_box("PPP - Seleccione las Puntadas por Pulgada", "(4 Agujas)", 30))])
        self.button_grid1.add_row([("Seleccione", lambda:  self.ventana_combo_box("PPP - Seleccione las Puntadas por Pulgada", "(1 Looper)", 31))])
        self.button_grid1.add_row([("Seleccione", lambda:  self.ventana_combo_box("PPP - Seleccione las Puntadas por Pulgada", "(1 Recubrid)", 32))])
        self.button_grid1.add_row([("Seleccione", lambda:  self.ventana_combo_box("PPP - Seleccione las Puntadas por Pulgada", "(Aguja/bob)", 33))])
        self.button_grid1.add_row([("Seleccione", lambda:  self.ventana_combo_box("PPP - Seleccione las Puntadas por Pulgada", "(Aguja/bob)", 34))])
        self.button_grid1.add_row([("Seleccione", lambda:  self.ventana_combo_box("PPP - Seleccione las Puntadas por Pulgada", "(Aguja)", 35))])

        self.label_grid2 = tk_tools.LabelGrid(self.ch_frame_row1, 3, ['M.H. a \n10 PPP.', 'M.H. a \n12 PPP.', 'M.H. a \n14 PPP.'],
                                         relief="flat")
        self.label_grid2.grid(row=1, column=2, sticky='ewn')

        self.label_grid2.add_row([1.23, 1.31, 1.44])
        self.label_grid2.add_row([1.23, 1.31, 1.44])
        self.label_grid2.add_row([2.46, 2.62, 2.88])
        self.label_grid2.add_row([2.46, 2.62, 2.88])
        self.label_grid2.add_row([2.40, 2.44, 2.50])
        self.label_grid2.add_row([2.40, 2.44, 2.50])
        self.label_grid2.add_row([3.35, 3.39, 3.45])
        self.label_grid2.add_row([3.35, 3.39, 3.45])
        self.label_grid2.add_row([3.35, 3.39, 3.45])
        self.label_grid2.add_row([3.35, 3.39, 3.45])
        self.label_grid2.add_row([1.55, 1.58, 1.61])
        self.label_grid2.add_row([11.51, 11.75, 12.12])
        self.label_grid2.add_row([1.55, 1.58, 1.61])
        self.label_grid2.add_row([10.71, 10.82, 10.99])
        self.label_grid2.add_row([3.44, 3.47, 3.52])
        self.label_grid2.add_row([10.71, 10.88, 11.13])
        self.label_grid2.add_row([2.21, 2.28, 2.39])
        self.label_grid2.add_row([3.12, 3.22, 3.37])
        self.label_grid2.add_row([4.52, 4.56, 4.62])
        self.label_grid2.add_row([2.21, 2.28, 2.39])
        self.label_grid2.add_row([3.12, 3.22, 3.37])
        self.label_grid2.add_row([5.15, 5.21, 5.31])
        self.label_grid2.add_row([8.26, 8.36, 8.52])
        self.label_grid2.add_row([5.15, 5.21, 5.31])
        self.label_grid2.add_row([8.26, 8.36, 8.52])
        self.label_grid2.add_row([4.06, 4.12, 4.18])
        self.label_grid2.add_row([8.59, 8.68, 8.80])
        self.label_grid2.add_row([8.59, 8.68, 8.80])
        self.label_grid2.add_row([6.40, 6.49, 6.61])
        self.label_grid2.add_row([11.76, 11.84, 11.94])
        self.label_grid2.add_row([8.91, 8.97, 9.05])
        self.label_grid2.add_row([7.27, 7.32, 7.38])
        self.label_grid2.add_row([0.39, 0.00, 0.00])
        self.label_grid2.add_row([0.47, 0.00, 0.00])
        self.label_grid2.add_row([0.13, 0.00, 0.00])

        self.entry_grid1 = tk_tools.EntryGrid(self.ch_frame_row1, 1, ['Metros\nCostura'], relief="flat", width=10)
        self.entry_grid1.grid(row=1, column=3, sticky='ewn')

        self.entry_grid1.add_row(data=[0])
        self.entry_grid1.add_row(data=[0])
        self.entry_grid1.add_row(data=[0])
        self.entry_grid1.add_row(data=[0])
        self.entry_grid1.add_row(data=[0])
        self.entry_grid1.add_row(data=[0])
        self.entry_grid1.add_row(data=[0])
        self.entry_grid1.add_row(data=[0])
        self.entry_grid1.add_row(data=[0])
        self.entry_grid1.add_row(data=[0])
        self.entry_grid1.add_row(data=[0])
        self.entry_grid1.add_row(data=[0])
        self.entry_grid1.add_row(data=[0])
        self.entry_grid1.add_row(data=[0])
        self.entry_grid1.add_row(data=[0])
        self.entry_grid1.add_row(data=[0])
        self.entry_grid1.add_row(data=[0])
        self.entry_grid1.add_row(data=[0])
        self.entry_grid1.add_row(data=[0])
        self.entry_grid1.add_row(data=[0])
        self.entry_grid1.add_row(data=[0])
        self.entry_grid1.add_row(data=[0])
        self.entry_grid1.add_row(data=[0])
        self.entry_grid1.add_row(data=[0])
        self.entry_grid1.add_row(data=[0])
        self.entry_grid1.add_row(data=[0])
        self.entry_grid1.add_row(data=[0])
        self.entry_grid1.add_row(data=[0])
        self.entry_grid1.add_row(data=[0])
        self.entry_grid1.add_row(data=[0])
        self.entry_grid1.add_row(data=[0])
        self.entry_grid1.add_row(data=[0])
        self.entry_grid1.add_row(data=[0])
        self.entry_grid1.add_row(data=[0])
        self.entry_grid1.add_row(data=[0])

        self.label_grid3 = tk_tools.LabelGrid(self.ch_frame_row1, 2, ['Metros\nHilo', 'Total\nHilo'], relief="flat")
        self.label_grid3.grid(row=1, column=4, sticky='ewn')

        self.label_grid3.add_row([0, 0])
        self.label_grid3.add_row([0, 0])
        self.label_grid3.add_row([0, 0])
        self.label_grid3.add_row([0, 0])
        self.label_grid3.add_row([0, 0])
        self.label_grid3.add_row([0, 0])
        self.label_grid3.add_row([0, 0])
        self.label_grid3.add_row([0, 0])
        self.label_grid3.add_row([0, 0])
        self.label_grid3.add_row([0, 0])
        self.label_grid3.add_row([0, 0])
        self.label_grid3.add_row([0, 0])
        self.label_grid3.add_row([0, 0])
        self.label_grid3.add_row([0, 0])
        self.label_grid3.add_row([0, 0])
        self.label_grid3.add_row([0, 0])
        self.label_grid3.add_row([0, 0])
        self.label_grid3.add_row([0, 0])
        self.label_grid3.add_row([0, 0])
        self.label_grid3.add_row([0, 0])
        self.label_grid3.add_row([0, 0])
        self.label_grid3.add_row([0, 0])
        self.label_grid3.add_row([0, 0])
        self.label_grid3.add_row([0, 0])
        self.label_grid3.add_row([0, 0])
        self.label_grid3.add_row([0, 0])
        self.label_grid3.add_row([0, 0])
        self.label_grid3.add_row([0, 0])
        self.label_grid3.add_row([0, 0])
        self.label_grid3.add_row([0, 0])
        self.label_grid3.add_row([0, 0])
        self.label_grid3.add_row([0, 0])
        self.label_grid3.add_row([0, 0])
        self.label_grid3.add_row([0, 0])
        self.label_grid3.add_row([0, 0])

        self.button_grid2 = tk_tools.ButtonGrid(self.ch_frame_row1, 2, ['Tipo de Hilo\n', 'Color -\nBco - Cdo'], relief="flat")
        self.button_grid2.grid(row=1, column=5, sticky='ewn')

        self.button_grid2.add_row([("Seleccione.", lambda: self.ventana_combo_box2(1)), ("Seleccione", lambda: self.ventana_combo_box3(1))])
        self.button_grid2.add_row([("Seleccione.", lambda: self.ventana_combo_box2(2)), ("Seleccione", lambda: self.ventana_combo_box3(2))])
        self.button_grid2.add_row([("Seleccione.", lambda: self.ventana_combo_box2(3)), ("Seleccione", lambda: self.ventana_combo_box3(3))])
        self.button_grid2.add_row([("Seleccione.", lambda: self.ventana_combo_box2(4)), ("Seleccione", lambda: self.ventana_combo_box3(4))])
        self.button_grid2.add_row([("Seleccione.", lambda: self.ventana_combo_box2(5)), ("Seleccione", lambda: self.ventana_combo_box3(5))])
        self.button_grid2.add_row([("Seleccione.", lambda: self.ventana_combo_box2(6)), ("Seleccione", lambda: self.ventana_combo_box3(6))])
        self.button_grid2.add_row([("Seleccione.", lambda: self.ventana_combo_box2(7)), ("Seleccione", lambda: self.ventana_combo_box3(7))])
        self.button_grid2.add_row([("Seleccione.", lambda: self.ventana_combo_box2(8)), ("Seleccione", lambda: self.ventana_combo_box3(8))])
        self.button_grid2.add_row([("Seleccione.", lambda: self.ventana_combo_box2(9)), ("Seleccione", lambda: self.ventana_combo_box3(9))])
        self.button_grid2.add_row([("Seleccione.", lambda: self.ventana_combo_box2(10)), ("Seleccione", lambda: self.ventana_combo_box3(10))])
        self.button_grid2.add_row([("Seleccione.", lambda: self.ventana_combo_box2(11)), ("Seleccione", lambda: self.ventana_combo_box3(11))])
        self.button_grid2.add_row([("Seleccione.", lambda: self.ventana_combo_box2(12)), ("Seleccione", lambda: self.ventana_combo_box3(12))])
        self.button_grid2.add_row([("Seleccione.", lambda: self.ventana_combo_box2(13)), ("Seleccione", lambda: self.ventana_combo_box3(13))])
        self.button_grid2.add_row([("Seleccione.", lambda: self.ventana_combo_box2(14)), ("Seleccione", lambda: self.ventana_combo_box3(14))])
        self.button_grid2.add_row([("Seleccione.", lambda: self.ventana_combo_box2(15)), ("Seleccione", lambda: self.ventana_combo_box3(15))])
        self.button_grid2.add_row([("Seleccione.", lambda: self.ventana_combo_box2(16)), ("Seleccione", lambda: self.ventana_combo_box3(16))])
        self.button_grid2.add_row([("Seleccione.", lambda: self.ventana_combo_box2(17)), ("Seleccione", lambda: self.ventana_combo_box3(17))])
        self.button_grid2.add_row([("Seleccione.", lambda: self.ventana_combo_box2(18)), ("Seleccione", lambda: self.ventana_combo_box3(18))])
        self.button_grid2.add_row([("Seleccione.", lambda: self.ventana_combo_box2(19)), ("Seleccione", lambda: self.ventana_combo_box3(19))])
        self.button_grid2.add_row([("Seleccione.", lambda: self.ventana_combo_box2(20)), ("Seleccione", lambda: self.ventana_combo_box3(20))])
        self.button_grid2.add_row([("Seleccione.", lambda: self.ventana_combo_box2(21)), ("Seleccione", lambda: self.ventana_combo_box3(21))])
        self.button_grid2.add_row([("Seleccione.", lambda: self.ventana_combo_box2(22)), ("Seleccione", lambda: self.ventana_combo_box3(22))])
        self.button_grid2.add_row([("Seleccione.", lambda: self.ventana_combo_box2(23)), ("Seleccione", lambda: self.ventana_combo_box3(23))])
        self.button_grid2.add_row([("Seleccione.", lambda: self.ventana_combo_box2(24)), ("Seleccione", lambda: self.ventana_combo_box3(24))])
        self.button_grid2.add_row([("Seleccione.", lambda: self.ventana_combo_box2(25)), ("Seleccione", lambda: self.ventana_combo_box3(25))])
        self.button_grid2.add_row([("Seleccione.", lambda: self.ventana_combo_box2(26)), ("Seleccione", lambda: self.ventana_combo_box3(26))])
        self.button_grid2.add_row([("Seleccione.", lambda: self.ventana_combo_box2(27)), ("Seleccione", lambda: self.ventana_combo_box3(27))])
        self.button_grid2.add_row([("Seleccione.", lambda: self.ventana_combo_box2(28)), ("Seleccione", lambda: self.ventana_combo_box3(28))])
        self.button_grid2.add_row([("Seleccione.", lambda: self.ventana_combo_box2(29)), ("Seleccione", lambda: self.ventana_combo_box3(29))])
        self.button_grid2.add_row([("Seleccione.", lambda: self.ventana_combo_box2(30)), ("Seleccione", lambda: self.ventana_combo_box3(30))])
        self.button_grid2.add_row([("Seleccione.", lambda: self.ventana_combo_box2(31)), ("Seleccione", lambda: self.ventana_combo_box3(31))])
        self.button_grid2.add_row([("Seleccione.", lambda: self.ventana_combo_box2(32)), ("Seleccione", lambda: self.ventana_combo_box3(32))])
        self.button_grid2.add_row([("Seleccione.", lambda: self.ventana_combo_box2(33)), ("Seleccione", lambda: self.ventana_combo_box3(33))])
        self.button_grid2.add_row([("Seleccione.", lambda: self.ventana_combo_box2(34)), ("Seleccione", lambda: self.ventana_combo_box3(34))])
        self.button_grid2.add_row([("Seleccione.", lambda: self.ventana_combo_box2(35)), ("Seleccione", lambda: self.ventana_combo_box3(35))])

        self.label_grid4 = tk_tools.LabelGrid(self.ch_frame_row1, 3, ['Total metros\nproducc', 'Total\nConos', 'Total conos\nredondeado'], relief="flat")
        self.label_grid4.grid(row=1, column=6, sticky='ewn')

        self.label_grid4.add_row([0, 0, 0])
        self.label_grid4.add_row([0, 0, 0])
        self.label_grid4.add_row([0, 0, 0])
        self.label_grid4.add_row([0, 0, 0])
        self.label_grid4.add_row([0, 0, 0])
        self.label_grid4.add_row([0, 0, 0])
        self.label_grid4.add_row([0, 0, 0])
        self.label_grid4.add_row([0, 0, 0])
        self.label_grid4.add_row([0, 0, 0])
        self.label_grid4.add_row([0, 0, 0])
        self.label_grid4.add_row([0, 0, 0])
        self.label_grid4.add_row([0, 0, 0])
        self.label_grid4.add_row([0, 0, 0])
        self.label_grid4.add_row([0, 0, 0])
        self.label_grid4.add_row([0, 0, 0])
        self.label_grid4.add_row([0, 0, 0])
        self.label_grid4.add_row([0, 0, 0])
        self.label_grid4.add_row([0, 0, 0])
        self.label_grid4.add_row([0, 0, 0])
        self.label_grid4.add_row([0, 0, 0])
        self.label_grid4.add_row([0, 0, 0])
        self.label_grid4.add_row([0, 0, 0])
        self.label_grid4.add_row([0, 0, 0])
        self.label_grid4.add_row([0, 0, 0])
        self.label_grid4.add_row([0, 0, 0])
        self.label_grid4.add_row([0, 0, 0])
        self.label_grid4.add_row([0, 0, 0])
        self.label_grid4.add_row([0, 0, 0])
        self.label_grid4.add_row([0, 0, 0])
        self.label_grid4.add_row([0, 0, 0])
        self.label_grid4.add_row([0, 0, 0])
        self.label_grid4.add_row([0, 0, 0])
        self.label_grid4.add_row([0, 0, 0])
        self.label_grid4.add_row([0, 0, 0])
        self.label_grid4.add_row([0, 0, 0])

        # Frame Consumo Hilo, Mediano

        self.ch_frame_row2 = CTkScrollableFrame(self.ch_frame, fg_color="gray85", height=300, orientation="vertical")
        # self.ch_frame_row2.grid(row=3, column=0, sticky="we", padx=10, pady=10)

        label_grid_mediano_1 = tk_tools.LabelGrid(self.ch_frame_row2, 2, ['Máquina/Puntada\n', 'Elemento de\nPuntada'], relief="flat")
        label_grid_mediano_1.grid(row=1, column=0, sticky='ewn')

        label_grid_mediano_1.add_row(["Plana 1A 301", "Aguja"])
        label_grid_mediano_1.add_row(["", "Bobina"])
        label_grid_mediano_1.add_row(["Plana 2A 301/2", "Aguja"])
        label_grid_mediano_1.add_row(["", "Bobinas"])
        label_grid_mediano_1.add_row(["Fileteadora 3 Hilos 504", "1 Aguja"])
        label_grid_mediano_1.add_row(["", "2 Loopers"])
        label_grid_mediano_1.add_row(["Puntada Seg. Cadeneta doble 1 Aguja 401", "Aguja"])
        label_grid_mediano_1.add_row(["", "Looper"])
        label_grid_mediano_1.add_row(["Cadeneta doble 1 Aguja 401", "Aguja"])
        label_grid_mediano_1.add_row(["", "Looper"])
        label_grid_mediano_1.add_row(["Recubridora 3 hilos 406", "2 Agujas"])
        label_grid_mediano_1.add_row(["", "1 Looper"])
        label_grid_mediano_1.add_row(["Recubridora 4 Hilos 602", "2 Agujas"])
        label_grid_mediano_1.add_row(["", "1 Looper"])
        label_grid_mediano_1.add_row(["", "Recubridor"])
        label_grid_mediano_1.add_row(["Recubridora 5 Hilos 605", "3 Agujas"])
        label_grid_mediano_1.add_row(["", "1 Looper"])
        label_grid_mediano_1.add_row(["", "Recubridor"])
        label_grid_mediano_1.add_row(["Presilladora 42 Puntadas (3045)", "Aguja/bob"])
        label_grid_mediano_1.add_row(["Ojaladora Recta 3043", "Aguja/bob"])
        label_grid_mediano_1.add_row(["Ojaladora Lágrima", "Aguja"])
        label_grid_mediano_1.add_row(["", "Looper"])
        label_grid_mediano_1.add_row(["", "Alma"])
        label_grid_mediano_1.add_row(["Botonadora 1012", "Aguja"])

        self.button_grid1_mediano = tk_tools.ButtonGrid(self.ch_frame_row2, 1, ['Punt. X \nPulg.'], relief="flat")
        self.button_grid1_mediano.grid(row=1, column=1, sticky='ewn')

        self.button_grid1_mediano.add_row([("Seleccione", lambda: self.ventana_combo_box_mediano("(Aguja)", 1))])
        self.button_grid1_mediano.add_row([("Seleccione", lambda: self.ventana_combo_box_mediano("(Bobina)", 2))])
        self.button_grid1_mediano.add_row([("Seleccione", lambda: self.ventana_combo_box_mediano("(Aguja)", 3))])
        self.button_grid1_mediano.add_row([("Seleccione", lambda: self.ventana_combo_box_mediano("(Bobinas)", 4))])
        self.button_grid1_mediano.add_row([("Seleccione", lambda: self.ventana_combo_box_mediano("(1 Aguja)", 5))])
        self.button_grid1_mediano.add_row([("Seleccione", lambda: self.ventana_combo_box_mediano("(2 Loopers)", 6))])
        self.button_grid1_mediano.add_row([("Seleccione", lambda: self.ventana_combo_box_mediano("(Aguja)", 7))])
        self.button_grid1_mediano.add_row([("Seleccione", lambda: self.ventana_combo_box_mediano("(Looper)", 8))])
        self.button_grid1_mediano.add_row([("Seleccione", lambda: self.ventana_combo_box_mediano("(Aguja)", 9))])
        self.button_grid1_mediano.add_row([("Seleccione", lambda: self.ventana_combo_box_mediano("(Looper)", 10))])
        self.button_grid1_mediano.add_row([("Seleccione", lambda: self.ventana_combo_box_mediano("(2 Agujas)", 11))])
        self.button_grid1_mediano.add_row([("Seleccione", lambda: self.ventana_combo_box_mediano("(1 Looper)", 12))])
        self.button_grid1_mediano.add_row([("Seleccione", lambda: self.ventana_combo_box_mediano("(2 Agujas)", 13))])
        self.button_grid1_mediano.add_row([("Seleccione", lambda: self.ventana_combo_box_mediano("(1 Looper)", 14))])
        self.button_grid1_mediano.add_row([("Seleccione", lambda: self.ventana_combo_box_mediano("(Recubridor)", 15))])
        self.button_grid1_mediano.add_row([("Seleccione", lambda: self.ventana_combo_box_mediano("(3 Agujas)", 16))])
        self.button_grid1_mediano.add_row([("Seleccione", lambda: self.ventana_combo_box_mediano("(1 Looper)", 17))])
        self.button_grid1_mediano.add_row([("Seleccione", lambda: self.ventana_combo_box_mediano("(Recubridor)", 18))])
        self.button_grid1_mediano.add_row([("Seleccione", lambda: self.ventana_combo_box_mediano("(Aguja/bob)", 19))])
        self.button_grid1_mediano.add_row([("Seleccione", lambda: self.ventana_combo_box_mediano("(Aguja/bob)", 20))])
        self.button_grid1_mediano.add_row([("Seleccione", lambda: self.ventana_combo_box_mediano("(Aguja)", 21))])
        self.button_grid1_mediano.add_row([("Seleccione", lambda: self.ventana_combo_box_mediano("(Looper)", 22))])
        self.button_grid1_mediano.add_row([("Seleccione", lambda: self.ventana_combo_box_mediano("(Alma)", 23))])
        self.button_grid1_mediano.add_row([("Seleccione", lambda: self.ventana_combo_box_mediano("(Aguja)", 24))])

        self.label_grid2_mediano = tk_tools.LabelGrid(self.ch_frame_row2, 3, ['M.H. a \n10 PPP.', 'M.H. a \n12 PPP.', 'M.H. a \n14 PPP.'],
                                         relief="flat")
        self.label_grid2_mediano.grid(row=1, column=2, sticky='ewn')

        self.label_grid2_mediano.add_row([1.23, 1.31, 1.44])
        self.label_grid2_mediano.add_row([1.23, 1.31, 1.44])
        self.label_grid2_mediano.add_row([2.46, 2.62, 2.88])
        self.label_grid2_mediano.add_row([2.46, 2.62, 2.88])
        self.label_grid2_mediano.add_row([1.55, 1.58, 1.61])
        self.label_grid2_mediano.add_row([11.51, 11.75, 12.12])
        self.label_grid2_mediano.add_row([2.21, 2.28, 2.39])
        self.label_grid2_mediano.add_row([3.12, 3.22, 3.37])
        self.label_grid2_mediano.add_row([2.21, 2.28, 2.39])
        self.label_grid2_mediano.add_row([3.12, 3.22, 3.37])
        self.label_grid2_mediano.add_row([5.15, 5.21, 5.31])
        self.label_grid2_mediano.add_row([8.26, 8.36, 8.52])
        self.label_grid2_mediano.add_row([5.07, 5.21, 5.31])
        self.label_grid2_mediano.add_row([8.14, 8.36, 8.52])
        self.label_grid2_mediano.add_row([4.06, 4.12, 4.18])
        self.label_grid2_mediano.add_row([8.59, 8.68, 8.80])
        self.label_grid2_mediano.add_row([8.59, 8.68, 8.80])
        self.label_grid2_mediano.add_row([6.40, 6.49, 6.61])
        self.label_grid2_mediano.add_row([0.39, 0.00, 0.00])
        self.label_grid2_mediano.add_row([0.47, 0.00, 0.00])
        self.label_grid2_mediano.add_row([0.65, 0.00, 0.00])
        self.label_grid2_mediano.add_row([0.15, 0.00, 0.00])
        self.label_grid2_mediano.add_row([0.10, 0.00, 0.00])
        self.label_grid2_mediano.add_row([0.13, 0.00, 0.00])

        self.entry_grid1_mediano = tk_tools.EntryGrid(self.ch_frame_row2, 1, ['Metros\nCostura'], relief="flat", width=10)
        self.entry_grid1_mediano.grid(row=1, column=3, sticky='ewn')

        self.entry_grid1_mediano.add_row(data=[0])
        self.entry_grid1_mediano.add_row(data=[0])
        self.entry_grid1_mediano.add_row(data=[0])
        self.entry_grid1_mediano.add_row(data=[0])
        self.entry_grid1_mediano.add_row(data=[0])
        self.entry_grid1_mediano.add_row(data=[0])
        self.entry_grid1_mediano.add_row(data=[0])
        self.entry_grid1_mediano.add_row(data=[0])
        self.entry_grid1_mediano.add_row(data=[0])
        self.entry_grid1_mediano.add_row(data=[0])
        self.entry_grid1_mediano.add_row(data=[0])
        self.entry_grid1_mediano.add_row(data=[0])
        self.entry_grid1_mediano.add_row(data=[0])
        self.entry_grid1_mediano.add_row(data=[0])
        self.entry_grid1_mediano.add_row(data=[0])
        self.entry_grid1_mediano.add_row(data=[0])
        self.entry_grid1_mediano.add_row(data=[0])
        self.entry_grid1_mediano.add_row(data=[0])
        self.entry_grid1_mediano.add_row(data=[0])
        self.entry_grid1_mediano.add_row(data=[0])
        self.entry_grid1_mediano.add_row(data=[0])
        self.entry_grid1_mediano.add_row(data=[0])
        self.entry_grid1_mediano.add_row(data=[0])
        self.entry_grid1_mediano.add_row(data=[0])

        self.label_grid3_mediano = tk_tools.LabelGrid(self.ch_frame_row2, 2, ['Metros\nHilo', 'Total\nHilo'], relief="flat")
        self.label_grid3_mediano.grid(row=1, column=4, sticky='ewn')

        self.label_grid3_mediano.add_row([0, 0])
        self.label_grid3_mediano.add_row([0, 0])
        self.label_grid3_mediano.add_row([0, 0])
        self.label_grid3_mediano.add_row([0, 0])
        self.label_grid3_mediano.add_row([0, 0])
        self.label_grid3_mediano.add_row([0, 0])
        self.label_grid3_mediano.add_row([0, 0])
        self.label_grid3_mediano.add_row([0, 0])
        self.label_grid3_mediano.add_row([0, 0])
        self.label_grid3_mediano.add_row([0, 0])
        self.label_grid3_mediano.add_row([0, 0])
        self.label_grid3_mediano.add_row([0, 0])
        self.label_grid3_mediano.add_row([0, 0])
        self.label_grid3_mediano.add_row([0, 0])
        self.label_grid3_mediano.add_row([0, 0])
        self.label_grid3_mediano.add_row([0, 0])
        self.label_grid3_mediano.add_row([0, 0])
        self.label_grid3_mediano.add_row([0, 0])
        self.label_grid3_mediano.add_row([0, 0])
        self.label_grid3_mediano.add_row([0, 0])
        self.label_grid3_mediano.add_row([0, 0])
        self.label_grid3_mediano.add_row([0, 0])
        self.label_grid3_mediano.add_row([0, 0])
        self.label_grid3_mediano.add_row([0, 0])

        self.button_grid2_mediano = tk_tools.ButtonGrid(self.ch_frame_row2, 2, ['Tipo de Hilo\n', 'Color -\nBco - Cdo'], relief="flat")
        self.button_grid2_mediano.grid(row=1, column=5, sticky='ewn')

        self.button_grid2_mediano.add_row([("Seleccione.", lambda: self.ventana_combo_box2_mediano(1)), ("Seleccione", lambda: self.ventana_combo_box3_mediano(1))])
        self.button_grid2_mediano.add_row([("Seleccione.", lambda: self.ventana_combo_box2_mediano(2)), ("Seleccione", lambda: self.ventana_combo_box3_mediano(2))])
        self.button_grid2_mediano.add_row([("Seleccione.", lambda: self.ventana_combo_box2_mediano(3)), ("Seleccione", lambda: self.ventana_combo_box3_mediano(3))])
        self.button_grid2_mediano.add_row([("Seleccione.", lambda: self.ventana_combo_box2_mediano(4)), ("Seleccione", lambda: self.ventana_combo_box3_mediano(4))])
        self.button_grid2_mediano.add_row([("Seleccione.", lambda: self.ventana_combo_box2_mediano(5)), ("Seleccione", lambda: self.ventana_combo_box3_mediano(5))])
        self.button_grid2_mediano.add_row([("Seleccione.", lambda: self.ventana_combo_box2_mediano(6)), ("Seleccione", lambda: self.ventana_combo_box3_mediano(6))])
        self.button_grid2_mediano.add_row([("Seleccione.", lambda: self.ventana_combo_box2_mediano(7)), ("Seleccione", lambda: self.ventana_combo_box3_mediano(7))])
        self.button_grid2_mediano.add_row([("Seleccione.", lambda: self.ventana_combo_box2_mediano(8)), ("Seleccione", lambda: self.ventana_combo_box3_mediano(8))])
        self.button_grid2_mediano.add_row([("Seleccione.", lambda: self.ventana_combo_box2_mediano(9)), ("Seleccione", lambda: self.ventana_combo_box3_mediano(9))])
        self.button_grid2_mediano.add_row([("Seleccione.", lambda: self.ventana_combo_box2_mediano(10)), ("Seleccione", lambda: self.ventana_combo_box3_mediano(10))])
        self.button_grid2_mediano.add_row([("Seleccione.", lambda: self.ventana_combo_box2_mediano(11)), ("Seleccione", lambda: self.ventana_combo_box3_mediano(11))])
        self.button_grid2_mediano.add_row([("Seleccione.", lambda: self.ventana_combo_box2_mediano(12)), ("Seleccione", lambda: self.ventana_combo_box3_mediano(12))])
        self.button_grid2_mediano.add_row([("Seleccione.", lambda: self.ventana_combo_box2_mediano(13)), ("Seleccione", lambda: self.ventana_combo_box3_mediano(13))])
        self.button_grid2_mediano.add_row([("Seleccione.", lambda: self.ventana_combo_box2_mediano(14)), ("Seleccione", lambda: self.ventana_combo_box3_mediano(14))])
        self.button_grid2_mediano.add_row([("Seleccione.", lambda: self.ventana_combo_box2_mediano(15)), ("Seleccione", lambda: self.ventana_combo_box3_mediano(15))])
        self.button_grid2_mediano.add_row([("Seleccione.", lambda: self.ventana_combo_box2_mediano(16)), ("Seleccione", lambda: self.ventana_combo_box3_mediano(16))])
        self.button_grid2_mediano.add_row([("Seleccione.", lambda: self.ventana_combo_box2_mediano(17)), ("Seleccione", lambda: self.ventana_combo_box3_mediano(17))])
        self.button_grid2_mediano.add_row([("Seleccione.", lambda: self.ventana_combo_box2_mediano(18)), ("Seleccione", lambda: self.ventana_combo_box3_mediano(18))])
        self.button_grid2_mediano.add_row([("Seleccione.", lambda: self.ventana_combo_box2_mediano(19)), ("Seleccione", lambda: self.ventana_combo_box3_mediano(19))])
        self.button_grid2_mediano.add_row([("Seleccione.", lambda: self.ventana_combo_box2_mediano(20)), ("Seleccione", lambda: self.ventana_combo_box3_mediano(20))])
        self.button_grid2_mediano.add_row([("Seleccione.", lambda: self.ventana_combo_box2_mediano(21)), ("Seleccione", lambda: self.ventana_combo_box3_mediano(21))])
        self.button_grid2_mediano.add_row([("Seleccione.", lambda: self.ventana_combo_box2_mediano(22)), ("Seleccione", lambda: self.ventana_combo_box3_mediano(22))])
        self.button_grid2_mediano.add_row([("Seleccione.", lambda: self.ventana_combo_box2_mediano(23)), ("Seleccione", lambda: self.ventana_combo_box3_mediano(23))])
        self.button_grid2_mediano.add_row([("Seleccione.", lambda: self.ventana_combo_box2_mediano(24)), ("Seleccione", lambda: self.ventana_combo_box3_mediano(24))])

        self.label_grid4_mediano = tk_tools.LabelGrid(self.ch_frame_row2, 3, ['Total metros\nproducc', 'Total\nConos', 'Total conos\nredondeado'], relief="flat")
        self.label_grid4_mediano.grid(row=1, column=6, sticky='ewn')

        self.label_grid4_mediano.add_row([0, 0, 0])
        self.label_grid4_mediano.add_row([0, 0, 0])
        self.label_grid4_mediano.add_row([0, 0, 0])
        self.label_grid4_mediano.add_row([0, 0, 0])
        self.label_grid4_mediano.add_row([0, 0, 0])
        self.label_grid4_mediano.add_row([0, 0, 0])
        self.label_grid4_mediano.add_row([0, 0, 0])
        self.label_grid4_mediano.add_row([0, 0, 0])
        self.label_grid4_mediano.add_row([0, 0, 0])
        self.label_grid4_mediano.add_row([0, 0, 0])
        self.label_grid4_mediano.add_row([0, 0, 0])
        self.label_grid4_mediano.add_row([0, 0, 0])
        self.label_grid4_mediano.add_row([0, 0, 0])
        self.label_grid4_mediano.add_row([0, 0, 0])
        self.label_grid4_mediano.add_row([0, 0, 0])
        self.label_grid4_mediano.add_row([0, 0, 0])
        self.label_grid4_mediano.add_row([0, 0, 0])
        self.label_grid4_mediano.add_row([0, 0, 0])
        self.label_grid4_mediano.add_row([0, 0, 0])
        self.label_grid4_mediano.add_row([0, 0, 0])
        self.label_grid4_mediano.add_row([0, 0, 0])
        self.label_grid4_mediano.add_row([0, 0, 0])
        self.label_grid4_mediano.add_row([0, 0, 0])
        self.label_grid4_mediano.add_row([0, 0, 0])

        # Frame Consumo Hilo, Pesado

        self.ch_frame_row3 = CTkScrollableFrame(self.ch_frame, fg_color="gray85", height=300, orientation="vertical")
        # self.ch_frame_col3.grid(row=3, column=0, sticky="we", padx=10, pady=10)

        label_grid_pesado_1 = tk_tools.LabelGrid(self.ch_frame_row3, 2, ['Máquina/Puntada\n', 'Elemento de\nPuntada'], relief="flat")
        label_grid_pesado_1.grid(row=1, column=0, sticky='ewn')

        label_grid_pesado_1.add_row(["Plana 1A 301", "Aguja"])
        label_grid_pesado_1.add_row(["", "Bobina"])
        label_grid_pesado_1.add_row(["Plana 2A 301/2", "Aguja"])
        label_grid_pesado_1.add_row(["", "Bobinas"])
        label_grid_pesado_1.add_row(["Fileteadora 3 Hilos 504", "Aguja"])
        label_grid_pesado_1.add_row(["", "Loopers"])
        label_grid_pesado_1.add_row(["Puntada Seg. Cadeneta doble 1 Aguja 401", "Aguja"])
        label_grid_pesado_1.add_row(["", "Looper"])
        label_grid_pesado_1.add_row(["Cadeneta doble 1 Aguja 401", "Agujas"])
        label_grid_pesado_1.add_row(["", "Loopers"])
        label_grid_pesado_1.add_row(["Recubridora 3 hilos 406", "Agujas"])
        label_grid_pesado_1.add_row(["", "Loopers"])
        label_grid_pesado_1.add_row(["Recubridora 4 Hilos 602", "Agujas"])
        label_grid_pesado_1.add_row(["", "Looper"])
        label_grid_pesado_1.add_row(["", "Recubridor"])
        label_grid_pesado_1.add_row(["Presilladora 36 Puntadas 3045", "Aguja/bob"])
        label_grid_pesado_1.add_row(["Ojaladora Recta 3043", "Aguja/bob"])
        label_grid_pesado_1.add_row(["Ojaladora Lágrima", "Aguja"])
        label_grid_pesado_1.add_row(["", "Looper"])
        label_grid_pesado_1.add_row(["", "Alma"])
        label_grid_pesado_1.add_row(["Botonadora 1020", "Aguja"])

        self.button_grid1_pesado = tk_tools.ButtonGrid(self.ch_frame_row3, 1, ['Punt. X \nPulg.'], relief="flat")
        self.button_grid1_pesado.grid(row=1, column=1, sticky='ewn')

        self.button_grid1_pesado.add_row([("Seleccione", lambda: self.ventana_combo_box_pesado("(Aguja)", 1))])
        self.button_grid1_pesado.add_row([("Seleccione", lambda: self.ventana_combo_box_pesado("(Bobina)", 2))])
        self.button_grid1_pesado.add_row([("Seleccione", lambda: self.ventana_combo_box_pesado("(Aguja)", 3))])
        self.button_grid1_pesado.add_row([("Seleccione", lambda: self.ventana_combo_box_pesado("(Bobinas)", 4))])
        self.button_grid1_pesado.add_row([("Seleccione", lambda: self.ventana_combo_box_pesado("(Aguja)", 5))])
        self.button_grid1_pesado.add_row([("Seleccione", lambda: self.ventana_combo_box_pesado("(Loopers)", 6))])
        self.button_grid1_pesado.add_row([("Seleccione", lambda: self.ventana_combo_box_pesado("(Aguja)", 7))])
        self.button_grid1_pesado.add_row([("Seleccione", lambda: self.ventana_combo_box_pesado("(Looper)", 8))])
        self.button_grid1_pesado.add_row([("Seleccione", lambda: self.ventana_combo_box_pesado("(Agujas)", 9))])
        self.button_grid1_pesado.add_row([("Seleccione", lambda: self.ventana_combo_box_pesado("(Loopers)", 10))])
        self.button_grid1_pesado.add_row([("Seleccione", lambda: self.ventana_combo_box_pesado("(Agujas)", 11))])
        self.button_grid1_pesado.add_row([("Seleccione", lambda: self.ventana_combo_box_pesado("(Loopers)", 12))])
        self.button_grid1_pesado.add_row([("Seleccione", lambda: self.ventana_combo_box_pesado("(Agujas)", 13))])
        self.button_grid1_pesado.add_row([("Seleccione", lambda: self.ventana_combo_box_pesado("(Looper)", 14))])
        self.button_grid1_pesado.add_row([("Seleccione", lambda: self.ventana_combo_box_pesado("(Recubridor)", 15))])
        self.button_grid1_pesado.add_row([("Seleccione", lambda: self.ventana_combo_box_pesado("(Aguja/bob)", 16))])
        self.button_grid1_pesado.add_row([("Seleccione", lambda: self.ventana_combo_box_pesado("(Aguja/bob)", 17))])
        self.button_grid1_pesado.add_row([("Seleccione", lambda: self.ventana_combo_box_pesado("(Aguja)", 18))])
        self.button_grid1_pesado.add_row([("Seleccione", lambda: self.ventana_combo_box_pesado("(Looper)", 19))])
        self.button_grid1_pesado.add_row([("Seleccione", lambda: self.ventana_combo_box_pesado("(Alma)", 20))])
        self.button_grid1_pesado.add_row([("Seleccione", lambda: self.ventana_combo_box_pesado("(Aguja)", 21))])

        self.label_grid2_pesado = tk_tools.LabelGrid(self.ch_frame_row3, 3, ['M.H. a \n10 PPP.', 'M.H. a \n12 PPP.', 'M.H. a \n14 PPP.'],
                                         relief="flat")
        self.label_grid2_pesado.grid(row=1, column=2, sticky='ewn')

        self.label_grid2_pesado.add_row([2.58, 2.94, 2.14])
        self.label_grid2_pesado.add_row([1.48, 1.69, 1.23])
        self.label_grid2_pesado.add_row([5.16, 5.88, 4.28])
        self.label_grid2_pesado.add_row([2.96, 3.38, 2.46])
        self.label_grid2_pesado.add_row([1.80, 1.82, 1.77])
        self.label_grid2_pesado.add_row([13.22, 13.40, 12.89])
        self.label_grid2_pesado.add_row([4.73, 5.40, 3.93])
        self.label_grid2_pesado.add_row([3.44, 3.93, 2.86])
        self.label_grid2_pesado.add_row([4.73, 5.40, 3.93])
        self.label_grid2_pesado.add_row([3.44, 3.93, 2.86])
        self.label_grid2_pesado.add_row([5.96, 6.02, 5.88])
        self.label_grid2_pesado.add_row([9.56, 9.66, 9.43])
        self.label_grid2_pesado.add_row([5.96, 6.02, 5.88])
        self.label_grid2_pesado.add_row([9.56, 9.66, 9.43])
        self.label_grid2_pesado.add_row([4.06, 4.12, 4.00])
        self.label_grid2_pesado.add_row([0.00, 0.00, 0.45])
        self.label_grid2_pesado.add_row([0.00, 0.00, 0.54])
        self.label_grid2_pesado.add_row([0.00, 0.00, 0.76])
        self.label_grid2_pesado.add_row([0.00, 0.00, 0.17])
        self.label_grid2_pesado.add_row([0.00, 0.00, 0.12])
        self.label_grid2_pesado.add_row([0.00, 0.00, 0.15])

        self.entry_grid1_pesado = tk_tools.EntryGrid(self.ch_frame_row3, 1, ['Metros\nCostura'], relief="flat", width=10)
        self.entry_grid1_pesado.grid(row=1, column=3, sticky='ewn')

        self.entry_grid1_pesado.add_row(data=[0])
        self.entry_grid1_pesado.add_row(data=[0])
        self.entry_grid1_pesado.add_row(data=[0])
        self.entry_grid1_pesado.add_row(data=[0])
        self.entry_grid1_pesado.add_row(data=[0])
        self.entry_grid1_pesado.add_row(data=[0])
        self.entry_grid1_pesado.add_row(data=[0])
        self.entry_grid1_pesado.add_row(data=[0])
        self.entry_grid1_pesado.add_row(data=[0])
        self.entry_grid1_pesado.add_row(data=[0])
        self.entry_grid1_pesado.add_row(data=[0])
        self.entry_grid1_pesado.add_row(data=[0])
        self.entry_grid1_pesado.add_row(data=[0])
        self.entry_grid1_pesado.add_row(data=[0])
        self.entry_grid1_pesado.add_row(data=[0])
        self.entry_grid1_pesado.add_row(data=[0])
        self.entry_grid1_pesado.add_row(data=[0])
        self.entry_grid1_pesado.add_row(data=[0])
        self.entry_grid1_pesado.add_row(data=[0])
        self.entry_grid1_pesado.add_row(data=[0])
        self.entry_grid1_pesado.add_row(data=[0])

        self.label_grid3_pesado = tk_tools.LabelGrid(self.ch_frame_row3, 2, ['Metros\nHilo', 'Total\nHilo'], relief="flat")
        self.label_grid3_pesado.grid(row=1, column=4, sticky='ewn')

        self.label_grid3_pesado.add_row([0, 0])
        self.label_grid3_pesado.add_row([0, 0])
        self.label_grid3_pesado.add_row([0, 0])
        self.label_grid3_pesado.add_row([0, 0])
        self.label_grid3_pesado.add_row([0, 0])
        self.label_grid3_pesado.add_row([0, 0])
        self.label_grid3_pesado.add_row([0, 0])
        self.label_grid3_pesado.add_row([0, 0])
        self.label_grid3_pesado.add_row([0, 0])
        self.label_grid3_pesado.add_row([0, 0])
        self.label_grid3_pesado.add_row([0, 0])
        self.label_grid3_pesado.add_row([0, 0])
        self.label_grid3_pesado.add_row([0, 0])
        self.label_grid3_pesado.add_row([0, 0])
        self.label_grid3_pesado.add_row([0, 0])
        self.label_grid3_pesado.add_row([0, 0])
        self.label_grid3_pesado.add_row([0, 0])
        self.label_grid3_pesado.add_row([0, 0])
        self.label_grid3_pesado.add_row([0, 0])
        self.label_grid3_pesado.add_row([0, 0])
        self.label_grid3_pesado.add_row([0, 0])

        self.button_grid2_pesado = tk_tools.ButtonGrid(self.ch_frame_row3, 2, ['Tipo de Hilo\n', 'Color -\nBco - Cdo'], relief="flat")
        self.button_grid2_pesado.grid(row=1, column=5, sticky='ewn')

        self.button_grid2_pesado.add_row([("Seleccione.", lambda: self.ventana_combo_box2_pesado(1)), ("Seleccione", lambda: self.ventana_combo_box3_pesado(1))])
        self.button_grid2_pesado.add_row([("Seleccione.", lambda: self.ventana_combo_box2_pesado(2)), ("Seleccione", lambda: self.ventana_combo_box3_pesado(2))])
        self.button_grid2_pesado.add_row([("Seleccione.", lambda: self.ventana_combo_box2_pesado(3)), ("Seleccione", lambda: self.ventana_combo_box3_pesado(3))])
        self.button_grid2_pesado.add_row([("Seleccione.", lambda: self.ventana_combo_box2_pesado(4)), ("Seleccione", lambda: self.ventana_combo_box3_pesado(4))])
        self.button_grid2_pesado.add_row([("Seleccione.", lambda: self.ventana_combo_box2_pesado(5)), ("Seleccione", lambda: self.ventana_combo_box3_pesado(5))])
        self.button_grid2_pesado.add_row([("Seleccione.", lambda: self.ventana_combo_box2_pesado(6)), ("Seleccione", lambda: self.ventana_combo_box3_pesado(6))])
        self.button_grid2_pesado.add_row([("Seleccione.", lambda: self.ventana_combo_box2_pesado(7)), ("Seleccione", lambda: self.ventana_combo_box3_pesado(7))])
        self.button_grid2_pesado.add_row([("Seleccione.", lambda: self.ventana_combo_box2_pesado(8)), ("Seleccione", lambda: self.ventana_combo_box3_pesado(8))])
        self.button_grid2_pesado.add_row([("Seleccione.", lambda: self.ventana_combo_box2_pesado(9)), ("Seleccione", lambda: self.ventana_combo_box3_pesado(9))])
        self.button_grid2_pesado.add_row([("Seleccione.", lambda: self.ventana_combo_box2_pesado(10)), ("Seleccione", lambda: self.ventana_combo_box3_pesado(10))])
        self.button_grid2_pesado.add_row([("Seleccione.", lambda: self.ventana_combo_box2_pesado(11)), ("Seleccione", lambda: self.ventana_combo_box3_pesado(11))])
        self.button_grid2_pesado.add_row([("Seleccione.", lambda: self.ventana_combo_box2_pesado(12)), ("Seleccione", lambda: self.ventana_combo_box3_pesado(12))])
        self.button_grid2_pesado.add_row([("Seleccione.", lambda: self.ventana_combo_box2_pesado(13)), ("Seleccione", lambda: self.ventana_combo_box3_pesado(13))])
        self.button_grid2_pesado.add_row([("Seleccione.", lambda: self.ventana_combo_box2_pesado(14)), ("Seleccione", lambda: self.ventana_combo_box3_pesado(14))])
        self.button_grid2_pesado.add_row([("Seleccione.", lambda: self.ventana_combo_box2_pesado(15)), ("Seleccione", lambda: self.ventana_combo_box3_pesado(15))])
        self.button_grid2_pesado.add_row([("Seleccione.", lambda: self.ventana_combo_box2_pesado(16)), ("Seleccione", lambda: self.ventana_combo_box3_pesado(16))])
        self.button_grid2_pesado.add_row([("Seleccione.", lambda: self.ventana_combo_box2_pesado(17)), ("Seleccione", lambda: self.ventana_combo_box3_pesado(17))])
        self.button_grid2_pesado.add_row([("Seleccione.", lambda: self.ventana_combo_box2_pesado(18)), ("Seleccione", lambda: self.ventana_combo_box3_pesado(18))])
        self.button_grid2_pesado.add_row([("Seleccione.", lambda: self.ventana_combo_box2_pesado(19)), ("Seleccione", lambda: self.ventana_combo_box3_pesado(19))])
        self.button_grid2_pesado.add_row([("Seleccione.", lambda: self.ventana_combo_box2_pesado(20)), ("Seleccione", lambda: self.ventana_combo_box3_pesado(20))])
        self.button_grid2_pesado.add_row([("Seleccione.", lambda: self.ventana_combo_box2_pesado(21)), ("Seleccione", lambda: self.ventana_combo_box3_pesado(21))])

        self.label_grid4_pesado = tk_tools.LabelGrid(self.ch_frame_row3, 3, ['Total metros\nproducc', 'Total\nConos', 'Total conos\nredondeado'], relief="flat")
        self.label_grid4_pesado.grid(row=1, column=6, sticky='ewn')

        self.label_grid4_pesado.add_row([0, 0, 0])
        self.label_grid4_pesado.add_row([0, 0, 0])
        self.label_grid4_pesado.add_row([0, 0, 0])
        self.label_grid4_pesado.add_row([0, 0, 0])
        self.label_grid4_pesado.add_row([0, 0, 0])
        self.label_grid4_pesado.add_row([0, 0, 0])
        self.label_grid4_pesado.add_row([0, 0, 0])
        self.label_grid4_pesado.add_row([0, 0, 0])
        self.label_grid4_pesado.add_row([0, 0, 0])
        self.label_grid4_pesado.add_row([0, 0, 0])
        self.label_grid4_pesado.add_row([0, 0, 0])
        self.label_grid4_pesado.add_row([0, 0, 0])
        self.label_grid4_pesado.add_row([0, 0, 0])
        self.label_grid4_pesado.add_row([0, 0, 0])
        self.label_grid4_pesado.add_row([0, 0, 0])
        self.label_grid4_pesado.add_row([0, 0, 0])
        self.label_grid4_pesado.add_row([0, 0, 0])
        self.label_grid4_pesado.add_row([0, 0, 0])
        self.label_grid4_pesado.add_row([0, 0, 0])
        self.label_grid4_pesado.add_row([0, 0, 0])
        self.label_grid4_pesado.add_row([0, 0, 0])

        # Total
        self.ch_frame_row4 = CTkFrame(self.ch_frame)
        # self.ch_frame_row4.grid(row=6, column=0, sticky="wen", padx=0, pady=10)

        """self.ch_resultados_l1 = tk_tools.LabelGrid(self.ch_frame_row4, 9, [
            ' TOTAL                                               ',
            '                          ',
            '                       ',
            '               ',
            '                ',
            '                ',
            ' TOTAL ',
            ' 0.00 ',
            ' 0.00 '
        ], relief="flat")
        self.ch_resultados_l1.grid(row=0, column=0, sticky='w', columnspan="6", padx=0, pady=0)"""

        self.ch_resultados_total = CTkLabel(self.ch_frame, text="TOTAL consumo Hilo:", font=('Calibri', 14, 'bold'))

        self.ch_resultados_tutul = CTkLabel(self.ch_frame, text="Escriba un sólo costo del hilo según la calidad deseada:")

        subframe = CTkFrame(self.ch_frame_row4, height=30)
        self.ch_label3 = CTkLabel(subframe, text="Sol:").place(relx=0.3)
        self.ch_sol = CTkEntry(subframe, justify="right")
        self.ch_sol.place(relx=0.4)
        self.ch_sol.insert(0, "0")
        subframe.pack(expand=True, fill=X, side=LEFT)

        subframe2 = CTkFrame(self.ch_frame_row4, height=30)
        self.ch_label4 = CTkLabel(subframe2, text="Astra:").place(relx=0.3)
        self.ch_astra = CTkEntry(subframe2, justify="right")
        self.ch_astra.place(relx=0.4)
        self.ch_astra.insert(0, "0")
        subframe2.pack(expand=True, fill=X, side=LEFT)

        subframe3 = CTkFrame(self.ch_frame_row4, height=30)
        self.ch_label5 = CTkLabel(subframe3, text="Epic:").place(relx=0.3)
        self.ch_epic = CTkEntry(subframe3, justify="right")
        self.ch_epic.place(relx=0.4)
        self.ch_epic.insert(0, "0")
        subframe3.pack(expand=True, fill=X, side=LEFT)

        self.ch_frame_button2 = CTkButton(self.ch_frame, text="Guardar consumo de Hilo del Producto",
                                         image=self.img_save,
                                         fg_color="#0AA316",
                                         text_color="#FFFFFF",
                                         hover_color="#0A7E15",
                                         command=self.consumo_hilo_guardar)


        # ejemplo del totalizador
        """print(self.ch_resultados_l1.headers[7].cget("text"))
        print(self.ch_resultados_l1.headers[8].cget("text"))
        self.ch_resultados_l1.headers[7].configure(text=f"{1.1:0.2f}")
        self.ch_resultados_l1.headers[8].configure(text=f"{2.1:0.2f}")"""
        # fin - ejemplo del totalizador
        # Fin - fila final ***************

        # FIN - consumo hilo frame

        # select default frame
        self.materiales_menu_event()
        self.select_frame_by_name("consumo_hilo")    # home

    def calc_parcial_ch(self, option, destino):
        desperdicio = float(self.ch_entry1.get())
        metros_costura = float(self.entry_grid1._rows[destino - 1][0].get())

        # inicio de cálculos
        if option == "0":
            mh = 0
        elif option == "10":
            # Obtener texto actual de botón
            mh = float(self.label_grid2._rows[destino - 1][0].cget("text"))
        elif option == "12":
            # Obtener texto actual de botón
            mh = float(self.label_grid2._rows[destino - 1][1].cget("text"))
        else:
            # Obtener texto actual de botón
            mh = float(self.label_grid2._rows[destino - 1][2].cget("text"))
        # fin cálculos

        metros_hilo = mh * metros_costura
        self.label_grid3._rows[destino - 1][0].configure(text=f"{metros_hilo:0.2f}")

        total_hilo_prenda = metros_hilo + (metros_hilo * desperdicio/100)
        self.label_grid3._rows[destino - 1][1].configure(text=f"{total_hilo_prenda:0.2f}")

        self.label_grid4._rows[destino - 1][0].configure(text=f"{total_hilo_prenda:0.2f}")

        # Cambiar textos a opción seleccionada y cálculos
        if option == "0":
            option = "Seleccione"
            self.button_grid1._rows[destino - 1][0].configure(fg="black")
        else:
            # self.button_grid1._rows[destino - 1][0].configure(bg='blue')
            self.button_grid1._rows[destino - 1][0].configure(fg="purple")

        self.button_grid1._rows[destino - 1][0].configure(text=f"{option}")

        # Cerramos la ventana
        self.ventana_ch.destroy()

    def calc_parcial_ch2(self, option, destino):
        total_metros_producc = float(self.label_grid4._rows[destino - 1][0].cget("text"))
        try:
            yardas = int(option.split("-")[2])
        except:
            yardas = 0
        # inicio de cálculos
        if yardas == 0:
            conos = 0
            conos_redondeado = 0
        else:
            # redondeo especial hacia arriba a dos decimales
            from decimal import Decimal, ROUND_UP
            import math
            conos = Decimal(total_metros_producc/yardas).quantize(Decimal('.01'), rounding=ROUND_UP)
            conos_redondeado = math.ceil(conos)

        # fin cálculos

        self.label_grid4._rows[destino - 1][1].configure(text=f"{conos:0.2f}")
        self.label_grid4._rows[destino - 1][2].configure(text=f"{conos_redondeado:0.0f}")

        # Cambiar textos a opción seleccionada y cálculos
        if yardas == 0:
            option = "Seleccione"
            self.button_grid2._rows[destino - 1][0].configure(fg="black")
        else:
            self.button_grid2._rows[destino - 1][0].configure(fg="purple")

        self.button_grid2._rows[destino - 1][0].configure(text=f"{option}")

        # Cerramos la ventana
        self.ventana_ch2.destroy()

    def calc_parcial_ch3(self, option, destino):

        # inicio de cálculos
        # fin - calculos

        # Cambiar textos a opción seleccionada y cálculos
        if option == "0":
            option = "Seleccione"
            self.button_grid2._rows[destino - 1][1].configure(fg="black")
        else:
            self.button_grid2._rows[destino - 1][1].configure(fg="purple")

        self.button_grid2._rows[destino - 1][1].configure(text=f"{option}")

        # Cerramos la ventana
        self.ventana_ch3.destroy()

    def calc_parcial_ch_mediano(self, option, destino):
        desperdicio = float(self.ch_entry1.get())
        metros_costura = float(self.entry_grid1_mediano._rows[destino - 1][0].get())

        # inicio de cálculos
        if option == "0":
            mh = 0
        elif option == "10":
            # Obtener texto actual de botón
            mh = float(self.label_grid2_mediano._rows[destino - 1][0].cget("text"))
        elif option == "12":
            # Obtener texto actual de botón
            mh = float(self.label_grid2_mediano._rows[destino - 1][1].cget("text"))
        else:
            # Obtener texto actual de botón
            mh = float(self.label_grid2_mediano._rows[destino - 1][2].cget("text"))
        # fin cálculos

        metros_hilo = mh * metros_costura
        self.label_grid3_mediano._rows[destino - 1][0].configure(text=f"{metros_hilo:0.2f}")

        total_hilo_prenda = metros_hilo + (metros_hilo * desperdicio/100)
        self.label_grid3_mediano._rows[destino - 1][1].configure(text=f"{total_hilo_prenda:0.2f}")

        self.label_grid4_mediano._rows[destino - 1][0].configure(text=f"{total_hilo_prenda:0.2f}")

        # Cambiar textos a opción seleccionada y cálculos
        if option == "0":
            option = "Seleccione"
            self.button_grid1_mediano._rows[destino - 1][0].configure(fg="black")
        else:
            # self.button_grid1_mediano._rows[destino - 1][0].configure(bg='blue')
            self.button_grid1_mediano._rows[destino - 1][0].configure(fg="purple")

        self.button_grid1_mediano._rows[destino - 1][0].configure(text=f"{option}")

        # Cerramos la ventana
        self.ventana_ch_mediano.destroy()

    def calc_parcial_ch2_mediano(self, option, destino):
        total_metros_producc = float(self.label_grid4_mediano._rows[destino - 1][0].cget("text"))
        try:
            yardas = int(option.split("-")[2])
        except:
            yardas = 0
        # inicio de cálculos
        if yardas == 0:
            conos = 0
            conos_redondeado = 0
        else:
            # redondeo especial hacia arriba a dos decimales
            from decimal import Decimal, ROUND_UP
            import math
            conos = Decimal(total_metros_producc/yardas).quantize(Decimal('.01'), rounding=ROUND_UP)
            conos_redondeado = math.ceil(conos)

        # fin cálculos

        self.label_grid4_mediano._rows[destino - 1][1].configure(text=f"{conos:0.2f}")
        self.label_grid4_mediano._rows[destino - 1][2].configure(text=f"{conos_redondeado:0.0f}")

        # Cambiar textos a opción seleccionada y cálculos
        if yardas == 0:
            option = "Seleccione"
            self.button_grid2_mediano._rows[destino - 1][0].configure(fg="black")
        else:
            self.button_grid2_mediano._rows[destino - 1][0].configure(fg="purple")

        self.button_grid2_mediano._rows[destino - 1][0].configure(text=f"{option}")

        # Cerramos la ventana
        self.ventana_ch2_mediano.destroy()

    def calc_parcial_ch3_mediano(self, option, destino):

        # inicio de cálculos
        # fin - calculos

        # Cambiar textos a opción seleccionada y cálculos
        if option == "0":
            option = "Seleccione"
            self.button_grid2_mediano._rows[destino - 1][1].configure(fg="black")
        else:
            self.button_grid2_mediano._rows[destino - 1][1].configure(fg="purple")

        self.button_grid2_mediano._rows[destino - 1][1].configure(text=f"{option}")

        # Cerramos la ventana
        self.ventana_ch3_mediano.destroy()

    def calc_parcial_ch_pesado(self, option, destino):
        desperdicio = float(self.ch_entry1.get())
        metros_costura = float(self.entry_grid1_pesado._rows[destino - 1][0].get())

        # inicio de cálculos
        if option == "0":
            mh = 0
        elif option == "10":
            # Obtener texto actual de botón
            mh = float(self.label_grid2_pesado._rows[destino - 1][0].cget("text"))
        elif option == "12":
            # Obtener texto actual de botón
            mh = float(self.label_grid2_pesado._rows[destino - 1][1].cget("text"))
        else:
            # Obtener texto actual de botón
            mh = float(self.label_grid2_pesado._rows[destino - 1][2].cget("text"))
        # fin cálculos

        metros_hilo = mh * metros_costura
        self.label_grid3_pesado._rows[destino - 1][0].configure(text=f"{metros_hilo:0.2f}")

        total_hilo_prenda = metros_hilo + (metros_hilo * desperdicio/100)
        self.label_grid3_pesado._rows[destino - 1][1].configure(text=f"{total_hilo_prenda:0.2f}")

        self.label_grid4_pesado._rows[destino - 1][0].configure(text=f"{total_hilo_prenda:0.2f}")

        # Cambiar textos a opción seleccionada y cálculos
        if option == "0":
            option = "Seleccione"
            self.button_grid1_pesado._rows[destino - 1][0].configure(fg="black")
        else:
            # self.button_grid1_pesado._rows[destino - 1][0].configure(bg='blue')
            self.button_grid1_pesado._rows[destino - 1][0].configure(fg="purple")

        self.button_grid1_pesado._rows[destino - 1][0].configure(text=f"{option}")

        # Cerramos la ventana
        self.ventana_ch_pesado.destroy()

    def calc_parcial_ch2_pesado(self, option, destino):
        total_metros_producc = float(self.label_grid4_pesado._rows[destino - 1][0].cget("text"))
        try:
            yardas = int(option.split("-")[2])
        except:
            yardas = 0
        # inicio de cálculos
        if yardas == 0:
            conos = 0
            conos_redondeado = 0
        else:
            # redondeo especial hacia arriba a dos decimales
            from decimal import Decimal, ROUND_UP
            import math
            conos = Decimal(total_metros_producc/yardas).quantize(Decimal('.01'), rounding=ROUND_UP)
            conos_redondeado = math.ceil(conos)

        # fin cálculos

        self.label_grid4_pesado._rows[destino - 1][1].configure(text=f"{conos:0.2f}")
        self.label_grid4_pesado._rows[destino - 1][2].configure(text=f"{conos_redondeado:0.0f}")

        # Cambiar textos a opción seleccionada y cálculos
        if yardas == 0:
            option = "Seleccione"
            self.button_grid2_pesado._rows[destino - 1][0].configure(fg="black")
        else:
            self.button_grid2_pesado._rows[destino - 1][0].configure(fg="purple")

        self.button_grid2_pesado._rows[destino - 1][0].configure(text=f"{option}")

        # Cerramos la ventana
        self.ventana_ch2_pesado.destroy()

    def calc_parcial_ch3_pesado(self, option, destino):

        # inicio de cálculos
        # fin - calculos

        # Cambiar textos a opción seleccionada y cálculos
        if option == "0":
            option = "Seleccione"
            self.button_grid2_pesado._rows[destino - 1][1].configure(fg="black")
        else:
            self.button_grid2_pesado._rows[destino - 1][1].configure(fg="purple")

        self.button_grid2_pesado._rows[destino - 1][1].configure(text=f"{option}")

        # Cerramos la ventana
        self.ventana_ch3_pesado.destroy()

    def ventana_combo_box(self, title, mensaje, origen):
        # Crear una ventana secundaria.
        self.ventana_ch = CTkToplevel()
        self.ventana_ch.resizable(False, False)
        # quitar minimizar....
        self.ventana_ch.title(title)

        ancho_ventana = 400
        alto_ventana = 350
        x_ventana = self.winfo_screenwidth() // 2 - ancho_ventana // 2
        y_ventana = self.winfo_screenheight() // 2 - alto_ventana // 2

        posicion = str(ancho_ventana) + "x" + str(alto_ventana) + "+" + str(x_ventana) + "+" + str(y_ventana)
        self.ventana_ch.geometry(posicion)
        self.ventana_ch.focus()

        self.ventana_ch.columnconfigure(0, weight=1)
        self.ventana_ch.columnconfigure(1, weight=2)

        # GUI
        CTkLabel(self.ventana_ch, text=mensaje, font=("Calibri", 14, "bold"), justify="center").grid(row=0, column=0, columnspan=2, pady=10, sticky="ew")

        # Botones
        CTkButton(self.ventana_ch, text='0', command=lambda: self.calc_parcial_ch("0", origen)).grid(row=1, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch, text='8', command=lambda: self.calc_parcial_ch("8", origen)).grid(row=1, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch, text='10', command=lambda: self.calc_parcial_ch("10", origen)).grid(row=2, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch, text='12', command=lambda: self.calc_parcial_ch("12", origen)).grid(row=2, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch, text='14', command=lambda: self.calc_parcial_ch("14", origen)).grid(row=3, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch, text='16', command=lambda: self.calc_parcial_ch("16", origen)).grid(row=3, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch, text='20', command=lambda: self.calc_parcial_ch("20", origen)).grid(row=4, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch, text='28', command=lambda: self.calc_parcial_ch("28", origen)).grid(row=4, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch, text='32', command=lambda: self.calc_parcial_ch("32", origen)).grid(row=5, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch, text='36', command=lambda: self.calc_parcial_ch("36", origen)).grid(row=5, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch, text='42', command=lambda: self.calc_parcial_ch("42", origen)).grid(row=6, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch, text='82', command=lambda: self.calc_parcial_ch("82", origen)).grid(row=6, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch, text='96', command=lambda: self.calc_parcial_ch("96", origen)).grid(row=7, column=0, padx=10, pady=5, sticky="ew")

        # fin - GUI

        # Modal...
        self.ventana_ch.transient(self)  # dialog window is related to main
        self.ventana_ch.wait_visibility()
        self.ventana_ch.grab_set()
        self.ventana_ch.wait_window()

    def ventana_combo_box2(self, origen):
        # Crear una ventana secundaria.
        self.ventana_ch2 = CTkToplevel()
        self.ventana_ch2.resizable(False, False)
        # quitar minimizar....
        self.ventana_ch2.title("Tipo de Hilo")

        ancho_ventana = 510
        alto_ventana = 350
        x_ventana = self.winfo_screenwidth() // 2 - ancho_ventana // 2
        y_ventana = self.winfo_screenheight() // 2 - alto_ventana // 2

        posicion = str(ancho_ventana) + "x" + str(alto_ventana) + "+" + str(x_ventana) + "+" + str(y_ventana)
        self.ventana_ch2.geometry(posicion)
        self.ventana_ch2.focus()

        self.ventana_ch2.columnconfigure(0, weight=1)

        # GUI
        CTkLabel(self.ventana_ch2, text="Elija el tipo de hilo, según la marca, el calibre y el metraje del cono", font=("Calibri", 14, "bold"), justify="center", wraplength="280").grid(row=0, column=0, pady=10, sticky="ew")

        self.frame_scroll = CTkScrollableFrame(self.ventana_ch2, height=290)
        self.frame_scroll.grid(row=1, column=0, padx=0, pady=0, sticky="nsew")
        self.frame_scroll.columnconfigure(0, weight=1)

        # Botones
        CTkButton(self.frame_scroll, text='AMETO-T18-5000', command=lambda: self.calc_parcial_ch2("AMETO-T18-5000", origen)).grid(row=1, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='AMETO-T18-4500', command=lambda: self.calc_parcial_ch2("AMETO-T18-4500", origen)).grid(row=1, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='AMETO-T27-2500', command=lambda: self.calc_parcial_ch2("AMETO-T27-2500", origen)).grid(row=1, column=2, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='AMETO-T27-5000', command=lambda: self.calc_parcial_ch2("AMETO-T27-5000", origen)).grid(row=2, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='AMETO-T40-2500', command=lambda: self.calc_parcial_ch2("AMETO-T40-2500", origen)).grid(row=2, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='AMETO-T40-5000', command=lambda: self.calc_parcial_ch2("AMETO-T40-5000", origen)).grid(row=2, column=2, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='AMETO-T60-2500', command=lambda: self.calc_parcial_ch2("AMETO-T60-2500", origen)).grid(row=3, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='AMETO-T80-2500', command=lambda: self.calc_parcial_ch2("AMETO-T80-2500", origen)).grid(row=3, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='AMETO-T105-2500', command=lambda: self.calc_parcial_ch2("AMETO-T105-2500", origen)).grid(row=3, column=2, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='CADENA-T35-5000', command=lambda: self.calc_parcial_ch2("CADENA-T35-5000", origen)).grid(row=4, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='CADENA-T50-5000', command=lambda: self.calc_parcial_ch2("CADENA-T50-5000", origen)).grid(row=4, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='CADENA-T70-5000', command=lambda: self.calc_parcial_ch2("CADENA-T70-5000", origen)).grid(row=4, column=2, padx=10, pady=5, sticky="ew")

        CTkButton(self.frame_scroll, text='CADENA-T85-4500', command=lambda: self.calc_parcial_ch2("CADENA-T85-4500", origen)).grid(row=5, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='CADENA-T140-2500', command=lambda: self.calc_parcial_ch2("CADENA-T140-2500", origen)).grid(row=5, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='EPIC-T18-5000', command=lambda: self.calc_parcial_ch2("EPIC-T18-5000", origen)).grid(row=5, column=2, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='EPIC-T24-2500', command=lambda: self.calc_parcial_ch2("EPIC-T24-2500", origen)).grid(row=6, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='EPIC-T35-2500', command=lambda: self.calc_parcial_ch2("EPIC-T35-2500", origen)).grid(row=6, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='EPIC-T40-2500', command=lambda: self.calc_parcial_ch2("EPIC-T40-2500", origen)).grid(row=6, column=2, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='EPIC-T60-2500', command=lambda: self.calc_parcial_ch2("EPIC-T60-2500", origen)).grid(row=7, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='EPIC-T80-2500', command=lambda: self.calc_parcial_ch2("EPIC-T80-2500", origen)).grid(row=7, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='EPIC-T105-2500', command=lambda: self.calc_parcial_ch2("EPIC-T105-2500", origen)).grid(row=7, column=2, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='EPIC-T120-2500', command=lambda: self.calc_parcial_ch2("EPIC-T120-2500", origen)).grid(row=8, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='KOBAN-T24-5000', command=lambda: self.calc_parcial_ch2("KOBAN-T24-5000", origen)).grid(row=8, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='KOBAN-T40-3000', command=lambda: self.calc_parcial_ch2("KOBAN-T40-3000", origen)).grid(row=8, column=2, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='KOBAN-T60-4000', command=lambda: self.calc_parcial_ch2("KOBAN-T60-4000", origen)).grid(row=9, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='KOBAN-T80-2500', command=lambda: self.calc_parcial_ch2("KOBAN-T80-2500", origen)).grid(row=9, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='KOBAN-T105-5000', command=lambda: self.calc_parcial_ch2("KOBAN-T105-5000", origen)).grid(row=9, column=2, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='KOBAN-T120-2000', command=lambda: self.calc_parcial_ch2("KOBAN-T120-2000", origen)).grid(row=10, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='KOBAN-T160-2000', command=lambda: self.calc_parcial_ch2("KOBAN-T160-2000", origen)).grid(row=10, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='BONANZA-120-4570', command=lambda: self.calc_parcial_ch2("BONANZA-120-4570", origen)).grid(row=10, column=2, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='BONANZA-75-4570', command=lambda: self.calc_parcial_ch2("BONANZA-75-4570", origen)).grid(row=11, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='BONANZA-50-3656', command=lambda: self.calc_parcial_ch2("BONANZA-50-3656", origen)).grid(row=11, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='BONANZA-25-2286', command=lambda: self.calc_parcial_ch2("BONANZA-25-2286", origen)).grid(row=11, column=2, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='TIGER-18-15000', command=lambda: self.calc_parcial_ch2("TIGER-18-15000", origen)).grid(row=12, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='TIGER-24-15000', command=lambda: self.calc_parcial_ch2("TIGER-24-15000", origen)).grid(row=12, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='SYLKO-120-1000', command=lambda: self.calc_parcial_ch2("SYLKO-120-1000", origen)).grid(row=12, column=2, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='SYLKO-120-4000', command=lambda: self.calc_parcial_ch2("SYLKO-120-4000", origen)).grid(row=13, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='-', command=lambda: self.calc_parcial_ch2("-0", origen)).grid(row=13, column=1, padx=10, pady=5, sticky="ew")

        # fin - GUI

        # Modal...
        self.ventana_ch2.transient(self)  # dialog window is related to main
        self.ventana_ch2.wait_visibility()
        self.ventana_ch2.grab_set()
        self.ventana_ch2.wait_window()

    def ventana_combo_box3(self, origen):
        # Crear una ventana secundaria.
        self.ventana_ch3 = CTkToplevel()
        self.ventana_ch3.resizable(False, False)
        # quitar minimizar....
        self.ventana_ch3.title("Color")

        ancho_ventana = 300
        alto_ventana = 140
        x_ventana = self.winfo_screenwidth() // 2 - ancho_ventana // 2
        y_ventana = self.winfo_screenheight() // 2 - alto_ventana // 2

        posicion = str(ancho_ventana) + "x" + str(alto_ventana) + "+" + str(x_ventana) + "+" + str(y_ventana)
        self.ventana_ch3.geometry(posicion)
        self.ventana_ch3.focus()

        self.ventana_ch3.columnconfigure(0, weight=1)
        self.ventana_ch3.columnconfigure(1, weight=2)

        # GUI
        CTkLabel(self.ventana_ch3, text="Elija el tipo de hilo, según el Color", font=("Calibri", 14, "bold"), justify="center", wraplength="280").grid(row=0, column=0, columnspan=2, pady=10, sticky="ew")

        # Botones
        CTkButton(self.ventana_ch3, text='Colores', command=lambda: self.calc_parcial_ch3("Colores", origen)).grid(row=1, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch3, text='Blanco', command=lambda: self.calc_parcial_ch3("Blanco", origen)).grid(row=1, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch3, text='Crudo', command=lambda: self.calc_parcial_ch3("Crudo", origen)).grid(row=2, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch3, text='-', command=lambda: self.calc_parcial_ch3("-", origen)).grid(row=2, column=1, padx=10, pady=5, sticky="ew")

        # fin - GUI

        # Modal...
        self.ventana_ch3.transient(self)  # dialog window is related to main
        self.ventana_ch3.wait_visibility()
        self.ventana_ch3.grab_set()
        self.ventana_ch3.wait_window()

    def ventana_combo_box_mediano(self, mensaje, origen):
        # Crear una ventana secundaria.
        self.ventana_ch_mediano = CTkToplevel()
        self.ventana_ch_mediano.resizable(False, False)
        # quitar minimizar....
        self.ventana_ch_mediano.title("PPP - Seleccione las Puntadas por Pulgada")

        ancho_ventana = 400
        alto_ventana = 350
        x_ventana = self.winfo_screenwidth() // 2 - ancho_ventana // 2
        y_ventana = self.winfo_screenheight() // 2 - alto_ventana // 2

        posicion = str(ancho_ventana) + "x" + str(alto_ventana) + "+" + str(x_ventana) + "+" + str(y_ventana)
        self.ventana_ch_mediano.geometry(posicion)
        self.ventana_ch_mediano.focus()

        self.ventana_ch_mediano.columnconfigure(0, weight=1)
        self.ventana_ch_mediano.columnconfigure(1, weight=2)

        # GUI
        CTkLabel(self.ventana_ch_mediano, text=mensaje, font=("Calibri", 14, "bold"), justify="center").grid(row=0, column=0, columnspan=2, pady=10, sticky="ew")

        # Botones
        CTkButton(self.ventana_ch_mediano, text='0', command=lambda: self.calc_parcial_ch_mediano("0", origen)).grid(row=1, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch_mediano, text='8', command=lambda: self.calc_parcial_ch_mediano("8", origen)).grid(row=1, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch_mediano, text='10', command=lambda: self.calc_parcial_ch_mediano("10", origen)).grid(row=2, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch_mediano, text='12', command=lambda: self.calc_parcial_ch_mediano("12", origen)).grid(row=2, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch_mediano, text='14', command=lambda: self.calc_parcial_ch_mediano("14", origen)).grid(row=3, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch_mediano, text='16', command=lambda: self.calc_parcial_ch_mediano("16", origen)).grid(row=3, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch_mediano, text='20', command=lambda: self.calc_parcial_ch_mediano("20", origen)).grid(row=4, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch_mediano, text='28', command=lambda: self.calc_parcial_ch_mediano("28", origen)).grid(row=4, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch_mediano, text='32', command=lambda: self.calc_parcial_ch_mediano("32", origen)).grid(row=5, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch_mediano, text='36', command=lambda: self.calc_parcial_ch_mediano("36", origen)).grid(row=5, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch_mediano, text='42', command=lambda: self.calc_parcial_ch_mediano("42", origen)).grid(row=6, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch_mediano, text='82', command=lambda: self.calc_parcial_ch_mediano("82", origen)).grid(row=6, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch_mediano, text='96', command=lambda: self.calc_parcial_ch_mediano("96", origen)).grid(row=7, column=0, padx=10, pady=5, sticky="ew")

        # fin - GUI

        # Modal...
        self.ventana_ch_mediano.transient(self)  # dialog window is related to main
        self.ventana_ch_mediano.wait_visibility()
        self.ventana_ch_mediano.grab_set()
        self.ventana_ch_mediano.wait_window()


    def ventana_combo_box2_mediano(self, origen):
        # Crear una ventana secundaria.
        self.ventana_ch2_mediano = CTkToplevel()
        self.ventana_ch2_mediano.resizable(False, False)
        # quitar minimizar....
        self.ventana_ch2_mediano.title("Tipo de Hilo")

        ancho_ventana = 510
        alto_ventana = 350
        x_ventana = self.winfo_screenwidth() // 2 - ancho_ventana // 2
        y_ventana = self.winfo_screenheight() // 2 - alto_ventana // 2

        posicion = str(ancho_ventana) + "x" + str(alto_ventana) + "+" + str(x_ventana) + "+" + str(y_ventana)
        self.ventana_ch2_mediano.geometry(posicion)
        self.ventana_ch2_mediano.focus()

        self.ventana_ch2_mediano.columnconfigure(0, weight=1)

        # GUI
        CTkLabel(self.ventana_ch2_mediano, text="Elija el tipo de hilo, según la marca, el calibre y el metraje del cono", font=("Calibri", 14, "bold"), justify="center", wraplength="280").grid(row=0, column=0, pady=10, sticky="ew")

        self.frame_scroll = CTkScrollableFrame(self.ventana_ch2_mediano, height=290)
        self.frame_scroll.grid(row=1, column=0, padx=0, pady=0, sticky="nsew")
        self.frame_scroll.columnconfigure(0, weight=1)

        # Botones
        CTkButton(self.frame_scroll, text='AMETO-T18-5000', command=lambda: self.calc_parcial_ch2_mediano("AMETO-T18-5000", origen)).grid(row=1, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='AMETO-T18-4500', command=lambda: self.calc_parcial_ch2_mediano("AMETO-T18-4500", origen)).grid(row=1, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='AMETO-T27-2500', command=lambda: self.calc_parcial_ch2_mediano("AMETO-T27-2500", origen)).grid(row=1, column=2, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='AMETO-T27-5000', command=lambda: self.calc_parcial_ch2_mediano("AMETO-T27-5000", origen)).grid(row=2, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='AMETO-T40-2500', command=lambda: self.calc_parcial_ch2_mediano("AMETO-T40-2500", origen)).grid(row=2, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='AMETO-T40-5000', command=lambda: self.calc_parcial_ch2_mediano("AMETO-T40-5000", origen)).grid(row=2, column=2, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='AMETO-T60-2500', command=lambda: self.calc_parcial_ch2_mediano("AMETO-T60-2500", origen)).grid(row=3, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='AMETO-T80-2500', command=lambda: self.calc_parcial_ch2_mediano("AMETO-T80-2500", origen)).grid(row=3, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='AMETO-T105-2500', command=lambda: self.calc_parcial_ch2_mediano("AMETO-T105-2500", origen)).grid(row=3, column=2, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='CADENA-T35-5000', command=lambda: self.calc_parcial_ch2_mediano("CADENA-T35-5000", origen)).grid(row=4, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='CADENA-T50-5000', command=lambda: self.calc_parcial_ch2_mediano("CADENA-T50-5000", origen)).grid(row=4, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='CADENA-T70-5000', command=lambda: self.calc_parcial_ch2_mediano("CADENA-T70-5000", origen)).grid(row=4, column=2, padx=10, pady=5, sticky="ew")

        CTkButton(self.frame_scroll, text='CADENA-T85-4500', command=lambda: self.calc_parcial_ch2_mediano("CADENA-T85-4500", origen)).grid(row=5, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='CADENA-T140-2500', command=lambda: self.calc_parcial_ch2_mediano("CADENA-T140-2500", origen)).grid(row=5, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='EPIC-T18-5000', command=lambda: self.calc_parcial_ch2_mediano("EPIC-T18-5000", origen)).grid(row=5, column=2, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='EPIC-T24-2500', command=lambda: self.calc_parcial_ch2_mediano("EPIC-T24-2500", origen)).grid(row=6, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='EPIC-T35-2500', command=lambda: self.calc_parcial_ch2_mediano("EPIC-T35-2500", origen)).grid(row=6, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='EPIC-T40-2500', command=lambda: self.calc_parcial_ch2_mediano("EPIC-T40-2500", origen)).grid(row=6, column=2, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='EPIC-T60-2500', command=lambda: self.calc_parcial_ch2_mediano("EPIC-T60-2500", origen)).grid(row=7, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='EPIC-T80-2500', command=lambda: self.calc_parcial_ch2_mediano("EPIC-T80-2500", origen)).grid(row=7, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='EPIC-T105-2500', command=lambda: self.calc_parcial_ch2_mediano("EPIC-T105-2500", origen)).grid(row=7, column=2, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='EPIC-T120-2500', command=lambda: self.calc_parcial_ch2_mediano("EPIC-T120-2500", origen)).grid(row=8, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='KOBAN-T24-5000', command=lambda: self.calc_parcial_ch2_mediano("KOBAN-T24-5000", origen)).grid(row=8, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='KOBAN-T40-3000', command=lambda: self.calc_parcial_ch2_mediano("KOBAN-T40-3000", origen)).grid(row=8, column=2, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='KOBAN-T60-4000', command=lambda: self.calc_parcial_ch2_mediano("KOBAN-T60-4000", origen)).grid(row=9, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='KOBAN-T80-2500', command=lambda: self.calc_parcial_ch2_mediano("KOBAN-T80-2500", origen)).grid(row=9, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='KOBAN-T105-5000', command=lambda: self.calc_parcial_ch2_mediano("KOBAN-T105-5000", origen)).grid(row=9, column=2, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='KOBAN-T120-2000', command=lambda: self.calc_parcial_ch2_mediano("KOBAN-T120-2000", origen)).grid(row=10, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='KOBAN-T160-2000', command=lambda: self.calc_parcial_ch2_mediano("KOBAN-T160-2000", origen)).grid(row=10, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='BONANZA-120-4570', command=lambda: self.calc_parcial_ch2_mediano("BONANZA-120-4570", origen)).grid(row=10, column=2, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='BONANZA-75-4570', command=lambda: self.calc_parcial_ch2_mediano("BONANZA-75-4570", origen)).grid(row=11, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='BONANZA-50-3656', command=lambda: self.calc_parcial_ch2_mediano("BONANZA-50-3656", origen)).grid(row=11, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='BONANZA-25-2286', command=lambda: self.calc_parcial_ch2_mediano("BONANZA-25-2286", origen)).grid(row=11, column=2, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='TIGER-18-15000', command=lambda: self.calc_parcial_ch2_mediano("TIGER-18-15000", origen)).grid(row=12, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='TIGER-24-15000', command=lambda: self.calc_parcial_ch2_mediano("TIGER-24-15000", origen)).grid(row=12, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='SYLKO-120-1000', command=lambda: self.calc_parcial_ch2_mediano("SYLKO-120-1000", origen)).grid(row=12, column=2, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='SYLKO-120-4000', command=lambda: self.calc_parcial_ch2_mediano("SYLKO-120-4000", origen)).grid(row=13, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='-', command=lambda: self.calc_parcial_ch2_mediano("-0", origen)).grid(row=13, column=1, padx=10, pady=5, sticky="ew")

        # fin - GUI

        # Modal...
        self.ventana_ch2_mediano.transient(self)  # dialog window is related to main
        self.ventana_ch2_mediano.wait_visibility()
        self.ventana_ch2_mediano.grab_set()
        self.ventana_ch2_mediano.wait_window()

    def ventana_combo_box3_mediano(self, origen):
        # Crear una ventana secundaria.
        self.ventana_ch3_mediano = CTkToplevel()
        self.ventana_ch3_mediano.resizable(False, False)
        # quitar minimizar....
        self.ventana_ch3_mediano.title("Color")

        ancho_ventana = 300
        alto_ventana = 140
        x_ventana = self.winfo_screenwidth() // 2 - ancho_ventana // 2
        y_ventana = self.winfo_screenheight() // 2 - alto_ventana // 2

        posicion = str(ancho_ventana) + "x" + str(alto_ventana) + "+" + str(x_ventana) + "+" + str(y_ventana)
        self.ventana_ch3_mediano.geometry(posicion)
        self.ventana_ch3_mediano.focus()

        self.ventana_ch3_mediano.columnconfigure(0, weight=1)
        self.ventana_ch3_mediano.columnconfigure(1, weight=2)

        # GUI
        CTkLabel(self.ventana_ch3_mediano, text="Elija el tipo de hilo, según el Color", font=("Calibri", 14, "bold"), justify="center", wraplength="280").grid(row=0, column=0, columnspan=2, pady=10, sticky="ew")

        # Botones
        CTkButton(self.ventana_ch3_mediano, text='Colores', command=lambda: self.calc_parcial_ch3_mediano("Colores", origen)).grid(row=1, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch3_mediano, text='Blanco', command=lambda: self.calc_parcial_ch3_mediano("Blanco", origen)).grid(row=1, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch3_mediano, text='Crudo', command=lambda: self.calc_parcial_ch3_mediano("Crudo", origen)).grid(row=2, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch3_mediano, text='-', command=lambda: self.calc_parcial_ch3_mediano("-", origen)).grid(row=2, column=1, padx=10, pady=5, sticky="ew")

        # fin - GUI

        # Modal...
        self.ventana_ch3_mediano.transient(self)  # dialog window is related to main
        self.ventana_ch3_mediano.wait_visibility()
        self.ventana_ch3_mediano.grab_set()
        self.ventana_ch3_mediano.wait_window()

    def ventana_combo_box_pesado(self, mensaje, origen):
        # Crear una ventana secundaria.
        self.ventana_ch_pesado = CTkToplevel()
        self.ventana_ch_pesado.resizable(False, False)
        # quitar minimizar....
        self.ventana_ch_pesado.title("PPP - Seleccione las Puntadas por Pulgada")

        ancho_ventana = 400
        alto_ventana = 350
        x_ventana = self.winfo_screenwidth() // 2 - ancho_ventana // 2
        y_ventana = self.winfo_screenheight() // 2 - alto_ventana // 2

        posicion = str(ancho_ventana) + "x" + str(alto_ventana) + "+" + str(x_ventana) + "+" + str(y_ventana)
        self.ventana_ch_pesado.geometry(posicion)
        self.ventana_ch_pesado.focus()

        self.ventana_ch_pesado.columnconfigure(0, weight=1)
        self.ventana_ch_pesado.columnconfigure(1, weight=2)

        # GUI
        CTkLabel(self.ventana_ch_pesado, text=mensaje, font=("Calibri", 14, "bold"), justify="center").grid(row=0, column=0, columnspan=2, pady=10, sticky="ew")

        # Botones
        CTkButton(self.ventana_ch_pesado, text='0', command=lambda: self.calc_parcial_ch_pesado("0", origen)).grid(row=1, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch_pesado, text='8', command=lambda: self.calc_parcial_ch_pesado("8", origen)).grid(row=1, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch_pesado, text='10', command=lambda: self.calc_parcial_ch_pesado("10", origen)).grid(row=2, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch_pesado, text='12', command=lambda: self.calc_parcial_ch_pesado("12", origen)).grid(row=2, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch_pesado, text='14', command=lambda: self.calc_parcial_ch_pesado("14", origen)).grid(row=3, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch_pesado, text='16', command=lambda: self.calc_parcial_ch_pesado("16", origen)).grid(row=3, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch_pesado, text='20', command=lambda: self.calc_parcial_ch_pesado("20", origen)).grid(row=4, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch_pesado, text='28', command=lambda: self.calc_parcial_ch_pesado("28", origen)).grid(row=4, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch_pesado, text='32', command=lambda: self.calc_parcial_ch_pesado("32", origen)).grid(row=5, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch_pesado, text='36', command=lambda: self.calc_parcial_ch_pesado("36", origen)).grid(row=5, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch_pesado, text='42', command=lambda: self.calc_parcial_ch_pesado("42", origen)).grid(row=6, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch_pesado, text='82', command=lambda: self.calc_parcial_ch_pesado("82", origen)).grid(row=6, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch_pesado, text='96', command=lambda: self.calc_parcial_ch_pesado("96", origen)).grid(row=7, column=0, padx=10, pady=5, sticky="ew")

        # fin - GUI

        # Modal...
        self.ventana_ch_pesado.transient(self)  # dialog window is related to main
        self.ventana_ch_pesado.wait_visibility()
        self.ventana_ch_pesado.grab_set()
        self.ventana_ch_pesado.wait_window()

    def ventana_combo_box2_pesado(self, origen):
        # Crear una ventana secundaria.
        self.ventana_ch2_pesado = CTkToplevel()
        self.ventana_ch2_pesado.resizable(False, False)
        # quitar minimizar....
        self.ventana_ch2_pesado.title("Tipo de Hilo")

        ancho_ventana = 510
        alto_ventana = 350
        x_ventana = self.winfo_screenwidth() // 2 - ancho_ventana // 2
        y_ventana = self.winfo_screenheight() // 2 - alto_ventana // 2

        posicion = str(ancho_ventana) + "x" + str(alto_ventana) + "+" + str(x_ventana) + "+" + str(y_ventana)
        self.ventana_ch2_pesado.geometry(posicion)
        self.ventana_ch2_pesado.focus()

        self.ventana_ch2_pesado.columnconfigure(0, weight=1)

        # GUI
        CTkLabel(self.ventana_ch2_pesado, text="Elija el tipo de hilo, según la marca, el calibre y el metraje del cono", font=("Calibri", 14, "bold"), justify="center", wraplength="280").grid(row=0, column=0, pady=10, sticky="ew")

        self.frame_scroll = CTkScrollableFrame(self.ventana_ch2_pesado, height=290)
        self.frame_scroll.grid(row=1, column=0, padx=0, pady=0, sticky="nsew")
        self.frame_scroll.columnconfigure(0, weight=1)

        # Botones
        CTkButton(self.frame_scroll, text='AMETO-T18-5000', command=lambda: self.calc_parcial_ch2_pesado("AMETO-T18-5000", origen)).grid(row=1, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='AMETO-T18-4500', command=lambda: self.calc_parcial_ch2_pesado("AMETO-T18-4500", origen)).grid(row=1, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='AMETO-T27-2500', command=lambda: self.calc_parcial_ch2_pesado("AMETO-T27-2500", origen)).grid(row=1, column=2, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='AMETO-T27-5000', command=lambda: self.calc_parcial_ch2_pesado("AMETO-T27-5000", origen)).grid(row=2, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='AMETO-T40-2500', command=lambda: self.calc_parcial_ch2_pesado("AMETO-T40-2500", origen)).grid(row=2, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='AMETO-T40-5000', command=lambda: self.calc_parcial_ch2_pesado("AMETO-T40-5000", origen)).grid(row=2, column=2, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='AMETO-T60-2500', command=lambda: self.calc_parcial_ch2_pesado("AMETO-T60-2500", origen)).grid(row=3, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='AMETO-T80-2500', command=lambda: self.calc_parcial_ch2_pesado("AMETO-T80-2500", origen)).grid(row=3, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='AMETO-T105-2500', command=lambda: self.calc_parcial_ch2_pesado("AMETO-T105-2500", origen)).grid(row=3, column=2, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='CADENA-T35-5000', command=lambda: self.calc_parcial_ch2_pesado("CADENA-T35-5000", origen)).grid(row=4, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='CADENA-T50-5000', command=lambda: self.calc_parcial_ch2_pesado("CADENA-T50-5000", origen)).grid(row=4, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='CADENA-T70-5000', command=lambda: self.calc_parcial_ch2_pesado("CADENA-T70-5000", origen)).grid(row=4, column=2, padx=10, pady=5, sticky="ew")

        CTkButton(self.frame_scroll, text='CADENA-T85-4500', command=lambda: self.calc_parcial_ch2_pesado("CADENA-T85-4500", origen)).grid(row=5, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='CADENA-T140-2500', command=lambda: self.calc_parcial_ch2_pesado("CADENA-T140-2500", origen)).grid(row=5, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='EPIC-T18-5000', command=lambda: self.calc_parcial_ch2_pesado("EPIC-T18-5000", origen)).grid(row=5, column=2, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='EPIC-T24-2500', command=lambda: self.calc_parcial_ch2_pesado("EPIC-T24-2500", origen)).grid(row=6, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='EPIC-T35-2500', command=lambda: self.calc_parcial_ch2_pesado("EPIC-T35-2500", origen)).grid(row=6, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='EPIC-T40-2500', command=lambda: self.calc_parcial_ch2_pesado("EPIC-T40-2500", origen)).grid(row=6, column=2, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='EPIC-T60-2500', command=lambda: self.calc_parcial_ch2_pesado("EPIC-T60-2500", origen)).grid(row=7, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='EPIC-T80-2500', command=lambda: self.calc_parcial_ch2_pesado("EPIC-T80-2500", origen)).grid(row=7, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='EPIC-T105-2500', command=lambda: self.calc_parcial_ch2_pesado("EPIC-T105-2500", origen)).grid(row=7, column=2, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='EPIC-T120-2500', command=lambda: self.calc_parcial_ch2_pesado("EPIC-T120-2500", origen)).grid(row=8, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='KOBAN-T24-5000', command=lambda: self.calc_parcial_ch2_pesado("KOBAN-T24-5000", origen)).grid(row=8, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='KOBAN-T40-3000', command=lambda: self.calc_parcial_ch2_pesado("KOBAN-T40-3000", origen)).grid(row=8, column=2, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='KOBAN-T60-4000', command=lambda: self.calc_parcial_ch2_pesado("KOBAN-T60-4000", origen)).grid(row=9, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='KOBAN-T80-2500', command=lambda: self.calc_parcial_ch2_pesado("KOBAN-T80-2500", origen)).grid(row=9, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='KOBAN-T105-5000', command=lambda: self.calc_parcial_ch2_pesado("KOBAN-T105-5000", origen)).grid(row=9, column=2, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='KOBAN-T120-2000', command=lambda: self.calc_parcial_ch2_pesado("KOBAN-T120-2000", origen)).grid(row=10, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='KOBAN-T160-2000', command=lambda: self.calc_parcial_ch2_pesado("KOBAN-T160-2000", origen)).grid(row=10, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='BONANZA-120-4570', command=lambda: self.calc_parcial_ch2_pesado("BONANZA-120-4570", origen)).grid(row=10, column=2, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='BONANZA-75-4570', command=lambda: self.calc_parcial_ch2_pesado("BONANZA-75-4570", origen)).grid(row=11, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='BONANZA-50-3656', command=lambda: self.calc_parcial_ch2_pesado("BONANZA-50-3656", origen)).grid(row=11, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='BONANZA-25-2286', command=lambda: self.calc_parcial_ch2_pesado("BONANZA-25-2286", origen)).grid(row=11, column=2, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='TIGER-18-15000', command=lambda: self.calc_parcial_ch2_pesado("TIGER-18-15000", origen)).grid(row=12, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='TIGER-24-15000', command=lambda: self.calc_parcial_ch2_pesado("TIGER-24-15000", origen)).grid(row=12, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='SYLKO-120-1000', command=lambda: self.calc_parcial_ch2_pesado("SYLKO-120-1000", origen)).grid(row=12, column=2, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='SYLKO-120-4000', command=lambda: self.calc_parcial_ch2_pesado("SYLKO-120-4000", origen)).grid(row=13, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.frame_scroll, text='-', command=lambda: self.calc_parcial_ch2_pesado("-0", origen)).grid(row=13, column=1, padx=10, pady=5, sticky="ew")

        # fin - GUI

        # Modal...
        self.ventana_ch2_pesado.transient(self)  # dialog window is related to main
        self.ventana_ch2_pesado.wait_visibility()
        self.ventana_ch2_pesado.grab_set()
        self.ventana_ch2_pesado.wait_window()

    def ventana_combo_box3_pesado(self, origen):
        # Crear una ventana secundaria.
        self.ventana_ch3_pesado = CTkToplevel()
        self.ventana_ch3_pesado.resizable(False, False)
        # quitar minimizar....
        self.ventana_ch3_pesado.title("Color")

        ancho_ventana = 300
        alto_ventana = 140
        x_ventana = self.winfo_screenwidth() // 2 - ancho_ventana // 2
        y_ventana = self.winfo_screenheight() // 2 - alto_ventana // 2

        posicion = str(ancho_ventana) + "x" + str(alto_ventana) + "+" + str(x_ventana) + "+" + str(y_ventana)
        self.ventana_ch3_pesado.geometry(posicion)
        self.ventana_ch3_pesado.focus()

        self.ventana_ch3_pesado.columnconfigure(0, weight=1)
        self.ventana_ch3_pesado.columnconfigure(1, weight=2)

        # GUI
        CTkLabel(self.ventana_ch3_pesado, text="Elija el tipo de hilo, según el Color", font=("Calibri", 14, "bold"), justify="center", wraplength="280").grid(row=0, column=0, columnspan=2, pady=10, sticky="ew")

        # Botones
        CTkButton(self.ventana_ch3_pesado, text='Colores', command=lambda: self.calc_parcial_ch3_pesado("Colores", origen)).grid(row=1, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch3_pesado, text='Blanco', command=lambda: self.calc_parcial_ch3_pesado("Blanco", origen)).grid(row=1, column=1, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch3_pesado, text='Crudo', command=lambda: self.calc_parcial_ch3_pesado("Crudo", origen)).grid(row=2, column=0, padx=10, pady=5, sticky="ew")
        CTkButton(self.ventana_ch3_pesado, text='-', command=lambda: self.calc_parcial_ch3_pesado("-", origen)).grid(row=2, column=1, padx=10, pady=5, sticky="ew")

        # fin - GUI

        # Modal...
        self.ventana_ch3_pesado.transient(self)  # dialog window is related to main
        self.ventana_ch3_pesado.wait_visibility()
        self.ventana_ch3_pesado.grab_set()
        self.ventana_ch3_pesado.wait_window()

    def consumo_hilo_guardar(self):
        # Capturando datos de todo el módulo
        pxp = "[" + "*".join([item[0].cget("text") for item in self.button_grid1._rows]) + "]"
        print(pxp)
        mc = "[" + "*".join([item[0].get() for item in self.entry_grid1._rows]) + "]"
        print(mc)
        mh = "[" + "*".join([item[0].cget("text") for item in self.label_grid3._rows]) + "]"
        print(mh)
        th = "[" + "*".join([item[1].cget("text") for item in self.label_grid3._rows]) + "]"
        print(th)
        tipoh = "[" + "*".join([item[0].cget("text") for item in self.button_grid2._rows]) + "]"
        print(tipoh)
        colorh = "[" + "*".join([item[1].cget("text") for item in self.button_grid2._rows]) + "]"
        print(colorh)
        tmpro = "[" + "*".join([item[0].cget("text") for item in self.label_grid4._rows]) + "]"
        print(tmpro)
        tconos = "[" + "*".join([item[1].cget("text") for item in self.label_grid4._rows]) + "]"
        print(tconos)
        tconosred = "[" + "*".join([item[2].cget("text") for item in self.label_grid4._rows]) + "]"
        print(tconosred)

        total_ch = self.ch_resultados_total.cget("text")
        print(total_ch)

        costo_sol = self.ch_sol.get()
        print(costo_sol)
        costo_astra = self.ch_astra.get()
        print(costo_astra)
        costo_epic = self.ch_epic.get()
        print(costo_epic)

        import traceback
        try:
            query = 'INSERT INTO scp_consumo_hilo VALUES(null, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'
            parameters = (self.material_id_consumo_hilo, self.consumo_hilo_tipo_tejido.get(), self.ch_entry1.get(), pxp, mc, mh, th, tipoh, colorh, tmpro, tconos, tconosred, total_ch, costo_sol, costo_astra, costo_epic)
            res = self.run_query(query, parameters)
            messagebox.showinfo(title="SCP", message=f"Consumo de Hilo Guardado correctamente!!")
        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error guardando en base de datos.\n{e}\n{traceback.format_exc()}")
        finally:
            self.conexion.close()

    def consumo_hilo_actualizar(self):
        pass

    def carga_opt1_event(self):
        if self.frame_col1_carga_opt1.get() == "on":
            self.total_costo_mensual_trabajador = float(self.total_costo_mensual_trabajador) + self.parafiscales

            self.frame_row1_entry11.configure(state='normal')
            self.frame_row1_entry11.delete(0, END)
            # self.frame_row1_entry11.insert(0, locale.format_string("%.2f", self.total_costo_mensual_trabajador, grouping=True))
            self.frame_row1_entry11.insert(0, (f"${self.total_costo_mensual_trabajador:,.0f}").replace(',','*').replace('.', ',').replace('*','.'))
            self.frame_row1_entry11.configure(state='readonly')
            # Actualizar porcentaje de Label Total costo mensual trabajador
            self.porcentajes_carga_prestacional[10] += float(self.parafiscales_porcentaje)
            self.frame_row1_label11.configure(text=f"Total Costo Mensual Trabajador ({self.porcentajes_carga_prestacional[10]}%)")
        else:
            self.total_costo_mensual_trabajador = float(self.total_costo_mensual_trabajador) - self.parafiscales

            self.frame_row1_entry11.configure(state='normal')
            self.frame_row1_entry11.delete(0, END)
            # self.frame_row1_entry11.insert(0, locale.format_string("%.2f", self.total_costo_mensual_trabajador, grouping=True))
            self.frame_row1_entry11.insert(0, (f"${self.total_costo_mensual_trabajador:,.0f}").replace(',','*').replace('.', ',').replace('*','.'))
            self.frame_row1_entry11.configure(state='readonly')
            # Actualizar porcentaje de Label Total costo mensual trabajador
            self.porcentajes_carga_prestacional[10] -= float(self.parafiscales_porcentaje)
            self.frame_row1_label11.configure(text=f"Total Costo Mensual Trabajador ({self.porcentajes_carga_prestacional[10]}%)")

    def carga_opt2_event(self):
        if self.frame_col2_carga_opt2.get() == "on":
            self.total_costo_mensual_trabajador = float(self.total_costo_mensual_trabajador) + self.dotacion

            self.frame_row1_entry11.configure(state='normal')
            self.frame_row1_entry11.delete(0, END)
            # self.frame_row1_entry11.insert(0, locale.format_string("%.2f", self.total_costo_mensual_trabajador, grouping=True))
            self.frame_row1_entry11.insert(0, (f"${self.total_costo_mensual_trabajador:,.0f}").replace(',','*').replace('.', ',').replace('*','.'))
            self.frame_row1_entry11.configure(state='readonly')
            # Actualizar porcentaje de Label Total costo mensual trabajador
            self.porcentajes_carga_prestacional[10] += float(self.dotacion_porcentaje)
            self.frame_row1_label11.configure(text=f"Total Costo Mensual Trabajador ({self.porcentajes_carga_prestacional[10]}%)")
        else:
            self.total_costo_mensual_trabajador = float(self.total_costo_mensual_trabajador) - self.dotacion

            self.frame_row1_entry11.configure(state='normal')
            self.frame_row1_entry11.delete(0, END)
            # self.frame_row1_entry11.insert(0, locale.format_string("%.2f", self.total_costo_mensual_trabajador, grouping=True))
            self.frame_row1_entry11.insert(0, (f"${self.total_costo_mensual_trabajador:,.0f}").replace(',','*').replace('.', ',').replace('*','.'))
            self.frame_row1_entry11.configure(state='readonly')
            # Actualizar porcentaje de Label Total costo mensual trabajador
            self.porcentajes_carga_prestacional[10] -= float(self.dotacion_porcentaje)
            self.frame_row1_label11.configure(text=f"Total Costo Mensual Trabajador ({self.porcentajes_carga_prestacional[10]}%)")

    def salud_opt3_event(self):
        if self.frame_col2_label10.get() == "on":
            self.total_costo_mensual_trabajador = float(self.total_costo_mensual_trabajador) + self.salud

            self.frame_row1_entry11.configure(state='normal')
            self.frame_row1_entry11.delete(0, END)
            # self.frame_row1_entry11.insert(0, locale.format_string("%.2f", self.total_costo_mensual_trabajador, grouping=True))
            self.frame_row1_entry11.insert(0, (f"${self.total_costo_mensual_trabajador:,.0f}").replace(',','*').replace('.', ',').replace('*','.'))
            self.frame_row1_entry11.configure(state='readonly')
            # Actualizar porcentaje de Label Total costo mensual trabajador
            self.porcentajes_carga_prestacional[10] += float(self.salud_porcentaje)
            self.frame_row1_label11.configure(text=f"Total Costo Mensual Trabajador ({self.porcentajes_carga_prestacional[10]}%)")
        else:
            self.total_costo_mensual_trabajador = float(self.total_costo_mensual_trabajador) - self.salud

            self.frame_row1_entry11.configure(state='normal')
            self.frame_row1_entry11.delete(0, END)
            # self.frame_row1_entry11.insert(0, locale.format_string("%.2f", self.total_costo_mensual_trabajador, grouping=True))
            self.frame_row1_entry11.insert(0, (f"${self.total_costo_mensual_trabajador:,.0f}").replace(',','*').replace('.', ',').replace('*','.'))
            self.frame_row1_entry11.configure(state='readonly')
            # Actualizar porcentaje de Label Total costo mensual trabajador
            self.porcentajes_carga_prestacional[10] -= float(self.salud_porcentaje)
            self.frame_row1_label11.configure(text=f"Total Costo Mensual Trabajador ({self.porcentajes_carga_prestacional[10]}%)")

    def OnDoubleClick(self, event):
        curItem = self.tree_productos.focus()
        item = self.tree_productos.item(curItem)

        messagebox.showinfo(title=f"{item['values'][1]}", message=f"""ID: {item['values'][0]}
        NOMBRE: {item['values'][1]}
        REFERENCIA: {item['values'][2]}
        TIEMPO ESTÁNDAR: {item['values'][3]}
        FOTO: '' """)

    def vm_valida(self, dato, decimales):
        try:
            valor = f"{float(dato):,.{decimales}f}"
        except ValueError:
            if dato != '':
                valor = "Error"
            else:
                valor = "0"
        return valor

    def vm_onkeyup_precio_venta(self, event):
        self.vm_frame_col2_label8.configure(text=f"${self.vm_valida(self.vm_frame_col1_entry1.get(), 0)}".replace(",", "X").replace(".", ",").replace("X", "."))

    def vm_onkeyup_costo_variable(self, event):
        self.vm_frame_col2_label12.configure(text=f"${self.vm_valida(self.vm_frame_col1_entry3.get(), 0)}".replace(",", "X").replace(".", ",").replace("X", "."))

    def vm_onkeyup_costos_gastos_fijos(self, event):
        self.vm_frame_col2_label18.configure(text=f"${self.vm_valida(self.vm_frame_col1_entry5.get(), 0)}".replace(",", "X").replace(".", ",").replace("X", "."))
        if event.keysym == "Return":
            self.calcular_vm_event()

    def vm_onkeyup_operarios(self, event):
        self.vm_frame_col2_label22.configure(text=f"{self.vm_valida(self.vm_frame_col1_entry7.get(), 0)}".replace(",", "X").replace(".", ",").replace("X", "."))

    def vm_onkeyup_jornada(self, event):
        self.vm_frame_col2_label26.configure(text=f"{self.vm_valida(self.vm_frame_col1_entry9.get(), 1)}".replace(",", "X").replace(".", ",").replace("X", "."))

    def vm_onkeyup_dias(self, event):
        self.vm_frame_col2_label30.configure(text=f"{self.vm_valida(self.vm_frame_col1_entry11.get(), 0)}".replace(",", "X").replace(".", ",").replace("X", "."))

    def vm_onkeyup_eficiencia(self, event):
        self.vm_frame_col2_label34.configure(text=f"{self.vm_valida(self.vm_frame_col1_entry13.get(), 1)}%".replace(",", "X").replace(".", ",").replace("X", "."))

    def vm_onkeyup_tiempo_estandar(self, event):
        self.vm_frame_col2_label42.configure(text=f"{self.vm_valida(self.vm_frame_col1_entry15.get(), 1)}".replace(",", "X").replace(".", ",").replace("X", "."))

    def abrir_conexion(self):
        import traceback
        try:
            fullpath = os.path.join(BASE_DIR, "scp1.db")
            self.conexion = sqlite3.connect(fullpath)
        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error:\n{e}\n{traceback.format_exc()}\n{fullpath}")
        return self.conexion

    def open_input_dialog_event(self):
        dialog = CTkInputDialog(text="Type in a number:", title="CTkInputDialog")
        print("CTkInputDialog:", dialog.get_input())

    def configuraciones_iniciales(self):
        try:
            self.abrir_conexion()
            self.conexion.row_factory = sqlite3.Row
            cursor = self.conexion.cursor()
            sql = "select * from scp_config where param='tema'"
            cursor.execute(sql)
            registro = cursor.fetchone()
            self.change_appearance_mode_event(registro["value1"])
            # messagebox.showinfo(title="SCP", message="Conexión establecida!")
        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error conectando a la base de datos.\n{e}")
        finally:
            self.conexion.close()

    def select_frame_by_name(self, name):
        # set button color for selected button
        self.home_button.configure(fg_color=("gray75", "gray25") if name == "home" else "transparent")
        self.productos_menu.configure(fg_color=("gray75", "gray25") if name == "productos" else "transparent")
        self.materiales_menu.configure(fg_color=("gray75", "gray25") if name == "materiales" else "transparent")
        self.hoja_costos_menu.configure(fg_color=("gray75", "gray25") if name == "hoja_costos" else "transparent")
        # self.carga_prestacional_menu.configure(fg_color=("gray75", "gray25") if name == "carga" else "transparent")
        self.valor_minuto_menu.configure(fg_color=("gray75", "gray25") if name == "valor_minuto" else "transparent")
        self.costo_corte_piezas_menu.configure(fg_color=("gray75", "gray25") if name == "costo_corte_piezas" else "transparent")
        self.consumo_hilo_menu.configure(fg_color=("gray75", "gray25") if name == "consumo_hilo" else "transparent")

        # show selected frame
        if name == "home":
            self.home_frame.grid(row=0, column=1, sticky="nsew")
        else:
            self.home_frame.grid_forget()

        if name == "productos":
            self.productos_frame.grid(row=0, column=1, sticky="nsew")
        else:
            self.productos_frame.grid_forget()

        if name == "materiales":
            self.materiales_frame.grid(row=0, column=1, sticky="nsew")
        else:
            self.materiales_frame.grid_forget()

        if name == "hoja_costos":
            self.third_frame.grid(row=0, column=1, sticky="nsew")
        else:
            self.third_frame.grid_forget()

        if name == "carga":
            self.carga_prestacional_frame.grid(row=0, column=1, sticky="nsew")
        else:
            self.carga_prestacional_frame.grid_forget()

        if name == "valor_minuto":
            self.valor_minuto_frame.grid(row=0, column=1, sticky="nsew")
        else:
            self.valor_minuto_frame.grid_forget()

        if name == "costo_corte_piezas":
            self.costos_corte_piezas_frame.grid(row=0, column=1, sticky="nsew")
        else:
            self.costos_corte_piezas_frame.grid_forget()

        if name == "consumo_hilo":
            self.consumo_hilo_frame.grid(row=0, column=1, sticky="nsew")
        else:
            self.consumo_hilo_frame.grid_forget()

    def home_button_event(self):
        self.select_frame_by_name("home")

    def productos_menu_event(self):
        self.select_frame_by_name("productos")

        try:
            self.abrir_conexion()
            self.conexion.row_factory = sqlite3.Row
            cursor = self.conexion.cursor()
            sql = "select * from scp_producto"
            cursor.execute(sql)
            registro = cursor.fetchall()

            # Clear the data in Treeview widget
            self.tree_productos.delete(*self.tree_productos.get_children())

            # Insert the data in Treeview widget
            for r in registro:
                if r["foto"] is None:
                    print("Ninguna")
                    imagen_miniatura_tk = ""
                    self.tree_productos.insert('', END, image=imagen_miniatura_tk, values=(r["id"], r["nombre"], r["referencia"], r["tiempo_estandar"], "Ninguna"))
                else:
                    print("Si foto")

                    img = Image.open(BytesIO(r["foto"]))
                    print(img)
                    imagen_miniatura_pil = img.resize((20, 20), Image.LANCZOS)
                    print(imagen_miniatura_pil)
                    imagen_miniatura_tk = ImageTk.PhotoImage(imagen_miniatura_pil)
                    print(imagen_miniatura_tk)
                    self.tree_productos.insert("", END, image=imagen_miniatura_tk, values=(r["id"], r["nombre"], r["referencia"], r["tiempo_estandar"], "Si foto"))
        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error conectando a la base de datos.\n{e}")
        finally:
            self.conexion.close()

    def materiales_menu_event(self):
        self.select_frame_by_name("materiales")

        try:
            self.abrir_conexion()
            self.conexion.row_factory = sqlite3.Row
            cursor = self.conexion.cursor()
            sql = "select * from scp_material"  # where tipo='Directo' "
            cursor.execute(sql)
            registro = cursor.fetchall()

            # Clear the data in Treeview widget
            self.tree_materiales.delete(*self.tree_materiales.get_children())

            # Insert the data in Treeview widget
            for r in registro:
                self.tree_materiales.insert('', 'end', values=(r["id"], r["nombre_tipo"], r["proveedor"], r["unidad_med"], (f"${r['costo_unitario']:,.1f}").replace(',','*').replace('.', ',').replace('*','.'), r["tipo"]))
        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error conectando a la base de datos.\n{e}")
        finally:
            self.conexion.close()

    def hoja_costos_menu_event(self):
        self.select_frame_by_name("hoja_costos")

    def carga_prestacional_event(self):
        # Habilitar los campos de sólo lectura para cálculos y luego volver a deshabilitarlos en cada caso
        self.frame_col1_entry1.configure(fg_color=('white', 'white'), text_color=('black', 'black'))
        self.frame_col2_entry2.configure(fg_color=('white', 'white'), text_color=('black', 'black'))

        self.frame_col1_entry3.configure(state='normal', fg_color=('silver', '#c0c0c0'), text_color=('black', 'black'))
        self.frame_col1_entry5.configure(state='normal', fg_color=('silver', '#c0c0c0'), text_color=('black', 'black'))
        self.frame_col1_entry7.configure(state='normal', fg_color=('silver', '#c0c0c0'), text_color=('black', 'black'))
        self.frame_col1_entry9.configure(state='normal', fg_color=('silver', '#c0c0c0'), text_color=('black', 'black'))
        self.frame_col2_entry4.configure(state='normal', fg_color=('silver', '#c0c0c0'), text_color=('black', 'black'))
        self.frame_col2_entry6.configure(state='normal', fg_color=('silver', '#c0c0c0'), text_color=('black', 'black'))
        self.frame_col2_entry8.configure(state='normal', fg_color=('silver', '#c0c0c0'), text_color=('black', 'black'))
        self.frame_col2_entry10.configure(state='normal', fg_color=('silver', '#c0c0c0'), text_color=('black', 'black'))
        self.frame_col1_entry91.configure(state='normal', fg_color=('silver', '#c0c0c0'), text_color=('black', 'black'))
        self.frame_col2_entry101.configure(state='normal', fg_color=('silver', '#c0c0c0'), text_color=('black', 'black'))

        self.frame_row1_entry11.configure(state='normal', fg_color=('silver', '#c0c0c0'), text_color=('black', 'black'))

        try:
            self.abrir_conexion()
            self.conexion.row_factory = sqlite3.Row
            cursor = self.conexion.cursor()
            sql = "select * from scp_config"
            cursor.execute(sql)
            datos = cursor.fetchall()

            suma_porcentaje = 0
            self.porcentajes_carga_prestacional[10] = 0

            for i in datos:
                if i["param"] == "salario_min":
                    self.salario_min = float(i["value1"])
                    self.frame_col1_entry1.delete(0, END)
                    # self.frame_col1_entry1.insert(0, locale.format_string("%.2f", self.salario_min, grouping=True))
                    self.frame_col1_entry1.insert(0, (f"${self.salario_min:,.0f}").replace(',','*').replace('.', ',').replace('*','.'))
                elif i["param"] == "aux_transporte":
                    self.aux_transporte = float(i["value1"])
                    self.frame_col2_entry2.delete(0, END)
                    # self.frame_col2_entry2.insert(0, locale.format_string("%.2f", self.aux_transporte, grouping=True))
                    self.frame_col2_entry2.insert(0, (f"${self.aux_transporte:,.0f}").replace(',','*').replace('.', ',').replace('*','.'))
                elif i["param"] == "prima_servicios":
                    self.prima_servicios = self.salario_min * (float(i["value2"]) / 100)
                    self.frame_col1_entry3.delete(0, END)
                    # self.frame_col1_entry3.insert(0, locale.format_string("%.2f", self.prima_servicios, grouping=True))
                    self.frame_col1_entry3.insert(0, (f"${self.prima_servicios:,.0f}").replace(',','*').replace('.', ',').replace('*','.'))
                    self.frame_col1_entry3.configure(state='readonly')
                    self.frame_col1_label3.configure(text=f"Prima de Servicios ({i['value2']}%)")

                    self.porcentajes_carga_prestacional[0] = float(i['value2'])
                elif i["param"] == "aux_cesantias":
                    self.aux_cesantias = self.salario_min * (float(i["value2"]) / 100)
                    self.frame_col2_entry4.delete(0, END)
                    # self.frame_col2_entry4.insert(0, locale.format_string("%.2f", self.aux_cesantias, grouping=True))
                    self.frame_col2_entry4.insert(0, (f"${self.aux_cesantias:,.0f}").replace(',','*').replace('.', ',').replace('*','.'))
                    self.frame_col2_entry4.configure(state='readonly')
                    self.frame_col2_label4.configure(text=f"Cesantías ({i['value2']}%)")

                    self.porcentajes_carga_prestacional[1] = float(i['value2'])
                elif i["param"] == "intereses_cesantias":
                    self.intereses_cesantias = self.salario_min * (float(i["value2"]) / 100)
                    self.frame_col1_entry5.delete(0, END)
                    # self.frame_col1_entry5.insert(0, locale.format_string("%.2f", self.intereses_cesantias, grouping=True))
                    self.frame_col1_entry5.insert(0, (f"${self.intereses_cesantias:,.0f}").replace(',','*').replace('.', ',').replace('*','.'))
                    self.frame_col1_entry5.configure(state='readonly')
                    self.frame_col1_label5.configure(text=f"Intereses de Cesantías ({i['value2']}%)")

                    self.porcentajes_carga_prestacional[2] = float(i['value2'])
                elif i["param"] == "vacaciones":
                    self.vacaciones = self.salario_min * (float(i["value2"]) / 100)
                    self.frame_col2_entry6.delete(0, END)
                    # self.frame_col2_entry6.insert(0, locale.format_string("%.2f", self.vacaciones, grouping=True))
                    self.frame_col2_entry6.insert(0, (f"${self.vacaciones:,.0f}").replace(',','*').replace('.', ',').replace('*','.'))
                    self.frame_col2_entry6.configure(state='readonly')
                    self.frame_col2_label6.configure(text=f"Vacaciones ({i['value2']}%)")

                    self.porcentajes_carga_prestacional[3] = float(i['value2'])
                elif i["param"] == "caja_compensacion":
                    self.caja_compensacion = self.salario_min * (float(i["value2"]) / 100)
                    self.frame_col1_entry7.delete(0, END)
                    # self.frame_col1_entry7.insert(0, locale.format_string("%.2f", self.caja_compensacion, grouping=True))
                    self.frame_col1_entry7.insert(0, (f"${self.caja_compensacion:,.0f}").replace(',','*').replace('.', ',').replace('*','.'))
                    self.frame_col1_entry7.configure(state='readonly')
                    self.frame_col1_label7.configure(text=f"Caja de Compensación ({i['value2']}%)")

                    self.porcentajes_carga_prestacional[4] = float(i['value2'])
                elif i["param"] == "pension":
                    self.pension = self.salario_min * (float(i["value2"]) / 100)
                    self.frame_col2_entry8.delete(0, END)
                    # self.frame_col2_entry8.insert(0, locale.format_string("%.2f", self.pension, grouping=True))
                    self.frame_col2_entry8.insert(0, (f"${self.pension:,.0f}").replace(',','*').replace('.', ',').replace('*','.'))
                    self.frame_col2_entry8.configure(state='readonly')
                    self.frame_col2_label8.configure(text=f"Pensión ({i['value2']}%)")

                    self.porcentajes_carga_prestacional[5] = float(i['value2'])
                elif i["param"] == "arl":
                    self.arl = self.salario_min * (float(i["value2"]) / 100)
                    self.frame_col1_entry9.delete(0, END)
                    # self.frame_col1_entry9.insert(0, locale.format_string("%.2f", self.arl, grouping=True))
                    self.frame_col1_entry9.insert(0, (f"${self.arl:,.0f}").replace(',','*').replace('.', ',').replace('*','.'))
                    self.frame_col1_entry9.configure(state='readonly')
                    self.frame_col1_label9.configure(text=f"Arl ({i['value2']}%)")

                    self.porcentajes_carga_prestacional[6] = float(i['value2'])
                elif i["param"] == "salud":
                    self.salud = self.salario_min * (float(i["value2"]) / 100)
                    self.salud_porcentaje = i["value2"]
                    self.frame_col2_entry10.delete(0, END)
                    # self.frame_col2_entry10.insert(0, locale.format_string("%.2f", self.salud, grouping=True))
                    self.frame_col2_entry10.insert(0, (f"${self.salud:,.0f}").replace(',','*').replace('.', ',').replace('*','.'))
                    self.frame_col2_entry10.configure(state='readonly')
                    self.frame_col2_label10.configure(text=f"Salud ({i['value2']}%)")

                    self.porcentajes_carga_prestacional[7] = float(i['value2'])

                    if i["value3"] == "on":
                        self.frame_col2_label10.select()
                        self.salud_opt3 = "on"
                    else:
                        self.frame_col2_label10.deselect()
                        self.salud_opt3 = "off"

                elif i["param"] == "dotacion":
                    self.dotacion = self.salario_min * (float(i["value2"]) / 100)
                    self.dotacion_porcentaje = i["value2"]
                    self.frame_col2_entry101.delete(0, END)
                    # self.frame_col2_entry101.insert(0, locale.format_string("%.2f", self.dotacion, grouping=True))
                    self.frame_col2_entry101.insert(0, (f"${self.dotacion:,.0f}").replace(',','*').replace('.', ',').replace('*','.'))
                    self.frame_col2_entry101.configure(state='readonly')
                    self.frame_col2_carga_opt2.configure(text=f"Dotación ({i['value2']}% aprox.)")

                    self.porcentajes_carga_prestacional[8] = float(i['value2'])

                    if i["value3"] == "on":
                        self.frame_col2_carga_opt2.select()
                        self.carga_opt2 = "on"
                    else:
                        self.frame_col2_carga_opt2.deselect()
                        self.carga_opt2 = "off"
                elif i["param"] == "parafiscales":
                    self.parafiscales = self.salario_min * (float(i["value2"]) / 100)
                    self.parafiscales_porcentaje = i["value2"]
                    self.frame_col1_entry91.delete(0, END)
                    # self.frame_col1_entry91.insert(0, locale.format_string("%.2f", self.parafiscales, grouping=True))
                    self.frame_col1_entry91.insert(0, (f"${self.parafiscales:,.0f}").replace(',','*').replace('.', ',').replace('*','.'))
                    self.frame_col1_entry91.configure(state='readonly')
                    self.frame_col1_carga_opt1.configure(text=f"SENA + ICBF ({i['value2']}%)")

                    self.porcentajes_carga_prestacional[9] = float(i['value2'])

                    if i["value3"] == "on":
                        self.frame_col1_carga_opt1.select()
                        self.carga_opt1 = "on"
                    else:
                        self.frame_col1_carga_opt1.deselect()
                        self.carga_opt1 = "off"
                elif i["param"] == "total_costo_mensual_trabajador":
                    for clave, valor in self.porcentajes_carga_prestacional.items():
                        if clave == 7 and self.salud_opt3 == "on":
                            suma_porcentaje += valor
                        if clave == 8 and self.carga_opt2 == "on":
                            suma_porcentaje += valor
                        if clave == 9 and self.carga_opt1 == "on":
                            suma_porcentaje += valor
                        
                        if clave != 7 and clave != 8 and clave != 9:
                            suma_porcentaje += valor

                    self.porcentajes_carga_prestacional[10] = suma_porcentaje

                    self.total_costo_mensual_trabajador = (self.salario_min * (suma_porcentaje) / 100) + self.aux_transporte + self.salario_min
                    self.frame_row1_entry11.delete(0, END)
                    # self.frame_row1_entry11.insert(0, locale.format_string("%.2f", self.total_costo_mensual_trabajador, grouping=True))
                    self.frame_row1_entry11.insert(0, (f"${self.total_costo_mensual_trabajador:,.0f}").replace(',','*').replace('.', ',').replace('*','.'))

                    self.frame_row1_entry11.configure(state='readonly')
                    self.frame_row1_label11.configure(text=f"Total Costo Mensual Trabajador ({suma_porcentaje}%)")
        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error recuperando los datos.\n{e}")
        finally:
            self.conexion.close()

        self.select_frame_by_name("carga")

    def guardar_carga_prestacional_event(self):
        records_to_update = [
            (self.frame_col1_entry1.get().replace("$", "").replace(".", "").replace(",", "."), "salario_min"),
            (self.frame_col2_entry2.get().replace("$", "").replace(".", "").replace(",", "."), "aux_transporte")
        ]
        records_to_update_value3 = [
            (self.frame_col2_label10.get(), "salud"),
            (self.frame_col2_carga_opt2.get(), "dotacion"),
            (self.frame_col1_carga_opt1.get(), "parafiscales")
        ]

        try:
            self.abrir_conexion()
            cursor = self.conexion.cursor()
            sql = "update scp_config set value1 = ? where param = ?"
            cursor.executemany(sql, records_to_update)
            self.conexion.commit()
            self.conexion.close()
            # actualizar los checkbox en base de datos
            self.abrir_conexion()
            cursor = self.conexion.cursor()
            sql = "update scp_config set value3 = ? where param = ?"
            cursor.executemany(sql, records_to_update_value3)
            self.conexion.commit()
            self.conexion.close()
            # Actualizar resto de datos después de guardar el salario mínimo y auxilio de transporte
            self.carga_prestacional_event()
            records_to_update = [
                (self.frame_col1_entry3.get().replace(".", "").replace(",", "."), "prima_servicios"),
                (self.frame_col2_entry4.get().replace(".", "").replace(",", "."), "aux_cesantias"),
                (self.frame_col1_entry5.get().replace(".", "").replace(",", "."), "intereses_cesantias"),
                (self.frame_col2_entry6.get().replace(".", "").replace(",", "."), "vacaciones"),
                (self.frame_col1_entry7.get().replace(".", "").replace(",", "."), "caja_compensacion"),
                (self.frame_col2_entry8.get().replace(".", "").replace(",", "."), "pension"),
                (self.frame_col1_entry9.get().replace(".", "").replace(",", "."), "arl"),
                (self.frame_col2_entry10.get().replace(".", "").replace(",", "."), "salud"),
                (self.frame_col2_entry101.get().replace(".", "").replace(",", "."), "dotacion"),
                (self.frame_col1_entry91.get().replace(".", "").replace(",", "."), "parafiscales"),
                (self.frame_row1_entry11.get().replace(".", "").replace(",", "."), "total_costo_mensual_trabajador")
            ]
            self.abrir_conexion()
            cursor = self.conexion.cursor()
            sql = "update scp_config set value1 = ? where param = ?"
            cursor.executemany(sql, records_to_update)
            self.conexion.commit()
            # Actualizar porcentaje de carga prestacional total configurada
            self.abrir_conexion()
            cursor = self.conexion.cursor()
            sql = "update scp_config set value2 = ? where param = 'total_costo_mensual_trabajador' "
            cursor.execute(sql, (self.porcentajes_carga_prestacional[10], ))
            self.conexion.commit()
            # fin - actualizar porcentaje
            messagebox.showinfo(title="SCP", message="Carga prestacional guardada!")
            
        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error guardando en base de datos.\n{e}")
        finally:
            self.conexion.close()

    def calcular_vm_event(self):
        try:
            # Costos y gastos fijos mensuales
            A = float(self.vm_frame_col1_entry5.get())
            # Operarios
            B = float(self.vm_frame_col1_entry7.get())
            # Jornada
            C = float(self.vm_frame_col1_entry9.get())
            # Dias promedio trabaja empresa
            D = float(self.vm_frame_col1_entry11.get())
            # Porcentaje eficiencia
            E = float(self.vm_frame_col1_entry13.get())
            # Minutos hora
            F = 60
            # tiempo estándar
            te = float(self.vm_frame_col1_entry15.get())

            VALOR_MINUTO = A / (B*C*D*(E/100)*F)
            COSTO_FIJO_UNITARIO = VALOR_MINUTO * te

            self.vm_frame_row1_entry1.delete(0, END)
            self.vm_frame_row1_entry1.insert(0, f"{VALOR_MINUTO:0.2f}")

            self.vm_frame_row1_entry2.delete(0, END)
            self.vm_frame_row1_entry2.insert(0, f"{COSTO_FIJO_UNITARIO:0.2f}")
        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error:\nSeleccione un producto o llene todos los campos...")
            self.vm_frame_col1_entry1.focus()


    def valor_minuto_menu_event(self):
        self.select_frame_by_name("valor_minuto")

    def costo_corte_piezas_menu_event(self):
        self.select_frame_by_name("costo_corte_piezas")

    def consumo_hilo_menu_event(self):
        # Llamo frame principal
        self.select_frame_by_name("consumo_hilo")

    def consumo_hilo_tipo_tejido_callback(self, choice):
        self.consumo_hilo_tipo_tejido = StringVar(value=choice)
        if choice == "Liviano":
            print(f"Selección: {choice}")
            self.ch_frame_row1.grid(row=3, column=0, sticky="wen", padx=0, pady=0)
        else:
            self.ch_frame_row1.grid_forget()

        if choice == "Mediano":
            print(f"Selección: {choice}")
            self.ch_frame_row2.grid(row=3, column=0, sticky="wen", padx=0, pady=0)
        else:
            self.ch_frame_row2.grid_forget()

        if choice == "Pesado":
            print(f"Selección: {choice}")
            self.ch_frame_row3.grid(row=3, column=0, sticky="wen", padx=0, pady=0)
        else:
            self.ch_frame_row3.grid_forget()

    def optionmenu_callback(self, choice):
        self.hoja_costos_producto = StringVar(value=choice)
        self.tab1_CIF_total_acumulado = 0
        if self.hoja_costos_producto.get() == "Seleccione el Producto":
            self.hoja_costos_frame_label.configure(text=f"Hoja de Costos")
        else:
            self.hoja_costos_frame_label.configure(text=f"Hoja de Costos ({self.hoja_costos_producto.get()})")

            id = choice.split(".")[0]
            # consulta resumen, tab1 ========================================
            try:
                import traceback
                self.abrir_conexion()
                self.conexion.row_factory = sqlite3.Row
                cursor = self.conexion.cursor()
                sql = "select * from scp_producto sp inner join scp_hoja_costo hc on sp.id = hc.producto and hc.producto = ? "
                cursor.execute(sql, (id,))
                registro = cursor.fetchone()

                if registro is None or len(registro) == 0:
                    raise sqlite3.Warning("La consulta no trajo datos.")

                self.tab1_ref_valor.configure(text=registro["referencia"])
                self.tab1_tiempoE_valor.configure(text=registro["tiempo_estandar"])
                self.tab1_fecha_valor.configure(text=registro["fecha_creacion"])
                self.tab1_eficiencia_valor.configure(text=f"{registro['eficiencia']}%")
                self.tab1_jornadaL_valor.configure(text=registro["jornada"])
                self.tab1_num_operarios_valor.configure(text=registro["operarios"])
                self.tab1_min_mes_valor.configure(text=registro["minutos_mes"])
                self.tab1_utilidad = registro['utilidad'] / 100
                self.tab1_utilidad_valor.configure(text=f"{registro['utilidad']}%")
                self.tab1_num_operarios_valor.configure(text=registro["operarios"])

                self.tab1_operarios = registro["operarios"]
                self.tab1_eficiencia = registro["eficiencia"]
                self.tab1_tiempo_estandar = registro["tiempo_estandar"]

                self.tab1_u_prod_mes_valor_calculo = (registro["minutos_mes"] / registro["tiempo_estandar"] * registro["operarios"]) * (registro["eficiencia"]/100)

                # actualizo dato en tab1
                self.tab1_u_prod_mes_valor.configure(text=(f"${self.tab1_u_prod_mes_valor_calculo:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))
            except sqlite3.Warning as e:
                self.tab1_ref_valor.configure(text="")
                self.tab1_tiempoE_valor.configure(text="")
                self.tab1_fecha_valor.configure(text="")
                self.tab1_eficiencia_valor.configure(text="")
                self.tab1_jornadaL_valor.configure(text="")
                self.tab1_num_operarios_valor.configure(text="")
                self.tab1_min_mes_valor.configure(text="")
                self.tab1_utilidad_valor.configure(text="")
                self.tab1_num_operarios_valor.configure(text="")

                self.tab1_mat_directo_valor.configure(text="---")
                self.tab1_u_prod_mes_valor.configure(text="---")
                self.tab1_MOD_valor.configure(text="---")
                self.tab1_CIF_valor.configure(text="---")
                self.tab1_COSTO_TOTAL_valor.configure(text="---")
                self.tab1_precio_venta_valor.configure(text="---")

                # self.tab1_u_prod_mes_valor_calculo = 0

                messagebox.showinfo(title="SCP", message=f"{e} en la hoja Resumen")
            except Exception as e:
                # messagebox.showerror(title="SCP", message=f"Error:\n{e}")
                messagebox.showerror(title="SCP", message=f"Error:\n{e}\n{traceback.format_exc()}")
            finally:
                self.conexion.close()

            # consulta costo de materiales directos, tab2 ==================================
            try:
                # Clear the data in Treeview widget
                self.tree_materiales_directos.delete(*self.tree_materiales_directos.get_children())

                self.abrir_conexion()
                self.conexion.row_factory = sqlite3.Row
                cursor = self.conexion.cursor()
                sql = """SELECT * from scp_hoja_material shm 
                inner join scp_hoja_costo shc on shm.hoja_costo_id = shc.id 
                inner join scp_material sm on shm.material_id = sm.id 
                inner join scp_producto sp on shc.producto = sp.id 
                where sp.id = ?"""
                cursor.execute(sql, (id,))
                registro = cursor.fetchall()

                if registro is None or len(registro) == 0:
                    raise sqlite3.Warning("La consulta no trajo datos.")

                # Insert the data in Treeview widget
                self.total_costo_materiales_directos = 0
                for r in registro:
                    self.total_costo_materiales_directos += (r["consumo_unit"] * r['costo_unit_historico'])
                    self.tree_materiales_directos.insert('', 'end', values=(r["id"], r["nombre_tipo"], r["proveedor"], r["unidad_med"], r["consumo_unit"], (f"${r['costo_unit_historico']:,.1f}").replace(',','*').replace('.', ',').replace('*','.'), (f"${(r['consumo_unit'] * r['costo_unit_historico']):,.0f}").replace(',','*').replace('.', ',').replace('*','.') ))
                    # self.tree_materiales_directos.insert("", END, values=r)

                self.tab2_total.configure(text=(f"TOTALES: ${self.total_costo_materiales_directos:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))
                # actualizo dato en tab1
                self.tab1_mat_directo_valor.configure(text=(f"${self.total_costo_materiales_directos:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))
            except sqlite3.Warning as e:
                messagebox.showinfo(title="SCP", message=f"{e} en materiales directos")
            except Exception as e:
                messagebox.showerror(title="SCP", message=f"Error:\n{e}")
            finally:
                self.conexion.close()

            # consulta costo de mano de obra directa, tab3 ==================================
            import traceback
            try:
                self.abrir_conexion()
                self.conexion.row_factory = sqlite3.Row
                cursor = self.conexion.cursor()
                sql = "select * from scp_config"
                cursor.execute(sql)
                datos = cursor.fetchall()

                if datos is None or len(datos) == 0:
                    raise sqlite3.Warning("La consulta no trajo datos.")

                for i in datos:
                    if i["param"] == "salario_min":
                        self.salario_min = float(i["value1"])
                        self.tab3_sal_basico_valor.configure(text=(f"${self.salario_min:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))

                    if i["param"] == "salud":
                        self.salud = self.salario_min * (float(i["value2"]) / 100)

                    if i["param"] == "aux_transporte":
                        self.aux_transporte = float(i["value1"])
                        self.tab3_sub_transp_valor.configure(text=(f"${self.aux_transporte:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))

                    if i["param"] == "total_costo_mensual_trabajador":
                        self.porcentaje_carga_prestacional = (float(i["value2"]) / 100)
                        self.tab3_carga_pres_valor.configure(text=(f"{self.porcentaje_carga_prestacional*100:,.2f}%").replace(',', '*').replace('.', ',').replace('*', '.'))

                        self.total_costo_mensual_trabajador = (self.salario_min * (float(i["value2"]) / 100)) + self.aux_transporte + self.salario_min
                        self.tab3_total_sal_valor.configure(text=(f"${self.total_costo_mensual_trabajador:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))
                        self.tab3_total_sal_otros_valor.configure(text=(f"${self.total_costo_mensual_trabajador:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))

                # Cálculos especiales tab3
                try:
                    self.tab3_mod = int(self.tab1_num_operarios_valor.cget('text')) * self.total_costo_mensual_trabajador
                    self.tab3_costo_mod_valor.configure(text=(f"${self.tab3_mod:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))
                except:
                    self.tab3_mod = 0
                    self.tab3_costo_mod_valor.configure(text="---")

                try:
                    self.tab3_min_efectivo = int(self.tab1_num_operarios_valor.cget('text')) * 24 * 8 * 60
                except:
                    self.tab3_min_efectivo = 0
                self.tab3_min_mes_valor.configure(text=(f"${self.tab3_min_efectivo:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))

                try:
                    self.tab3_factor_minuto = self.tab3_mod / self.tab3_min_efectivo
                except:
                    self.tab3_factor_minuto = 0
                self.tab3_fact_min_valor.configure(text=(f"${self.tab3_factor_minuto:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))

                try:
                    self.tab3_costo_unit_mod = self.tab3_factor_minuto * float(self.tab1_tiempoE_valor.cget("text"))
                    # actualizo dato en tab1
                    self.tab1_MOD_valor.configure(text=(f"${self.tab3_costo_unit_mod:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))
                except:
                    self.tab3_costo_unit_mod = 0
                self.tab3_unitario_mod_valor.configure(text=(f"${self.tab3_costo_unit_mod:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))

            except sqlite3.Warning as e:
                messagebox.showinfo(title="SCP", message=f"{e} en mano de obra directa")
            except Exception as e:
                messagebox.showerror(title="SCP", message=f"Error:\n{e}\n{traceback.format_exc()}")
            finally:
                self.conexion.close()

            # consulta costo de materiales indirectos, tab4 ==================================
            try:
                # Clear the data in Treeview widget
                self.tree_materiales_indirectos.delete(*self.tree_materiales_indirectos.get_children())

                self.abrir_conexion()
                self.conexion.row_factory = sqlite3.Row
                cursor = self.conexion.cursor()
                sql = """SELECT * from scp_hoja_material_indirecto shmi
                inner join scp_hoja_costo shc on shmi.hoja_costo_id = shc.id
                inner join scp_material sm on shmi.material_id = sm.id 
                inner join scp_producto sp on shc.producto = sp.id 
                where sp.id = ?"""
                cursor.execute(sql, (id,))
                registro = cursor.fetchall()

                if registro is None or len(registro) == 0:
                    raise sqlite3.Warning("La consulta no trajo datos.")

                # Insert the data in Treeview widget
                self.total_costo_materiales_indirectos = 0
                self.total_costo_materiales_indirectos_asignado = 0
                for r in registro:
                    self.total_costo_materiales_indirectos += (r["consumo_mes"] * r['costo_historico'])
                    self.total_costo_materiales_indirectos_asignado += (r["consumo_mes"] * r['costo_historico']) / self.tab1_u_prod_mes_valor_calculo

                    self.tree_materiales_indirectos.insert('', 'end', values=(
                        r["id"], r["nombre_tipo"], r["unidad_med"], r["consumo_mes"],
                        (f"${r['costo_historico']:,.1f}").replace(',', '*').replace('.', ',').replace('*', '.'),
                        (f"${(r['consumo_mes'] * r['costo_historico']):,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'),
                        (f"${(r['consumo_mes'] * r['costo_historico']) / self.tab1_u_prod_mes_valor_calculo:,.2f}").replace(',', '*').replace('.', ',').replace('*', '.')))
                    # self.tree_materiales_indirectos.insert("", END, values=r)

                self.tab4_total1.configure(text=(f"TOTALES: ${self.total_costo_materiales_indirectos:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))
                self.tab4_total2.configure(text=(f"TOTALES ASIGNADO: ${self.total_costo_materiales_indirectos_asignado:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))

                # actualizo dato en tab1
                self.tab1_CIF_total_acumulado += self.total_costo_materiales_indirectos_asignado
                self.tab1_CIF_valor.configure(text=(f"${self.tab1_CIF_total_acumulado:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))
            except sqlite3.Warning as e:
                messagebox.showinfo(title="SCP", message=f"{e} en materiales indirectos")
            except Exception as e:
                messagebox.showerror(title="SCP", message=f"Error:\n{e}")
            finally:
                self.conexion.close()

            # consulta costo de materiales indirectos, tab5 ==================================
            try:
                # Clear the data in Treeview widget
                self.tree_mano_obra_indirecta.delete(*self.tree_mano_obra_indirecta.get_children())

                self.abrir_conexion()
                self.conexion.row_factory = sqlite3.Row
                cursor = self.conexion.cursor()
                # sql = """SELECT * from scp_hoja_mano_obra_indirecta shmoi
                # inner join scp_cargos_mano_obra scmo on shmoi.cargos_id = scmo.id
                # where shmoi.hoja_costo_id = ?"""

                sql = """SELECT shmoi.*, scmo.* from scp_hoja_mano_obra_indirecta shmoi 
				inner join scp_hoja_costo shc on shmoi.hoja_costo_id = shc.id
                inner join scp_cargos_mano_obra scmo on shmoi.cargos_id = scmo.id  
                inner join scp_producto sp on shc.producto = sp.id 
                where sp.id = ? """

                cursor.execute(sql, (id,))
                registro = cursor.fetchall()

                if registro is None or len(registro) == 0:
                    raise sqlite3.Warning("La consulta no trajo datos.")

                # Insert the data in Treeview widget
                self.total_salario_mes = 0
                self.total_salario = 0
                self.total_costo_mano_obra_indirecta_asignado = 0
                for r in registro:
                    if r["cargo"] == "GERENTE":
                        salario = r["salario_mes_historico"]
                    else:
                        salario = r["salario_mes_historico"] + (r["salario_mes_historico"] * self.porcentaje_carga_prestacional)

                    self.total_salario_mes += r["salario_mes_historico"]
                    self.total_salario += salario

                    costo_asignado = salario / self.tab1_u_prod_mes_valor_calculo
                    self.total_costo_mano_obra_indirecta_asignado += costo_asignado

                    self.tree_mano_obra_indirecta.insert('', 'end', values=(
                        r["id"], r["cargo"], (f"${r['salario_mes_historico']:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'),
                        (f"${salario:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'),
                        (f"${costo_asignado:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.')))
                    # self.tree_mano_obra_indirecta.insert("", END, values=r)

                self.tab5_total1.configure(text=(f"TOTALES SALARIO/MES: ${self.total_salario_mes:,.2f}").replace(',', '*').replace('.', ',').replace('*', '.'))
                self.tab5_total2.configure(text=(f"TOTALES SALARIO: ${self.total_salario:,.2f}").replace(',', '*').replace('.', ',').replace('*', '.'))
                self.tab5_total3.configure(text=(f"TOTALES COSTO ASIGNADO: ${self.total_costo_mano_obra_indirecta_asignado:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))

                # actualizo dato en tab1
                self.tab1_CIF_total_acumulado += self.total_costo_mano_obra_indirecta_asignado
                self.tab1_CIF_valor.configure(text=(f"${self.tab1_CIF_total_acumulado:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))
            except sqlite3.Warning as e:
                messagebox.showinfo(title="SCP", message=f"{e} en mano de obra indirecta")
            except Exception as e:
                messagebox.showerror(title="SCP", message=f"Error:\n{e}")
            finally:
                self.conexion.close()

            # consulta costo de materiales indirectos, tab6 ==================================
            try:
                # Clear the data in Treeview widget
                self.tree_costos_administrativos.delete(*self.tree_costos_administrativos.get_children())

                self.abrir_conexion()
                self.conexion.row_factory = sqlite3.Row
                cursor = self.conexion.cursor()
                sql = """SELECT shca.*, sca.* from scp_hoja_costos_administrativos shca  
				inner join scp_hoja_costo shc on shca.hoja_costo_id = shc.id
                inner join scp_costos_admon sca on shca.costos_admin_id = sca.id  
                inner join scp_producto sp on shc.producto = sp.id 
                where sp.id = ? """
                cursor.execute(sql, (id,))
                registro = cursor.fetchall()

                if registro is None or len(registro) == 0:
                    raise sqlite3.Warning("La consulta no trajo datos.")

                # Insert the data in Treeview widget
                self.total_costo_total_mes = 0
                self.total_costos_administrativos_asignado = 0

                for r in registro:
                    self.total_costo_total_mes += r["costo_mes_historico"]

                    total_costos_administrativos_asignado = r["costo_mes_historico"] / self.tab1_u_prod_mes_valor_calculo
                    self.total_costos_administrativos_asignado += total_costos_administrativos_asignado

                    self.tree_costos_administrativos.insert('', 'end', values=(
                        r["id"], r["tipo_costo"], (f"${r['costo_mes_historico']:,.2f}").replace(',', '*').replace('.', ',').replace('*', '.'),
                        (f"${total_costos_administrativos_asignado:,.2f}").replace(',', '*').replace('.', ',').replace('*', '.')))
                    # self.tree_costos_administrativos.insert("", END, values=r)

                self.tab6_total1.configure(text=(f"TOTALES COSTO TOTAL/MES: ${self.total_costo_total_mes:,.2f}").replace(',', '*').replace('.', ',').replace('*', '.'))
                self.tab6_total2.configure(text=(f"TOTALES COSTO ASIGNADO: ${self.total_costos_administrativos_asignado:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))

                # actualizo dato en tab1
                self.tab1_CIF_total_acumulado += self.total_costos_administrativos_asignado
                self.tab1_CIF_valor.configure(text=(f"${self.tab1_CIF_total_acumulado:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))
            except sqlite3.Warning as e:
                messagebox.showinfo(title="SCP", message=f"{e} en costos administrativos")
            except Exception as e:
                messagebox.showerror(title="SCP", message=f"Error:\n{e}")
            finally:
                self.conexion.close()

            # consulta costo de materiales indirectos, tab7 ==================================
            try:
                # Clear the data in Treeview widget
                self.tree_deprecia_maquinaria.delete(*self.tree_deprecia_maquinaria.get_children())

                self.abrir_conexion()
                self.conexion.row_factory = sqlite3.Row
                cursor = self.conexion.cursor()
                sql = """SELECT shdm.*, sm.* from scp_hoja_depreciacion_maquinaria shdm  
				inner join scp_hoja_costo shc on shdm.hoja_costo_id = shc.id
                inner join scp_maquinaria sm on shdm.bien_id  = sm.id 
                inner join scp_producto sp on shc.producto = sp.id 
                where sp.id = ? """
                cursor.execute(sql, (id,))
                registro = cursor.fetchall()

                if registro is None or len(registro) == 0:
                    raise sqlite3.Warning("La consulta no trajo datos.")

                # Insert the data in Treeview widget
                self.total_depreciacion_mes = 0
                self.total_depreciacion_maquinaria_mes = 0

                for r in registro:
                    self.total_depreciacion_mes += r["valor_comercial_historico"]

                    total_depreciacion_maquinaria_mes = r["cantidad"] * r["valor_comercial_historico"]
                    self.total_depreciacion_maquinaria_mes += total_depreciacion_maquinaria_mes / r["vida_util"]

                    self.tree_deprecia_maquinaria.insert('', 'end', values=(
                        r["id"], r["tipo_bien"], r["cantidad"], (f"${r['valor_comercial_historico']:,.2f}").replace(',', '*').replace('.', ',').replace('*', '.'),
                        (f"${total_depreciacion_maquinaria_mes:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'), r["vida_util"],
                        (f"${total_depreciacion_maquinaria_mes / r['vida_util']:,.2f}").replace(',', '*').replace('.', ',').replace('*', '.')))
                    # self.tree_deprecia_maquinaria.insert("", END, values=r)

                self.tab7_total1.configure(text=(f"TOTAL DEPRECIACION /MES: ${self.total_depreciacion_maquinaria_mes:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))
                costo_asignado_total = self.total_depreciacion_maquinaria_mes / self.tab1_u_prod_mes_valor_calculo
                self.tab7_total2.configure(text=(f"COSTO ASIGNADO: ${costo_asignado_total:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))

                # actualizo dato en tab1
                self.tab1_CIF_total_acumulado += costo_asignado_total
                self.tab1_CIF_valor.configure(text=(f"${self.tab1_CIF_total_acumulado:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))

                self.tab1_COSTO_TOTAL = self.total_costo_materiales_directos + self.tab3_costo_unit_mod + self.tab1_CIF_total_acumulado
                self.tab1_COSTO_TOTAL_valor.configure(text=(f"${self.tab1_COSTO_TOTAL:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))

                self.tab1_PRECIO_VENTA = self.tab1_COSTO_TOTAL / (1 - self.tab1_utilidad)
                self.tab1_precio_venta_valor.configure(text=(f"${self.tab1_PRECIO_VENTA:,.0f}").replace(',', '*').replace('.', ',').replace('*', '.'))
            except sqlite3.Warning as e:
                messagebox.showinfo(title="SCP", message=f"{e} en depreciación de maquinaria")
            except Exception as e:
                messagebox.showerror(title="SCP", message=f"Error:\n{e}")
            finally:
                self.conexion.close()

    # fin callback - hoja de costos
    def valor_minuto_seleccionar_producto(self, choice):
        # limpiar valores
        self.vm_frame_col2_label4.configure(text=f"<< PRODUCTO >>")
        self.vm_frame_col2_label8.configure(text=f"$0")
        self.vm_frame_col2_label26.configure(text=f"8")
        self.vm_frame_col2_label30.configure(text=f"25")

        self.vm_frame_col2_label12.configure(text=f"$0")

        self.vm_frame_col1_entry1.delete(0, END)
        self.vm_frame_col1_entry1.insert(0, "")

        self.vm_frame_col1_entry3.delete(0, END)
        self.vm_frame_col1_entry3.insert(0, "")

        # costos y gastos fijos mensuales
        self.vm_frame_col1_entry5.delete(0, END)
        self.vm_frame_col1_entry5.insert(0, "")
        self.vm_frame_col2_label18.configure(text="0")

        self.vm_frame_col2_label22.configure(text="0")
        self.vm_frame_col1_entry7.delete(0, END)
        self.vm_frame_col1_entry7.insert(0, "")

        self.vm_frame_col2_label34.configure(text="0%")
        self.vm_frame_col1_entry13.delete(0, END)
        self.vm_frame_col1_entry13.insert(0, "")

        self.vm_frame_col2_label42.configure(text="0")
        self.vm_frame_col1_entry15.delete(0, END)
        self.vm_frame_col1_entry15.insert(0, "")
        # fin - limpiar valores

        # ejecuto función de hoja de costos para el producto
        self.tab1_producto.set(choice)
        self.optionmenu_callback(choice)

        if choice != "Seleccione el Producto":
            # conseguir datos para valor minuto
            self.valor_minuto_producto = StringVar(value=choice)

            self.vm_frame_col2_label4.configure(text=f"<< {self.valor_minuto_producto.get()} >>")
            # valor = f"{self.tab1_PRECIO_VENTA:0.0f}"
            # self.vm_frame_col2_label8.configure(text=f"${str(valor).replace(',','*').replace('.', ',').replace('*','.')}")
            self.vm_frame_col2_label8.configure(text=f"${self.vm_valida(self.tab1_PRECIO_VENTA, 0)}".replace(",", "X").replace(".", ",").replace("X", "."))
            # valor = f"{self.tab1_COSTO_TOTAL:0.0f}"
            # self.vm_frame_col2_label12.configure(text=f"${str(valor).replace(',','*').replace('.', ',').replace('*','.')}")
            self.vm_frame_col2_label12.configure(text=f"${self.vm_valida(self.tab1_COSTO_TOTAL, 0)}".replace(",", "X").replace(".", ",").replace("X", "."))

            self.vm_frame_col1_entry1.delete(0, END)
            self.vm_frame_col1_entry1.insert(0, f"{self.tab1_PRECIO_VENTA:0.0f}")

            self.vm_frame_col1_entry3.delete(0, END)
            self.vm_frame_col1_entry3.insert(0, f"{self.tab1_COSTO_TOTAL:0.0f}")

            self.vm_frame_col2_label22.configure(text=self.tab1_operarios)
            self.vm_frame_col1_entry7.delete(0, END)
            self.vm_frame_col1_entry7.insert(0, self.tab1_operarios)

            self.vm_frame_col2_label34.configure(text=f"{self.tab1_eficiencia}%")
            self.vm_frame_col1_entry13.delete(0, END)
            self.vm_frame_col1_entry13.insert(0, self.tab1_eficiencia)

            self.vm_frame_col2_label42.configure(text=f"{self.tab1_tiempo_estandar}")
            self.vm_frame_col1_entry15.delete(0, END)
            self.vm_frame_col1_entry15.insert(0, self.tab1_tiempo_estandar)

            self.vm_frame_col1_entry5.focus()
        else:
            self.vm_frame_row1_entry1.delete(0, END)
            self.vm_frame_row1_entry1.insert(0, "0")

            self.vm_frame_row1_entry2.delete(0, END)
            self.vm_frame_row1_entry2.insert(0, "0")

            self.vm_frame_col1_entry1.focus()

    def consumo_hilo_seleccionar_producto(self, choice):
        # buscar id de material consumo hilo
        query1 = f'SELECT * FROM scp_material WHERE nombre_tipo = "Hilo-{choice}"'
        registro1 = self.run_query(query1)
        datos1 = registro1.fetchone()
        if datos1 is not None:
            # Si existe el material, consultar consumo de hilo
            query2 = f'SELECT * FROM scp_consumo_hilo WHERE id_material = {datos1[0]}'
            registro2 = self.run_query(query2)
            datos2 = registro2.fetchone()

            if datos2 is not None:
                self.material_id_consumo_hilo = datos1[0]
                print(f"ID MATERIAL _: {self.material_id_consumo_hilo}")
                # Si existe el material, consultar consumo de hilo
                print(datos2)
                # Mostrar elementos GUI, según tipo de tejido de base de datos
                if datos2[2] == "Liviano":
                    self.consumo_hilo_tipo_tejido = StringVar(value="Liviano")
                    self.ch_frame_row1.grid(row=3, column=0, sticky="wen", padx=0, pady=0)
                    # Colocar valores en campos y tabla

                    # Desperdicio
                    self.ch_entry1.delete(0, END)
                    self.ch_entry1.insert(0, f"{datos2[3]:0.1f}")

                    # Punt. x Pulg. Botones
                    botones1 = datos2[4][1:-1].split("*")
                    for i, v in enumerate(botones1):
                        self.button_grid1._rows[i][0].configure(fg="purple")
                        self.button_grid1._rows[i][0].configure(text=f"{v}")

                    # Metros Costura
                    entradas1 = datos2[5][1:-1].split("*")
                    for i, v in enumerate(entradas1):
                        self.entry_grid1._rows[i][0].delete(0, END)
                        self.entry_grid1._rows[i][0].insert(0, f"{v}")

                    # Metros Hilo
                    label1 = datos2[6][1:-1].split("*")
                    for i, v in enumerate(label1):
                        self.label_grid3._rows[i][0].configure(text=f"{v}")

                    # Total Hilo
                    label1 = datos2[7][1:-1].split("*")
                    for i, v in enumerate(label1):
                        self.label_grid3._rows[i][1].configure(text=f"{v}")

                    # Tipo de Hilo
                    botones2 = datos2[8][1:-1].split("*")
                    for i, v in enumerate(botones2):
                        self.button_grid2._rows[i][0].configure(fg="purple")
                        self.button_grid2._rows[i][0].configure(text=f"{v}")

                    # Color de Hilo
                    botones2 = datos2[9][1:-1].split("*")
                    for i, v in enumerate(botones2):
                        self.button_grid2._rows[i][1].configure(fg="purple")
                        self.button_grid2._rows[i][1].configure(text=f"{v}")

                    # Total metros producc
                    label2 = datos2[10][1:-1].split("*")
                    for i, v in enumerate(label2):
                        self.label_grid4._rows[i][0].configure(text=f"{v}")

                    # Total conos
                    label2 = datos2[11][1:-1].split("*")
                    for i, v in enumerate(label2):
                        self.label_grid4._rows[i][1].configure(text=f"{v}")

                    # Total conos redondeado
                    label2 = datos2[12][1:-1].split("*")
                    for i, v in enumerate(label2):
                        self.label_grid4._rows[i][2].configure(text=f"{v}")

                    # TOTAL consumo de hilo
                    self.ch_resultados_total.configure(text=f"TOTAL consumo Hilo: {datos2[13]} mts.")

                    # Sol
                    self.ch_sol.delete(0, END)
                    self.ch_sol.insert(0, f"{datos2[14]}")

                    # Astra
                    self.ch_astra.delete(0, END)
                    self.ch_astra.insert(0, f"{datos2[15]}")

                    # Epic
                    self.ch_epic.delete(0, END)
                    self.ch_epic.insert(0, f"{datos2[16]}")

                    # Llamar función que recalcule de nuevo, revisar si es necesario.
                elif datos2[2] == "Mediano":
                    self.consumo_hilo_tipo_tejido = StringVar(value="Mediano")
                    self.ch_frame_row2.grid(row=3, column=0, sticky="wen", padx=0, pady=0)
                    # Colocar valores en campos y tabla

                    # Desperdicio
                    self.ch_entry1.delete(0, END)
                    self.ch_entry1.insert(0, f"{datos2[3]:0.1f}")

                    # Punt. x Pulg. Botones
                    botones1 = datos2[4][1:-1].split("*")
                    for i, v in enumerate(botones1):
                        self.button_grid1_mediano._rows[i][0].configure(fg="purple")
                        self.button_grid1_mediano._rows[i][0].configure(text=f"{v}")

                    # Metros Costura
                    entradas1 = datos2[5][1:-1].split("*")
                    for i, v in enumerate(entradas1):
                        self.entry_grid1_mediano._rows[i][0].delete(0, END)
                        self.entry_grid1_mediano._rows[i][0].insert(0, f"{v}")

                    # Metros Hilo
                    label1 = datos2[6][1:-1].split("*")
                    for i, v in enumerate(label1):
                        self.label_grid3_mediano._rows[i][0].configure(text=f"{v}")

                    # Total Hilo
                    label1 = datos2[7][1:-1].split("*")
                    for i, v in enumerate(label1):
                        self.label_grid3_mediano._rows[i][1].configure(text=f"{v}")

                    # Tipo de Hilo
                    botones2 = datos2[8][1:-1].split("*")
                    for i, v in enumerate(botones2):
                        self.button_grid2_mediano._rows[i][0].configure(fg="purple")
                        self.button_grid2_mediano._rows[i][0].configure(text=f"{v}")

                    # Color de Hilo
                    botones2 = datos2[9][1:-1].split("*")
                    for i, v in enumerate(botones2):
                        self.button_grid2_mediano._rows[i][1].configure(fg="purple")
                        self.button_grid2_mediano._rows[i][1].configure(text=f"{v}")

                    # Total metros producc
                    label2 = datos2[10][1:-1].split("*")
                    for i, v in enumerate(label2):
                        self.label_grid4_mediano._rows[i][0].configure(text=f"{v}")

                    # Total conos
                    label2 = datos2[11][1:-1].split("*")
                    for i, v in enumerate(label2):
                        self.label_grid4_mediano._rows[i][1].configure(text=f"{v}")

                    # Total conos redondeado
                    label2 = datos2[12][1:-1].split("*")
                    for i, v in enumerate(label2):
                        self.label_grid4_mediano._rows[i][2].configure(text=f"{v}")

                    # TOTAL consumo de hilo
                    self.ch_resultados_total.configure(text=f"TOTAL consumo Hilo: {datos2[13]} mts.")

                    # Sol
                    self.ch_sol.delete(0, END)
                    self.ch_sol.insert(0, f"{datos2[14]}")

                    # Astra
                    self.ch_astra.delete(0, END)
                    self.ch_astra.insert(0, f"{datos2[15]}")

                    # Epic
                    self.ch_epic.delete(0, END)
                    self.ch_epic.insert(0, f"{datos2[16]}")

                    # Llamar función que recalcule de nuevo, revisar si es necesario.
                elif datos2[2] == "Pesado":
                    self.consumo_hilo_tipo_tejido = StringVar(value="Pesado")
                    self.ch_frame_row3.grid(row=3, column=0, sticky="wen", padx=0, pady=0)
                    # Colocar valores en campos y tabla

                    # Desperdicio
                    self.ch_entry1.delete(0, END)
                    self.ch_entry1.insert(0, f"{datos2[3]:0.1f}")

                    # Punt. x Pulg. Botones
                    botones1 = datos2[4][1:-1].split("*")
                    for i, v in enumerate(botones1):
                        self.button_grid1_pesado._rows[i][0].configure(fg="purple")
                        self.button_grid1_pesado._rows[i][0].configure(text=f"{v}")

                    # Metros Costura
                    entradas1 = datos2[5][1:-1].split("*")
                    for i, v in enumerate(entradas1):
                        self.entry_grid1_pesado._rows[i][0].delete(0, END)
                        self.entry_grid1_pesado._rows[i][0].insert(0, f"{v}")

                    # Metros Hilo
                    label1 = datos2[6][1:-1].split("*")
                    for i, v in enumerate(label1):
                        self.label_grid3_pesado._rows[i][0].configure(text=f"{v}")

                    # Total Hilo
                    label1 = datos2[7][1:-1].split("*")
                    for i, v in enumerate(label1):
                        self.label_grid3_pesado._rows[i][1].configure(text=f"{v}")

                    # Tipo de Hilo
                    botones2 = datos2[8][1:-1].split("*")
                    for i, v in enumerate(botones2):
                        self.button_grid2_pesado._rows[i][0].configure(fg="purple")
                        self.button_grid2_pesado._rows[i][0].configure(text=f"{v}")

                    # Color de Hilo
                    botones2 = datos2[9][1:-1].split("*")
                    for i, v in enumerate(botones2):
                        self.button_grid2_pesado._rows[i][1].configure(fg="purple")
                        self.button_grid2_pesado._rows[i][1].configure(text=f"{v}")

                    # Total metros producc
                    label2 = datos2[10][1:-1].split("*")
                    for i, v in enumerate(label2):
                        self.label_grid4_pesado._rows[i][0].configure(text=f"{v}")

                    # Total conos
                    label2 = datos2[11][1:-1].split("*")
                    for i, v in enumerate(label2):
                        self.label_grid4_pesado._rows[i][1].configure(text=f"{v}")

                    # Total conos redondeado
                    label2 = datos2[12][1:-1].split("*")
                    for i, v in enumerate(label2):
                        self.label_grid4_pesado._rows[i][2].configure(text=f"{v}")

                    # TOTAL consumo de hilo
                    self.ch_resultados_total.configure(text=f"TOTAL consumo Hilo: {datos2[13]} mts.")

                    # Sol
                    self.ch_sol.delete(0, END)
                    self.ch_sol.insert(0, f"{datos2[14]}")

                    # Astra
                    self.ch_astra.delete(0, END)
                    self.ch_astra.insert(0, f"{datos2[15]}")

                    # Epic
                    self.ch_epic.delete(0, END)
                    self.ch_epic.insert(0, f"{datos2[16]}")

                    # Llamar función que recalcule de nuevo, revisar si es necesario.
                # Tarea todo revisar cuando se camba de tipo de tejido, LIMPIAR
                # Cargar tabla

                self.ch_button_tipo_tejido.set(self.consumo_hilo_tipo_tejido.get())
                self.consumo_hilo_tipo_tejido_callback(self.consumo_hilo_tipo_tejido.get())
                self.consumo_hilo_menu_event()
                # Mostrar elementos GUI, resultados
                self.ch_frame_row0.grid(row=2, column=0, sticky="wen", padx=0, pady=0)
                self.ch_frame_row4.grid(row=6, column=0, sticky="wen", padx=0, pady=10)
                self.ch_resultados_total.grid(row=4, column=0, sticky="ewn", pady=(10, 0))
                self.ch_resultados_tutul.grid(row=5, column=0, sticky="ewn", pady=0)
                self.ch_frame_button2.grid(row=7, column=0, sticky="wen", padx=0, pady=10)

            else:
                self.material_id_consumo_hilo = datos1[0]
                print(f"ID MATERIAL _: {self.material_id_consumo_hilo}")
                # Existe el material, pero no se ha almacenado información: INSERT
                messagebox.showinfo(title="SCP", message=f"Por favor llenar datos y luego clic en Guardar.")
                # Mostrar elementos GUI de tabla Liviano
                self.ch_frame_row0.grid(row=2, column=0, sticky="wen", padx=0, pady=0)
                self.ch_frame_row1.grid(row=3, column=0, sticky="wen", padx=0, pady=0)
                # Mostrar elementos GUI, resultados
                self.ch_frame_row4.grid(row=6, column=0, sticky="wen", padx=0, pady=10)
                self.ch_resultados_total.grid(row=4, column=0, sticky="ewn", pady=(10, 0))
                self.ch_resultados_tutul.grid(row=5, column=0, sticky="ewn", pady=0)
                self.ch_frame_button2.grid(row=7, column=0, sticky="wen", padx=0, pady=10)
                # Pre-cargo tabla en tejido Liviano
                self.consumo_hilo_tipo_tejido = StringVar(value="Liviano")
                self.ch_button_tipo_tejido.set(self.consumo_hilo_tipo_tejido.get())
                self.consumo_hilo_tipo_tejido_callback(self.consumo_hilo_tipo_tejido.get())
                self.consumo_hilo_menu_event()
        else:
            # Ocultar elementos GUI
            self.ch_frame_row0.grid_forget()
            self.ch_frame_row1.grid_forget()
            self.ch_frame_row2.grid_forget()
            self.ch_frame_row3.grid_forget()
            self.ch_frame_row4.grid_forget()
            self.ch_resultados_total.grid_forget()
            self.ch_resultados_tutul.grid_forget()
            self.ch_frame_button2.grid_forget()
            messagebox.showwarning(title="SCP", message=f"Error, no existe el material Hilo para este Producto")

    def change_appearance_mode_event(self, modo=False):
        if modo:
            # Modificamos la apariencia desde la base de datos
            set_appearance_mode(modo)
            # Cambiamos valor a varible <class 'tkinter.StringVar'>
            self.selected_option.set(modo)
        else:
            # Modificamos la apariencia desde el menú de configuración
            set_appearance_mode(self.selected_option.get())
            # Guardamos cambios en DB
            try:
                self.abrir_conexion()
                cursor = self.conexion.cursor()
                sql = "update scp_config set value1 = ? where param = 'tema'"
                datos = (self.selected_option.get(),)
                cursor.execute(sql, datos)
                self.conexion.commit()
                # messagebox.showinfo(title="SCP", message="Configuración guardada!")
            except Exception as e:
                messagebox.showerror(title="SCP", message=f"Error guardando en base de datos.\n{e}")
            finally:
                self.conexion.close()


    def help_menu_event(self):
        # Crear una ventana secundaria.
        ventana_secundaria = CTkToplevel()
        ventana_secundaria.resizable(False, False)
        # quitar minimizar....
        ventana_secundaria.title("Ayuda SCP")
        ventana_secundaria.config(width=300, height=200)
        # Crear un botón dentro de la ventana secundaria
        # para cerrar la misma.
        boton_cerrar = CTkButton(
            ventana_secundaria,
            text="Cerrar ventana",
            command=ventana_secundaria.destroy
        )
        boton_cerrar.place(x=75, y=75)
        ventana_secundaria.focus()
        # Modal...
        ventana_secundaria.transient(self)  # dialog window is related to main
        ventana_secundaria.wait_visibility()
        ventana_secundaria.grab_set()
        ventana_secundaria.wait_window()

    def help_menu_about_event(self):
        messagebox.showinfo("Acerca de ...", "\nProducto Técnico-Pedagógico:\n\nSIMULADOR DE COSTOS DE PRODUCCIÓN PARA LA CONFECCIÓN DE PRENDAS DE VESTIR\n\n\nSENA - Centro de Formación en Diseño, Confección y Moda\n\nColombia - 2023")

    def productos_guardar(self):
        # Crear una ventana secundaria.
        self.ventana_operaciones = CTkToplevel()
        self.ventana_operaciones.resizable(False, False)
        # quitar minimizar....
        self.ventana_operaciones.title("Agregar Producto")

        ancho_ventana = 400
        alto_ventana = 350
        x_ventana = self.winfo_screenwidth() // 2 - ancho_ventana // 2
        y_ventana = self.winfo_screenheight() // 2 - alto_ventana // 2

        posicion = str(ancho_ventana) + "x" + str(alto_ventana) + "+" + str(x_ventana) + "+" + str(y_ventana)
        self.ventana_operaciones.geometry(posicion)
        self.ventana_operaciones.focus()

        self.ventana_operaciones.columnconfigure(0, weight=1)
        self.ventana_operaciones.columnconfigure(1, weight=2)

        # GUI
        global filename, aux
        filename = ""
        aux = ""

        # Nombre
        CTkLabel(self.ventana_operaciones, text='Nombre: ').grid(row=1, column=0)
        self.operaciones_nombre = CTkEntry(self.ventana_operaciones)
        self.operaciones_nombre.focus()
        self.operaciones_nombre.grid(row=1, column=1, padx=10, pady=5, sticky="ew")

        # Referencia
        CTkLabel(self.ventana_operaciones, text='Referencia: ').grid(row=2, column=0)
        self.operaciones_referencia = CTkEntry(self.ventana_operaciones)
        self.operaciones_referencia.grid(row=2, column=1, padx=10, pady=5, sticky="ew")

        # Tiempo Estándar
        CTkLabel(self.ventana_operaciones, text='Tiempo Estándar: ').grid(row=3, column=0)
        self.operaciones_tiempo_e = CTkEntry(self.ventana_operaciones)
        self.operaciones_tiempo_e.grid(row=3, column=1, padx=10, pady=5, sticky="ew")

        # Foto
        CTkLabel(self.ventana_operaciones, text='Foto: ').grid(row=4, column=0)
        self.operaciones_foto = CTkButton(self.ventana_operaciones, text='Subir archivo', fg_color="#878787",
                                          hover_color="#6D6D6D", command=self.upload_file)
        self.operaciones_foto.grid(row=4, column=1, padx=10, pady=5, sticky="ew")

        self.operaciones_label_foto = CTkLabel(self.ventana_operaciones, wraplength="380", text="...", image=None, compound="top")
        self.operaciones_label_foto.grid(row=5, column=0, columnspan=2, pady=(5, 0))

        boton_guardar = CTkButton(self.ventana_operaciones, text="Guardar", command=self.add_product)
        boton_guardar.grid(row=6, column=0, columnspan=2, padx=10, pady=10, sticky="ew")
        # fin - GUI

        # Modal...
        self.ventana_operaciones.transient(self)  # dialog window is related to main
        self.ventana_operaciones.wait_visibility()
        self.ventana_operaciones.grab_set()
        self.ventana_operaciones.wait_window()

    def upload_file(self):
        global filename, aux
        # Image upload and display
        f_types = [('Png files', '*.png'), ('Jpg Files', '*.jpg'), ('Jpeg Files', '*.jpeg')]
        try:
            filename = filedialog.askopenfilename(filetypes=f_types)
            img = CTkImage(Image.open(filename), size=(110, 110))  # using Button
            self.operaciones_label_foto.configure(text=f"{filename.split('/')[-1]}", image=img)
            aux = filename
        except Exception as e:
            print("No seleccionó foto")

        # Mantener la última foto seleccionada o limpiar si se cierra la ventana de operaciones
        filename = aux if aux else ""

    def productos_eliminar(self):
        try:
            item = self.tree_productos.item(self.tree_productos.selection())['values']
            respuesta = messagebox.askokcancel(title=f"Confirmación", message=f"""Está seguro de eliminar el producto <<{item[1]}>>""")
            if respuesta:
                respuesta2 = messagebox.askretrycancel(title=f"Advertencia!!", message=f"""Advertencia!!\n\nSi elimina este producto perderá los datos asociado como: Hoja de Costos y Consumo de Hilo.\n\nAún así desea eliminar el producto  <<{item[1]}>> y todos sus datos?""")
                if respuesta2:
                    import traceback
                    try:
                        # buscar hoja de costo del producto
                        query1 = 'SELECT * FROM scp_hoja_costo WHERE producto = ?'
                        registro1 = self.run_query(query1, (item[0],))
                        datos1 = registro1.fetchone()

                        # buscar id de material consumo hilo
                        query2 = f'SELECT * FROM scp_material WHERE nombre_tipo = "Hilo-{item[0]}.{item[1]}"'
                        print(query2)
                        registro2 = self.run_query(query2)
                        datos2 = registro2.fetchone()

                        # eliminar hoja_material por id de material e id de hoja_costo
                        query3 = 'DELETE FROM scp_hoja_material WHERE material_id = ? and hoja_costo_id = ?'
                        self.run_query(query3, (datos2[0], datos1[0]))

                        # eliminar material
                        query4 = 'DELETE FROM scp_material WHERE id = ?'
                        self.run_query(query4, (datos2[0],))

                        # eliminar hoja de costo
                        query5 = 'DELETE FROM scp_hoja_costo WHERE id = ?'
                        self.run_query(query5, (datos1[0],))

                        # falta eliminar otras hojas <=================================================================
                        # falta eliminar otras hojas <=================================================================
                        # falta eliminar otras hojas <=================================================================

                        # finalmente eliminar el producto
                        query = 'DELETE FROM scp_producto WHERE id = ?'

                        self.run_query(query, (item[0],))
                        messagebox.showinfo(title="SCP", message=f"Producto '{item[1]}' eliminado correctamente!!")
                        # actualizamos listado en productos
                        self.productos_menu_event()
                    except Exception as e:
                        messagebox.showerror(title="SCP", message=f"Error:\n{e}\n{traceback.format_exc()}")
                    finally:
                        self.conexion.close()
        except IndexError as e:
            messagebox.showwarning(title=f"Error: ", message=f"""Seleccione un Producto...""")

    def productos_editar(self):
        try:
            item = self.tree_productos.item(self.tree_productos.selection())['values']

            query = 'SELECT * FROM scp_producto WHERE id = ?'
            parameters = (item[0],)
            registro = self.run_query(query, parameters)
            datos = registro.fetchone()

            # =================================================
            # Crear una ventana secundaria.
            self.ventana_operaciones = CTkToplevel()
            self.ventana_operaciones.resizable(False, False)
            # quitar minimizar....
            self.ventana_operaciones.title(f"Editar Producto: <<{datos[1]}>>")

            ancho_ventana = 400
            alto_ventana = 350
            x_ventana = self.winfo_screenwidth() // 2 - ancho_ventana // 2
            y_ventana = self.winfo_screenheight() // 2 - alto_ventana // 2

            posicion = str(ancho_ventana) + "x" + str(alto_ventana) + "+" + str(x_ventana) + "+" + str(y_ventana)
            self.ventana_operaciones.geometry(posicion)
            self.ventana_operaciones.focus()

            self.ventana_operaciones.columnconfigure(0, weight=1)
            self.ventana_operaciones.columnconfigure(1, weight=2)

            # GUI
            global filename, aux
            filename = ""
            aux = ""

            # ID
            self.operacionesid_label = CTkLabel(self.ventana_operaciones, text='Id: ')
            self.operacionesid_label.grid(row=0, column=0)
            self.operacionesid_label.grid_remove()


            self.operaciones_id = CTkEntry(self.ventana_operaciones)
            self.operaciones_id.grid(row=0, column=1, padx=10, pady=5, sticky="ew")
            self.operaciones_id.delete(0, END)
            self.operaciones_id.insert(0, datos[0])
            self.operaciones_id.configure(state='readonly', fg_color=('silver', '#c0c0c0'), text_color=('black', 'black'))
            self.operaciones_id.grid_remove()

            # Nombre
            CTkLabel(self.ventana_operaciones, text='Nombre: ').grid(row=1, column=0)
            self.operaciones_nombre = CTkEntry(self.ventana_operaciones)
            self.operaciones_nombre.focus()
            self.operaciones_nombre.grid(row=1, column=1, padx=10, pady=5, sticky="ew")
            self.operaciones_nombre.delete(0, END)
            self.operaciones_nombre.insert(0, datos[1])

            # Referencia
            CTkLabel(self.ventana_operaciones, text='Referencia: ').grid(row=2, column=0)
            self.operaciones_referencia = CTkEntry(self.ventana_operaciones)
            self.operaciones_referencia.grid(row=2, column=1, padx=10, pady=5, sticky="ew")
            self.operaciones_referencia.delete(0, END)
            self.operaciones_referencia.insert(0, datos[2])

            # Tiempo Estándar
            CTkLabel(self.ventana_operaciones, text='Tiempo Estándar: ').grid(row=3, column=0)
            self.operaciones_tiempo_e = CTkEntry(self.ventana_operaciones)
            self.operaciones_tiempo_e.grid(row=3, column=1, padx=10, pady=5, sticky="ew")
            self.operaciones_tiempo_e.delete(0, END)
            self.operaciones_tiempo_e.insert(0, datos[3])

            # Foto
            CTkLabel(self.ventana_operaciones, text='Foto: ').grid(row=4, column=0)
            self.operaciones_foto = CTkButton(self.ventana_operaciones, text='Subir archivo', fg_color="#878787",
                                              hover_color="#6D6D6D", command=self.upload_file)
            self.operaciones_foto.grid(row=4, column=1, padx=10, pady=5, sticky="ew")

            self.operaciones_label_foto = CTkLabel(self.ventana_operaciones, wraplength="380", text="...", image=None,
                                                   compound="top")
            self.operaciones_label_foto.grid(row=5, column=0, columnspan=2, pady=(5, 0))
            # cargar foto guardada
            if datos[4] is None:
                self.operaciones_label_foto.configure(text="Ninguna foto")
            else:
                img_byte = BytesIO(datos[4])
                img = ImageTk.PhotoImage(Image.open(img_byte).resize((110, 110)))
                self.operaciones_label_foto.configure(image=img, text="")

                # img = ImageTk.PhotoImage(data=datos[4])
                # self.operaciones_label_foto.configure(image=img).resize(110, 110)

            boton_guardar = CTkButton(self.ventana_operaciones, text="Actualizar", command=self.update_product)
            boton_guardar.grid(row=6, column=0, columnspan=2, padx=10, pady=20, sticky="ew")
            # fin - GUI

            # Modal...
            self.ventana_operaciones.transient(self)  # dialog window is related to main
            self.ventana_operaciones.wait_visibility()
            self.ventana_operaciones.grab_set()
            self.ventana_operaciones.wait_window()
            # =================================================

        except IndexError as e:
            messagebox.showwarning(title=f"Error: ", message=f"""Seleccione un Producto...""")

    # Function to Execute Database Querys
    def run_query(self, query, parameters=()):
        self.abrir_conexion()
        cursor = self.conexion.cursor()
        result = cursor.execute(query, parameters)
        self.conexion.commit()

        return result

    def add_product(self):
        global filename
        import traceback
        try:
            nombre = self.operaciones_nombre.get()
            referencia = self.operaciones_referencia.get()
            tiempo_e = self.operaciones_tiempo_e.get()
            if filename:
                fob = open(filename, 'rb')  # filename from upload_file()
                fob = fob.read()
            else:
                fob = None

            if len(nombre) != 0 and len(referencia) != 0 and len(tiempo_e) != 0:
                query = 'INSERT INTO scp_producto VALUES(null, ?, ?, ?, ?)'
                parameters = (nombre, referencia, tiempo_e, fob)
                res = self.run_query(query, parameters)
                # Capturar last insert ID
                id_producto = res.lastrowid

                # Crear material especial de Consumo de Hilo
                query2 = 'INSERT INTO scp_material VALUES(null, ?, ?, ?, ?, ?, ?)'
                parameters2 = (f"Hilo-{id_producto}.{nombre}", "Especial Consumo HILO", "mts", "0", "Hilo", None)
                res2 = self.run_query(query2, parameters2)
                id_material = res2.lastrowid

                # Crear hoja de costos para el producto
                query3 = 'INSERT INTO scp_hoja_costo VALUES(null, ?, ?, ?, ?, ?, ?, ?)'
                parameters3 = ("2014-01-01", "0", "0", "0", "0", "0", id_producto)
                res3 = self.run_query(query3, parameters3)
                id_hoja_costo = res3.lastrowid

                # Asocial material a Hoja de Costo del Producto
                query4 = 'INSERT INTO scp_hoja_material VALUES(NULL, ?, ?, ?, ?)'
                parameters4 = (id_material, "0", "0", id_hoja_costo)
                res4 = self.run_query(query4, parameters4)
                id_hoja_costo_material = res4.lastrowid

                messagebox.showinfo(title="SCP", message=f"Producto agregado correctamente!!")

                # Cerramos la ventana
                self.ventana_operaciones.destroy()
                # actualizamos listado en productos
                self.productos_menu_event()
            else:
                messagebox.showwarning(title="SCP", message=f"Por favor llene todos los datos..")
        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error guardando en base de datos.\n{e}\n{traceback.format_exc()}")
        finally:
            self.conexion.close()

    def update_product(self):
        import traceback
        try:
            id = self.operaciones_id.get()
            nombre = self.operaciones_nombre.get()
            referencia = self.operaciones_referencia.get()
            tiempo_e = self.operaciones_tiempo_e.get()

            if len(nombre) != 0 and len(referencia) != 0 and len(tiempo_e) != 0:
                query = 'UPDATE scp_producto SET nombre = ?, referencia = ?, tiempo_estandar = ? WHERE id = ? '
                parameters = (nombre, referencia, tiempo_e, id)
                self.run_query(query, parameters)

                messagebox.showinfo(title="SCP", message=f"Producto '{id}' actualizado correctamente!!")

                # Cerramos la ventana
                self.ventana_operaciones.destroy()
                # actualizamos listado en productos
                self.productos_menu_event()
            else:
                messagebox.showwarning(title="SCP", message=f"Por favor llene todos los datos..")
        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error guardando en base de datos.\n{e}\n{traceback.format_exc()}")
        finally:
            self.conexion.close()

    # Materiales CRUD
    def materiales_guardar(self):
        # Crear una ventana secundaria.
        self.ventana_operaciones2 = CTkToplevel()
        self.ventana_operaciones2.resizable(False, False)
        # quitar minimizar....
        self.ventana_operaciones2.title("Agregar Material")

        ancho_ventana = 400
        alto_ventana = 410
        x_ventana = self.winfo_screenwidth() // 2 - ancho_ventana // 2
        y_ventana = self.winfo_screenheight() // 2 - alto_ventana // 2

        posicion = str(ancho_ventana) + "x" + str(alto_ventana) + "+" + str(x_ventana) + "+" + str(y_ventana)
        self.ventana_operaciones2.geometry(posicion)
        self.ventana_operaciones2.focus()

        self.ventana_operaciones2.columnconfigure(0, weight=1)
        self.ventana_operaciones2.columnconfigure(1, weight=2)

        # GUI
        global filename, aux
        filename = ""
        aux = ""

        # Nombre
        CTkLabel(self.ventana_operaciones2, text='Nombre Material: ').grid(row=1, column=0)
        self.operaciones2_nombre_tipo = CTkEntry(self.ventana_operaciones2)
        self.operaciones2_nombre_tipo.focus()
        self.operaciones2_nombre_tipo.grid(row=1, column=1, padx=10, pady=5, sticky="ew")

        # Proveedor
        CTkLabel(self.ventana_operaciones2, text='Proveedor: ').grid(row=2, column=0)
        self.operaciones2_proveedor = CTkEntry(self.ventana_operaciones2)
        self.operaciones2_proveedor.grid(row=2, column=1, padx=10, pady=5, sticky="ew")

        # Unidad Medida
        CTkLabel(self.ventana_operaciones2, text='Unidad de Medida: ').grid(row=3, column=0)
        self.operaciones2_unidad_med = CTkEntry(self.ventana_operaciones2)
        self.operaciones2_unidad_med.grid(row=3, column=1, padx=10, pady=5, sticky="ew")

        # Costo Unitario
        CTkLabel(self.ventana_operaciones2, text='Costo Unitario: ').grid(row=4, column=0)
        self.operaciones2_costo_unitario = CTkEntry(self.ventana_operaciones2)
        self.operaciones2_costo_unitario.grid(row=4, column=1, padx=10, pady=5, sticky="ew")

        # Tipo Material
        CTkLabel(self.ventana_operaciones2, text='Tipo de Material: ').grid(row=5, column=0)
        self.operaciones2_tipo = CTkOptionMenu(self.ventana_operaciones2, values=["Directo", "Indirecto"], fg_color="#878787")
        self.operaciones2_tipo.grid(row=5, column=1, padx=10, pady=5, sticky="ew")

        # Foto
        CTkLabel(self.ventana_operaciones2, text='Foto: ').grid(row=6, column=0)
        self.operaciones2_foto = CTkButton(self.ventana_operaciones2, text='Subir archivo', fg_color="#878787",
                                          hover_color="#6D6D6D", command=self.upload_file_materiales)
        self.operaciones2_foto.grid(row=6, column=1, padx=10, pady=5, sticky="ew")

        self.operaciones2_label_foto = CTkLabel(self.ventana_operaciones2, wraplength="380", text="...", image=None, compound="top")
        self.operaciones2_label_foto.grid(row=7, column=0, columnspan=2, pady=(5, 0))

        boton_guardar = CTkButton(self.ventana_operaciones2, text="Guardar", command=self.add_material)
        boton_guardar.grid(row=8, column=0, columnspan=2, padx=10, pady=10, sticky="ew")
        # fin - GUI

        # Modal...
        self.ventana_operaciones2.transient(self)  # dialog window is related to main
        self.ventana_operaciones2.wait_visibility()
        self.ventana_operaciones2.grab_set()
        self.ventana_operaciones2.wait_window()

    def upload_file_materiales(self):
        global filename, aux
        # Image upload and display
        f_types = [('Png files', '*.png'), ('Jpg Files', '*.jpg'), ('Jpeg Files', '*.jpeg')]
        try:
            filename = filedialog.askopenfilename(filetypes=f_types)
            img = CTkImage(Image.open(filename), size=(110, 110))  # using Button
            self.operaciones2_label_foto.configure(text=f"{filename.split('/')[-1]}", image=img)
            aux = filename
        except Exception as e:
            print("No seleccionó foto")

        # Mantener la última foto seleccionada o limpiar si se cierra la ventana de operaciones
        filename = aux if aux else ""

    def add_material(self):
        global filename
        import traceback
        try:
            nombre_tipo = self.operaciones2_nombre_tipo.get()
            proveedor = self.operaciones2_proveedor.get()
            unidad_med = self.operaciones2_unidad_med.get()
            costo_unitario = self.operaciones2_costo_unitario.get()
            tipo = self.operaciones2_tipo.get()
            if filename:
                fob = open(filename, 'rb')  # filename from upload_file()
                fob = fob.read()
            else:
                fob = None

            if len(nombre_tipo) != 0 and len(unidad_med) != 0 and len(costo_unitario) != 0:
                query = 'INSERT INTO scp_material VALUES(NULL, ?, ?, ?, ?, ?, ?)'
                parameters = (nombre_tipo, proveedor, unidad_med, costo_unitario, tipo, fob)
                self.run_query(query, parameters)

                messagebox.showinfo(title="SCP", message=f"Material agregado correctamente!!")

                # Cerramos la ventana
                self.ventana_operaciones2.destroy()
                # actualizamos listado en productos
                self.materiales_menu_event()
            else:
                messagebox.showwarning(title="SCP", message=f"Por favor llene todos los datos..")
        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error guardando en base de datos.\n{e}\n{traceback.format_exc()}")
        finally:
            self.conexion.close()

    def materiales_eliminar(self):
        try:
            item = self.tree_materiales.item(self.tree_materiales.selection())['values']
            respuesta = messagebox.askokcancel(title=f"Confirmación", message=f"""Está seguro de eliminar el material <<{item[1]}>>""")
            if respuesta:
                query = 'DELETE FROM scp_material WHERE id = ?'
                import traceback
                try:
                    self.run_query(query, (item[0],))
                    messagebox.showinfo(title="SCP", message=f"Material '{item[1]}' eliminado correctamente!!")
                    # actualizamos listado en productos
                    self.materiales_menu_event()
                except Exception as e:
                    messagebox.showerror(title="SCP", message=f"Error:\n{e}\n{traceback.format_exc()}")
                finally:
                    self.conexion.close()
        except IndexError as e:
            messagebox.showwarning(title=f"Error: ", message=f"""Seleccione un Material...""")

    def materiales_editar(self):
        try:
            item = self.tree_materiales.item(self.tree_materiales.selection())['values']

            query = 'SELECT * FROM scp_material WHERE id = ?'
            parameters = (item[0],)
            registro = self.run_query(query, parameters)
            datos = registro.fetchone()

            # =================================================
            # Crear una ventana secundaria.
            self.ventana_operaciones2 = CTkToplevel()
            self.ventana_operaciones2.resizable(False, False)
            # quitar minimizar....
            self.ventana_operaciones2.title(f"Editar Material: <<{datos[1]}>>")

            ancho_ventana = 400
            alto_ventana = 410
            x_ventana = self.winfo_screenwidth() // 2 - ancho_ventana // 2
            y_ventana = self.winfo_screenheight() // 2 - alto_ventana // 2

            posicion = str(ancho_ventana) + "x" + str(alto_ventana) + "+" + str(x_ventana) + "+" + str(y_ventana)
            self.ventana_operaciones2.geometry(posicion)
            self.ventana_operaciones2.focus()

            self.ventana_operaciones2.columnconfigure(0, weight=1)
            self.ventana_operaciones2.columnconfigure(1, weight=2)

            # GUI
            global filename, aux
            filename = ""
            aux = ""

            # ID
            self.operaciones2id_label = CTkLabel(self.ventana_operaciones2, text='Id: ')
            self.operaciones2id_label.grid(row=0, column=0)
            self.operaciones2id_label.grid_remove()


            self.operaciones2_id = CTkEntry(self.ventana_operaciones2)
            self.operaciones2_id.grid(row=0, column=1, padx=10, pady=5, sticky="ew")
            self.operaciones2_id.delete(0, END)
            self.operaciones2_id.insert(0, datos[0])
            self.operaciones2_id.configure(state='readonly', fg_color=('silver', '#c0c0c0'), text_color=('black', 'black'))
            self.operaciones2_id.grid_remove()

            # Nombre
            CTkLabel(self.ventana_operaciones2, text='Nombre Material: ').grid(row=1, column=0)
            self.operaciones2_nombre_tipo = CTkEntry(self.ventana_operaciones2)
            self.operaciones2_nombre_tipo.focus()
            self.operaciones2_nombre_tipo.grid(row=1, column=1, padx=10, pady=5, sticky="ew")
            self.operaciones2_nombre_tipo.delete(0, END)
            self.operaciones2_nombre_tipo.insert(0, datos[1])

            # Proveedor
            CTkLabel(self.ventana_operaciones2, text='Proveedor: ').grid(row=2, column=0)
            self.operaciones2_proveedor = CTkEntry(self.ventana_operaciones2)
            self.operaciones2_proveedor.grid(row=2, column=1, padx=10, pady=5, sticky="ew")
            self.operaciones2_proveedor.delete(0, END)
            # Set value on combobox
            self.operaciones2_proveedor.insert(0, datos[2] if datos[2] != None else "" )

            # Unidad Medida
            CTkLabel(self.ventana_operaciones2, text='Unidad de Medida: ').grid(row=3, column=0)
            self.operaciones2_unidad_med = CTkEntry(self.ventana_operaciones2)
            self.operaciones2_unidad_med.grid(row=3, column=1, padx=10, pady=5, sticky="ew")
            self.operaciones2_unidad_med.delete(0, END)
            self.operaciones2_unidad_med.insert(0, datos[3])

            # Costo Unitario
            CTkLabel(self.ventana_operaciones2, text='Costo Unitario: ').grid(row=4, column=0)
            self.operaciones2_costo_unitario = CTkEntry(self.ventana_operaciones2)
            self.operaciones2_costo_unitario.grid(row=4, column=1, padx=10, pady=5, sticky="ew")
            self.operaciones2_costo_unitario.delete(0, END)
            self.operaciones2_costo_unitario.insert(0, datos[4])

            # Tipo Material
            CTkLabel(self.ventana_operaciones2, text='Tipo de Material: ').grid(row=5, column=0)
            self.operaciones2_tipo = CTkOptionMenu(self.ventana_operaciones2, values=["Directo", "Indirecto"],
                                                   fg_color="#878787")
            self.operaciones2_tipo.grid(row=5, column=1, padx=10, pady=5, sticky="ew")
            self.operaciones2_tipo.set(datos[5])

            # Foto
            CTkLabel(self.ventana_operaciones2, text='Foto: ').grid(row=6, column=0)
            self.operaciones2_foto = CTkButton(self.ventana_operaciones2, text='Subir archivo', fg_color="#878787",
                                              hover_color="#6D6D6D", command=self.upload_file_materiales)
            self.operaciones2_foto.grid(row=6, column=1, padx=10, pady=5, sticky="ew")

            self.operaciones2_label_foto = CTkLabel(self.ventana_operaciones2, wraplength="380", text="...", image=None,
                                                   compound="top")
            self.operaciones2_label_foto.grid(row=7, column=0, columnspan=2, pady=(5, 0))
            # cargar foto guardada
            if datos[6] is None:
                self.operaciones2_label_foto.configure(text="Ninguna foto")
            else:
                img_byte = BytesIO(datos[6])
                img = ImageTk.PhotoImage(Image.open(img_byte).resize((110, 110)))
                self.operaciones2_label_foto.configure(image=img, text="")

                # img = ImageTk.PhotoImage(data=datos[4])
                # self.operaciones_label_foto.configure(image=img).resize(110, 110)

            boton_guardar = CTkButton(self.ventana_operaciones2, text="Actualizar", command=self.update_material)
            boton_guardar.grid(row=8, column=0, columnspan=2, padx=10, pady=20, sticky="ew")
            # fin - GUI

            # Modal...
            self.ventana_operaciones2.transient(self)  # dialog window is related to main
            self.ventana_operaciones2.wait_visibility()
            self.ventana_operaciones2.grab_set()
            self.ventana_operaciones2.wait_window()
            # =================================================

        except IndexError as e:
            messagebox.showwarning(title=f"Error: ", message=f"""Seleccione un Material...""")

    def update_material(self):
        import traceback
        try:
            id = self.operaciones2_id.get()
            nombre_tipo = self.operaciones2_nombre_tipo.get()
            proveedor = self.operaciones2_proveedor.get()
            unidad_med = self.operaciones2_unidad_med.get()
            costo_unitario = self.operaciones2_costo_unitario.get()
            tipo = self.operaciones2_tipo.get()

            if len(nombre_tipo) != 0 and len(unidad_med) != 0 and len(costo_unitario) != 0:
                query = 'UPDATE scp_material SET nombre_tipo = ?, proveedor = ?, unidad_med = ? , costo_unitario = ? , tipo = ? WHERE id = ? '
                parameters = (nombre_tipo, proveedor, unidad_med, costo_unitario, tipo, id)
                self.run_query(query, parameters)

                messagebox.showinfo(title="SCP", message=f"Material '{id}' actualizado correctamente!!")

                # Cerramos la ventana
                self.ventana_operaciones2.destroy()
                # actualizamos listado en productos
                self.materiales_menu_event()
            else:
                messagebox.showwarning(title="SCP", message=f"Por favor llene todos los datos...")
        except Exception as e:
            messagebox.showerror(title="SCP", message=f"Error guardando en base de datos.\n{e}\n{traceback.format_exc()}")
        finally:
            self.conexion.close()

    def hoja_costo_exportar(self):
        if self.hoja_costos_producto.get() == "Seleccione el Producto":
            messagebox.showwarning(title="SCP", message=f"Por favor seleccione un producto...")
        else:
            id = self.hoja_costos_producto.get().split(".")[0]
            import traceback
            try:
                import xlsxwriter

                # Create an new Excel file and add a worksheet.
                workbook = xlsxwriter.Workbook(f"{BASE_DIR}/Hoja de Costos {self.hoja_costos_producto.get()}.xlsx")
                worksheet = workbook.add_worksheet(f"Hoja de Costos {self.hoja_costos_producto.get()}")

                # cambio de ancho de columnas
                worksheet.set_column('A:A', 22.83)
                worksheet.set_column('B:B', 20.67)
                worksheet.set_column('C:C', 17)
                worksheet.set_column('D:D', 16)
                worksheet.set_column('E:E', 15)
                worksheet.set_column('F:F', 18.83)

                # cambio de altura de filas, índice 0
                worksheet.set_row(0, 40)
                worksheet.set_row(1, 26)
                worksheet.set_row(2, 18.75)

                worksheet.set_row(3, 22.5)
                worksheet.set_row(4, 22.5)
                worksheet.set_row(5, 22.5)
                worksheet.set_row(6, 22.5)
                worksheet.set_row(7, 22.5)

                worksheet.set_row(19, 22.5)
                worksheet.set_row(20, 22.5)
                worksheet.set_row(21, 22.5)
                worksheet.set_row(22, 22.5)

                # formatos para aplicar a celdas
                bold = workbook.add_format({'bold': True})
                titul1 = workbook.add_format({'bold': True, 'font_color': 'purple', 'font_size': 16, 'align': 'left', 'valign': 'vcenter'})
                titul2 = workbook.add_format({'bold': True, 'font_color': 'black', 'font_size': 18, 'align': 'center', 'valign': 'vcenter', 'border': 6})
                titul2_small = workbook.add_format({'bold': True, 'font_color': 'black', 'font_size': 14, 'align': 'center', 'valign': 'vcenter', 'border': 6})
                text_normal = workbook.add_format({'bold': False, 'font_color': 'black', 'font_size': 12, 'align': 'left', 'valign': 'vcenter', 'border': 6})
                text_negrita = workbook.add_format({'bold': True, 'font_color': 'black', 'font_size': 12, 'align': 'left', 'valign': 'vcenter', 'border': 6})
                text_normal_centrado = workbook.add_format({'bold': False, 'font_color': 'black', 'font_size': 12, 'align': 'center', 'valign': 'vcenter', 'border': 6})
                text_negrita_centrado = workbook.add_format({'bold': True, 'font_color': 'black', 'font_size': 12, 'align': 'center', 'valign': 'vcenter', 'border': 6})
                text_normal_fondo_azul = workbook.add_format({'bold': False, 'font_color': 'black', 'bg_color': 'cyan', 'font_size': 12, 'align': 'center', 'valign': 'vcenter', 'border': 6})
                text_negrita_fondo_azul = workbook.add_format({'bold': True, 'font_color': 'black', 'bg_color': 'cyan', 'font_size': 12, 'align': 'center', 'valign': 'vcenter', 'border': 6})

                # Insert an image.
                worksheet.insert_image('A1', f"{BASE_DIR}/img/logo.png", {'x_offset': 10, 'y_offset': 5})
                worksheet.insert_image('F1', f"{BASE_DIR}/img/logoSena.png", {'x_offset': 85, 'y_offset': 5, 'x_scale': 0.1,'y_scale': 0.1})

                # Combinación de celdas
                worksheet.merge_range('A1:F1', '                  Simulador de Costos de Producción para la Confección de prendas de vestir', titul1)

                # Texto en celdas con formato
                worksheet.merge_range('A2:D2', 'HOJA DE COSTOS', titul2)
                worksheet.write('E2', 'FECHA:', text_negrita_centrado)
                worksheet.write('F2', self.tab1_fecha_valor.cget("text"), text_normal_centrado)

                worksheet.merge_range('A3:B3', 'RESUMEN DEL COSTO', titul2_small)
                worksheet.write('C3', '% UTILIDAD:', text_negrita_centrado)
                worksheet.write('D3', self.tab1_utilidad_valor.cget("text"), text_normal_centrado)
                worksheet.write('E3', 'REF:', text_negrita_centrado)
                worksheet.write('F3', self.tab1_ref_valor.cget("text"), text_normal_centrado)

                worksheet.write('A4', 'MATERIALES DIRECTOS:', text_negrita_centrado)
                worksheet.write('A5', 'M.O.D:', text_negrita_centrado)
                worksheet.write('A6', 'CIF:', text_negrita_centrado)
                worksheet.write('A7', 'COSTO TOTAL:', text_negrita_centrado)

                worksheet.write('B4', self.tab1_mat_directo_valor.cget("text"), text_normal_fondo_azul)
                worksheet.write('B5', self.tab1_MOD_valor.cget("text"), text_normal_fondo_azul)
                worksheet.write('B6', self.tab1_CIF_valor.cget("text"), text_normal_fondo_azul)
                worksheet.write('B7', self.tab1_COSTO_TOTAL_valor.cget("text"), text_normal_fondo_azul)

                worksheet.write('C4', 'PRECIO DE VENTA:', text_negrita)
                worksheet.write('C5', 'Tiempo Estándar:', text_negrita_centrado)
                worksheet.write('C6', 'Jornada Laboral:', text_negrita_centrado)
                worksheet.write('C7', 'Minutos / Mes', text_negrita_centrado)

                worksheet.write('D4', self.tab1_precio_venta_valor.cget("text"), text_normal_fondo_azul)
                worksheet.write('D5', self.tab1_tiempoE_valor.cget("text"), text_normal_centrado)
                worksheet.write('D6', self.tab1_jornadaL_valor.cget("text"), text_normal_centrado)
                worksheet.write('D7', self.tab1_min_mes_valor.cget("text"), text_negrita_centrado)

                worksheet.write('E4', 'PRODUCTO:', text_negrita)
                worksheet.write('E5', 'Eficiencia:', text_negrita_centrado)
                worksheet.write('E6', 'N° Operarios:', text_negrita_centrado)
                worksheet.write('E7', 'Unid a Prod/mes', text_negrita_centrado)

                worksheet.write('F4', self.hoja_costos_producto.get(), text_normal_centrado)
                worksheet.write('F5', self.tab1_eficiencia_valor.cget("text"), text_normal_centrado)
                worksheet.write('F6', self.tab1_num_operarios_valor.cget("text"), text_normal_centrado)
                worksheet.write('F7', self.tab1_u_prod_mes_valor.cget("text"), text_normal_fondo_azul)

                worksheet.merge_range('A8:F8', 'COSTO MATERIALES DIRECTOS', titul2)
                worksheet.write('A9', 'TIPO DE MATERIAL:', text_negrita_centrado)
                worksheet.write('B9', 'PROVEEDOR:', text_negrita_centrado)
                worksheet.write('C9', 'UNIDAD MEDIDA:', text_negrita_centrado)
                worksheet.write('D9', 'CONSUMO UNIT', text_negrita_centrado)
                worksheet.write('E9', 'COSTO UNITARIO', text_negrita_centrado)
                worksheet.write('F9', 'COSTO TOTAL', text_negrita_centrado)

                # lectura de TreeView para recorrer la matriz MATERIALES DIRECTOS
                fila = 9
                for line in self.tree_materiales_directos.get_children():
                    # print(self.tree_materiales_directos.item(line)['values'])
                    for columna, valor in enumerate(self.tree_materiales_directos.item(line)['values'][1:]):
                        worksheet.write(fila, columna, valor, text_normal)
                    fila += 1

                fila += 1
                worksheet.merge_range(f'A{fila}:D{fila}', 'TOTALES', titul2_small)
                worksheet.write(f'E{fila}', '', text_normal)
                try:
                    t1 = (self.tab2_total.cget("text").split("TOTALES: "))[1]
                except Exception:
                    t1 = ""

                worksheet.write(f'F{fila}', t1, text_negrita_fondo_azul)
                fila += 1
                worksheet.merge_range(f'A{fila}:F{fila}', 'COSTO MANO DE OBRA DIRECTA', titul2)
                fila += 1
                worksheet.write(f'A{fila}', 'SAL BASICO:', text_negrita_centrado)
                worksheet.write(f'B{fila}', self.tab3_total_sal_valor.cget("text"), text_normal_centrado)
                worksheet.write(f'C{fila}', 'CARGA PREST:', text_negrita_centrado)
                worksheet.write(f'D{fila}', self.tab3_carga_pres_valor.cget("text"), text_normal_centrado)
                worksheet.write(f'E{fila}', 'TOTAL SALARIO:', text_negrita_centrado)
                worksheet.write(f'F{fila}', self.tab3_total_sal_valor.cget("text"), text_normal_centrado)
                fila += 1
                worksheet.write(f'A{fila}', 'SUB TRANSP:', text_negrita_centrado)
                worksheet.write(f'B{fila}', self.tab3_sub_transp_valor.cget("text"), text_normal_centrado)
                worksheet.write(f'C{fila}', 'OTROS::', text_negrita_centrado)
                worksheet.write(f'D{fila}', self.tab3_otros_valor.cget("text"), text_normal_centrado)
                worksheet.write(f'E{fila}', 'TOTAL SALARIO:', text_negrita_centrado)
                worksheet.write(f'F{fila}', self.tab3_total_sal_otros_valor.cget("text"), text_normal_centrado)
                fila += 1
                worksheet.merge_range(f'A{fila}:B{fila}', 'COSTO TOTAL M.O.D:', text_negrita_centrado)
                worksheet.write(f'C{fila}', self.tab3_costo_mod_valor.cget("text"), text_normal_centrado)
                worksheet.merge_range(f'D{fila}:E{fila}', 'MINUTOS EFECTIVOS/MES:', text_negrita_centrado)
                worksheet.write(f'F{fila}', self.tab3_min_mes_valor.cget("text"), text_normal_centrado)
                fila += 1
                worksheet.merge_range(f'A{fila}:B{fila}', 'FACTOR MINUTO:', text_negrita_centrado)
                worksheet.write(f'C{fila}', self.tab3_fact_min_valor.cget("text"), text_normal_centrado)
                worksheet.merge_range(f'D{fila}:E{fila}', 'COSTO TOTAL UNITARIO M.O.D:', text_negrita_centrado)
                worksheet.write(f'F{fila}', self.tab3_unitario_mod_valor.cget("text"), text_normal_fondo_azul)
                fila += 1
                worksheet.merge_range(f'A{fila}:F{fila}', 'COSTOS INDIRECTOS DE FABRICACIÓN', titul2)
                fila += 1
                worksheet.merge_range(f'A{fila}:F{fila}', 'COSTO MATERIALES INDIRECTOS', titul2_small)
                fila += 1
                worksheet.merge_range(f'A{fila}:B{fila}', 'TIPO DE MATERIAL:', text_negrita_centrado)
                worksheet.write(f'C{fila}', 'CONSUMO/MES:', text_negrita_centrado)
                worksheet.write(f'D{fila}', 'COSTO', text_negrita_centrado)
                worksheet.write(f'E{fila}', 'COSTO TOTAL', text_negrita_centrado)
                worksheet.write(f'F{fila}', 'COSTO ASIGNADO', text_negrita_centrado)

                # lectura de TreeView para recorrer la matriz COSTOS INDIRECTOS DE FABRICACIÓN
                for line in self.tree_materiales_indirectos.get_children():
                    # print(list(enumerate(self.tree_materiales_indirectos.item(line)['values'][1:])))
                    control = 0
                    for columna, valor in enumerate(self.tree_materiales_indirectos.item(line)['values'][1:]):
                        if control == 0:
                            #worksheet.merge_range(f'A{fila}:B{fila}', valor, text_normal)
                            worksheet.merge_range(fila, 0, fila, 1, valor, text_normal)
                            control += 1
                        elif control == 1:
                            control += 1
                        else:
                            worksheet.write(fila, columna, valor, text_normal)
                    fila += 1

                fila += 1
                worksheet.merge_range(f'A{fila}:D{fila}', 'TOTALES', titul2_small)
                try:
                    t1 = (self.tab4_total1.cget("text").split("TOTALES: "))[1]
                except Exception:
                    t1 = ""

                try:
                    t2 = (self.tab4_total2.cget("text").split("TOTALES ASIGNADO: "))[1]
                except Exception:
                    t2 = ""

                worksheet.write(f'E{fila}', t1, text_negrita)
                worksheet.write(f'F{fila}', t2, text_negrita)
                fila += 1
                worksheet.merge_range(f'A{fila}:F{fila}', 'COSTOS MANO DE OBRA INDIRECTA', titul2)
                fila += 1
                worksheet.merge_range(f'A{fila}:C{fila}', 'CARGO:', text_negrita_centrado)
                worksheet.write(f'D{fila}', 'SALARIO/MES:', text_negrita_centrado)
                worksheet.write(f'E{fila}', 'TOTAL SALARIO', text_negrita_centrado)
                worksheet.write(f'F{fila}', 'COSTO ASIGNADO', text_negrita_centrado)

                # lectura de TreeView para recorrer la matriz COSTOS MANO DE OBRA INDIRECTA
                for line in self.tree_mano_obra_indirecta.get_children():
                    control = 0
                    for columna, valor in enumerate(self.tree_mano_obra_indirecta.item(line)['values'][1:]):
                        if control == 0:
                            # worksheet.merge_range(f'A{fila}:C{fila}', valor, text_normal)
                            worksheet.merge_range(fila, 0, fila, 2, valor, text_normal)
                            control += 1
                        else:
                            worksheet.write(fila, columna+2, valor, text_normal)
                    fila += 1

                fila += 1

                worksheet.merge_range(f'A{fila}:C{fila}', 'TOTALES', titul2_small)
                try:
                    t1 = (self.tab5_total1.cget("text").split("TOTALES SALARIO/MES: "))[1]
                except Exception:
                    t1 = ""

                try:
                    t2 = (self.tab5_total2.cget("text").split("TOTALES SALARIO: "))[1]
                except Exception:
                    t2 = ""

                try:
                    t3 = (self.tab5_total3.cget("text").split("TOTALES COSTO ASIGNADO: "))[1]
                except Exception:
                    t3 = ""

                worksheet.write(f'D{fila}', t1, text_negrita)
                worksheet.write(f'E{fila}', t2, text_negrita)
                worksheet.write(f'F{fila}', t3, text_negrita_fondo_azul)
                fila += 1
                worksheet.merge_range(f'A{fila}:F{fila}', 'COSTOS ADMINISTRATIVOS', titul2)
                fila += 1
                worksheet.merge_range(f'A{fila}:C{fila}', 'TIPO DE COSTO:', text_negrita_centrado)
                worksheet.merge_range(f'D{fila}:E{fila}', 'COSTO TOTAL/MES	:', text_negrita_centrado)
                worksheet.write(f'F{fila}', 'COSTO ASIGNADO:', text_negrita_centrado)

                # lectura de TreeView para recorrer la matriz COSTOS ADMINISTRATIVOS
                for line in self.tree_costos_administrativos.get_children():
                    control = 0
                    for columna, valor in enumerate(self.tree_costos_administrativos.item(line)['values'][1:]):
                        if control == 0:
                            # worksheet.merge_range(f'A{fila}:C{fila}', valor, text_normal)
                            worksheet.merge_range(fila, 0, fila, 2, valor, text_normal)
                            control += 1
                        elif control == 1:
                            # worksheet.merge_range(f'D{fila}:E{fila}', valor, text_normal)
                            worksheet.merge_range(fila, 3, fila, 4, valor, text_normal)
                            control += 1
                        else:
                            worksheet.write(fila, 5, valor, text_normal)
                    fila += 1
                fila += 1
                worksheet.merge_range(f'A{fila}:C{fila}', 'TOTALES', titul2_small)
                try:
                    t1 = (self.tab6_total1.cget("text").split("TOTALES COSTO TOTAL/MES: "))[1]
                except Exception:
                    t1 = ""

                try:
                    t2 = (self.tab6_total2.cget("text").split("TOTALES COSTO ASIGNADO: "))[1]
                except Exception:
                    t2 = ""

                worksheet.merge_range(f'D{fila}:E{fila}', t1, text_negrita)
                worksheet.write(f'F{fila}', t2, text_negrita_fondo_azul)
                fila += 1
                worksheet.merge_range(f'A{fila}:F{fila}', 'DEPRECIACIÓN MAQUINARIA', titul2)
                fila += 1
                worksheet.write(f'A{fila}', 'TIPO BIEN:', text_negrita_centrado)
                worksheet.write(f'B{fila}', 'CANTIDAD:', text_negrita_centrado)
                worksheet.write(f'C{fila}', 'VALOR COMERCIAL:', text_negrita_centrado)
                worksheet.write(f'D{fila}', 'VALOR TOTAL', text_negrita_centrado)
                worksheet.write(f'E{fila}', 'VIDA UTIL', text_negrita_centrado)
                worksheet.write(f'F{fila}', 'DEPRECIACION/MES', text_negrita_centrado)

                # lectura de TreeView para recorrer la matriz COSTOS DEPRECIACIÓN MAQUINARIA
                for line in self.tree_deprecia_maquinaria.get_children():
                    # print(self.tree_deprecia_maquinaria.item(line)['values'])
                    for columna, valor in enumerate(self.tree_deprecia_maquinaria.item(line)['values'][1:]):
                        worksheet.write(fila, columna, valor, text_normal)
                    fila += 1

                fila += 1
                worksheet.merge_range(f'A{fila}:E{fila}', 'TOTAL DEPRECIACION /MES', titul2_small)
                try:
                    t1 = (self.tab7_total1.cget("text").split(" "))[3]
                except Exception:
                    t1 = ""
                worksheet.write(f'F{fila}', t1, text_negrita)
                fila += 1

                worksheet.merge_range(f'A{fila}:E{fila}', 'COSTO ASIGNADO', titul2_small)
                try:
                    t1 = (self.tab7_total2.cget("text").split("COSTO ASIGNADO: "))[1]
                except Exception:
                    t1 = ""

                worksheet.write(f'F{fila}', t1, text_negrita_fondo_azul)
                fila += 1
                fila += 1
                worksheet.merge_range(f'A{fila}:F{fila}', 'Archivo generado desde software. Producto Técnico Pedagógico SENA CFDCM 2024')
                fila += 1
                import time
                worksheet.merge_range(f'A{fila}:F{fila}', f'{time.strftime("%c")}')
                workbook.close()
                print("ok")
                messagebox.showinfo(title="SCP", message=f"Archivo generado con éxito!!")
            except Exception as e:
                print("error")
                messagebox.showerror(title="SCP",message=f"Error:\n{e}\n{traceback.format_exc()}")


if __name__ == "__main__":
    # Check if we're on OS X, first.
    from sys import platform
    if platform == 'darwin':
        from Foundation import NSBundle

        bundle = NSBundle.mainBundle()
        if bundle:
            info = bundle.localizedInfoDictionary() or bundle.infoDictionary()
            if info and info['CFBundleName'] == 'Python':
                info['CFBundleDevelopmentRegion'] = 'Spanish'
                info['CFBundleGetInfoString'] = '1.0, SENA - Colombia 2023'
                info['CFBundleShortVersionString'] = '1.0'
                info['CFBundleVersion'] = '1.0'
                info['NSHumanReadableCopyright'] = 'SENA - Colombia 2023'
                info['CFBundleName'] = 'SCP'
                info['CFBundleExecutable'] = 'SCP'
                # info['CFBundleIdentifier'] = 'net.tikole.scp.1'
    set_appearance_mode("system")  # light | dark | system
    set_default_color_theme(f"{BASE_DIR}/purple.json")

    app = App()

    icono_chico = PhotoImage(file=f"{BASE_DIR}/favicon_io/logo.png")
    icono_grande = PhotoImage(file=f"{BASE_DIR}/favicon_io/logo256.png")
    app.iconphoto(False, icono_grande, icono_chico)

    app.title("Simulador de Costos de Producción")
    app.resizable(True, True)

    # app.state('zoomed')
    app.state('normal')

    # fix quit app on BigSur o newer
    app.createcommand("::tk::mac::Quit", quit)
    app.createcommand("tk::mac::ReopenApplication", app.deiconify)

    app.configuraciones_iniciales()

    app.mainloop()


