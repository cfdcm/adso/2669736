from customtkinter import *
from tkinter import messagebox, PhotoImage
import tkinter as tk
import os
from PIL import Image

from pathlib import Path

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent


class App(CTk):
    def __init__(self):
        super().__init__()

        self.geometry("800x500")

        #icon
        self.iconpath = PhotoImage(file=BASE_DIR/"favicon_io/logo32.png")
        self.wm_iconbitmap()
        self.iconphoto(False, self.iconpath)

        # menu de plataforma macOS
        plattform = tk.Menu(self)
        app_menu = tk.Menu(plattform, name='apple')
        plattform.add_cascade(menu=app_menu)


        app_menu.add_command(label='Acerca de SCP', command=self.help_menu_about_event)
        app_menu.add_separator()
        self.config(menu=plattform)
        self.createcommand('tk::mac::ShowPreferences', self.help_menu_event)
        # self.createcommand('tk::mac::standardAboutPanel', self.help_menu_event)
        # fin - menu

        # menu
        # menubar = tk.Menu(self)

        file_menu = tk.Menu(plattform, tearoff=0)
        file_menu.add_command(label="Exportar datos...")
        file_menu.add_command(label="Importar datos")
        file_menu.add_separator()
        file_menu.add_command(label="Hoja de Costos")
        file_menu.add_separator()
        file_menu.add_command(label="Salir", command=self.quit)

        edit_menu = tk.Menu(plattform, tearoff=0)
        edit_menu.add_command(label="Productos")
        edit_menu.add_command(label="Carga Prestacional")

        help_menu = tk.Menu(plattform, tearoff=0)
        help_menu.add_command(label="Ayuda", accelerator="Control+A", command=self.help_menu_event)
        help_menu.add_separator()
        help_menu.add_command(label="Acerca de...", accelerator="Control+X", command=self.help_menu_about_event)
        self.bind_all("<Control-X>", self.help_menu_event)

        plattform.add_cascade(label="Archivo", menu=file_menu)
        plattform.add_cascade(label="Editar", menu=edit_menu)
        plattform.add_cascade(label="Ayuda", menu=help_menu)

        # self.config(menu=menubar)

        # fin - menu

        # set grid layout 1x2
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(1, weight=1)

        self.logo_image = CTkImage(Image.open(BASE_DIR/"favicon_io/logo32.png"), size=(32, 32))
        self.large_test_image = CTkImage(Image.open(BASE_DIR/"test_images/logoSena.png"), size=(180, 176))
        self.image_icon_image = CTkImage(Image.open(BASE_DIR/"test_images/image_icon_light.png"), size=(20, 20))
        self.home_image = CTkImage(light_image=Image.open(BASE_DIR/"test_images/home_dark.png"), dark_image=Image.open(BASE_DIR/"test_images/home_light.png"), size=(20, 20))
        self.chat_image = CTkImage(light_image=Image.open(BASE_DIR/"test_images/chat_dark.png"), dark_image=Image.open(BASE_DIR/"test_images/chat_light.png"), size=(20, 20))
        self.add_user_image = CTkImage(light_image=Image.open(BASE_DIR/"test_images/add_user_dark.png"), dark_image=Image.open(BASE_DIR/"test_images/add_user_light.png"), size=(20, 20))

        # create navigation frame
        self.navigation_frame = CTkFrame(self, corner_radius=10)
        self.navigation_frame.grid(row=0, column=0, sticky="nsew")
        self.navigation_frame.grid_rowconfigure(6, weight=1)

        self.navigation_frame_label = CTkLabel(self.navigation_frame, text="  SIMULADOR\n  Costos de Producción", image=self.logo_image,
                                                             compound="left", font=CTkFont(size=15, weight="bold"))
        self.navigation_frame_label.grid(row=0, column=0, padx=20, pady=20)

        self.home_button = CTkButton(self.navigation_frame, corner_radius=0, height=40, border_spacing=10, text="Inicio",
                                                   fg_color="transparent", text_color=("gray10", "gray90"), hover_color=("gray70", "gray30"),
                                                   image=self.home_image, anchor="w", command=self.home_button_event)
        self.home_button.grid(row=1, column=0, sticky="ew")

        self.frame_2_button = CTkButton(self.navigation_frame, corner_radius=0, height=40, border_spacing=10, text="Productos",
                                                      fg_color="transparent", text_color=("gray10", "gray90"), hover_color=("gray70", "gray30"),
                                                      image=self.chat_image, anchor="w", command=self.frame_2_button_event)
        self.frame_2_button.grid(row=2, column=0, sticky="ew")

        self.frame_3_button = CTkButton(self.navigation_frame, corner_radius=0, height=40, border_spacing=10, text="Hoja de Costos",
                                                      fg_color="transparent", text_color=("gray10", "gray90"), hover_color=("gray70", "gray30"),
                                                      image=self.add_user_image, anchor="w", command=self.frame_3_button_event)
        self.frame_3_button.grid(row=3, column=0, sticky="ew")

        self.frame_4_button = CTkButton(self.navigation_frame, corner_radius=0, height=40, border_spacing=10, text="Carga Prestacional",
                                                      fg_color="transparent", text_color=("gray10", "gray90"), hover_color=("gray70", "gray30"),
                                                      image=self.add_user_image, anchor="w", command=self.frame_3_button_event)
        self.frame_4_button.grid(row=4, column=0, sticky="ew")

        self.frame_5_button = CTkButton(self.navigation_frame, corner_radius=0, height=40, border_spacing=10, text="Valor minuto",
                                                      fg_color="transparent", text_color=("gray10", "gray90"), hover_color=("gray70", "gray30"),
                                                      image=self.add_user_image, anchor="w", command=self.frame_3_button_event)
        self.frame_5_button.grid(row=5, column=0, sticky="ew")

        self.appearance_mode_menu = CTkOptionMenu(self.navigation_frame, values=["Light", "Dark", "System"],
                                                                command=self.change_appearance_mode_event)
        self.appearance_mode_menu.grid(row=6, column=0, padx=20, pady=20, sticky="s")

        # create home frame
        self.home_frame = CTkFrame(self, corner_radius=0, fg_color="transparent")
        self.home_frame.grid_columnconfigure(0, weight=1)

        self.home_frame_large_image_label = CTkLabel(self.home_frame, text="\nProducto Técnico-Pedagógico:\n\nSIMULADOR DE COSTOS DE PRODUCCIÓN\nPARA LA CONFECCIÓN DE PRENDAS DE VESTIR\n\n\nSENA - Centro de Formación en Diseño, Confección y Moda\n\nColombia - 2023", image=self.large_test_image, compound="top", font=CTkFont(size=15, weight="bold"))
        self.home_frame_large_image_label.place(relx=0.5, rely=0.5, anchor=CENTER)

        # self.home_frame_large_image_label = CTkLabel(self.home_frame, text="", image=self.large_test_image)
        #self.home_frame_large_image_label.grid(row=0, column=0, padx=20, pady=10)
        """
        self.home_frame_button_1 = CTkButton(self.home_frame, text="", image=self.image_icon_image)
        self.home_frame_button_1.grid(row=1, column=0, padx=20, pady=10)
        self.home_frame_button_2 = CTkButton(self.home_frame, text="CTkButton2", image=self.image_icon_image, compound="right")
        self.home_frame_button_2.grid(row=2, column=0, padx=20, pady=10)
        self.home_frame_button_3 = CTkButton(self.home_frame, text="CTkButton3", image=self.image_icon_image, compound="top")
        self.home_frame_button_3.grid(row=3, column=0, padx=20, pady=10)
        self.home_frame_button_4 = CTkButton(self.home_frame, text="CTkButton4", image=self.image_icon_image, compound="bottom", anchor="w")
        self.home_frame_button_4.grid(row=4, column=0, padx=20, pady=10)
        """

        # create second frame
        self.second_frame = CTkFrame(self, corner_radius=0, fg_color="transparent")

        # create third frame
        self.third_frame = CTkFrame(self, corner_radius=0, fg_color="transparent")

        # select default frame
        self.select_frame_by_name("home")

    def select_frame_by_name(self, name):
        # set button color for selected button
        self.home_button.configure(fg_color=("gray75", "gray25") if name == "home" else "transparent")
        self.frame_2_button.configure(fg_color=("gray75", "gray25") if name == "frame_2" else "transparent")
        self.frame_3_button.configure(fg_color=("gray75", "gray25") if name == "frame_3" else "transparent")

        # show selected frame
        if name == "home":
            self.home_frame.grid(row=0, column=1, sticky="nsew")
        else:
            self.home_frame.grid_forget()
        if name == "frame_2":
            self.second_frame.grid(row=0, column=1, sticky="nsew")
        else:
            self.second_frame.grid_forget()
        if name == "frame_3":
            self.third_frame.grid(row=0, column=1, sticky="nsew")
        else:
            self.third_frame.grid_forget()

    def home_button_event(self):
        self.select_frame_by_name("home")

    def frame_2_button_event(self):
        self.select_frame_by_name("frame_2")

    def frame_3_button_event(self):
        self.select_frame_by_name("frame_3")

    def change_appearance_mode_event(self, new_appearance_mode):
        set_appearance_mode(new_appearance_mode)

    def help_menu_event(self):
        # Crear una ventana secundaria.
        ventana_secundaria = CTkToplevel()

        # icon
        ventana_secundaria.wm_iconbitmap()
        ventana_secundaria.after(300, lambda: ventana_secundaria.iconphoto(False, self.iconpath))
        # fin - icon

        ventana_secundaria.resizable(False, False)
        # quitar minimizar....
        ventana_secundaria.title("Ventana secundaria")
        ventana_secundaria.config(width=300, height=200)
        # Crear un botón dentro de la ventana secundaria
        # para cerrar la misma.
        boton_cerrar = CTkButton(
            ventana_secundaria,
            text="Cerrar ventana",
            command=ventana_secundaria.destroy
        )
        boton_cerrar.place(x=75, y=75)
        ventana_secundaria.focus()
        # Modal...
        ventana_secundaria.transient(self)  # dialog window is related to main
        ventana_secundaria.wait_visibility()
        ventana_secundaria.grab_set()
        ventana_secundaria.wait_window()


    def help_menu_about_event(self):
        messagebox.showinfo("Acerca de ...", "\nProducto Técnico-Pedagógico:\n\nSIMULADOR DE COSTOS DE PRODUCCIÓN PARA LA CONFECCIÓN DE PRENDAS DE VESTIR\n\n\nSENA - Centro de Formación en Diseño, Confección y Moda\n\nColombia - 2023")


if __name__ == "__main__":
    set_appearance_mode("system")  # light | dark | system
    set_default_color_theme(BASE_DIR/"purple.json")
    # set_default_color_theme("green")

    app = App()

    # app.iconbitmap(BASE_DIR/"favicon_io/favicon.ico")
    # app.wm_iconbitmap(BASE_DIR/"favicon_io/favicon_windows.ico")

    # app.after(201, lambda: app.iconbitmap(BASE_DIR/"favicon_io/favicon_windows.ico"))

    # icono_chico = PhotoImage(file=BASE_DIR/"favicon_io/favicon-16x16.png")
    # icono_grande = PhotoImage(file=BASE_DIR/"favicon_io/favicon-32x32.png")
    # app.iconphoto(False, icono_grande, icono_chico)

    import ctypes

    myappid = 'net.tikole.scp.1'  # arbitrary string
    ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)

    app.title("Simulador de Costos de Producción")
    app.resizable(True, True)

    app.state('zoomed')

    app.mainloop()
