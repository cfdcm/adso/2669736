from customtkinter import *
from tkinter import messagebox, PhotoImage
import tkinter as tk
import os
from PIL import Image


class App(CTk):
    def __init__(self):
        super().__init__()

        self.geometry("700x450")


        """
        # menu
        menubar = tk.Menu(self)

        file_menu = tk.Menu(menubar, tearoff=0)
        file_menu.add_command(label="Exportar datos...")
        file_menu.add_command(label="Importar datos")
        file_menu.add_separator()
        file_menu.add_command(label="Hoja de Costos")
        file_menu.add_separator()
        file_menu.add_command(label="Salir", command=self.quit)

        edit_menu = tk.Menu(menubar, tearoff=0)
        edit_menu.add_command(label="Productos")
        edit_menu.add_command(label="Carga Prestacional")

        help_menu = tk.Menu(menubar, tearoff=0)
        help_menu.add_command(label="Ayuda", accelerator="Command+A", command=self.help_menu_event)
        help_menu.add_separator()
        help_menu.add_command(label="Acerca de...", accelerator="Command+X", command=self.help_menu_about_event)
        self.bind_all("<Command-X>", self.help_menu_event)

        menubar.add_cascade(label="Archivo", menu=file_menu)
        menubar.add_cascade(label="Editar", menu=edit_menu)
        menubar.add_cascade(label="Ayuda", menu=help_menu)
        self.config(menu=menubar)
        """
        # fin - menu

        # set grid layout 1x2
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(1, weight=1)

        # load images with light and dark mode image
        image_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "test_images")
        self.logo_image = CTkImage(Image.open(os.path.join(image_path, "../favicon_io/logo32.png")), size=(32, 32))
        self.large_test_image = CTkImage(Image.open(os.path.join(image_path, "logoSena.png")), size=(180, 176))
        self.image_icon_image = CTkImage(Image.open(os.path.join(image_path, "image_icon_light.png")), size=(20, 20))
        self.home_image = CTkImage(light_image=Image.open(os.path.join(image_path, "home_dark.png")),
                                                 dark_image=Image.open(os.path.join(image_path, "home_light.png")), size=(20, 20))
        self.chat_image = CTkImage(light_image=Image.open(os.path.join(image_path, "chat_dark.png")),
                                                 dark_image=Image.open(os.path.join(image_path, "chat_light.png")), size=(20, 20))
        self.add_user_image = CTkImage(light_image=Image.open(os.path.join(image_path, "add_user_dark.png")),
                                                     dark_image=Image.open(os.path.join(image_path, "add_user_light.png")), size=(20, 20))

        # create navigation frame
        self.navigation_frame = CTkFrame(self, corner_radius=10)
        self.navigation_frame.grid(row=0, column=0, sticky="nsew")
        self.navigation_frame.grid_rowconfigure(6, weight=1)

        self.navigation_frame_label = CTkLabel(self.navigation_frame, text="  SIMULADOR\n  Costos de Producción", image=self.logo_image,
                                                             compound="left", font=CTkFont(size=15, weight="bold"))
        self.navigation_frame_label.grid(row=0, column=0, padx=20, pady=20)

        self.home_button = CTkButton(self.navigation_frame, corner_radius=0, height=40, border_spacing=10, text="Inicio",
                                                   fg_color="transparent", text_color=("gray10", "gray90"), hover_color=("gray70", "gray30"),
                                                   image=self.home_image, anchor="w", command=self.home_button_event)
        self.home_button.grid(row=1, column=0, sticky="ew")

        self.frame_2_button = CTkButton(self.navigation_frame, corner_radius=0, height=40, border_spacing=10, text="Productos",
                                                      fg_color="transparent", text_color=("gray10", "gray90"), hover_color=("gray70", "gray30"),
                                                      image=self.chat_image, anchor="w", command=self.frame_2_button_event)
        self.frame_2_button.grid(row=2, column=0, sticky="ew")

        self.frame_3_button = CTkButton(self.navigation_frame, corner_radius=0, height=40, border_spacing=10, text="Hoja de Costos",
                                                      fg_color="transparent", text_color=("gray10", "gray90"), hover_color=("gray70", "gray30"),
                                                      image=self.add_user_image, anchor="w", command=self.frame_3_button_event)
        self.frame_3_button.grid(row=3, column=0, sticky="ew")

        self.frame_4_button = CTkButton(self.navigation_frame, corner_radius=0, height=40, border_spacing=10, text="Carga Prestacional",
                                                      fg_color="transparent", text_color=("gray10", "gray90"), hover_color=("gray70", "gray30"),
                                                      image=self.add_user_image, anchor="w", command=self.frame_3_button_event)
        self.frame_4_button.grid(row=4, column=0, sticky="ew")

        self.frame_5_button = CTkButton(self.navigation_frame, corner_radius=0, height=40, border_spacing=10, text="Valor minuto",
                                                      fg_color="transparent", text_color=("gray10", "gray90"), hover_color=("gray70", "gray30"),
                                                      image=self.add_user_image, anchor="w", command=self.frame_3_button_event)
        self.frame_5_button.grid(row=5, column=0, sticky="ew")

        self.appearance_mode_menu = CTkOptionMenu(self.navigation_frame, values=["Light", "Dark", "System"],
                                                                command=self.change_appearance_mode_event)
        self.appearance_mode_menu.grid(row=6, column=0, padx=20, pady=20, sticky="s")

        # create home frame
        self.home_frame = CTkFrame(self, corner_radius=0, fg_color="transparent")
        self.home_frame.grid_columnconfigure(0, weight=1)

        self.home_frame_large_image_label = CTkLabel(self.home_frame, text="\nProducto Técnico-Pedagógico:\n\nSIMULADOR DE COSTOS DE PRODUCCIÓN\nPARA LA CONFECCIÓN DE PRENDAS DE VESTIR\n\n\nSENA - Centro de Formación en Diseño, Confección y Moda\n\nColombia - 2023", image=self.large_test_image, compound="top", font=CTkFont(size=15, weight="bold"))
        self.home_frame_large_image_label.place(relx=0.5, rely=0.5, anchor=CENTER)

        # self.home_frame_large_image_label = CTkLabel(self.home_frame, text="", image=self.large_test_image)
        #self.home_frame_large_image_label.grid(row=0, column=0, padx=20, pady=10)
        """
        self.home_frame_button_1 = CTkButton(self.home_frame, text="", image=self.image_icon_image)
        self.home_frame_button_1.grid(row=1, column=0, padx=20, pady=10)
        self.home_frame_button_2 = CTkButton(self.home_frame, text="CTkButton2", image=self.image_icon_image, compound="right")
        self.home_frame_button_2.grid(row=2, column=0, padx=20, pady=10)
        self.home_frame_button_3 = CTkButton(self.home_frame, text="CTkButton3", image=self.image_icon_image, compound="top")
        self.home_frame_button_3.grid(row=3, column=0, padx=20, pady=10)
        self.home_frame_button_4 = CTkButton(self.home_frame, text="CTkButton4", image=self.image_icon_image, compound="bottom", anchor="w")
        self.home_frame_button_4.grid(row=4, column=0, padx=20, pady=10)
        """

        # create second frame
        self.second_frame = CTkFrame(self, corner_radius=0, fg_color="transparent")

        # create third frame
        self.third_frame = CTkFrame(self, corner_radius=0, fg_color="transparent")

        # select default frame
        self.select_frame_by_name("home")

    def select_frame_by_name(self, name):
        # set button color for selected button
        self.home_button.configure(fg_color=("gray75", "gray25") if name == "home" else "transparent")
        self.frame_2_button.configure(fg_color=("gray75", "gray25") if name == "frame_2" else "transparent")
        self.frame_3_button.configure(fg_color=("gray75", "gray25") if name == "frame_3" else "transparent")

        # show selected frame
        if name == "home":
            self.home_frame.grid(row=0, column=1, sticky="nsew")
        else:
            self.home_frame.grid_forget()
        if name == "frame_2":
            self.second_frame.grid(row=0, column=1, sticky="nsew")
        else:
            self.second_frame.grid_forget()
        if name == "frame_3":
            self.third_frame.grid(row=0, column=1, sticky="nsew")
        else:
            self.third_frame.grid_forget()

    def home_button_event(self):
        self.select_frame_by_name("home")

    def frame_2_button_event(self):
        self.select_frame_by_name("frame_2")

    def frame_3_button_event(self):
        self.select_frame_by_name("frame_3")

    def change_appearance_mode_event(self, new_appearance_mode):
        set_appearance_mode(new_appearance_mode)

    def help_menu_event(self):
        # Crear una ventana secundaria.
        ventana_secundaria = CTkToplevel()
        ventana_secundaria.resizable(False, False)
        # quitar minimizar....
        ventana_secundaria.title("Ventana secundaria")
        ventana_secundaria.config(width=300, height=200)
        # Crear un botón dentro de la ventana secundaria
        # para cerrar la misma.
        boton_cerrar = CTkButton(
            ventana_secundaria,
            text="Cerrar ventana",
            command=ventana_secundaria.destroy
        )
        boton_cerrar.place(x=75, y=75)
        ventana_secundaria.focus()
        # Modal...
        ventana_secundaria.transient(self)  # dialog window is related to main
        ventana_secundaria.wait_visibility()
        ventana_secundaria.grab_set()
        ventana_secundaria.wait_window()


    def help_menu_about_event(self):
        messagebox.showinfo("Acerca de ...", "\nProducto Técnico-Pedagógico:\n\nSIMULADOR DE COSTOS DE PRODUCCIÓN PARA LA CONFECCIÓN DE PRENDAS DE VESTIR\n\n\nSENA - Centro de Formación en Diseño, Confección y Moda\n\nColombia - 2023")


if __name__ == "__main__":
    # Check if we're on OS X, first.
    """from sys import platform
    if platform == 'darwin':
        from Foundation import NSBundle

        bundle = NSBundle.mainBundle()
        if bundle:
            print(bundle)
            print("*"*100)
            info = bundle.localizedInfoDictionary() or bundle.infoDictionary()
            print(info)
            if info and info['CFBundleName'] == 'Python':
                info['CFBundleDevelopmentRegion'] = 'Spanish'
                info['CFBundleGetInfoString'] = '1.0, SENA - Colombia 2023'
                info['CFBundleShortVersionString'] = '1.0'
                info['CFBundleVersion'] = '1.0'
                info['NSHumanReadableCopyright'] = 'SENA - Colombia 2023'
                info['CFBundleName'] = 'SCP'
                info['CFBundleExecutable'] = 'SCP'
                # info['CFBundleIdentifier'] = 'net.tikole.SCP'
    """

    set_appearance_mode("system")  # light | dark | system
    set_default_color_theme("./purple.json")  # set_default_color_theme("green")

    app = App()

    win = tk.Toplevel(app)
    especial = tk.Menu(win)
    appmenu = tk.Menu(especial, name='apple')
    especial.add_cascade(menu=appmenu)
    appmenu.add_command(label='Acerca de SCP', command=app.help_menu_about_event)
    appmenu.add_separator()
    win['menu'] = especial

    app.createcommand('tk::mac::ShowPreferences', app.help_menu_event)

    helpmenu = tk.Menu(especial, name='help')
    especial.add_cascade(menu=helpmenu, label='Ayuda')
    app.createcommand('tk::mac::ShowHelp', app.help_menu_event)

    windowmenu = tk.Menu(especial, name='window')
    especial.add_cascade(menu=windowmenu, label='Window')

    sysmenu = tk.Menu(especial, name='system')
    especial.add_cascade(menu=sysmenu)

    icono_chico = PhotoImage(file="favicon_io/logo32.png")
    icono_grande = PhotoImage(file="favicon_io/logo512.png")
    app.iconphoto(False, icono_grande, icono_chico)

    app.title("Simulador de Costos de Producción")
    app.resizable(True, True)

    # app.state('zoomed')

    app.mainloop()
