#Mini-taller
"""
1. Construya un programa que dados dos valores X y Y, verifique si las siguientes ecuaciones arrojan el mismo resultado:
a) X + 3Y        if x+3*y == 2*x - 5*y:
b) 2X - 5Y
Ejemplo: 
x=16, y=2
x=8, y=1 
x=0, y=0
x=-8, y = -1

2. Averiguar entre dos horas dadas en formato AM/PM, cual es mayor. Pedir las horas así: Horas, Minutos, Meridiano.
Controlar:
las Horas que no superen las 12
los Minutos que no superen los 59
Meridiano, sólo sean: AM|PM.

3. Averiguar entre dos horas dadas en formato militar, cual es menor. Pedir las horas así: Horas, Minutos
Controlar:
las Horas que no superen las 23
los Minutos que no superen los 59
4. Construir un programa que redondee un número decimal entre 1 y 10, a su equivalente en entero. 
# 4.6   -> 5
# 4.4   -> 4
Nota: Si el decimal es .5 o menor, redondear hacia abajo
Nota: Si el decimal es mayor a .5, redondear hacia arriba

5. Preguntar al usuario si quiere dibujar un: Cuadrado, Rectángulo, Triángulo o una línea.
Nota usar asteriscos (*)
Ejemplo: Cuadrado.
* * * * *
*       *
* * * * *

"""