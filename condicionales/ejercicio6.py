# Pedir un número del 1 al 99, y averigüar:
"""
- Si es positivo o no
- Si es negativo o no
- Si es par o no
- Si es impar o no
- Si es factor de 3
- Si su potencia de 2, es igual a 16.....  4^² = 16
- Si su residuo es 7, con respecto al MOD 10..... Ej: 77 MOD 10 = 7

20 % 10 = 0,,  0 == 7, no es igual a siete...
77 % 10 = 7,,  7 == 7, su residuo si es siete!!

- Si su cociente es 8, con respecto al / 10
- Si es un número capicúa: 2002 -> 2002 SI es!!,   34   43, no es capicúa
- Si la suma de sus caracteres es igual a 10..     34   3 + 4 = 7, no
                                                   55   5 + 5 = 10, si
"""
# print(f"{800/23:0.2f}")

num = int(input("Digite un num entre 1 - 99: "))

if num >= 1 and num <= 99:
    # 1 ejercicio
    if num > 0:
        print("Es positivo")
    else:
        print("No es positivo")

    # 2 ejercicio
    if num < 0:
        print("Es negativo")
    else:
        print("No es negativo")

    # 3 ejercicio
    if num % 2 == 0:
        print("Es par")
    else:
        print("No es par")

    # 4 ejercicio
    if num % 2 != 0:
        print("Es impar")
    else:
        print("No es impar")

    # 5 ejercicio
    if num % 3 == 0:
        print("Es factor de 3")
    else:
        print("No es factor de 3")

    # 6 ejercicio
    if (num * num) == 16:
        print("Si, su potencia es 16")
    else:
        print(f"No, su potencia no es 16, sino: {num * num}")

    # 7 ejercicio
    if num % 10 == 7:
        print("Si, el residuo es 7")
    else:
        print(f"No, es residuo no es 7, sino: {num % 10}")

    # 8 ejercicio
    if num / 10 == 8:
        print("Si, su cociente es 8")
    else:
        print(f"No, si cociente no es 8, sino: {num/10}")

    # 9 ejercicio
    unidades = num % 10
    decenas = int((num - unidades) / 10)

    if decenas >= 1:
        nuevo = unidades * 10 + decenas
    else:
        nuevo = unidades

    if num == nuevo:
        print("Es capicúa")
    else:
        print("No es capicúa")

    # 10 ejercicio
    suma = decenas + unidades
    if suma == 10:
        print("La suma de sus caracteres es 10")
    else:
        print(f"La suma de sus caracteres no es 10, sino: {suma}")
    # fin
else:
    print("Error")







