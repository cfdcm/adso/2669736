# Condicional completo, multicaso.  if ... elif ... else
"""
if <condicion>:
    pass
elif <condicion>:
    pass
elif <condicion>:
    pass
else:
    pass
"""

# Diseñe un algoritmo que piga la nota de matemáticas y muestre lo siguiente:
# Si la nota es menor que 3.5 , perdió.
# Si la nota es menor que 4, Ganó de suerte.
# Si la nota es menor que 4.5, Muy bien.
# Si la nota es mayor o igual que 4, felicitaciones!

nota = float(input("Digite su nota final: "))

if nota < 3.5:
    print("Perdió....")
elif nota < 4:
    print("Ganó de suerte.")
elif nota < 4.5:
    print("Muy bien!")
else:
    print("Felicitaciones!!")







