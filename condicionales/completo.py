# Condicionales completos:   if ... else

#       if <condicion>:
#           pass
#       else:
#           pass

# Averiguar si una persona puede votar o no.

edad = int(input("Digite su edad: "))

if edad < 18:
    print("Usted no puede votar. Menor de edad")
else:
    print("Usted SI puede votar!!")
