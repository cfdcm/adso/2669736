# Condicional anidado

# averiguar sin un número es par. En caso afirmativo, averiguar si es negativo o positivo.
# Si no es par. Multiplicarlo por 3.

# División: Numerador. denominador, cociente y residuo.
# Residuo (0), saber si la división es exacta.

num = int(input("Digite un número entero: "))

if num % 2 == 0:
    print("Es PAR")
    # Condicional anidado
    if num < 0:
        print("y es negativo-...")
    else:
        print("y es positivo-...")
else:
    print("No es PAR, es un IMPAR")
    num = num*3