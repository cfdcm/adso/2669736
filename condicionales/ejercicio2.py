# Condicionales con and, or, not

# Pregunte la nota final de algoritmos (ENTRE 0 y 5) y si está:

# Entre 0 y 2, decir insuficiente-
# Entre mayor que 2 y 3.4, perdió...
# Entre mayor que 3.4 y 4.5, ganó!
# Mayor de 4.5, felicitaciones.

# Adicionalmente, si sacó 5.0 exactamente, decir que "Usted ha ganado una Beca de $20millones".

# Además, si sacó 0 exactamente, decir "Usted ha sido expulsado de la universidad".


nota = float(input("Digite su nota final de algoritmos: "))

if nota < 0 or nota > 5:
    print("Error en la nota...")
else:
    if nota >= 0 and nota <= 2.0:
        print("Insuficiente...")
        if nota == 0:
            print("Usted ha sido expulsado de la universidad...")
    elif nota > 2.0 and nota <= 3.4:
        print("Perdió...")
    elif nota > 3.4 and nota <= 4.5:
        print("Ganó!")
    elif nota > 4.5:
        print("Felicitaciones!!")
        if nota == 5:
            print("Usted ha ganado una Beca de $20millones.")
    else:
        print("Nota fuera de rango")
        

    