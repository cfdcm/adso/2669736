# Construya un programa que lance dos dados (cuatro veces). Y averiguar lo siguiente:
"""
- Si la suma de ambos, dió 3.
- Cuántas veces, la suma dió 5.
- Cuántas veces, la suma dió 12.
"""

import random

cincos = 0      # inicializar los contadores
doces = 0       # inicializar los contadores

# Lanzada 1
dado1 = random.randint(1, 6)
dado2 = random.randint(1, 6)

suma = dado1 + dado2
print(f"Lanzada 1: Dado1: {dado1}, Dado2: {dado2}, SUMA: {suma}")

if suma == 3:
    print("Urra!, dió tres")
elif suma == 5:
    cincos = cincos + 1
elif suma == 12:
    doces = doces + 1


# Lanzada 2
dado1 = random.randint(1, 6)
dado2 = random.randint(1, 6)

suma = dado1 + dado2
print(f"Lanzada 2: Dado1: {dado1}, Dado2: {dado2}, SUMA: {suma}")

if suma == 3:
    print("Urra!, dió tres")
elif suma == 5:
    cincos = cincos + 1
elif suma == 12:
    doces = doces + 1


# Lanzada 3
dado1 = 1
dado2 = 2

suma = dado1 + dado2
print(f"Lanzada 3: Dado1: {dado1}, Dado2: {dado2}, SUMA: {suma}")

if suma == 3:
    print("Urra!, dió tres")
elif suma == 5:
    cincos = cincos + 1
elif suma == 12:
    doces = doces + 1


# Lanzada 4
dado1 = random.randint(1, 6)
dado2 = random.randint(1, 6)

suma = dado1 + dado2
print(f"Lanzada 4: Dado1: {dado1}, Dado2: {dado2}, SUMA: {suma}")

if suma == 3:
    print("Urra!, dió tres")
elif suma == 5:
    cincos = cincos + 1
elif suma == 12:
    doces = doces + 1

# Informe final
print(f"La cantidad de veces que la suma fue cinco es: {cincos}")
print(f"La cantidad de veces que la suma fue doce es: {doces}")

