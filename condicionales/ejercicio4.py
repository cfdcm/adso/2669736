# Pedir al usuario un número entre 1 - 5, convertirlo a letras.
# Ejemplo: 3 ---> tres

num = int(input("Digite un num entre 1 y 5: "))
palabra = ""
if num == 1:
    palabra = "Uno"
elif num == 2:
    palabra = "Dos"
elif num == 3:
    palabra = "Tres"
elif num == 4:
    palabra = "Cuatro"
elif num == 5:
    palabra = "Cinco"
else:
    palabra = "Error"

print(f"El número {num} convertido es {palabra}")



# Averiguar el estado de un semáforo e indicar un mensaje según cada color.

estado = input("En que color está el semáforo?: ").upper()


if estado == "ROJO":
    print("Pare!!")
elif estado == "AMARILLO":
    print("Precaución...........")
elif estado == "VERDE":
    print("Siga...!!!!!!!!")
else:
    print("Error")






