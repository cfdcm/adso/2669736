# [] listas -> vectores                 list

edades = []
frutas = ["manzana", "pera", "piña"]
print(len(frutas))
# Lectura
print(frutas[0])
print(frutas[1])
print(frutas[2])

# Inserción
frutas.append("uva")         # final
frutas.insert(1, "fresa")    # inicio

# Actualización
frutas[0] = "banano"
frutas[0] = ""

# Borrado o eliminado
del frutas[2]               # borrado por índice
frutas.remove("uva")        # borrado por valor
frutas.pop(-1)              # borrado por índice

# Slicing - Tajada, porción
print(frutas[0:3])      # 0,1,2
print(frutas[:3])       # 0,1,2 desde el principio
print(frutas[0:])       # 0,1,2, hasta el final
print(frutas[:])        # 0,1,2, todo.
print(frutas[::-1])     # voltear los valores al revés

