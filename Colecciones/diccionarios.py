# { clave: valor}       Diccionarios.   dict()

estudiantes = ["A", "A apellido", 20, "caldas"]
print(estudiantes[2])
estudiantes[2] = 50

estudiantes = {
    "nombre": "A",
    "apellido": "A apellido",
    "edad": 20,
    "direcciones": "caldas"
}

# Leer o acceso
print(estudiantes.get("apellido"))
print(estudiantes["apellido"])
print(estudiantes["edad"])

# Insertar (la clave no debe existir, de lo contrario sería una actualización)
estudiantes["correo"] = "a@misena.edu.co"

# Actualización
estudiantes["edad"] = 50
estudiantes.update({"edad": 50})
print(estudiantes)

# Borrar
# del estudiantes                     # elimina el diccionario completo
del(estudiantes["apellido"])
# del estudiantes["apellido"]
# estudiantes.pop("apellido")         # eliminado a través de llave
# estudiantes.popitem()               # elimina el último item insertado
# estudiantes.clear()                 # limpia el diccionario {} sin eliminar la variable.
print(estudiantes)


# Otros métodos

# Obtener listado de claves del diccionario
claves = estudiantes.keys()
print(list(claves))


# Obtener listado de valores del diccionario
valores = estudiantes.values()
print(list(valores))


# Obtener otodos los items del diccionario, en modo clave:valor
valores = estudiantes.items()
print(list(valores))


# transformar una lista a diccionario
frutas = ["manzana", "pera", "piña"]

frutas_diccionario = dict(enumerate(frutas))
print(frutas_diccionario)
