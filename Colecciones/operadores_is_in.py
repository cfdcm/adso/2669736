# Identity operators are used to compare the objects, not if
# they are equal, but if they are actually the same object,
# with the same memory location:

print("================= Operador 'is'")
print(1 is 1.0)

x = ["apple", "banana"]
y = ["apple", "banana"]
z = x

print(x is z)

# returns True because z is the same object as x

print(x is y)

# returns False because x is not the same object as y, even if they have the same content

print(x == y)

# to demonstrate the difference betweeen "is" and "==": this comparison returns True because x is equal to y
print(2 in [2, 3, 4])

print("================= Operador 'in'")

frutas = ["A", "B", "C"]

if "D" in frutas:
    print("Si se encuentra")
else:
    print("No se encuentra")

encontrado = False
for i in frutas:
    if "D" == i:
        encontrado = True
        break

if encontrado:
    print("Si se encuentra")
else:
    print("No se encuentra")
