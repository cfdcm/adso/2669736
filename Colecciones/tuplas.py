# (, ) tuplas -> listas sólo lectura    tuple

edades = ()
pares = (2, )
frutas = ("manzana", "pera", "piña")
print(len(frutas))
# Lectura
print(frutas[0])
print(frutas[1])
print(frutas[2])

# Inserción

# Actualización

# Borrado o eliminado
del frutas              # borrado toda la tupla

# Slicing - Tajada, porción
print(frutas[0:3])      # 0,1,2
print(frutas[:3])       # 0,1,2 desde el principio
print(frutas[0:])       # 0,1,2, hasta el final
print(frutas[:])        # 0,1,2, todo.
print(frutas[::-1])     # voltear los valores al revés