class Edificio:
	tipo = "Residencial"

	def __init__(self, nombre, pisos):
		self.nombre = nombre
		self.pisos = pisos

		self.terminado = True
		self.color = "blanco"

	def pintar(self, color):
		self.color = color

	def __str__(self):
		return f"Nombre: {self.nombre}\tTerminado: {self.nombre}\tcolor: {self.color}"


if __name__ == "__main__":
	pass
	# Pedir al usuario la cantidad de edificios a crear:

	# Crearlos usando un ciclo for:
	#			asignar nombre, pisos y si está terminado o no
	# 			asignarle COLOR a través del método pintar

	# Imprimir todos los edificios de la siguiente manera: Usar ciclo for
	#		Nombre:	xxxxxxxx		Terminado: xxxxxxx		Color: xxxxx
	listado = []
	n = int(input("Cantidad de edificios a crear?: "))
	for i in range(n):
		nombre = input(f"Nombre del edificio {i+1}: ")
		pisos = int(input(f"Cantidad de pisos del edificio {nombre}: "))
		terminado = input(f"Está terminado el edificio {nombre}? (SI/NO): ").upper()
		color = input(f"De qué color es el edificio {nombre}?: ")

		listado.append(Edificio(nombre, pisos))
		listado[i].terminado = terminado
		listado[i].pintar(color)

	for i in range(n):
		print(listado[i])