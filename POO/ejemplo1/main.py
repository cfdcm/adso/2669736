from roles import Grupo


class Usuario(Grupo):

	cantidad = 0

	def __init__(self, nombre, user, passw, tipo):
		# invocar el método constructor de Grupo
		super().__init__(tipo)
		self.nombre = nombre
		self.user = user
		self.passw = passw
		Usuario.cantidad += 1
		self.id = Usuario.cantidad


	def cambiar_clave(self, nueva1, nueva2):
		if nueva1 != "" and nueva2 != "" and nueva1 == nueva2:
			self.passw = nueva1
			return True
		else:
			return False

	def __str__(self):
		return f"{self.nombre}"


if __name__ == "__main__":
	admin_inicial = Usuario("Jorge", "jorge", "13245", "admin")
	usuarios = [admin_inicial]
	user_selected = admin_inicial
	while True:
		op = int(input("""
		Qué desea hacer?
		1. Escoger usuario
		2. Registrar usuario
		3. Ver todos los usuarios
		4. Eliminar usuario
		5. Modificar usuario
		6. Salir
		: """))
		if op == 6:
			print("Bye!")
			break
		elif op == 1:
			for u in enumerate(usuarios):
				print(f"{u[0]+1}. {u[1]}")

			esc = int(input("Seleccione un usuario:"))
			# validar primero....
			user_selected = esc










