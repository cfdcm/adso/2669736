from roles import Grupo


class Usuario(Grupo):

	cantidad = 0

	def __init__(self, nombre, user, passw, tipo):
		# invocar el método constructor de Grupo
		super().__init__(tipo)
		self.nombre = nombre
		self.user = user
		self.passw = passw
		Usuario.cantidad += 1
		self.id = Usuario.cantidad


	def cambiar_clave(self, nueva1, nueva2):
		if nueva1 != "" and nueva2 != "" and nueva1 == nueva2:
			self.passw = nueva1
			return True
		else:
			return False

	def __str__(self):
		return f"registro: {self.nombre}"


if __name__ == "__main__":

	u1 = Usuario("Sofia", "sofia", "13245", "admin")

	if u1:
		print("usuario agregado correctamente!!")
		print(u1)
		print(f"Rol: {u1.tipo} - Permisos:")
		for i in u1.permisos:
			print(i)
	else:
		print("No se pudo crear el usuario, porque el grupo no existe.")




