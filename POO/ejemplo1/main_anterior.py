class Usuario:

	cantidad = 0

	def __init__(self, nombre, user, passw):
		self.nombre = nombre
		self.user = user
		self.passw = passw
		Usuario.cantidad += 1
		self.id = Usuario.cantidad

	def cambiar_clave(self, nueva1, nueva2):
		if nueva1 != "" and nueva2 != "" and nueva1 == nueva2:
			self.passw = nueva1
			return True
		else:
			return False

	def __str__(self):
		return f"registro: {self.nombre}"

if __name__ == "__main__":
	usuarios = []

	ob1 = Usuario("Sofia", "sofia", "13245")
	usuarios.append(ob1)
	ob2 = Usuario("Deivy", "deivy", "13245")
	usuarios.append(ob2)
	ob3 = Usuario("Juan", "juan", "13245")
	usuarios.append(ob3)
	ob4 = Usuario("Deivy José", "deivyj", "65487")
	usuarios.append(ob4)

	for registro in usuarios:
		if registro.nombre.startswith('Deivy'):
			print(f"{registro.id} - {registro.nombre} - {registro.passw} ")
