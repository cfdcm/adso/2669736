class Temperatura:

	def __init__(self):
		self.f = 0.0
		self.c = 0.0

	def celsius_to_farenheit(self, temp):
		# F=(1,8)C + 32
		self.f = 1.8 * temp + 32
		return self.f

	def farenheit_to_celsius(self, temp):
		# C=(F - 32)/1,8
		self.c = (temp - 32) / 1.8
		return self.c

if __name__ == "__main__":
	o1 = Temperatura()
	while True:
		op = int(input("""Qué desea hacer?:
		1. Convertir °F a °C
		2. Convertir °C a °F
		3. Salir
		"""))
		if op == 1:
			f = float(input("Digite la temperatura en °F: "))
			r = o1.farenheit_to_celsius(f)
			print(f"{f:.0f}°F son {r:.0f}°C")
		elif op == 2:
			c = float(input("Digite la temperatura en °C: "))
			r = o1.celsius_to_farenheit(c)
			print(f"{c:.0f}°C son {r:.0f}°F")
		elif op == 3:
			break

