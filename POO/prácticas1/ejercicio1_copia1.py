class Temperatura:

	def __init__(self):
		self.f = 0.0
		self.c = 0.0

	def celsius_to_farenheit(self, temp):
		self.c = temp
		self.f = 1.8 * self.c + 32

	def farenheit_to_celsius(self, temp):
		self.f = temp
		self.c = (self.f - 32) / 1.8

if __name__ == "__main__":
	o1 = Temperatura()
	while True:
		op = int(input("""Qué desea hacer?:
		1. Convertir °F a °C
		2. Convertir °C a °F
		3. Salir
		"""))
		if op == 1:
			f = float(input("Digite la temperatura en °F: "))
			o1.farenheit_to_celsius(f)
			print(f"{o1.f:.0f}°F son {o1.c:.0f}°C")
		elif op == 2:
			c = float(input("Digite la temperatura en °C: "))
			o1.celsius_to_farenheit(c)
			print(f"{o1.c:.0f}°C son {o1.f:.0f}°F")
		elif op == 3:
			break

