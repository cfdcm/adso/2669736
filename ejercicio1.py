# Calcular el área de un cuadrado que mide 5 metros de lado. Usar variables.

# entradas
lado = 5

#proceso
area = lado * lado

#salida
print("El área del cuadro es: ", area)
print("El área del cuadrado de lado", lado, " es ", area)
print("Área del cuadrado: A=LxL -> A=", lado, "*", lado, "=", area) 


