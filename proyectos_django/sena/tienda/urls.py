from django.urls import path, include
from . import views
from rest_framework.routers import DefaultRouter
from rest_framework.authtoken import views as especial

router = DefaultRouter()
router.register(r'categoria', views.CategoriaViewSet)
router.register(r'usuario', views.UsuarioViewSet)


urlpatterns = [
	path("api/1.0/", include(router.urls)),
	# path('api/1.0/token-auth/', especial.obtain_auth_token),
 	# Ruta para login y obtener token desde app móvil
	path('api/1.0/token-auth/', views.CustomAuthToken.as_view()),
	path('api/1.0/api-auth/', include('rest_framework.urls')),

	path('', views.index, name="index"),
	path('inicio/', views.inicio, name="inicio"),

	# Autenticación de usuarios del sistema
	path('login/', views.login, name="login"),
	path('logout/', views.logout, name="logout"),
	path('registrar_usuario/', views.registrar_usuario, name="registrar_usuario"),


	# CRUD de Categorías
	path("categorias_listar/", views.categorias, name="categorias_listar"),
	path("categorias_form/", views.categorias_form, name="categorias_form"),
	path("categorias_crear/", views.categorias_crear, name="categorias_crear"),
	path("categorias_eliminar/<int:id>", views.categorias_eliminar, name="categorias_eliminar"),
	path("categorias_formulario_editar/<int:id>", views.categorias_formulario_editar, name="categorias_formulario_editar"),
	path("categorias_actualizar/", views.categorias_actualizar, name="categorias_actualizar"),

	# CRUD de Productos
	path("productos_listar/", views.productos, name="productos_listar"),
	path("productos_form/", views.productos_form, name="productos_form"),
	path("productos_crear/", views.productos_crear, name="productos_crear"),
	path("productos_eliminar/<int:id>", views.productos_eliminar, name="productos_eliminar"),
	path("productos_formulario_editar/<int:id>", views.productos_formulario_editar, name="productos_formulario_editar"),
	path("productos_actualizar/", views.productos_actualizar, name="productos_actualizar"),

	path("ver_perfil/", views.ver_perfil, name="ver_perfil"),
	path("cc_formulario/", views.cambio_clave_formulario, name="cc_formulario"),
	path("cambiar_clave/", views.cambiar_clave, name="cambiar_clave"),
	path("recuperar_clave/", views.recuperar_clave, name="recuperar_clave"),
	path("verificar_recuperar/", views.verificar_recuperar, name="verificar_recuperar"),

	# carrito de compra
	path("carrito_add/", views.carrito_add, name="carrito_add"),
	path("carrito_ver/", views.carrito_ver, name="carrito_ver"),
	path("vaciar_carrito/", views.vaciar_carrito, name="vaciar_carrito"),
	path("eliminar_item_carrito/<int:id_producto>", views.eliminar_item_carrito, name="eliminar_item_carrito"),
	path("actualizar_totales_carrito/<int:id_producto>/", views.actualizar_totales_carrito, name="actualizar_totales_carrito"),
	path("guardar_venta/", views.guardar_venta, name="guardar_venta"),

	path("prueba_correo/", views.prueba_correo, name="prueba_correo"),

	path("quejas/", views.quejas, name="quejas"),
	path("guardar_queja_cita/", views.guardar_queja_cita, name="guardar_queja_cita"),
	path("cancelar_queja_cita/", views.cancelar_queja_cita, name="cancelar_queja_cita"),

]

"""
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token

from .models import Usuario

for user in Usuario.objects.all():
    Token.objects.get_or_create(user=user)
"""