from .models import *
from rest_framework import serializers


class CategoriaSerializer(serializers.ModelSerializer):
	class Meta:
		model = Categoria
		# fields = ['id', 'titulo', 'imagen', 'estreno', 'resumen']
		fields = '__all__'


class UsuarioSerializer(serializers.ModelSerializer):
	class Meta:
		model = Usuario
		fields = ['id','username', 'email', 'rol', 'foto', 'token_recuperar']

