from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib import messages
# Para tomar el from desde el settings
from django.conf import settings
from django.core.mail import BadHeaderError, EmailMessage
# Importamos todos los modelos de la base de datos
from django.db import IntegrityError, transaction

import datetime
from django.utils.timezone import localtime
import pytz
est = pytz.timezone('America/Bogota')

from django.db.models import Q

from .models import *
from .crypt import *

from .serializers import *
from rest_framework import viewsets

# Create your views here.

def view_404(request, exception=None):
    # make a redirect to homepage
    # you can use the name of url or just the plain link
    return redirect('/')

def index(request):
	logueo = request.session.get("logueo", False)

	if logueo == False:
		return render(request, "tienda/login/login.html")
	else:
		return redirect("inicio")


def login(request):
	if request.method == "POST":
		user = request.POST.get("correo")
		passw = request.POST.get("clave")
		# select * from Usuario where correo = "user" and clave = "passw"
		try:
			q = Usuario.objects.get(email=user)
			if verify_password(passw, q.password):
				# Crear variable de sesión
				request.session["logueo"] = {
					"id": q.id,
					"nombre": q.nombre,
					"rol": q.rol,
					"nombre_rol": q.get_rol_display()
				}
				request.session["carrito"] = []
				request.session["items"] = 0
				messages.success(request, f"Bienvenido {q.nombre}!!")
			else:
				messages.error(request, "Error: Usuario o contraseña incorrectos...")
			return redirect("inicio")
		except Exception as e:
			messages.error(request, "Error: ocurrió un error, intente de nuevo...")
			return redirect("index")
	else:
		messages.warning(request, "Error: No se enviaron datos...")
		return redirect("index")


def logout(request):
	try:
		del request.session["logueo"]
		del request.session["carrito"]
		del request.session["items"]
		messages.success(request, "Sesión cerrada correctamente!")
		return redirect("index")
	except Exception as e:
		messages.warning(request, "No se pudo cerrar sesión...")
		return redirect("inicio")


def registrar_usuario(request):
	if request.method == "POST":
		nombre = request.POST.get("nombre")
		correo = request.POST.get("correo")
		clave1 = request.POST.get("clave1")
		clave2 = request.POST.get("clave2")
		if clave1 == clave2:
			q = Usuario(
				nombre=nombre,
				correo=correo,
				clave=hash_password(clave1)
			)
			q.save()
			messages.success(request, "Usuario registrado correctamente!!")
			return redirect("index")
		else:
			messages.warning(request, "No concuerdan las contraseñas")
			return redirect("registrar_usuario")
	else:
		return render(request, "tienda/login/registro.html")


def recuperar_clave(request):
	if request.method == "POST":
		correo = request.POST.get("correo")
		try:
			q = Usuario.objects.get(correo=correo)
			from random import randint
			import base64
			token = base64.b64encode(str(randint(100000, 999999)).encode("ascii")).decode("ascii")
			print(token)
			q.token_recuperar = token
			q.save()
			# enviar correo de recuperación
			destinatario = correo
			mensaje = f"""
					<h1 style='color:blue;'>Tienda virtual</h1>
					<p>Usted ha solicitado recuperar su contraseña, haga clic en el link y digite el token.</p>
					<p>Token: <strong>{token}</strong></p>
					<a href='http://127.0.0.1:8000/tienda/verificar_recuperar/?correo={correo}'>Recuperar...</a>
					"""
			try:
				msg = EmailMessage("Tienda ADSO", mensaje, settings.EMAIL_HOST_USER, [destinatario])
				msg.content_subtype = "html"  # Habilitar contenido html
				msg.send()
				messages.success(request, "Correo enviado!!")
			except BadHeaderError:
				messages.error(request, "Encabezado no válido")
			except Exception as e:
				messages.error(request, f"Error: {e}")
			# fin -
		except Usuario.DoesNotExist:
			messages.error(request, "No existe el usuario....")
		return redirect("recuperar_clave")
	else:
		return render(request, "tienda/login/recuperar.html")


def verificar_recuperar(request):
	if request.method == "POST":
		if request.POST.get("check"):
			# caso en el que el token es correcto
			correo = request.POST.get("correo")
			q = Usuario.objects.get(correo=correo)

			c1 = request.POST.get("nueva1")
			c2 = request.POST.get("nueva2")

			if c1 == c2:
				# cambiar clave en DB
				q.clave = hash_password(c1)
				q.token_recuperar = ""
				q.save()
				messages.success(request, "Contraseña guardada correctamente!!")
				return redirect("index")
			else:
				messages.info(request, "Las contraseñas nuevas no coinciden...")
				return redirect("verificar_recuperar")+"/?correo="+correo
		else:
			# caso en el que se hace clic en el correo-e para digitar token
			correo = request.POST.get("correo")
			token = request.POST.get("token")
			q = Usuario.objects.get(correo=correo)
			if (q.token_recuperar == token) and q.token_recuperar != "":
				contexto = {"check": "ok", "correo":correo}
				return render(request, "tienda/login/verificar_recuperar.html", contexto)
			else:
				messages.error(request, "Token incorrecto")
				return redirect("verificar_recuperar")	# falta agregar correo como parametro url
	else:
		correo = request.GET.get("correo")
		contexto = {"correo":correo}
		return render(request, "tienda/login/verificar_recuperar.html", contexto)


def inicio(request):
	logueo = request.session.get("logueo", False)

	if logueo:
		categorias = Categoria.objects.all()

		cat = request.GET.get("cat")
		if cat == None:
			productos = Producto.objects.all()
		else:
			c = Categoria.objects.get(pk=cat)
			productos = Producto.objects.filter(categoria=c)

		contexto = {"data": productos, "cat": categorias}
		return render(request, "tienda/inicio.html", contexto)
	else:
		return redirect("index")


from .decorador_especial import *


@login_requerido
def categorias(request):
	q = Categoria.objects.all()
	contexto = {"data": q}
	return render(request, "tienda/categorias/categorias.html", contexto)



def categorias_form(request):
	return render(request, "tienda/categorias/categorias_form.html")


def categorias_crear(request):
	if request.method == "POST":
		nomb = request.POST.get("nombre")
		desc = request.POST.get("descripcion")
		try:
			q = Categoria(
				nombre=nomb,
				descripcion=desc
			)
			q.save()
			messages.success(request, "Guardado correctamente!!")
		except Exception as e:
			messages.error(request, f"Error: {e}")
		return redirect("categorias_listar")

	else:
		messages.warning(request, "Error: No se enviaron datos...")
		return redirect("categorias_listar")


def categorias_eliminar(request, id):
	try:
		q = Categoria.objects.get(pk=id)
		q.delete()
		messages.success(request, "Categoría eliminada correctamente!!")
	except Exception as e:
		messages.error(request, f"Error: {e}")

	return redirect("categorias_listar")


def categorias_formulario_editar(request, id):
	q = Categoria.objects.get(pk=id)
	contexto = {"data": q}
	return render(request, "tienda/categorias/categorias_formulario_editar.html", contexto)

def categorias_actualizar(request):
	if request.method == "POST":
		id = request.POST.get("id")
		nomb = request.POST.get("nombre")
		desc = request.POST.get("descripcion")
		try:
			q = Categoria.objects.get(pk=id)
			q.nombre = nomb
			q.descripcion = desc
			q.save()
			messages.success(request, "Categoría actualizada correctamente!!")
		except Exception as e:
			messages.error(request, f"Error: {e}")
	else:
		messages.warning(request, "Error: No se enviaron datos...")

	return redirect("categorias_listar")


@login_requerido
def productos(request):
	q = Producto.objects.all()
	contexto = {"data": q}
	return render(request, "tienda/productos/productos.html", contexto)


def productos_form(request):
	q = Categoria.objects.all()
	contexto = {"data": q}
	return render(request, "tienda/productos/productos_form.html", contexto)


def productos_crear(request):
	if request.method == "POST":
		nombre = request.POST.get("nombre")
		precio = request.POST.get("precio")
		inventario = request.POST.get("inventario")
		fecha_creacion = request.POST.get("fecha_creacion")
		categoria = Categoria.objects.get(pk=request.POST.get("categoria"))
		try:
			q = Producto(
				nombre=nombre,
				precio=precio,
				inventario=inventario,
				fecha_creacion=fecha_creacion,
				categoria=categoria
			)
			q.save()
			messages.success(request, "Guardado correctamente!!")
		except Exception as e:
			messages.error(request, f"Error: {e}")
		return redirect("productos_listar")

	else:
		messages.warning(request, "Error: No se enviaron datos...")
		return redirect("productos_listar")


def productos_eliminar(request, id):
	try:
		q = Producto.objects.get(pk=id)
		q.delete()
		messages.success(request, "Producto eliminada correctamente!!")
	except Exception as e:
		messages.error(request, f"Error: {e}")

	return redirect("productos_listar")


def productos_formulario_editar(request, id):
	q = Producto.objects.get(pk=id)
	c = Categoria.objects.all()
	contexto = {"data": q, "categoria": c}
	return render(request, "tienda/productos/productos_formulario_editar.html", contexto)

def productos_actualizar(request):
	if request.method == "POST":
		id = request.POST.get("id")
		nombre = request.POST.get("nombre")
		precio = request.POST.get("precio")
		inventario = request.POST.get("inventario")
		fecha_creacion = request.POST.get("fecha_creacion")
		categoria = Categoria.objects.get(pk=request.POST.get("categoria"))
		try:
			q = Producto.objects.get(pk=id)
			q.nombre = nombre
			q.precio = precio
			q.inventario = inventario
			q.fecha_creacion = fecha_creacion
			q.categoria = categoria
			q.save()
			messages.success(request, "Producto actualizado correctamente!!")
		except Exception as e:
			messages.error(request, f"Error: {e}")
	else:
		messages.warning(request, "Error: No se enviaron datos...")

	return redirect("productos_listar")


def ver_perfil(request):
	logueo = request.session.get("logueo", False)
	# Consultamos en DB por el ID del usuario logueado....
	q = Usuario.objects.get(pk=logueo["id"])
	contexto = {"data": q}
	return render(request, "tienda/login/perfil.html", contexto)


def cambio_clave_formulario(request):
	return render(request, "tienda/login/cambio_clave.html")


def cambiar_clave(request):
	if request.method == "POST":
		logueo = request.session.get("logueo", False)
		q = Usuario.objects.get(pk=logueo["id"])

		c1 = request.POST.get("nueva1")
		c2 = request.POST.get("nueva2")

		if verify_password(request.POST.get("clave"), q.clave):
			if c1 == c2:
				# cambiar clave en DB
				q.clave = hash_password(c1)
				q.save()
				messages.success(request, "Contraseña guardada correctamente!!")
			else:
				messages.info(request, "Las contraseñas nuevas no coinciden...")
		else:
			messages.error(request, "Contraseña no válida...")
	else:
		messages.warning(request, "Error: No se enviaron datos...")

	return redirect('cc_formulario')


def carrito_add(request):
	if request.method == "POST":
		try:
			carrito = request.session.get("carrito", False)
			if not carrito:
				request.session["carrito"] = []
				request.session["items"] = 0
				carrito = []

			id_producto = int(request.POST.get("id"))
			cantidad = request.POST.get("cantidad")
			# Consulto el producto en DB...........................
			q = Producto.objects.get(pk=id_producto)
			for p in carrito:
				if p["id"] == id_producto:
					if q.inventario >= (p["cantidad"] + int(cantidad)) and int(cantidad) > 0:
						p["cantidad"] += int(cantidad)
						p["subtotal"] = p["cantidad"] * q.precio
					else:
						print("Cantidad supera inventario...")
						messages.warning(request, "Cantidad supera inventario...")
					break
			else:
				print("No existe en carrito... lo agregamos")
				if q.inventario >= int(cantidad) and int(cantidad) > 0:
					carrito.append(
						{
							"id": q.id,
							"foto": q.foto.url,
							"producto": q.nombre,
							"precio": q.precio,
							"cantidad": int(cantidad),
							"subtotal": int(cantidad) * q.precio
						}
					)
				else:
					print("Cantidad supera inventario...")
					messages.warning(request, "No se puede agregar, no hay suficiente inventario.")

			# Actualizamos variable de sesión carrito...
			request.session["carrito"] = carrito

			contexto = {
				"items": len(carrito),
				"total": sum(p["subtotal"] for p in carrito)
			}
			request.session["items"] = len(carrito)

			return render(request, "tienda/carrito/carrito.html", contexto)
		except ValueError as e:
			messages.error(request, f"Error: Digite un valor correcto para cantidad")
			return HttpResponse("Error")
		except Exception as e:
			messages.error(request, f"Ocurrió un Error: {e}")
			return HttpResponse("Error")
	else:
		messages.warning(request, "No se enviaron datos.")
		return HttpResponse("Error")


def carrito_ver(request):
	carrito = request.session.get("carrito", False)
	if not carrito:
		request.session["carrito"] = []
		request.session["items"] = 0
		contexto = {
			"items": 0,
			"total": 0
		}
	else:
		contexto = {
			"items": len(carrito),
			"total": sum(p["subtotal"] for p in carrito)
		}
		request.session["items"] = len(carrito)

	return render(request, "tienda/carrito/carrito.html", contexto)


def vaciar_carrito(request):
	request.session["carrito"] = []
	request.session["items"] = 0
	return redirect("inicio")


def eliminar_item_carrito(request, id_producto):
	try:
		carrito = request.session.get("carrito", False)
		if carrito != False:
			for i, item in enumerate(carrito):
				if item["id"] == id_producto:
					carrito.pop(i)
					break
			else:
				messages.warning(request, "No se encontró el ítem en el carrito.")

		request.session["items"] = len(carrito)
		request.session["carrito"] = carrito
		return redirect("carrito_ver")
	except:
		return HttpResponse("Error")


def actualizar_totales_carrito(request, id_producto):
	carrito = request.session.get("carrito", False)
	cantidad = request.GET.get("cantidad")

	if carrito != False:
		for i, item in enumerate(carrito):
			if item["id"] == id_producto:
				item["cantidad"] = int(cantidad)
				item["subtotal"] = int(cantidad) * item["precio"]
				break
		else:
			messages.warning(request, "No se encontró el ítem en el carrito.")

	request.session["items"] = len(carrito)
	request.session["carrito"] = carrito
	return redirect("carrito_ver")


@transaction.atomic
def guardar_venta(request):
	carrito = request.session.get("carrito", False)
	logueo = request.session.get("logueo", False)
	try:
		# Genero encabezado de venta, para tener ID y guardar detalle
		r = Venta(usuario=Usuario.objects.get(pk=logueo["id"]))
		r.save()

		for i, p in enumerate(carrito):
			try:
				pro = Producto.objects.get(pk=p["id"])
				print(f"ok producto {p['producto']}")
			except Producto.DoesNotExist:
				# elimino el producto no existente del carrito...
				carrito.pop(i)
				request.session["carrito"] = carrito
				request.session["items"] = len(carrito)
				raise Exception(f"El producto '{p['producto']}' ya no existe")

			if int(p["cantidad"]) > pro.inventario:
				raise Exception(f"La cantidad del producto '{p['producto']}' supera el inventario")

			det = DetalleVenta(
				venta=r,
				producto=pro,
				cantidad=int(p["cantidad"]),
				precio_historico=int(p["precio"])
			)
			det.save()
			# disminuir inventario
			pro.inventario -= int(p["cantidad"])
			pro.save()
		# vaciar carrito
		request.session["carrito"] = []
		request.session["items"] = 0
		messages.success(request, "Venta realizada correctamente!!")
	except Exception as e:
		transaction.set_rollback(True)
		messages.error(request, f"Error: {e}")

	return redirect("inicio")


def prueba_correo(request):
	destinatario = "jor@misena.edu.co"
	mensaje = """
		<h1 style='color:blue;'>Tienda virtual</h1>
		<p>Su pedido está listo y en estado "creado".</p>
		<p>Tienda ADSO, 2024</p>
		"""

	try:
		msg = EmailMessage("Tienda ADSO", mensaje, settings.EMAIL_HOST_USER, [destinatario])
		msg.content_subtype = "html"  # Habilitar contenido html
		msg.send()
		return HttpResponse("Correo enviado")
	except BadHeaderError:
		return HttpResponse("Encabezado no válido")
	except Exception as e:
		return HttpResponse(f"Error: {e}")


def quejas(request):
	logueo = request.session.get("logueo", False)

	# Consultar los usuarios tipo despachador
	destino = Usuario.objects.filter(rol=2)

	usuario = Usuario.objects.get(pk=logueo["id"])
	if usuario.rol == 2:
		citas_reservadas = Cita.objects.filter(destino=usuario).order_by("fecha_hora_ini")
	else:
		citas_reservadas = Cita.objects.filter(
			Q(origen=usuario) & (Q(estado=1) | Q(estado=3))
		).order_by("fecha_hora_ini")

	contexto = {"despachadores": destino, "citas": citas_reservadas}
	return render(request, "tienda/quejas/quejas.html", contexto)


def guardar_queja_cita(request):
	import traceback
	# falta implementar controles de fecha, anteriores o no laborales....
	try:
		logueo = request.session.get("logueo", False)
		o = Usuario.objects.get(pk=logueo["id"])
		d = Usuario.objects.get(pk=request.POST.get("destino"))
		fecha_ini = request.POST.get("fecha_hora_ini")
		# convertir string de fecha a objeto fecha python
		fecha_ini = datetime.datetime.strptime(fecha_ini, '%Y-%m-%dT%H:%M')
		# sumar 1 hora a la fecha_hora_ini
		fecha_fin = fecha_ini + datetime.timedelta(hours=0, minutes=59)

		todos = Cita.objects.filter(Q(origen=o) | Q(destino=d))

		for reg in todos:
			ini = reg.fecha_hora_ini
			fin = reg.fecha_hora_fin
			if (est.localize(fecha_ini) >= ini and est.localize(fecha_ini) <= fin) or (est.localize(fecha_fin) >= ini and est.localize(fecha_fin) <= fin):
				print(f"Si en rango")
				messages.warning(request, "Usuario ya tiene cita en ese rango....")
				break
		else:
			print(f"No en rango, se puede guardar la cita")
			q = Cita(
				fecha_hora_ini = fecha_ini,
				fecha_hora_fin = fecha_fin,
				origen = o,
				destino = d
			)
			q.save()
			messages.success(request, "Cita agendada correctamente!!")
	except Exception as e:
		messages.error(request, f"Error: {e}\n{traceback.format_exc()}")

	return redirect("quejas")


def cancelar_queja_cita(request):
	logueo = request.session.get("logueo", False)
	if request.method == "POST":
		usuario = Usuario.objects.get(pk=logueo["id"])
		cita = Cita.objects.get(pk=request.POST.get("id"))
		if usuario.rol == 2:
			cita.estado = int(request.POST.get("estado"))
			cita.obs = request.POST.get("obs")
			cita.save()
			messages.success(request, "Cita gestionada correctamente!!")
		else:
			cita.estado = 3
			cita.obs = request.POST.get("obs")
			cita.save()
			messages.success(request, "Cita cancelada correctamente!!")
	else:
		messages.error(request, "No se enviarion datos..")
	return redirect("quejas")


# -------------------------------------------------------------------------------------------
from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated

class CategoriaViewSet(viewsets.ModelViewSet):
	permission_classes = [IsAuthenticated]
	queryset = Categoria.objects.all()
	serializer_class = CategoriaSerializer


class UsuarioViewSet(viewsets.ModelViewSet):
	# authentication_classes = [TokenAuthentication, SessionAuthentication]
	authentication_classes = [TokenAuthentication]
	permission_classes = [IsAuthenticated]
	queryset = Usuario.objects.all()
	serializer_class = UsuarioSerializer



# ------------------------------- Personalización de Token Autenticación ------------
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response


class CustomAuthToken(ObtainAuthToken):
	def post(self, request, *args, **kwargs):
		serializer = self.serializer_class(data=request.data,
										   context={'request': request})
		serializer.is_valid(raise_exception=True)
		user = serializer.validated_data['username']
		# traer datos del usuario para bienvenida y ROL
		usuario = Usuario.objects.get(email=user)
		token, created = Token.objects.get_or_create(user=usuario)

		return Response({
			'token': token.key,
			'user': {
				'user_id': usuario.pk,
				'email': usuario.email,
				'nombre': usuario.nombre,
				'rol': usuario.rol,
				'foto': usuario.foto.url
			}
		})
