# Custom model authentication: paso 1
from django.contrib.auth.models import AbstractUser

from .authentication import CustomUserManager
from django.db import models

# Create your models here.
class Categoria(models.Model):
	nombre = models.CharField(max_length=254)
	descripcion = models.TextField()

	def __str__(self):
		return self.nombre


class Producto(models.Model):
	nombre = models.CharField(max_length=254, unique=True)
	precio = models.FloatField()
	inventario = models.IntegerField()
	fecha_creacion = models.DateField()
	categoria = models.ForeignKey(Categoria, on_delete=models.DO_NOTHING)
	foto = models.ImageField(upload_to="fotos_productos/", default="fotos_productos/default.png")

	def __str__(self):
		return self.nombre

# Custom model authentication: paso 2
class Usuario(AbstractUser):
	# Custom model authentication: paso 3
	username = None
	nombre = models.CharField(max_length=254)
	# Custom model authentication: paso 4, el campo email para django es obligatorio, cambiar correo -> email
	email = models.EmailField(max_length=254, unique=True)
	# Custom model authentication: paso 5, el campo password para django es obligatorio, cambiar clave -> password
	password = models.CharField(max_length=254)
	ROLES = (
		(1, "Administrador"),
		(2, "Despachador"),
		(3, "Cliente"),
	)
	rol = models.IntegerField(choices=ROLES, default=3)
	foto = models.ImageField(upload_to="fotos/", default="fotos/default.png", blank=True)
	token_recuperar = models.CharField(max_length=254, default="", blank=True, null=True)
	# Custom model authentication: paso 6
	USERNAME_FIELD = "email"
	REQUIRED_FIELDS = ["nombre"]
	objects = CustomUserManager()

	def __str__(self):
		return self.nombre


class Venta(models.Model):
	fecha_venta = models.DateTimeField(auto_now=True)
	usuario = models.ForeignKey(Usuario, on_delete=models.DO_NOTHING)
	ESTADOS = (
		(1, 'Pendiente'),
		(2, 'Enviado'),
		(3, 'Rechazada'),
	)
	estado = models.IntegerField(choices=ESTADOS, default=1)

	def __str__(self):
		return f"{self.id} - {self.usuario}"


class DetalleVenta(models.Model):
	venta = models.ForeignKey(Venta, on_delete=models.DO_NOTHING)
	producto = models.ForeignKey(Producto, on_delete=models.DO_NOTHING)
	cantidad = models.IntegerField()
	precio_historico = models.IntegerField()

	def __str__(self):
		return f"{self.id} - {self.venta}"


class Cita(models.Model):
	fecha_hora_ini = models.DateTimeField()
	fecha_hora_fin = models.DateTimeField()		# duración por defecto 1 hora
	ESTADOS = (
		(1, "Reservada"),
		(2, "Atendida"),
		(3, "Cancelada"),
	)
	estado = models.IntegerField(choices=ESTADOS, default=1)
	obs = models.TextField(null=True, blank=True)
	origen = models.ForeignKey(Usuario, on_delete=models.DO_NOTHING, related_name="usuario_fk1")
	destino = models.ForeignKey(Usuario, on_delete=models.DO_NOTHING, related_name="usuario_fk2")

	def __str__(self):
		return f"{self.id} ---- {self.fecha_hora_ini} - {self.origen} - {self.destino}"

# ---------------------------------------------------------------------------------
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token

@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)