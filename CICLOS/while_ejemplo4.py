# Pedir usuario y contraseña al usuario. En caso de ser correcto decir "Bienvenido!!". De lo contrario seguir preguntando.
# Nota: máximo 3 intentos. Poner mensaje "Intente más tarde..."

# usuario   contraseña
# admin     12345

c = 1
supera_limite = True
while c <= 3:
    usuario = input("Digite su nombre de usuario: ")
    clave = input("Digite su contraseña: ")
    if usuario == "admin" and clave == "12345":
        supera_limite = False
        break
    else:
        print("* Usuario o contraseña incorrectos.")
    c += 1

if supera_limite:
    print("Se equivocó 3 veces. Intente más tarde.")
else:
    print("Bienvenido!!")
