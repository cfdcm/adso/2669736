# Pedir 3 edades y calcular el promedio de ellas.
# Usar while.

x = 1
suma = 0
while x <= 3:
    edad = int(input(f"Digite la edad {x}: "))
    suma += edad        # acumulador
    x += 1              # contador

promedio = suma / 3
print(f"El promedio de las edades es {promedio:0.1f}")
