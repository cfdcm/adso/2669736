# Pedir al usuario, la cantidad de compras a realizar. N
# Solicitar nombre, cantidad y el precio del producto.
# Calcular subtotales y total global.

# Nota: usar ciclo while.

n = int(input("Cuántos productos quiere comprar?: "))
cont = 1    # Contador
suma = 0    # Acumulador
while cont <= n:
    nombre = input(f"Digite el nombre del producto ({cont}): ")
    precio = int(input(f"Digite el precio del producto ({cont}): "))
    cantidad = int(input(f"Digite la cantidad a comprar ({cont}): "))
    subtotal = precio * cantidad
    print(f"Subtotal: {subtotal}")
    suma += subtotal        # Acumulador
    cont += 1               # Contador

print(f"\nTotal: {suma}")
