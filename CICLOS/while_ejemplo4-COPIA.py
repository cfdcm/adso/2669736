# Pedir usuario y contraseña al usuario. En caso de ser correcto decir "Bienvenido!!". De lo contrario seguir preguntando.
# Nota: máximo 3 intentos. Poner mensaje "Intente más tarde..."

# usuario   contraseña
# admin     12345

usuario = input("Digite su nombre de usuario: ")
clave = input("Digite su contraseña: ")

cont = 1
ok = True
while usuario != "admin" or clave != "12345":
    cont += 1
    usuario = input("Digite su nombre de usuario: ")
    clave = input("Digite su contraseña: ")

    if cont == 3:
        ok = False
        break

if ok:
    print("Bienvenido!!")
else:
    print("Supera el límite...")
