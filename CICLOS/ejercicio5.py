# Averiguar cuantos números primos hay entre los números 50 y 1200.
# Mostrar cuáles fueron.

numeros = 50
cantidad = 0
while numeros <= 1200:

    if numeros % 2 != 0:
        cont = 1
        divisores = 0
        while cont < numeros:
            # print(f"Obtener divisores del {numeros}, así {numeros} % {cont} = {numeros % cont}")
            if numeros % cont == 0:
                divisores += 1

            if divisores == 2:
                # print("Romper ciclo....")
                # rompo el ciclo, cuando detecto no primero, tercer divisor...
                break

            cont += 2
        # print(f"Divisores: {divisores}")
        if divisores == 1:
            cantidad += 1
            print(f"Num primo encontrado: {numeros}")

    numeros += 1
print(f"La cantidad de primos encontrados fue: {cantidad}")
