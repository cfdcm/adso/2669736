# Construir un algoritmo con for. Cuenta regresiva, para el factorial de 80!.
# Recuerde factorial 5! es: 5x4x3x2x1 = 120

n = int(input("Digite un entero positivo: "))
iteraciones = range(n, 0, -1)

# print(list(iteraciones))

total = 1   # Acumulador multiplicación
for i in iteraciones:
    if i == 1:
        print(f"{i} = ", end="")
    else:
        print(f"{i}x", end="")
    total *= i

print(total)


"""
print(f"Hola {total} mundo", end="***\n")

print("Hola", 5, "mundo", sep="")

print("Hola", 5, "mundo", sep=" ", end="\n")

# Hola-5-mundo
# Hola5mundo
# Hola 5 mundo
"""










