# Control de datos.
# Seleccionar de un menú un producto a comprar.
# Luego, Mostrar el precio del producto escogido.
pregunta = "s"
suma = 0
while pregunta == "s":
    while True:
        opt = input("""Seleccione el producto a comprar.
        PS5     -   $4.000.000
        Xbox    -   $1.600.000
        NSwitch -   $2.000.000
        Escriba el nombre:  
        """)
        if opt == "PS5" or opt == "Xbox" or opt == "NSwitch":
            print("Gracias por su compra!!")
            break
        else:
            print("Error, producto no existe...")

    if opt == "PS5":
        suma += 4000000
        print("Total: $4.000.000")
    elif opt == "Xbox":
        suma += 1600000
        print("Total: $1.600.000")
    elif opt == "NSwitch":
        suma += 2000000
        print("Total: $2.000.000")

    pregunta = input("Desea seguir comprando? S/N: ").lower()

print(f"El Total global de compra es : {suma}")
