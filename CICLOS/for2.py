# For. Listas o vectores, arrays, colecciones
"""
frutas = ["pera","fresa", "manzana", "pera"]
cont = 0
for i in frutas:
    if i == "pera":
        cont += 1
        print("* Pera encontrada....")
    else:
        print("No es una pera...")

print(f"Se encontraron {cont} peras")
"""
"""
# Lista vacía....
nombre = []
# Agregar elementos.

# Al final
nombre.append("Pedro")

n = input("Digite un nombre de jugador: ")
nombre.append(n)

print(nombre)

# o al principio o en cualquier parte.
nombre.insert(0, "Andrea")
print(nombre)
nombre.insert(2, "Sonia")
print(nombre)
"""

# Pedir 3 edades (llenar lista o vector), y calcular su promedio
edades = []
for i in range(3):
    e = int(input(f"Digite la edad {i+1}: "))
    edades.append(e)

print(edades)

suma = 0
for i in edades:
    suma += i

print(f"El promedio es: {suma/len(edades):.2f}")