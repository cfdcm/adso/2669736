# Usar ciclo while. Crear un juego de batalla pokemon.

# Pedir por pantalla, nombre del pokemon 1 y pokemon 2
# Pedir poder de ataque de cada uno, entre 1-35. CONTROLAR.
# Las vidas de ambos son de 200.
# Controlar el turno. Un pokemon a la vez...
# Mostrar como va la pelea (turno de quien, etc.) y los estados de cada pokemon.
# Si un pokemon muere termina el juego.
# Decir quién ganó.
import random
import time

poke1 = input("Digite el nombre del pokemon1: ")
poke2 = input("Digite el nombre del pokemon2: ")

while True:
    ataque_p1 = int(input(f"Digite el poder de ataque de '{poke1}' entre 1-35: "))
    if ataque_p1 >= 1 and ataque_p1 <= 35:
        break
    else:
        print("*** error, intente de nuevo...")
print("OK, pokemón configurado correctamente!!")
time.sleep(2)

while True:
    ataque_p2 = int(input(f"Digite el poder de ataque de '{poke2}' entre 1-35: "))
    if ataque_p2 >= 1 and ataque_p2 <= 35:
        break
    else:
        print("*** error, intente de nuevo...")
print("OK, pokemón configurado correctamente!!")
time.sleep(2)

vida_p1 = 200
vida_p2 = 200
turno = random.randint(1, 2)
print("========= Estado inicial ===========")
print(f"'{poke1}' tiene {vida_p1} de vida")
print(f"'{poke2}' tiene {vida_p2} de vida")
time.sleep(2)
print("========= Empieza la batalla ===========")
time.sleep(2)
while vida_p1 > 0 and vida_p2 > 0:
    print("-"*30)
    if turno == 1:
        print(f"El turno es para '{poke1}':")
        vida_p2 -= ataque_p1
        print(f"'{poke1}' ataca a '{poke2}' con sus {ataque_p1} de fuerza!!")
        turno = 2
    elif turno == 2:
        print(f"El turno es para '{poke2}':")
        vida_p1 = vida_p1 - ataque_p2
        print(f"'{poke2}' ataca a '{poke1}' con sus {ataque_p2} de fuerza!!")
        turno = 1

    print(f"'{poke1}' tiene {vida_p1} de vida")
    print(f"'{poke2}' tiene {vida_p2} de vida")
    time.sleep(3)

print("Fin de la batalla...!")

if vida_p1 <= 0:
    print(f"El pokemón '{poke1}' murió. Su vida quedó en: {vida_p1}")
    print(f"Victoria para '{poke2}' !!!")
else:
    print(f"El pokemón '{poke2}' murió. Su vida quedó en: {vida_p2}")
    print(f"Victoria para '{poke1}' !!!")
