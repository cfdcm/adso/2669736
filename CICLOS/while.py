# Ciclo while

a = 1       # contador
suma = 0    # acumulador
while a <= 5:                   # 1   2   3   4   5 = 15
    # print(a)
    suma = suma + a     # acumulador
    a = a + 1           # contador

print(f"La suma es: {suma}")
