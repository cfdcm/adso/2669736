import random

cont = 1
conteo_especial = 0
while True:
    dado = random.randint(1, 6)
    print(f"Lanzamiento {dado}")
    saco_uno = False
    if cont <= 10:
        if dado != 1:
            conteo_especial += 1
        else:
            saco_uno = True
            print("Perdió!!")
            break

    if conteo_especial == 10 and not saco_uno:
        print("Ganó!!")
        break

    cont += 1

print(f"La cantidad de lanzamientos fue {cont}")
