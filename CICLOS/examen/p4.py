frutas_espanol = ["manzana", "uva", "coco"]
frutas_ingles = ["apple", "grape", "coconut"]


while True:
    sel = input("\nDigite una fruta en español para saber su traducción al inglés: ")
    encontrado = False
    indice = 0
    for i in frutas_espanol:
        if i == sel:
            encontrado = True
            print(f"Su traducción es: {frutas_ingles[indice]}")
        indice += 1

    if encontrado == False:
        print("La fruta NO se encuentra en nuestra base de datos.")
        op = input("Quiere agregar la fruta al diccionario? (S/N)").upper()
        if op == "S":
            traduccion = input(f"Ingrese la traducción de {sel} al inglés: ")
            frutas_espanol.append(sel)
            frutas_ingles.append(traduccion)
            print("Fruta agrega correctamente!!")
        else:
            print("\n\n***Muchas gracias por usar nuestro programa...")
            break
