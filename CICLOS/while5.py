# Ciclo infinito while.

cont = 1
while True:
    usuario = input("Digite su nombre de usuario: ")
    clave = input("Digite su contraseña: ")
    if (usuario == "admin" and clave == "12345") and cont <= 3:
        print("Bienvenido!!")
        break
    else:
        print(cont)
        if cont >= 3:
            print("Superó el límite de intentos...")
            break
        else:
            print("Usuario o contraseña incorrectos")

    cont += 1
