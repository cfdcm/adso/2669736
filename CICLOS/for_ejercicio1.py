# Pedir los nombres y las notas de 3 aprendices. (Llenar los vectores)
# Averiguar cuál aprendiz tiene mejor nota y mostrar nombre y nota.

nombres = []
notas = []
for i in range(3):
    nombre = input(f"Digite el nombre del aprendiz {i+1}: ")
    nombres.append(nombre)
    nota = float(input(f"Digite la nota del aprendiz {nombres[i]}: "))
    notas.append(nota)

print(nombres)
print(notas)


# averiguar la mejor nota
cont = 0
mejor = 0
indice_mejor = -1
for i in notas:
    if cont == 0:
        mejor = i
        indice_mejor = cont
    else:
        if mejor < i:
            mejor = i
            indice_mejor = cont

    cont += 1
print(f"La mejor nota es: {mejor} y corresponde a {nombres[indice_mejor]}")