# Averiguar si un número entero positivo (CONTROLAR), es primo o no.
# Primo: Dividir solo por 1 y por el mismo número.
while True:
    n = int(input("Digite un entero positivo: "))
    if n > 0:
        break
    else:
        print("Error, digite un número mayor que cero...")

cont = 1
divisores = 0
while cont <= n:
    print(f"Obtener divisores del {n}, así {n} % {cont} = {n % cont}")
    if n % cont == 0:
        divisores += 1

    cont += 1

if divisores <= 2:
    print("Es primo")
else:
    print(f"No es primo, ya que tiene {divisores} divisores")
