# Parámetros arbitrarios diccionarios

def jugar(nombre_j1, nombre_j2, **kwargs):
	if "consola" in kwargs:
		if kwargs["consola"] == "ps5":
			print(f"{nombre_j1} y {nombre_j2} juegan {kwargs['consola']}")
		elif kwargs["consola"] == "Xbox":
			if "vidas" in kwargs:
				print(f"{nombre_j1} y {nombre_j2} tienen {kwargs['vidas']} vidas")
			else:
				print("sin vidas...")
		elif kwargs["consola"] == "Nintendo":
			print(f"{nombre_j1} y {nombre_j2} están jugando Nintendo...")
	else:
		print("No están jugando correctamente.")


if __name__ == "__main__":
	jugar("Juan", "Kevin")
	jugar("Andrés", "Pamela", consola="Xbox")
	jugar("Sofia", "Mateo", consola="Nintendo", vidas=3)
