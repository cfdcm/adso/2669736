# Mezcla de parámetros

def jugar(nombre, apellido, *args, **kwargs):
	print(f"Hola {nombre}")
	print(args)
	print(kwargs)


if __name__ == "__main__":
	jugar("Pedro", "Martínez", 1, "hola", 3, consola="NS", vidas=6)
