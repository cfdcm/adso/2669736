# Ejercicio 1 de funciones avanzadas.

"""
Construir un juego que permita crear personajes, usando funciones.
El nombre_del_personaje debe ser obligatorio.
Además, tendrá la posibilidad de tener poderes, las opciones son:
	- Volar
	- Disparar
	- Super fuerza
	- Super salto
Cada uno de estos poderes serán funciones que deben crear.

Cuando se crea un personaje se debe imprimir su nombre_del_personaje y ejecutar
las funciones según los poderes seleccionados.

Crear un menú para que el usuario final, decida los poderes que quiere tener
su personaje, 0, 1 o todos.
"""