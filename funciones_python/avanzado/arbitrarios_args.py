# Parámetros arbitrarios *args

def operacion(num1, num2, *args):
	tipo = "+"
	if isinstance(args[0], str):
		tipo = args[0]

	if tipo == "+":
		r = num1 + num2
		if isinstance(args[0], str):
			for i in range(1, len(args)):
				r += int(args[i])
		else:
			for i in args:
				r += i
		print(f"Suma: {r}")
	elif tipo == "-":
		r = num1 - num2
		if isinstance(args[0], str):
			for i in range(1, len(args)):
				r -= int(args[i])
		else:
			for i in args:
				r -= i
		print(f"Resta: {r}")
	elif tipo == "*":
		r = num1 * num2
		if isinstance(args[0], str):
			for i in range(1, len(args)):
				r *= int(args[i])
		else:
			for i in args:
				r *= i
		print(f"Mult: {r}")
	elif tipo == "/":
		if num2 != 0:
			print(f"Div: {num1 / num2}")
		else:
			print("Error división por cero.")
	else:
		print("Operación mat. no permitida.")


if __name__ == "__main__":
	operacion(1, 1, "-", 5, 3)
	operacion(1, 2, 5, 3)
	operacion(1, 1, "*", 6)
	operacion(4, 2, "/")
	operacion(9, 9, "+", 2)
	operacion(9, 2, "%")












