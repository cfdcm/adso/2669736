# Scope (Alcance) variables. Funciones

def prueba():
	centros.append("cuero")
	print(f"func: {centros}")
	est["apellido"] = "Martínez"
	print(f"func: {est}")

if __name__ == "__main__":
	centros = ["moda", "mobiliario"]
	est = {
		"nombre": "Pedro",
		"edad": 30
	}
	print(f"main: {centros}")
	print(f"main: {est}")
	prueba()
	print(f"después: {centros}")
	print(f"después: {est}")

