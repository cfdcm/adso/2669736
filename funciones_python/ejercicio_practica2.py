"""
****Ejercicio 2
Construir un programa usando funciones que permita revisar si una lista de números (pedir al usuario los N y los números) son todos primos o no.
Además guardar en un diccionario con clave "primos" la lista de los primos encontrados y otra clave llamada "no_primos" una lista con los que no sean primos.
Imprimir todo.
"""

def primo_o_no(num):
	es_primo = True
	if num % 2 == 0 and num != 2:
		es_primo = False
	else:
		cont = 1
		for i in range(1, num, 2):
			if num % i == 0:
				cont += 1

			if cont == 3:
				es_primo = False
				break

	return es_primo


if __name__ == "__main__":
	listado = []
	primos = []
	no_primos = []
	estado = {}
	N = int(input("Cuántos números quiere revisar?: "))
	for i in range(N):
		num = int(input(f"{i+1}/{N}. Digite un num entero: "))
		listado.append(num)

	for x in listado:
		if primo_o_no(x):
			primos.append(x)
		else:
			no_primos.append(x)

	estado["primos"] = primos
	estado["no_primos"] = no_primos

	if len(no_primos) == 0:
		print("Todos son primos!!")
	else:
		print(f"Resultado: {estado}")
