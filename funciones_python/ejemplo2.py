def promedio(total, cantidad):
    return total/cantidad


if __name__ == "__main__":
    n = int(input("Digite la cantidad de personas: "))

    suma = 0
    for i in range(1, n+1):
        edad = int(input(f"Digite la edad {i}: "))
        suma += edad

    r = promedio(suma, n)
    print(f"El promedio de edades es: {r}")


