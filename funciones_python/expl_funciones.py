def comer():
    print("estoy comiendo....")


def saludar(nombre, apellido):
    print(f"Hola {nombre} {apellido}!")


def suma(n1, n2, n3):
    resultado = n1 + n2 + n3
    return resultado


# programa principal
if __name__ == "__main__":
    comer()
    saludar("Mateo", "Herreño")
    suma(5, 20, 30)
    a = int(input("Digite el valor 1: "))
    b = int(input("Digite el valor 2: "))
    c = int(input("Digite el valor 3: "))

    suma(a, b, c)



