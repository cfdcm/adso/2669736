# Construya una calculadora que sume dos números.

# Casting

# str		convierte un dato a string	"5"	str("5")	"5"
#						5	str(5)		"5"

# int		convierte a entero		"hola"	int("hola")	0
#						"5"	int("5")	5
#						5.8	int(5.8)	5

# float		convierte a decimal		"4.3"	float("4.3")	4.3
#						"hola"	float("hola")	0.0
#						4	float(4)	4.0

num1 = int( input("Digite el num 1: ") )

num2 = int( input("Digite el num 2: ") )

suma = num1 + num2


print("La suma es", suma)

print("La suma de ", num1, " + ", num2, " es ", suma)




